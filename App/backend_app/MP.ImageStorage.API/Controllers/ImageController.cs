﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MP.ImageStorage.API.Model;
using MP.ImageStorage.API.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MP.ImageStorage.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        private readonly string _baseUploadDirectory = Directory.GetCurrentDirectory() + @"/uploads/";
        private readonly string _imageFileName = "Image.img";

        IImageStorageRepository _imageStorageRepository;

        public ImageController(IImageStorageRepository imageStorageRepository)
        {
            _imageStorageRepository = imageStorageRepository;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Image>> GetAllImages()
        {
            try
            {
                return StatusCode(200, _imageStorageRepository.GetAll());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]
        public ActionResult Post(IFormFile images)
        {
            
            int pictureId;
            try
            {
                var byteArray = GetByteArray(images);

                if (!Directory.Exists(_baseUploadDirectory))
                {
                    Directory.CreateDirectory(_baseUploadDirectory);
                }

                pictureId = _imageStorageRepository.SaveImage(_baseUploadDirectory);

                if (pictureId != -1)
                {
                    var filePath = GetImagePath(pictureId);
                    using (var fs = new FileStream(filePath, FileMode.Create, FileAccess.Write))
                    {
                        fs.Write(byteArray, 0, byteArray.Length);
                       
                    }
                    return Ok(new { Message = "Picture successfully uploaded.", id = pictureId });

                }
                return BadRequest(new { Message = "Picture did not upload succsesfully." });
               
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("{id}")]
        public ActionResult getImageById(int id)
        {
            var image = _imageStorageRepository.GetAll().FirstOrDefault(x => x.Id == id);
            var imageFile = GetImagePath(image.Id);

            if (!System.IO.File.Exists(imageFile))
            {
                return BadRequest(new { Message = "Image not found." });
            }
            Byte[] b = System.IO.File.ReadAllBytes(imageFile);
            return File(b, "image/jpeg");
        }

        private byte[] GetByteArray(IFormFile file)
        {
            /* Random rnd = new Random();
             byte[] b = new byte[sizeInKb * 1024]; 
             rnd.NextBytes(b);
             return b; */

            using (var ms = new MemoryStream())
            {
                file.CopyTo(ms);
                var fileBytes = ms.ToArray();
                return fileBytes;
            }
        }

        private string GetImagePath(int imageId)
		{
            return _baseUploadDirectory + imageId + "_" + _imageFileName;
        }
    }
}
