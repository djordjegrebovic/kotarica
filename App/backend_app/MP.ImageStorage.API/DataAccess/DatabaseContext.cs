﻿using Microsoft.EntityFrameworkCore;
using MP.ImageStorage.API.Model;

namespace MP.ImageStorage.API.DataAccess
{
	public class DatabaseContext : DbContext
	{
        public DatabaseContext()
		{
            Database.EnsureCreated();
		}

        public DbSet<Image> Images { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Image>()
                .HasIndex(entity => entity.Id)
                .IsUnique(true);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=Codex.db");
        }
    }
}
