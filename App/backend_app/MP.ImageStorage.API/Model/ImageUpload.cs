﻿using Microsoft.AspNetCore.Http;

namespace MP.ImageStorage.API.Model
{
    public class ImageUpload
    {
        public IFormFile images { get; set; } // used as key for post method: "images"
    }
}
