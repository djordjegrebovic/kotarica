﻿using System.Collections.Generic;
using MP.ImageStorage.API.Model;

namespace MP.ImageStorage.API.Repository
{
	public interface IImageStorageRepository
	{
		List<Image> GetAll();

		int SaveImage(string location);
	}
}
