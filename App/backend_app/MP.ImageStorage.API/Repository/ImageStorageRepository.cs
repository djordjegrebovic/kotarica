﻿using MP.ImageStorage.API.DataAccess;
using MP.ImageStorage.API.Model;
using System.Collections.Generic;
using System.Linq;

namespace MP.ImageStorage.API.Repository
{
	public class ImageStorageRepository : IImageStorageRepository
	{
		public List<Image> GetAll()
		{
			using (var context = GetContext())
				return context.Set<Image>().ToList();
		}

		public int SaveImage(string location)
		{
			using (var context = GetContext())
			{
				var image = new Image { location = location };
				context.Images.Add(image);
				context.SaveChanges();
				return image.Id;
			}
		}

		private DatabaseContext GetContext()
		{
			return new DatabaseContext();
		}
	}
}
