// SPDX-License-Identifier: MIT
pragma experimental ABIEncoderV2;
pragma solidity >=0.4.22 <0.9.0;
 
contract poruke_ugovor {

    uint256 public brojPoruka;
    
     struct Poruka {
        uint256 idProizvoda;
        uint256 idKupca;
        uint256 idOglasavaca;
        string msg;
        string datum;
    }
    
    struct VratiZadnjePoruke
    {
        string msg;
        string datum;
    }
 
    mapping(uint256 => Poruka) public poruke;
    
    event kreiranaPoruka(
        uint256 idProizvoda,
        uint256 idKupca,
        uint256 idOglasavaca,
        string msg,
        string datum,
        uint256 msgNumber
    );
    
    constructor() public {
        
        poruke[0] = Poruka(1,1,2,"Ovo je neka poruka.","02-06-2021");
        poruke[1] = Poruka(1,2,1,"Ovo je neka poruka.","02-06-2021");
        poruke[2] = Poruka(1,1,2,"Ovo je neka poruka.","02-06-2021");
        

        brojPoruka = 3;
    }

     function novaPoruka(
        uint256 _idProizvoda,uint256 _idKupca,uint256 _idOglasavaca,string memory _msg,string memory _datum
        ) public{
           

            poruke[brojPoruka++] = Poruka(_idProizvoda,_idKupca,_idOglasavaca,_msg,_datum);
            emit kreiranaPoruka(_idProizvoda,_idKupca,_idOglasavaca,_msg,_datum,brojPoruka-1);
            return;
            
        }
        
    function vratiMojePoslatePoruke(uint256 _userID) public view returns(Poruka[] memory)
    {
       uint256 br = 0;
       for(uint256 i = 0 ;i < brojPoruka;i++)
       {
           if(poruke[i].idKupca == _userID)
           {
                br++;
           }
       }
       
         Poruka[] memory mojiZahtevi = new Poruka[](br);
         br = 0;
         for(uint256 i = 0 ;i < brojPoruka;i++)
           {
               if(poruke[i].idKupca == _userID)
               {
                   mojiZahtevi[br++] = poruke[i];
               }
           }
       
       return mojiZahtevi;
    }
    
    function vratiMojePrimljeneRecenzije(uint256 _userID) public view returns(Poruka[] memory)
    {
       uint256 br = 0;
       for(uint256 i = 0 ;i < brojPoruka;i++)
       {
           if(poruke[i].idOglasavaca == _userID)
           {
                br++;
           }
       }
       
         Poruka[] memory mojiZahtevi = new Poruka[](br);
         br = 0;
         for(uint256 i = 0 ;i < brojPoruka;i++)
           {
               if(poruke[i].idOglasavaca == _userID)
               {
                   mojiZahtevi[br++] = poruke[i];
               }
           }
       
       return mojiZahtevi;
    }
    
    function vratiPorukeZaProizvod(uint256 _productID) public view returns(Poruka[] memory)
    {
       uint256 br = 0;
       for(uint256 i = 0 ;i < brojPoruka;i++)
       {
           if(poruke[i].idProizvoda == _productID)
           {
                br++;
           }
       }
       
         Poruka[] memory mojiZahtevi = new Poruka[](br);
         br = 0;
         for(uint256 i = 0 ;i < brojPoruka;i++)
           {
               if(poruke[i].idProizvoda == _productID)
               {
                   mojiZahtevi[br++] = poruke[i];
               }
           }
       
       return mojiZahtevi;
    }
    
    function vratiLjudeSaKojimaSeDopisujem(uint256 _mojNalog) public view returns(uint256[] memory)
    {
       uint256 br = 0;
       for(uint256 i = 0 ;i < brojPoruka;i++)
       {
           if(poruke[i].idOglasavaca == _mojNalog || poruke[i].idKupca == _mojNalog)
           {
                br++;
           }
       }
       
         uint256[] memory likovi = new uint256[](br);
         br = 0;
         uint256 ima = 0;
         for(uint256 i = 0 ;i < brojPoruka;i++)
           {
               if(poruke[i].idOglasavaca == _mojNalog )
               {
                   for(uint256 j = 0;j < br;j++)
                   {
                       if(likovi[j] == poruke[i].idKupca)
                       ima = 1;
                   }
                   if(ima != 1)
                   likovi[br++] = poruke[i].idKupca;
                   ima = 0;
               }
               else if(poruke[i].idKupca == _mojNalog)
               {
                    for(uint256 j = 0;j < br;j++)
                   {
                       if(likovi[j] == poruke[i].idOglasavaca)
                       ima = 1;
                   }
                   if(ima != 1)
                   likovi[br++] = poruke[i].idOglasavaca;
                   ima = 0;
               }
           }
           
        
       
       return likovi;
    }
    
    function vratiZadnjePoruke(uint256 mojId,uint256[] memory likovi,uint256 duzinaLikova) public view returns(VratiZadnjePoruke[] memory)
    {
        VratiZadnjePoruke[] memory ovoVracam = new VratiZadnjePoruke[](duzinaLikova);
        string memory msqs;
        string memory datum;
        uint256 br = 0;
        for(uint256 i = 0;i < duzinaLikova;i++)
        {
            for(uint256 j = 0;j < brojPoruka;j++)
            {
                if((likovi[i] == poruke[j].idKupca && mojId == poruke[j].idOglasavaca) || (likovi[i] == poruke[j].idOglasavaca && mojId == poruke[j].idKupca))
                {
                    msqs = poruke[j].msg;
                    datum = poruke[j].datum;
                }
            }
            ovoVracam[br++] = VratiZadnjePoruke(msqs,datum);
            msqs = "";
            datum = "";
        }
        
        return ovoVracam;
        
    }
    
    function celaKonverzacija(uint256 _kupac,uint256 _prodavac) public view returns(Poruka[] memory){
         uint256 br = 0;
       for(uint256 i = 0 ;i < brojPoruka;i++)
       {
           if((poruke[i].idKupca == _kupac && poruke[i].idOglasavaca == _prodavac) || (poruke[i].idKupca == _prodavac && poruke[i].idOglasavaca == _kupac))
           {
                br++;
           }
       }
       
         Poruka[] memory mojiZahtevi = new Poruka[](br);
         br = 0;
         for(uint256 i = 0 ;i < brojPoruka;i++)
           {
               if((poruke[i].idKupca == _kupac && poruke[i].idOglasavaca == _prodavac) || (poruke[i].idKupca == _prodavac && poruke[i].idOglasavaca == _kupac))
               {
                   mojiZahtevi[br++] = poruke[i];
               }
           }
       
       return mojiZahtevi;
    }
    
   
}