// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract users_login {
    uint256 public usersCount;
    uint256 public usersDataCount;

    struct UserData {
        string profilePictureHash;
        string name;
        string surname;
        string phoneNumber;
        string home_address;
        string email;
        uint256 userID;
    }

    struct User {
        // string name;
        //string surname;
        string username;
        string password;
        string privateKey;
        uint256 userID;
        bool isLogged;
        
    }
    

    mapping(uint256 => User) public users;
    mapping(uint256 => UserData) public users_data;

    event UserCreated(
        /*string name,string surname,*/
        string username,
        string password,
        string privateKey,
        uint256 userID,
        uint256 userNumber
    );

    event UserDataCreated(
        string profilePictureHash,
        string name,
        string surname,
        string phoneNumber,
        string home_address,
        string email,
        uint256 userID,
        uint256 userDataNumber
    );

    constructor() public {
        users[0] = User(
            /*"Dusan","Djulakovic",*/
            "djulakovic",
            "12345678",
            "6749cd466f2098265162693f8408658b4a5b1d8cd766f2da22020ec32ff01cb2",
            1,
            false
        );
        users_data[0] = UserData("","Dusan","Djulakovic","0637520178","Velika Drenova","dusan_1989@hotmail.rs",1);
        usersCount = 1;
        usersDataCount = 1;
    }
           
    function changePrivateKey(uint256 _userID,string memory _privKey) public
    {
          for (uint256 i = 0; i < usersCount; i++) {
            if(users[i].userID == _userID)
            {
                users[i].privateKey = _privKey;
                return;
            }
          }
    }
    
    function changePassword(uint256 _userID,string memory _password) public
    {
          for (uint256 i = 0; i < usersCount; i++) {
            if(users[i].userID == _userID)
            {
                users[i].password = _password;
                return;
            }
          }
    }
            
    function returnHashCode(uint256 _userID) public view returns(string memory)
    {
        for (uint256 i = 0; i < usersDataCount; i++) {
            if(users_data[i].userID == _userID)
            {
                return users_data[i].profilePictureHash;
            }
            
        }
    }
    function createUser(
        /*string memory _name,string memory _surname,*/
        string memory _username,
        string memory _password,
        string memory _key
    ) public returns (bool) {
        for (uint256 i = 0; i < usersCount; i++) {
            if (
                keccak256(bytes(users[i].username)) ==
                keccak256(bytes(_username))
            ) {
                return false;
            }
        }
        users[usersCount++] = User(
            /*_name,_surname,*/
            _username,
            _password,
            _key,
            usersCount + 1,
            false
        );

        emit UserCreated(
            /*_name,_surname,*/
            _username,
            _password,
            _key,
            usersCount - 1,
            usersCount - 1
        );
        return true;
    }
    
    function createUserData(
       
        string memory _profile_picture_hash,
        string memory _name,
        string memory _surname,
        string memory _phoneNumber,
        string memory _home_address,
        string memory _email,
        uint256 _userID
    ) public {
       
        for (uint256 i = 0; i < usersDataCount; i++) {
            if (users_data[i].userID == _userID) {
                 if(keccak256(bytes(_profile_picture_hash)) != keccak256(bytes(""))) users_data[i].profilePictureHash = _profile_picture_hash;
                 if(keccak256(bytes(_name)) != keccak256(bytes(""))) users_data[i].name = _name;
                 if(keccak256(bytes(_surname)) != keccak256(bytes(""))) users_data[i].surname = _surname;
                 if(keccak256(bytes(_phoneNumber)) != keccak256(bytes(""))) users_data[i].phoneNumber = _phoneNumber;
                 if(keccak256(bytes(_home_address)) != keccak256(bytes(""))) users_data[i].home_address = _home_address;
                 if(keccak256(bytes(_email)) != keccak256(bytes(""))) users_data[i].email = _email;
                 
                 return;
                
            }
        }
    
        for (uint256 i = 0; i < usersCount; i++) {                     
             if (users[i].userID == _userID) {
                users_data[usersDataCount++] = UserData(
                    _profile_picture_hash,
                    _name,
                    _surname,
                    _phoneNumber,
                    _home_address,
                    _email,
                    _userID   );
                emit UserDataCreated(
                    _profile_picture_hash,
                    _name,
                    _surname,
                    _phoneNumber,
                    _home_address,
                    _email,
                    _userID,
                    usersDataCount - 1
                );
                return;
            }
        }
    }

    function findUserAndLogin(string memory _username, string memory _password)
        public
        returns (bool)
    {
        for (uint256 i = 0; i < usersCount; i++) {
            if (
                keccak256(bytes(users[i].username)) ==
                keccak256(bytes(_username)) &&
                keccak256(bytes(users[i].password)) ==
                keccak256(bytes(_password))
            ) {
                users[i].isLogged = true;
                return true;
            }
        }
        return false;
    }

    function findUserAndLogout(string memory _username)
        public
        returns (bool)
    {
        for (uint256 i = 0; i < usersCount; i++) {
            if (
                keccak256(bytes(users[i].username)) ==
                keccak256(bytes(_username))
            ) {
                if (users[i].isLogged == true) {
                    users[i].isLogged = false;
                    return true;
                } else return false;
            }
        }
        return false;
    }
    /*
    function findUserAndLogoutWithID(uint256 id) public {
        users[id].isLogged = false;
    }
    */
}
