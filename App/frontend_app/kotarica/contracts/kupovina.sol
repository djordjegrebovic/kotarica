// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;
pragma experimental ABIEncoderV2;
contract kupovina {

    

    uint256 public brojZahtevaZaKupovinu;
    
     struct ZahtevZaKupovinu {
        uint256 id;
        uint256 idProizvoda;
        uint256 idKupca;
        uint256 cena;
        string adresa;
        bool ocenjen;
        string broj_telefona;
        uint256 kolicina;
        uint256 idOglasavaca;
        uint256 status;
        string poruka;
        string datum;
    }
 
    mapping(uint256 => ZahtevZaKupovinu) public zahtev;
    
    event kreiranZahtevZaKupovinu(
        uint256 id,
        uint256 idProizvoda,
        uint256 idKupca,
        uint256 cena,
        string adresa,
        bool ocenjen,
        string broj_telefona,
        uint256 kolicina,
        uint256 idOglasavaca,
        uint256 status,
        string poruka,
        string datum,
        uint256 productNumber
    );
    
    constructor() public {
        zahtev[0] = ZahtevZaKupovinu(1,1,1,90,"Atinska 150",false,"0637520178",1,1,0,"","2021-5-5");
        brojZahtevaZaKupovinu = 1;
    }

     function noviZahtevZaKupovinu(
        uint256 _idProizvoda,uint256 _idKupca,uint256 _kolicina,uint256 _idOglasavaca,  string memory _datum,uint256 _cena,
        string memory _adresa,string memory _broj_telefona)
         public{
             
            
             for(uint256 i = 0 ;i < brojZahtevaZaKupovinu;i++)
            {
                if(zahtev[i].id == 0 )
                {
                    zahtev[i].id = i+1;
                    zahtev[i].idProizvoda = _idProizvoda;
                    zahtev[i].idKupca = _idKupca;
                    zahtev[i].kolicina = _kolicina;
                    zahtev[i].idOglasavaca = _idOglasavaca;  
                    zahtev[i].status = 0;
                    zahtev[i].datum = _datum;

                    zahtev[i].cena = _cena;
                    zahtev[i].adresa = _adresa;
                    zahtev[i].ocenjen = false;
                    zahtev[i].broj_telefona = _broj_telefona;
                    
                    
                    return;
                }
            }
            
            zahtev[brojZahtevaZaKupovinu++] = ZahtevZaKupovinu(brojZahtevaZaKupovinu+1,_idProizvoda,_idKupca,_cena,_adresa,false,_broj_telefona,_kolicina,_idOglasavaca,0,"",_datum);
            emit kreiranZahtevZaKupovinu(brojZahtevaZaKupovinu,_idProizvoda,_idKupca,_cena,_adresa,false,_broj_telefona,_kolicina,_idOglasavaca,0,"",_datum, brojZahtevaZaKupovinu-1);
            return;
            
        }
        
    function obrisiZahtev(uint256 _id)
        public{
            for(uint256 i = 0 ;i < brojZahtevaZaKupovinu;i++)
            {
                if(zahtev[i].id == _id)
                {
                    zahtev[i].id = 0;
                    zahtev[i].idProizvoda = 0;
                    zahtev[i].idKupca = 0;
                    zahtev[i].cena = 0;
                    zahtev[i].adresa = "";
                    zahtev[i].ocenjen = false;
                    zahtev[i].broj_telefona = "";
                    zahtev[i].idOglasavaca = 0;
                    zahtev[i].status = 4;
                    zahtev[i].kolicina = 0;
                    zahtev[i].poruka = "";
                    zahtev[i].datum = "";
                    
                }
            }
        }
        
    function vratiMojePoslateZahteveZaKupovinu(uint256 _userID) public view returns(ZahtevZaKupovinu[] memory)
    {
        uint256 br;
        for(uint256 i =0 ; i < brojZahtevaZaKupovinu; i++)
        {
            if(zahtev[i].idKupca == _userID && zahtev[i].idProizvoda != 0)
            {
                br++;
            }
        }
       ZahtevZaKupovinu[] memory mojiZahtevi  = new ZahtevZaKupovinu[](br) ;
       br = 0;
       for(uint256 i = 0 ;i < brojZahtevaZaKupovinu;i++)
       {
           if(zahtev[i].idKupca == _userID && zahtev[i].idProizvoda != 0)
           {
               mojiZahtevi[br++]= zahtev[i];
           }
       }
       
       return mojiZahtevi;
    }
    
    function vratiMojePristigleZahteveZaKupovinu(uint256 _userID) public view returns(ZahtevZaKupovinu[] memory)
    {
        
        uint256 br;
        for(uint256 i =0 ; i < brojZahtevaZaKupovinu; i++)
        {
            if(zahtev[i].idOglasavaca == _userID && zahtev[i].id != 0)
            {
                br++;
            }
        }
       ZahtevZaKupovinu[] memory mojiZahtevi  = new ZahtevZaKupovinu[](br) ;
       br = 0;
       for(uint256 i = 0 ;i < brojZahtevaZaKupovinu;i++)
       {
           if(zahtev[i].idOglasavaca == _userID && zahtev[i].idProizvoda != 0)
           {
               mojiZahtevi[br++]= zahtev[i];
           }
       }
       
       
       return mojiZahtevi;
    }
    
    function dajOcenu(uint256 _id) public
    {
        for( uint256 i = 0; i < brojZahtevaZaKupovinu; i++){
            if(zahtev[i].id == _id){
                zahtev[i].ocenjen = true;
            }
        }
    }
    
    function promeniStatusZahtevaZaKupovinu(uint256 _id,uint256 _status, string memory _poruka, string memory _datum) public{
        
        for( uint256 i = 0; i < brojZahtevaZaKupovinu; i++){
            if(zahtev[i].id == _id && zahtev[i].status == 0){
                zahtev[i].status = _status;
                zahtev[i].poruka = _poruka;
                string memory s1 = zahtev[i].datum;
                zahtev[i].datum =string(abi.encodePacked(s1,"!",_datum));
            }
        }
    }
}