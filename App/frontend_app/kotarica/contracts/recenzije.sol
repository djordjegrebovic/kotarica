// SPDX-License-Identifier: MIT
pragma experimental ABIEncoderV2;
pragma solidity >=0.4.22 <0.9.0;
 
contract recenzije {

    uint256 public brojRecenzija;
    
     struct Recenzija {
        uint256 idProizvoda;
        uint256 idKupca;
        uint256 idOglasavaca;
        uint256 ocena;
        string komentar;
        string datum;
    }
 
    mapping(uint256 => Recenzija) public recenzija;
    
    event kreiranZahtevZaRecenzijom(
        uint256 idProizvoda,
        uint256 idKupca,
        uint256 idOglasavaca,
        uint256 ocena,
        string komentar,
        string datum,
        uint256 productNumber
    );
    
    constructor() public {
        
        recenzija[0] = Recenzija(1,1,1,5,"Odlicna saradnja.","2021-06-02");
        recenzija[1] = Recenzija(1,2,1,3,"Nista ne valja.","2021-06-02");
       
        
        brojRecenzija = 2;
    }

     function novaRecenzija(
        uint256 _idProizvoda,uint256 _idKupca,uint256 _idOglasavaca,uint256 _ocena,string memory _komentar,string memory _datum
        ) public{
           
            for(uint256 i = 0 ;i < brojRecenzija;i++)
            {
                if(recenzija[i].idProizvoda == _idProizvoda && recenzija[i].idKupca == _idKupca && recenzija[i].idOglasavaca == _idOglasavaca)
                return;
            }
            
             for(uint256 i = 0 ;i < brojRecenzija;i++)
            {
                if(recenzija[i].idProizvoda == 0 && recenzija[i].idKupca == 0 && recenzija[i].idOglasavaca == 0 && recenzija[i].ocena == 0 )
                {
                    recenzija[i].idProizvoda = _idProizvoda;
                    recenzija[i].idKupca = _idKupca;
                    recenzija[i].ocena = _ocena;
                    recenzija[i].idOglasavaca = _idOglasavaca;
                    recenzija[i].komentar = _komentar;
                    
                    return;
                }
            }

            recenzija[brojRecenzija++] = Recenzija(_idProizvoda,_idKupca,_idOglasavaca,_ocena,_komentar,_datum);
            emit kreiranZahtevZaRecenzijom(_idProizvoda,_idKupca,_idOglasavaca,_ocena,_komentar,_datum,brojRecenzija-1);
            return;
            
        }
        
    function obrisiZahtev(
        uint256 _idProizvoda,uint256 _idKupca,uint256 _idOglasavaca
        )
        public{
            for(uint256 i = 0 ;i < brojRecenzija;i++)
            {
                if(recenzija[i].idProizvoda == _idProizvoda && recenzija[i].idKupca == _idKupca && recenzija[i].idOglasavaca == _idOglasavaca)
                {
                    recenzija[i].idProizvoda = 0;
                    recenzija[i].idKupca = 0;
                    recenzija[i].idOglasavaca = 0;
                    recenzija[i].ocena = 0;
                }
            }
        }
        
    function vratiMojePoslateRecenzije(uint256 _userID) public view returns(Recenzija[] memory)
    {
       uint256 br = 0;
       for(uint256 i = 0 ;i < brojRecenzija;i++)
       {
           if(recenzija[i].idKupca == _userID)
           {
                br++;
           }
       }
       
         Recenzija[] memory mojiZahtevi = new Recenzija[](br);
         br = 0;
         for(uint256 i = 0 ;i < brojRecenzija;i++)
           {
               if(recenzija[i].idKupca == _userID)
               {
                   mojiZahtevi[br++] = recenzija[i];
               }
           }
       
       return mojiZahtevi;
    }
    
    function vratiMojePrimljeneRecenzije(uint256 _userID) public view returns(Recenzija[] memory)
    {
       uint256 br = 0;
       for(uint256 i = 0 ;i < brojRecenzija;i++)
       {
           if(recenzija[i].idOglasavaca == _userID)
           {
                br++;
           }
       }
       
         Recenzija[] memory mojiZahtevi = new Recenzija[](br);
         br = 0;
         for(uint256 i = 0 ;i < brojRecenzija;i++)
           {
               if(recenzija[i].idOglasavaca == _userID)
               {
                   mojiZahtevi[br++] = recenzija[i];
               }
           }
       
       return mojiZahtevi;
    }
    
    function vratiRecenzijeZaProizvod(uint256 _productID) public view returns(Recenzija[] memory)
    {
       uint256 br = 0;
       for(uint256 i = 0 ;i < brojRecenzija;i++)
       {
           if(recenzija[i].idProizvoda == _productID)
           {
                br++;
           }
       }
       
         Recenzija[] memory mojiZahtevi = new Recenzija[](br);
         br = 0;
         for(uint256 i = 0 ;i < brojRecenzija;i++)
           {
               if(recenzija[i].idProizvoda == _productID)
               {
                   mojiZahtevi[br++] = recenzija[i];
               }
           }
       
       return mojiZahtevi;
    }
    
   
}