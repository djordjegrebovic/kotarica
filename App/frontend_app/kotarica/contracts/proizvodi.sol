// SPDX-License-Identifier: MIT
pragma experimental ABIEncoderV2;
pragma solidity >=0.4.22 <0.9.0;
 
contract proizvodi {
    
    uint256 public brojProizvoda;
    uint256 public brojOmiljeninih;
    uint256 public brojUKorpi;
    uint256 public zadnjiProizvod;
     struct Proizvod {
        uint256 id;
        uint256 idOglasavaca;
        uint256 cena;
        string nazivProizvoda;
        string kategorijaProizvoda;
        string vrstaProizvoda;
        string mernaJedincia;
        string opis;
        string hesh;
        uint256 zbirOcena;
        uint256 brojOcena;
    }
    
    struct FavouritesProducts{
        uint256 productID;
        uint256 userID;
    }
    
    struct Cart{
        uint256 productID;
        uint256 userID; 
    }
 
    mapping(uint256 => Proizvod) public proizvod;
    mapping(uint256 => FavouritesProducts) public omiljeni;
    mapping(uint256 => Cart) public korpa;
    
    
    event ProizvodKreiran(
        uint256 id,
        uint256 idOglasavaca,
        uint256 cena,
        string nazivProizvoda,
        string kategorijaProizvoda,
        string vrstaProizvoda,
        string mernaJedincia,
        string opis,
        string hesh,
        uint256 zbirOcena,
        uint256 brojOcena,
        uint256 productNumber
    );
    
    event OmiljeniKreiran(
        uint256 productID,
        uint256 userID,
        uint256 productNumber
    );
    
    event DodatUKorpu(
        uint256 productID,
        uint256 userID,
        uint256 productNumber
    );
 
    constructor() public {
        proizvod[0] = Proizvod(1,1,90,"Crvena paprika1","Povrce","Paprika","kg","Najbolja crvena paprika u Sumadiji", "",8,2);
        proizvod[1] = Proizvod(2,1,90,"Zelena paprika2","Povrce","Paprika","kg","Najbolja crvena paprika u Sumadiji", "",0,0);
      
        
        brojProizvoda = 2;
        zadnjiProizvod = 2;
        omiljeni[0] = FavouritesProducts(1,1);
        brojOmiljeninih = 1;
        korpa[0] = Cart(1,1);
        brojUKorpi = 1;
    }
    
    function dodajOmiljeni(
        uint256 _idProizvoda,uint256 _idKorisnika
        ) public{
           
            for(uint256 i = 0 ;i < brojOmiljeninih;i++)
            {
                if(omiljeni[i].productID == _idProizvoda && omiljeni[i].userID == _idKorisnika)
                return;
            }
            
            for(uint256 i = 0 ;i < brojOmiljeninih;i++)
            {
                if(omiljeni[i].productID == 0 && omiljeni[i].userID == _idKorisnika)
                {
                    omiljeni[i].productID = _idProizvoda;
                    return;
                }
            }
        
            omiljeni[brojOmiljeninih++] = FavouritesProducts(_idProizvoda,_idKorisnika);
            emit OmiljeniKreiran(_idProizvoda,_idKorisnika,brojOmiljeninih-1);
            return;
            
        }
        
           
    function dodajUKorpu(
        uint256 _idProizvoda,uint256 _idKorisnika
        ) public{
           
            for(uint256 i = 0 ;i < brojUKorpi;i++)
            {
                if(korpa[i].productID == _idProizvoda && korpa[i].userID == _idKorisnika)
                return;
            }
            
            for(uint256 i = 0 ;i < brojUKorpi;i++)
            {
                if(korpa[i].productID == 0 && korpa[i].userID == _idKorisnika)
                {
                    korpa[i].productID = _idProizvoda;
                    return;
                }
            }
        
            korpa[brojUKorpi++] = Cart(_idProizvoda,_idKorisnika);
            emit DodatUKorpu(_idProizvoda,_idKorisnika,brojUKorpi-1);
            return;
            
        }
    
 
    function dodajProizvod(
       string memory _nazivProizvoda,
       string memory _kategorijaProizvoda,
       string memory _vrstaProizvoda,
       string memory _mernaJedincia,
       string memory _opis,
       uint256 _idOglasavaca,
       uint256 _cena,
       string memory _hesh
    ) public {
        
        proizvod[brojProizvoda++] = Proizvod(zadnjiProizvod + 1,_idOglasavaca,_cena,_nazivProizvoda,_kategorijaProizvoda,_vrstaProizvoda,_mernaJedincia,_opis,_hesh,0,0);
        zadnjiProizvod++;
        emit ProizvodKreiran(zadnjiProizvod,_idOglasavaca,_cena,_nazivProizvoda,_kategorijaProizvoda,_vrstaProizvoda,_mernaJedincia,_opis,_hesh,0,0,zadnjiProizvod - 1);
    }
    
    function obrisiOmiljeni(
        uint256 _productID,uint256 _userID
        )
        public{
            for(uint256 i = 0 ;i < brojOmiljeninih;i++)
            {
                if(omiljeni[i].userID == _userID && omiljeni[i].productID == _productID)
                {
                    omiljeni[i].productID = 0;
                    return;
                }
            }
        }
        
        function obrisiProizvod(
        uint256 _productID,uint256 _userID
        )
        public{
            for(uint256 i = 0 ;i < brojProizvoda;i++)
            {
                if(proizvod[i].id == _productID && proizvod[i].idOglasavaca == _userID)
                {
                    proizvod[i].idOglasavaca = 0;
                    proizvod[i].id = 0;
                    proizvod[i].cena = 0;
                    proizvod[i].nazivProizvoda = "";
                    proizvod[i].kategorijaProizvoda = "";
                    proizvod[i].vrstaProizvoda = "";
                    proizvod[i].mernaJedincia = "";
                    proizvod[i].opis = "";
                    proizvod[i].hesh = "";
                    proizvod[i].zbirOcena = 0;
                    proizvod[i].brojOcena = 0;

                    for (uint256 c = i; c < brojProizvoda - 1 ; c++ ) 
                    proizvod[c] = proizvod[c+1];  
                    brojProizvoda--;
                    return;
                }
            }
            
        }
        
        function izmeniProizvod(
        uint256 _productID,uint256 _userID,uint256 _cena,string memory _opis
        )
        public{
            for(uint256 i = 0 ;i < brojProizvoda;i++)
            {
                if(proizvod[i].id == _productID && proizvod[i].idOglasavaca == _userID)
                {
                    proizvod[i].cena = _cena;
                    proizvod[i].opis = _opis;
                    return;
                }
            }
        }
        
        function updateOcenu(uint256 _idProizvoda,uint256 ocena) public
        {
            for(uint256 i = 0 ;i < brojProizvoda;i++)
            {
                if(proizvod[i].id == _idProizvoda)
                {
                    proizvod[i].zbirOcena += ocena;
                    proizvod[i].brojOcena++;
                    return;
                }
            }
        }
        
        
        function obrisiIzKorpe(
        uint256 _productID,uint256 _userID
        )
        public{
            for(uint256 i = 0 ;i < brojUKorpi;i++)
            {
                if(korpa[i].userID == _userID && korpa[i].productID == _productID)
                {
                    korpa[i].productID = 0;
                }
            }
        }
        
    
    function vratiOmiljeneProizvodeKorisnika(uint256 _userID) public view returns(uint256[] memory)
    {
       uint256[] memory fav = new uint256[](brojOmiljeninih);
       uint256 br = 0;
       for(uint256 i = 0 ;i < brojOmiljeninih;i++)
       {
           if(omiljeni[i].userID == _userID && omiljeni[i].productID != 0)
           {
               fav[br++] = omiljeni[i].productID;
           }
       }
       
       return fav;
    }
    
    function vratiSadrzajKorpeZaKorisnika(uint256 _userID) public view returns(uint256[] memory)
    {
       uint256[] memory korp = new uint256[](brojUKorpi);
       uint256 br = 0;
       for(uint256 i = 0 ;i < brojUKorpi;i++)
       {
           if(korpa[i].userID == _userID && korpa[i].productID != 0)
           {
               korp[br++] = korpa[i].productID;
           }
       }
       
       return korp;
    }

    function izlistajProizvodePoStranicama(uint256 stranica) public view returns(Proizvod[] memory)
    {
        Proizvod[] memory desetProizvoda;
        uint256 idiDo = 0;
        uint256 pocetak = brojProizvoda - 1;
        
        
        uint256  br = 0;
        if(brojProizvoda <= 12)
        {
            desetProizvoda = new Proizvod[](brojProizvoda);
        }
        else
        {
            pocetak = brojProizvoda + 11 - 12 * stranica;
            if(pocetak <= 11)
            {
                idiDo = 0;
                desetProizvoda = new Proizvod[](pocetak+1);
            }
            else{
                idiDo = pocetak - 11;
                desetProizvoda = new Proizvod[](12);
            }
        }
        
       for(uint256 i = pocetak ;i >= idiDo;i--)
        {
            desetProizvoda[br++] = proizvod[i];
            if(i==0)
            break;
        }
             
        return desetProizvoda;
    }
}