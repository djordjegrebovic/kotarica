module.exports = {
    networks: {
        development: {
        host: "147.91.204.116",
        //host: "192.168.0.50",
        //host: "192.168.0.14",
            port: 11070,
            network_id: "*", // Match any network id
        },
        advanced: {
            websockets: true, // Enable EventEmitter interface for web3 (default: false)
        },
    },
    contracts_build_directory: "./src/abis/",
    compilers: {
        solc: {
            optimizer: {
                enabled: true,
                runs: 200,
            },
        },
    },
};