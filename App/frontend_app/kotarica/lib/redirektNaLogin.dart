import 'package:flutter/material.dart';
import 'package:kotarica/utils/themes.dart';
import 'package:kotarica/widgets/LoginSingin/sign_in_screen.dart';
import 'package:provider/provider.dart';
import 'eth_models/user_loginModels.dart';

void main() {
  runApp(RedirektNaLogin());
}

class RedirektNaLogin extends StatelessWidget {
  static String routeName = '/prijava_registracija';

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (context) => UserLoginModel(),
        child: MaterialApp(
            theme: Themes.light,
            debugShowCheckedModeBanner: false,
            title: 'flutter demo',
            home: SignInScreen()));
  }
}
