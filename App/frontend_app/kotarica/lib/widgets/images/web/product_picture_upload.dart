import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:image_picker_web_redux/image_picker_web_redux.dart';

// ignore: must_be_immutable
class UploadProductImage extends StatefulWidget {
  Uint8List image;

  get imagee => image;
  @override
  _UploadProductImageState createState() => _UploadProductImageState();
}

class _UploadProductImageState extends State<UploadProductImage> {
  List<Uint8List> bytesFromPicker;
  pickImage() async {
    widget.image = await ImagePickerWeb.getImage(outputType: ImageType.bytes);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          height: 150,
          width: 150,
          child: GestureDetector(
              onTap: () async {
                await pickImage();
                setState(() {});
              },
              child: Container(
                width: 250,
                height: 250,
                color: Color(0xffFDCF09),
                child: widget.image != null
                    ? Stack(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.zero,
                            child: Image.memory(
                              widget.image,
                              width: 250,
                              height: 250,
                              fit: BoxFit.fill,
                            ),
                          ),
                          Positioned(
                            right: 0,
                            top: 0,
                            child: IconButton(
                              onPressed: () {
                                setState(() {
                                  widget.image = null;
                                });
                              },
                              icon: Icon(Icons.cancel),
                              color: Theme.of(context).colorScheme.surface,
                            ),
                          )
                        ],
                      )
                    : Container(
                        color: Colors.grey[200],
                        width: 250,
                        height: 250,
                        child: Icon(
                          Icons.camera_alt,
                          color: Colors.grey[800],
                        ),
                      ),
              )),
        ),
      ],
    );
  }
}
