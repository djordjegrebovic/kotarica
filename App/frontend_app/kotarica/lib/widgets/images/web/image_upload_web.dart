import 'dart:convert';
import 'dart:typed_data';

import 'package:http/http.dart' as http;
import 'package:kotarica/utils/const.dart';

class UploadImageIpfs {
  String hes;
  uploadFileWeb(bool multiple, Uint8List file2) async {
    var url = Const.url + "/api/image";
    var uri = Uri.parse(url);

    if (file2 != null) {
      print("ULAZI...");
      var request = new http.MultipartRequest("POST", uri);
      request.files.add(new http.MultipartFile.fromBytes('images', file2,
          filename: "stagod"));
      print("SALJEM...");
      var response = await request.send();
      print("SALJEM1...");
      final respStr = await response.stream.bytesToString();
      print("SALJEM2...");

      Map<String, dynamic> image = jsonDecode(respStr);
      print(response);
      print(image['message']);

      if (response.statusCode == 200) {
        print("Image ID: ${image['id']}");
        hes = image['id'].toString();
      }
    } else {
      print("Nesto nije u redu sa serverom i kacenjem slike");
    }
  }
}