import 'dart:typed_data';
import 'package:image_picker_web_redux/image_picker_web_redux.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ProfileImage extends StatefulWidget {
  Uint8List image;
  @override
  _ProfileImageState createState() => _ProfileImageState();
}

class _ProfileImageState extends State<ProfileImage> {
  pickImage() async {
    widget.image = await ImagePickerWeb.getImage(outputType: ImageType.bytes);

    //UploadImageIpfs().upload(mageFile);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          height: 150,
          width: 150,
          child: GestureDetector(
              onTap: () async {
                await pickImage();
                setState(() {});
              },
              child: CircleAvatar(
                radius: 55,
                backgroundColor: Color(0xffFDCF09),
                child: widget.image != null
                    ? Stack(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(73),
                            child: Image.memory(
                              widget.image,
                              width: 150,
                              height: 150,
                              fit: BoxFit.fill,
                            ),
                          ),
                          Positioned(
                            right: 0,
                            top: 0,
                            child: IconButton(
                              onPressed: () {
                                setState(() {
                                  widget.image = null;
                                });
                              },
                              icon: Icon(Icons.cancel),
                              color: Theme.of(context).colorScheme.surface,
                            ),
                          )
                        ],
                      )
                    : Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.circular(73)),
                        width: 150,
                        height: 150,
                        child: Icon(
                          Icons.camera_alt,
                          color: Colors.grey[800],
                        ),
                      ),
              )),
        ),
      ],
    );
  }
}
