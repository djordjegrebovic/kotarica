import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

// ignore: must_be_immutable
class UploadProductImage extends StatefulWidget {
  File image;

  get imagee => image;
  @override
  _UploadProductImageState createState() => _UploadProductImageState();
}

class _UploadProductImageState extends State<UploadProductImage> {
  _imgFromCamera() async {
    try {
      // ignore: deprecated_member_use
      File image = await ImagePicker.pickImage(
          source: ImageSource.camera, imageQuality: 50);

      setState(() {
        widget.image = image;
      });
    } catch (Exeption) {
      print("Ne postoji kamera");
    }
    // ignore: empty_statements
    ;
  }

  _imgFromGallery() async {
    // ignore: deprecated_member_use
    File image = await ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 50);

    setState(() {
      widget.image = image;
    });
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Galerija'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Kamera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          height: 150,
          width: 150,
          child: GestureDetector(
              onTap: () async {
                _showPicker(context);
              },
              child: Container(
                width: 250,
                height: 250,
                color: Color(0xffFDCF09),
                child: widget.image != null
                    ? Stack(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.zero,
                            child: Image.file(
                              widget.image,
                              width: 250,
                              height: 250,
                              fit: BoxFit.fill,
                            ),
                          ),
                          Positioned(
                            right: 0,
                            top: 0,
                            child: IconButton(
                              onPressed: () {
                                setState(() {
                                  widget.image = null;
                                });
                              },
                              icon: Icon(Icons.cancel),
                              color: Theme.of(context).colorScheme.surface,
                            ),
                          )
                        ],
                      )
                    : Container(
                        color: Colors.grey[200],
                        width: 250,
                        height: 250,
                        child: Icon(
                          Icons.camera_alt,
                          color: Colors.grey[800],
                        ),
                      ),
              )),
        ),
      ],
    );
  }
}
