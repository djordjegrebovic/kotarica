import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class LoadImageIpfs {
  Future<ImageProvider> loadImage(String thumbnail) async {
    print("hash: " + thumbnail);
    String image = await ipfsImage(thumbnail);
    return Image.memory(base64Decode(image)).image;
  }

  //download slike sa ipfs
  Future<String> ipfsImage<Object>(String hash) async {
    var response = await http.post("http://192.168.0.50:3000/download", body: {
      "hash": hash,
    });
    print(response);
    String decoded;
    if (response.statusCode == 200) {
      decoded = json.decode(response.body)['content'];
    }
    return decoded;
  }
}

// ignore: must_be_immutable
class ShowIpfsImage extends StatelessWidget {
  String _hash;
  ShowIpfsImage(this._hash);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<ImageProvider>(
      future: LoadImageIpfs().loadImage(_hash),
      // ignore: missing_return
      builder: (BuildContext context, AsyncSnapshot<ImageProvider> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return CircularProgressIndicator();
          case ConnectionState.done:
            if (snapshot.hasError) {
              return Text('Error: ${snapshot.error}');
            } else if (!snapshot.hasData) {
              print("Usao u stranicu jer nema podata");
              return Text("NEMA");
            }
            print("Usao u stranicu");
            return Image(
              image: snapshot.data,
              width: 150,
              height: 150,
              fit: BoxFit.fill,
            );
          case ConnectionState.none:
            return Text('No connection');
          case ConnectionState.active:
            break;
        }
      },
    );
  }
}
