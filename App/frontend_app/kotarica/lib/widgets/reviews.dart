import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kotarica/widgets/userView.dart';

class Reviews extends StatefulWidget {
  Reviews({Key key}) : super(key: key);

  @override
  _ReviewsState createState() => _ReviewsState();
}

class Review extends Object {
  UserView user;
  double review;
  String comment;

  Review(this.user, this.review, this.comment);
}

class _ReviewsState extends State<Reviews> {
  List<Review> gridItems = [
    new Review(UserView(), 3, 'Komentar'),
    new Review(UserView(), 3, 'Komentar'),
    new Review(UserView(), 3, 'Komentar'),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(children: [
      Expanded(
        child: GridView.count(
          crossAxisCount: 1,
          childAspectRatio: (3 / 1),
          children: gridItems
              .map((data) => GestureDetector(
                      child: Container(
                    margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                    child: Center(
                        child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          data.user,
                          Align(
                              alignment: Alignment.centerRight,
                              child: Text(
                                data.comment.toString(),
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              )),
                          Align(
                            alignment: Alignment.topRight,
                            child: Row(
                              children: [
                                Icon(Icons.star),
                                Icon(Icons.star),
                                Icon(Icons.star),
                                Icon(Icons.star_border),
                                Icon(Icons.star_border),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10),
                          bottomLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10)),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                  )))
              .toList(),
        ),
      ),
    ]));
  }
}
