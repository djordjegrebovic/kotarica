import 'package:flutter/material.dart';
import 'package:kotarica/eth_models/user_loginModels.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/widgets/user/profileDescription.dart';

// ignore: must_be_immutable
class DescriptionWeb extends StatelessWidget {
  UserData _data;
  DescriptionWeb(this._data);
  static UserData userDataWeb;
  static int web;
  @override
  Widget build(BuildContext context) {
    ProfileDescription.userData = _data;
    DescriptionWeb.web = 1;
    double fontSizeWeb = 20;
    double fontSizeApp = 16;
    double width = MediaQuery.of(context).size.width;
    Color dataBoxColor = Theme.of(context).colorScheme.secondary;
    double fieldWidth = 281;
    return Container(
      width: width < 800 ? width * 0.87 : 592,
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        // boxShadow: [BoxShadow(blurRadius: 10)],
        color: Theme.of(context)
            .colorScheme
            .onBackground
            .withOpacity(0.4), //Boje.userPageContainerColor,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          //IME PREZIME
          Row(
            children: [
              Container(
                width: fieldWidth,
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                decoration: BoxDecoration(
                  color: dataBoxColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Row(
                  children: [
                    Text("Ime: ", style: profileLabelData()),
                    Flexible(
                      child: Text(_data.name,
                          style: width < 800
                              ? profileData(fontSizeApp)
                              : profileData(fontSizeWeb),
                          overflow: TextOverflow.ellipsis),
                    ),
                  ],
                ),
              ),
              SizedBox(width: 10),
              Container(
                width: fieldWidth,
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                decoration: BoxDecoration(
                  color: dataBoxColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Row(
                  children: [
                    Text("Prezime: ", style: profileLabelData()),
                    Flexible(
                        child: Text(_data.surname,
                            style: width < 800
                                ? profileData(fontSizeApp)
                                : profileData(fontSizeWeb),
                            overflow: TextOverflow.ellipsis))
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 8),
          // ADRESA GRAD
          Row(
            children: [
              Container(
                width: fieldWidth * 2 + 10,
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                decoration: BoxDecoration(
                  color: dataBoxColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Row(
                  children: [
                    Text("Adresa i grad: ", style: profileLabelData()),
                    Flexible(
                        child: Text(_data.homeAddress,
                            style: width < 800
                                ? profileData(fontSizeApp)
                                : profileData(fontSizeWeb),
                            overflow: TextOverflow.ellipsis))
                  ],
                ),
              ),
              // SizedBox(width: 10),
              // Container(
              //   width: fieldWidth,
              //   padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              //   decoration: BoxDecoration(
              //     color: dataBoxColor,
              //     borderRadius: BorderRadius.circular(10),
              //   ),
              //   child: Row(children: [
              //     Text("Grad: "),
              //     Flexible(
              //       child: Text(_data.homeAddress, // Treba da se stavi GRAD
              //           style: width < 800
              //               ? profileData(fontSizeApp)
              //               : profileData(fontSizeWeb),
              //           overflow: TextOverflow.ellipsis),
              //     ),
              //   ]),
              // ),
            ],
          ),
          SizedBox(height: 8),
          Row(
            children: [
              Container(
                width: fieldWidth - 50,
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                decoration: BoxDecoration(
                  color: dataBoxColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Row(
                  children: [
                    Text("Broj telefona: ", style: profileLabelData()),
                    Flexible(
                        child: Text(_data.phoneNumber,
                            style: width < 800
                                ? profileData(fontSizeApp)
                                : profileData(fontSizeWeb),
                            overflow: TextOverflow.ellipsis))
                  ],
                ),
              ),
              SizedBox(width: 10),
              Container(
                width: fieldWidth + 50,
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                decoration: BoxDecoration(
                  color: dataBoxColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Row(
                  children: [
                    Text("Email: ", style: profileLabelData()),
                    Flexible(
                        child: Text(_data.email,
                            style: width < 800
                                ? profileData(fontSizeApp)
                                : profileData(fontSizeWeb),
                            overflow: TextOverflow.ellipsis))
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

TextStyle profileData(double size) {
  return TextStyle(
      fontFamily: 'Open-Sans',
      color: Boje.mainText, // surface //Colors.teal[700],
      fontSize: size,
      fontWeight: FontWeight.bold);
}

TextStyle profileLabelData() {
  return TextStyle(
      fontFamily: 'Open-Sans',
      color: Boje.mainText, // surface //Colors.teal[700],
      fontSize: 13,
      fontWeight: FontWeight.w500);
}
