import 'package:flutter/material.dart';
import 'package:kotarica/eth_models/user_loginModels.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/widgets/LoginSingin/default_button.dart';
import 'package:kotarica/widgets/LoginSingin/form_error.dart';
import 'package:kotarica/widgets/SignUp/InputBorderDecoration.dart';
import 'package:kotarica/widgets/cart/purchaseSuccess.dart';
import 'package:provider/provider.dart';

class ChangePrivateKey extends StatefulWidget {
  @override
  _ChangeKeyState createState() => _ChangeKeyState();
}

class _ChangeKeyState extends State<ChangePrivateKey> {
  // ignore: non_constant_identifier_names
  final TextEditingController privateKeyController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  String email;
  String password;
  String username;
  bool remember = false;
  final List<String> errors = [];
  double iconSize = 26;
  Color iconColor;

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  bool showProgress = false;

  @override
  Widget build(BuildContext context) {
    var listModel = Provider.of<UserLoginModel>(context);
    double width = MediaQuery.of(context).size.width;
    Color color = Theme.of(context).colorScheme.surface;

    // var jwt;
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios, color: Boje.mainText),
            onPressed: () {
              GetInfo.korpa = false;
              Navigator.pop(context);
              // Navigator.push(context,
              //     MaterialPageRoute(builder: (context) => WelcomeScreen()));
            },
          ),
          title: Text(
            "Promena Etherium novčanika",
            style: TextStyle(color: Boje.mainText, fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
        ),
        body: Center(
          child: Consumer<UserLoginModel>(
            builder: (context, provider, child) => Form(
              key: _formKey,
              child: Container(
                width: width < 500 ? width - 20 : 500 - 10,
                child: Column(children: [
                  SizedBox(height: 30),
                  // Text("Trenutni privatni kljuc : " + UserLoginModel.privKey),
                  SizedBox(height: 30),
                  buildPrivateKeyFormField(),
                  SizedBox(height: 30),
                  FormError(
                    errors: errors,
                    error_color: Theme.of(context).colorScheme.error,
                  ),
                  SizedBox(height: 20),
                  DefaultButton(
                      text: "Nastavi",
                      width: width < 500 ? width : 500,
                      press: () async {
                        // print(UserLoginModel.privKey);
                        if (_formKey.currentState.validate() &&
                            privateKeyController.text !=
                                UserLoginModel.privKey) {
                          _formKey.currentState.save();
                          setState(() {
                            // set the progress indicator to true so it would not be visible
                            showProgress = true;
                          });
                          listModel.changePrivateKey(
                              GetInfo.id, privateKeyController.text);
                          UserLoginModel.privKey = privateKeyController.text;
                          setState(() {
                            Navigator.push(
                                context,
                                new MaterialPageRoute(
                                    builder: (context) => PurchaseSuccessBody(
                                        'Uspešno ste promenili Etherium novcanik.')));
                          });
                          // RESITI
                        } else {
                          provider.displayDialog(context, "Desila se greška!",
                              "Izmena Etherium naloga nije uspela.");
                        }
                      }),
                ]),
              ),
            ),
          ),
        ));
  }

//Polja za upisivanje podataka:

  TextFormField buildPrivateKeyFormField() {
    return TextFormField(
      controller: privateKeyController,
      autofocus: true,
      obscureText: true,
      onSaved: (newValue) => password = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPrivateKeyNullError);
        }
        if (privateKeyValidatorRegExp.hasMatch(value)) {
          removeError(error: kInvalidPrivateKey);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPrivateKeyNullError);
          return "";
        } else if (!privateKeyValidatorRegExp.hasMatch(value)) {
          addError(error: kInvalidPrivateKey);
          return "";
        }

        return null;
      },
      decoration: buildInputDecoration(
          "Privatni ključ",
          "Unesite novi privatni ključ",
          Icon(Icons.privacy_tip_outlined, size: iconSize, color: iconColor),
          context),
    );
  }
}
