import 'package:flutter/material.dart';
import 'package:kotarica/eth_models/user_loginModels.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/widgets/LoginSingin/default_button.dart';
import 'package:kotarica/widgets/LoginSingin/form_error.dart';
import 'package:kotarica/widgets/SignUp/InputBorderDecoration.dart';
import 'package:kotarica/widgets/cart/purchaseSuccess.dart';
import 'package:provider/provider.dart';

class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  // ignore: non_constant_identifier_names
  final TextEditingController password_controller_prva =
      TextEditingController();
  // ignore: non_constant_identifier_names
  final TextEditingController password_controller_druga =
      TextEditingController();
  // ignore: non_constant_identifier_names
  final TextEditingController password_controller_nova =
      TextEditingController();

  final _formKey = GlobalKey<FormState>();
  String email;
  String passwordNova;
  String passwordStara;

  String username;
  bool remember = false;
  final List<String> errors = [];
  double iconSize = 26;
  Color iconColor;
  // ignore: non_constant_identifier_names
  String confirm_password;

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  bool showProgress = false;

  @override
  Widget build(BuildContext context) {
    var listModel = Provider.of<UserLoginModel>(context);
    double width = MediaQuery.of(context).size.width;
    Color color = Theme.of(context).colorScheme.surface;

    // var jwt;
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios, color: Boje.mainText),
            onPressed: () {
              GetInfo.korpa = false;
              Navigator.pop(context);
              // Navigator.pop();
            },
          ),
          title: Text(
            "Promena lozinke",
            style: TextStyle(color: Boje.mainText, fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
        ),
        body: Center(
          child: Container(
            child: Consumer<UserLoginModel>(
              builder: (context, provider, child) => Form(
                key: _formKey,
                child: Container(
                  width: width < 500 ? width - 20 : 500 - 10,
                  child: Column(children: [
                    SizedBox(
                      height: 30,
                    ),
                    buildPasswordFormField1(),
                    SizedBox(height: 30),
                    buildPasswordFormField2(),
                    SizedBox(height: 30),
                    buildPasswordFormField3(),
                    SizedBox(height: 30),
                    FormError(
                      errors: errors,
                      error_color: Theme.of(context).colorScheme.error,
                    ),
                    SizedBox(height: 20),
                    DefaultButton(
                        text: "Nastavi",
                        width: width < 500 ? width : 500,
                        press: () async {
                          // print(password_controller.text);
                          if (_formKey.currentState.validate() &&
                              UserLoginModel.sifra ==
                                  password_controller_prva.text &&
                              password_controller_druga.text ==
                                  password_controller_nova.text &&
                              UserLoginModel.sifra !=
                                  password_controller_nova.text) {
                            _formKey.currentState.save();
                            setState(() {
                              // set the progress indicator to true so it would not be visible
                              showProgress = true;
                            });
                            listModel.changePassword(
                                GetInfo.id, password_controller_nova.text);
                            UserLoginModel.sifra =
                                password_controller_nova.text;
                            Navigator.push(
                                context,
                                new MaterialPageRoute(
                                    builder: (context) => PurchaseSuccessBody(
                                        'Uspešno ste promenili šifru.')));
                          }
                          // else {
                          //   provider.displayDialog(context, "Desila se greška!",
                          //       "Izmena lozinke nije uspela.");
                          // }
                        }),
                  ]),
                ),
              ),
            ),
          ),
        ));
  }

//Polja za upisivanje podataka:

  TextFormField buildPasswordFormField1() {
    return TextFormField(
      controller: password_controller_prva,
      autofocus: true,
      obscureText: true,
      onSaved: (newValue) => passwordStara = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kOldPassNullError);
        }
        if (UserLoginModel.sifra == value) {
          removeError(error: kWrongOldPassError);
        }
        if (value.length >= 8) {
          removeError(error: kShortPassError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kOldPassNullError);
          return "";
        } else if (UserLoginModel.sifra != value) {
          addError(error: kWrongOldPassError);
          return "";
        } else if (value.length < 8) {
          addError(error: kShortPassError);
          return "";
        }

        return null;
      },
      decoration: buildInputDecoration("Stara lozinka", "Unesite vašu lozinku",
          Icon(Icons.lock_outline, size: iconSize, color: iconColor), context),
    );
  }

  TextFormField buildPasswordFormField2() {
    return TextFormField(
      controller: password_controller_druga,
      obscureText: true,
      onSaved: (newValue) => passwordNova = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kNewPassNullError);
        }
        if (value.length >= 8) {
          removeError(error: kShortPassError);
        }
        if (UserLoginModel.sifra != value) {
          removeError(error: kNewPassSamePasswordError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kNewPassNullError);
          return "";
        } else if (value.length < 8) {
          addError(error: kShortPassError);
          return "";
        } else if (UserLoginModel.sifra == value) {
          addError(error: kNewPassSamePasswordError);
          return "";
        }
        return null;
      },
      decoration: buildInputDecoration(
          "Nova lozinka",
          "Unesite novu lozinku sa najmanje 8 karaktera",
          Icon(Icons.lock_outline, size: iconSize, color: iconColor),
          context),
    );
  }

  TextFormField buildPasswordFormField3() {
    return TextFormField(
      controller: password_controller_nova,
      obscureText: true,
      onSaved: (newValue) => confirm_password = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPassNullErrorConfirm);
        }
        if (password_controller_druga.text == value) {
          removeError(error: kMatchPassError);
        }
        confirm_password = value;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPassNullErrorConfirm);
          return "";
        } else if ((password_controller_druga.text != value)) {
          addError(error: kMatchPassError);
          return "";
        }
        return null;
      },
      decoration: buildInputDecoration(
          "Potvrda nove lozinke",
          "Ponovite vašu novu lozinku",
          Icon(Icons.lock_outline, size: iconSize, color: iconColor),
          context),
    );
  }
}
