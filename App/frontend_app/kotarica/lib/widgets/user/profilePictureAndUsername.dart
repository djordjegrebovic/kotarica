import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:kotarica/eth_models/user_loginModels.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/pages/edit_user_info.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/utils/const.dart';
import 'package:kotarica/widgets/user/descriptionWeb.dart';
import 'package:kotarica/widgets/user/profileDescription.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:universal_html/html.dart' as universal;

class BoxWithUserNameAndImage extends StatelessWidget {
  BoxWithUserNameAndImage({this.id});
  static var userData;
  static int dataExists;

  final BigInt id;
  @override
  Widget build(BuildContext context) {
    Future<String> _loadPreferences(String dataNeeded) async {
      SharedPreferences pref = await SharedPreferences.getInstance();
      return pref.getString(dataNeeded);
    }

    // print("ulazi ovde");
    var listModel = Provider.of<UserLoginModel>(context);
    double width = MediaQuery.of(context).size.width;

    // String username = listModel.getUserById(id).username;

    return Center(
      child: Container(
        padding: EdgeInsets.only(bottom: 10, top: 10),
        width: width < 800 ? width : 800,
        // height: MediaQuery.of(context).size.height * 0.30,
        decoration: BoxDecoration(
          gradient: RadialGradient(
            center: width < 800 ? Alignment.center : Alignment.centerLeft,
            radius: 0.8,
            colors: [
              Color.fromARGB(255, 181, 202, 198),
              Theme.of(context).colorScheme.secondary
            ],
          ),
          boxShadow: [
            BoxShadow(
              blurRadius: 10,
            ),
          ],
        ),
        child: !kIsWeb
            ? FutureBuilder<String>(
                future: _loadPreferences("username"),
                builder:
                    (BuildContext context, AsyncSnapshot<String> snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                      return SpinKitFadingCube(
                          size: 35,
                          color: Theme.of(context).colorScheme.primary);
                    default:
                      if (snapshot.hasError) {
                        return Text('Error: ${snapshot.error}');
                      } else {
                        return Container(
                          // color: Colors.red,
                          alignment: width < 800
                              ? Alignment.center
                              : Alignment.centerLeft,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Center(child: ProfileImage(this.id)),
                              SizedBox(height: width < 800 ? 5 : 15),
                              this.id == GetInfo.id
                                  ? Center(
                                      child: Text(
                                        snapshot.data,
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .surface,
                                          fontSize: width < 800 ? 20 : 25,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    )
                                  : SizedBox(),
                            ],
                          ),
                        );
                      }
                  }
                },
              )
            : Row(
                mainAxisAlignment: width < 800
                    ? MainAxisAlignment.center
                    : MainAxisAlignment.start,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: width < 800
                        ? CrossAxisAlignment.center
                        : CrossAxisAlignment.start,
                    children: [
                      // SLIKA PROFILA
                      Container(
                          width: 200,
                          alignment: Alignment.center,

                          // alignment:
                          //     width < 800 ? Alignment.center : Alignment.centerLeft,

                          child: ProfileImage(this.id)),
                      SizedBox(height: 8),
                      // USERNAME
                      this.id == GetInfo.id
                          ? Container(
                              width: width < 800 ? null : 170,
                              alignment: Alignment.center,
                              // alignment: width < 800
                              //     ? Alignment.center
                              //     : Alignment.centerLeft,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 5),
                              decoration: BoxDecoration(
                                // color: width < 800
                                //     ? Theme.of(context)
                                //         .colorScheme
                                //         .onBackground
                                //         .withOpacity(0.4)
                                //     : null,

                                color: Theme.of(context)
                                    .colorScheme
                                    .onBackground
                                    .withOpacity(0.4),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              margin: width < 800
                                  ? null
                                  : EdgeInsets.only(left: 15),
                              child: Text(
                                GetInfo.id != BigInt.zero
                                    ? universal.window.localStorage["username"]
                                    : "",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  // color: width < 800
                                  //     ? Theme.of(context).colorScheme.surface
                                  //     : Theme.of(context).colorScheme.onBackground,
                                  color: Boje.mainText,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            )
                          : SizedBox(),
                    ],
                  ),
                  width >= 800
                      ? Container(
                          child: listModel.isLoading1 == true
                              ? LoadingDataforUser()
                              : FutureBuilder<UserData>(
                                  future: listModel.giveDataForUser1(this.id),
                                  // ignore: missing_return
                                  builder: (BuildContext context,
                                      AsyncSnapshot<UserData> snapshot) {
                                    switch (snapshot.connectionState) {
                                      case ConnectionState.waiting:
                                        return SpinKitFadingCube(
                                            size: 35,
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary);
                                      case ConnectionState.done:
                                        if (snapshot.hasError) {
                                          return Text(
                                              'Error: ${snapshot.error}');
                                        } else if (!snapshot.hasData) {
                                          dataExists = 0;
                                          return NoDataToShow();
                                        }
                                        dataExists = 1;
                                        userData = snapshot.data;
                                        return listModel.isLoading1 == true
                                            ? LoadingDataforUser()
                                            : DescriptionWeb(snapshot.data);
                                      // return DescriptionWeb(snapshot.data);

                                      case ConnectionState.none:
                                        return Text('No connection');
                                      case ConnectionState.active:
                                        return CircularProgressIndicator();
                                    }
                                  },
                                ),
                        )
                      : SizedBox(),
                ],
              ),
      ),
    );
  }
}

// ignore: must_be_immutable
class ProfileImage extends StatelessWidget {
  final int _indikator = 1;

  ProfileImage(this.id);
  BigInt id;
  @override
  Widget build(BuildContext context) {
    var listModel = Provider.of<UserLoginModel>(context);
    UserData data1 = listModel.vratiInformacijeOKorisniku(id);
    return SizedBox(
      height: 150,
      width: 150,
      child: Center(
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            listModel.isLoading3
                ? CircularProgressIndicator()
                : data1.profilePictureHash != ""
                    ? CircleAvatar(
                        radius: 75,
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(73),
                            child: listModel.isLoading3 == true
                                ? CircularProgressIndicator()
                                : Image(
                                    image: NetworkImage(Const.url +
                                        "/api/Image/" +
                                        data1.profilePictureHash),
                                    height: 800,
                                    width: 800,
                                    fit: BoxFit.fill,
                                  )),
                      )
                    : CircleAvatar(
                        radius: 75,
                        backgroundImage: NetworkImage(
                            "https://images.megapixl.com/1796/17965459.jpg"),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(73),
                        )),
            this.id == GetInfo.id
                ? Positioned(
                    right: 15,
                    bottom: 5,
                    child: (_indikator == 1)
                        ? Container(
                            decoration: BoxDecoration(boxShadow: [
                              BoxShadow(
                                blurRadius: 3,
                                color: Theme.of(context)
                                    .colorScheme
                                    .secondaryVariant,
                                offset: Offset(13, 13),
                              ),
                            ]),
                            width: 35,
                            height: 35,
                            child: IconButton(
                                onPressed: () {
                                  Navigator.pushNamed(
                                      context, EditInfo.routeName,
                                      arguments:
                                          EditInfoArguments(id: this.id));
                                },
                                splashColor:
                                    Theme.of(context).colorScheme.secondary,
                                icon: Icon(
                                  Icons.settings_applications_rounded,
                                  size: 40,
                                  color: Theme.of(context)
                                      .colorScheme
                                      .onBackground,
                                )),
                          )
                        : SizedBox(),
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }
}
