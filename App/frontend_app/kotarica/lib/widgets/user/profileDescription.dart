import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:kotarica/eth_models/user_loginModels.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:provider/provider.dart';

class ProfileDescription extends StatelessWidget {
  static var userData;
  static int dataExists;

  ProfileDescription({this.id});

  final BigInt id;
  @override
  Widget build(BuildContext context) {
    // print("ulazi ovde1");
    var listModel = Provider.of<UserLoginModel>(context);

    if (listModel.isLoading1) {
      print("uso");
      // listModel.getUsersData();
      return LoadingDataforUser();
    } else {
      UserData data = listModel.giveDataForUser(this.id);
      userData = data;
      if (data == null) {
        return Container(
          child: NoDataToShow(),
        );
      } else {
        dataExists = 1;
        return Container(
          child: DataToBeShown(data),
        );
      }
    }
  }
  //   return Container(
  //     child: FutureBuilder<UserData>(
  //       future: listModel.giveDataForUser(this.id),
  //       // ignore: missing_return
  //       builder: (BuildContext context, AsyncSnapshot<UserData> snapshot) {
  //         switch (snapshot.connectionState) {
  //           case ConnectionState.waiting:
  //             return SpinKitFadingCube(
  //                 size: 35, color: Theme.of(context).colorScheme.primary);
  //           case ConnectionState.done:
  //             if (snapshot.hasError) {
  //               return Text('Error: ${snapshot.error}');
  //             } else if (!snapshot.hasData) {
  //               dataExists = 0;
  //               return NoDataToShow();
  //             }
  //             dataExists = 1;
  //             userData = snapshot.data;
  //             return DataToBeShown(snapshot.data);
  //           // return DescriptionWeb(snapshot.data);

  //           case ConnectionState.none:
  //             return Text('No connection');
  //           case ConnectionState.active:
  //             return CircularProgressIndicator();
  //         }
  //       },
  //     ),
  //   );
}
// }

// ignore: must_be_immutable
class DataToBeShownCheckout extends StatefulWidget {
  // ignore: non_constant_identifier_names
  static final TextEditingController adresa_kontroler = TextEditingController();
  // ignore: non_constant_identifier_names
  static final TextEditingController telefon_kontroler =
      TextEditingController();
  static String adresa1;
  static String brojTelefona1;
  static String adresa;
  static String brojTelefona;

  UserData _data;
  DataToBeShownCheckout(this._data);

  @override
  _DataToBeShownCheckoutState createState() => _DataToBeShownCheckoutState();
}

class _DataToBeShownCheckoutState extends State<DataToBeShownCheckout> {
  String text = "Adresa: ";
  int ind = 0;
  int ind1 = 0;
  int ind2 = 0;
  int ind3 = 0;
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double fontSizeWeb = 20;
    double fontSizeApp = 16;
    DataToBeShownCheckout.adresa = widget._data.homeAddress;
    DataToBeShownCheckout.brojTelefona = widget._data.phoneNumber;
    Color dataBoxColor = Theme.of(context).colorScheme.secondary;
    return Container(
      width: width < 800 ? width : 800,
      padding: EdgeInsets.all(width < 500 ? 15 : 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        // boxShadow: [BoxShadow(blurRadius: 10)],
        color: Theme.of(context).primaryColor, //Boje.userPageContainerColor,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // IME
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            decoration: BoxDecoration(
              color: dataBoxColor,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(children: [
              Text("Ime: "),
              Flexible(
                child: Text(widget._data.name,
                    style: width < 800
                        ? profileData(fontSizeApp)
                        : profileData(fontSizeWeb),
                    overflow: TextOverflow.ellipsis),
              ),
            ]),
          ),
          SizedBox(height: 3),
          // PREZIME
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            decoration: BoxDecoration(
              color: dataBoxColor,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(children: [
              Text("Prezime: "),
              Flexible(
                  child: Text(widget._data.surname,
                      style: width < 800
                          ? profileData(fontSizeApp)
                          : profileData(fontSizeWeb),
                      overflow: TextOverflow.ellipsis))
            ]),
          ),
          SizedBox(height: 3),
          // BROJ TELEFONA
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            decoration: BoxDecoration(
              color: dataBoxColor,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Text("Broj telefona: "),
                    Container(
                      width: width < 500
                          ? width * 0.425
                          : width < 800
                              ? width * 0.5
                              : 500,
                      child: ind == 0
                          ? SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Text(
                                  ind3 == 0
                                      ? widget._data.phoneNumber
                                      : DataToBeShownCheckout
                                          .telefon_kontroler.text,
                                  style: width < 800
                                      ? profileData(fontSizeApp)
                                      : profileData(fontSizeWeb),
                                  overflow: TextOverflow.ellipsis),
                            )
                          : Container(
                              width: width < 500
                                  ? width * 0.4
                                  : width < 800
                                      ? width * 0.5
                                      : 500,
                              child: TextField(
                                controller:
                                    DataToBeShownCheckout.telefon_kontroler,
                                decoration: InputDecoration(
                                    hintText: widget._data.phoneNumber),
                              ),
                            ),
                    ),
                  ],
                ),
                Container(
                  child: ind == 0
                      ? GestureDetector(
                          child: Icon(Icons.edit),
                          onTap: () {
                            setState(() {
                              ind = 1;
                            });
                          },
                        )
                      : Row(
                          children: [
                            GestureDetector(
                              child: Icon(Icons.check),
                              onTap: () {
                                setState(() {
                                  ind3 = 2;
                                  ind = 0;
                                  DataToBeShownCheckout.brojTelefona1 =
                                      DataToBeShownCheckout
                                          .telefon_kontroler.text;
                                });
                              },
                            ),
                            GestureDetector(
                              child: Icon(Icons.close),
                              onTap: () {
                                setState(() {
                                  ind = 0;
                                });
                              },
                            )
                          ],
                        ),
                )
              ],
            ),
          ),
          // SizedBox(height: 3),
          // Container(
          //   padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          //   decoration: BoxDecoration(
          //     color: dataBoxColor,
          //     borderRadius: BorderRadius.circular(10),
          //   ),
          //   child: Row(children: [
          //     Text("Grad: "),
          //     Flexible(
          //       child: Text(widget._data.homeAddress, // Treba da se stavi GRAD
          //           style: width < 800
          //               ? profileData(fontSizeApp)
          //               : profileData(fontSizeWeb),
          //           overflow: TextOverflow.ellipsis),
          //     ),
          //   ]),
          // ),
          SizedBox(height: 3),
          // ADRESA
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            decoration: BoxDecoration(
              color: dataBoxColor,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Text("Adresa i grad: "),
                    Container(
                      width: width < 450
                          ? width * 0.41
                          : width < 800
                              ? width * 0.5
                              : 500,
                      child: ind1 == 0
                          ? SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Text(
                                ind2 == 2
                                    ? DataToBeShownCheckout
                                        .adresa_kontroler.text
                                    : widget._data.homeAddress,
                                style: width < 800
                                    ? profileData(fontSizeApp)
                                    : profileData(fontSizeWeb),
                                overflow: TextOverflow.ellipsis,
                              ),
                            )
                          : Container(
                              width: width < 450
                                  ? width * 0.345
                                  : width < 800
                                      ? width * 0.5
                                      : 500,
                              child: TextField(
                                controller:
                                    DataToBeShownCheckout.adresa_kontroler,
                                decoration: InputDecoration(
                                    // helperText: widget._data.homeAddress,
                                    // labelText: widget._data.homeAddress,
                                    hintText: widget._data.homeAddress),
                              ),
                            ),
                    ),
                  ],
                ),
                Container(
                  child: ind1 == 0
                      ? GestureDetector(
                          child: Icon(Icons.edit),
                          onTap: () {
                            setState(() {
                              ind1 = 1;
                            });
                          },
                        )
                      : Row(
                          children: [
                            GestureDetector(
                              child: Icon(Icons.check),
                              onTap: () {
                                setState(() {
                                  ind2 = 2;
                                  ind1 = 0;
                                  DataToBeShownCheckout.adresa1 =
                                      DataToBeShownCheckout
                                          .adresa_kontroler.text;
                                });
                              },
                            ),
                            GestureDetector(
                              child: Icon(Icons.close),
                              onTap: () {
                                setState(() {
                                  ind1 = 0;
                                });
                              },
                            )
                          ],
                        ),
                )
              ],
            ),
          ),
          SizedBox(height: 3),
          // EMAIL
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            decoration: BoxDecoration(
              color: dataBoxColor,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              children: [
                Text("Email: "),
                Flexible(
                    child: Text(widget._data.email,
                        style: width < 800
                            ? profileData(fontSizeApp)
                            : profileData(fontSizeWeb),
                        overflow: TextOverflow.ellipsis)),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

// ignore: must_be_immutable
class DataToBeShown extends StatelessWidget {
  UserData _data;
  DataToBeShown(this._data);
  @override
  Widget build(BuildContext context) {
    double fontSizeWeb = 20;
    double fontSizeApp = 16;
    double width = MediaQuery.of(context).size.width;
    Color dataBoxColor = Theme.of(context).colorScheme.secondary;

    return Container(
      width: width < 800 ? width * 0.87 : 800,
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        boxShadow: [BoxShadow(blurRadius: 10)],
        color: Theme.of(context).primaryColor, //Boje.userPageContainerColor,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            decoration: BoxDecoration(
              color: dataBoxColor,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              children: [
                Text("Ime: "),
                Flexible(
                  child: Text(_data.name,
                      style: width < 800
                          ? profileData(fontSizeApp)
                          : profileData(fontSizeWeb),
                      overflow: TextOverflow.ellipsis),
                ),
              ],
            ),
          ),
          SizedBox(height: 3),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            decoration: BoxDecoration(
              color: dataBoxColor,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              children: [
                Text("Prezime: "),
                Flexible(
                    child: Text(_data.surname,
                        style: width < 800
                            ? profileData(fontSizeApp)
                            : profileData(fontSizeWeb),
                        overflow: TextOverflow.ellipsis))
              ],
            ),
          ),
          SizedBox(height: 3),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            decoration: BoxDecoration(
              color: dataBoxColor,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              children: [
                Text("Broj telefona: "),
                Flexible(
                    child: Text(_data.phoneNumber,
                        style: width < 800
                            ? profileData(fontSizeApp)
                            : profileData(fontSizeWeb),
                        overflow: TextOverflow.ellipsis))
              ],
            ),
          ),
          // SizedBox(height: 3),
          // Container(
          //   padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          //   decoration: BoxDecoration(
          //     color: dataBoxColor,
          //     borderRadius: BorderRadius.circular(10),
          //   ),
          //   child: Row(children: [
          //     Text("Grad: "),
          //     Flexible(
          //       child: Text(_data.homeAddress, // Treba da se stavi GRAD
          //           style: width < 800
          //               ? profileData(fontSizeApp)
          //               : profileData(fontSizeWeb),
          //           overflow: TextOverflow.ellipsis),
          //     ),
          //   ]),
          // ),
          SizedBox(height: 3),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            decoration: BoxDecoration(
              color: dataBoxColor,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              children: [
                Text("Adresa i grad: "),
                Flexible(
                    child: Text(_data.homeAddress,
                        style: width < 800
                            ? profileData(fontSizeApp)
                            : profileData(fontSizeWeb),
                        overflow: TextOverflow.ellipsis))
              ],
            ),
          ),
          SizedBox(height: 3),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            decoration: BoxDecoration(
              color: dataBoxColor,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              children: [
                Text("Email: "),
                Flexible(
                    child: Text(_data.email,
                        style: width < 800
                            ? profileData(fontSizeApp)
                            : profileData(fontSizeWeb),
                        overflow: TextOverflow.ellipsis))
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class LoadingDataforUser extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width < 800 ? width * 0.87 : 600,
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        // boxShadow: [BoxShadow(blurRadius: 10)],
        color: Theme.of(context)
            .colorScheme
            .secondary
            .withOpacity(0.5), //Boje.userPageContainerColor,
      ),
      child: Column(
        children: [
          Text(
            "Podaci o korisniku se učitavaju",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: width < 800 ? 15 : 21,
                color: Boje.mainText),
          ),
          SizedBox(height: 20),
          SpinKitFadingCube(
              size: 35, color: Theme.of(context).colorScheme.primary)
        ],
      ),
    );
  }
}

class NoDataToShow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width < 800 ? width * 0.87 : 600,
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        // boxShadow: [BoxShadow(blurRadius: 10)],
        color: Theme.of(context)
            .colorScheme
            .secondary
            .withOpacity(0.5), //Boje.userPageContainerColor,
      ),
      child: Text(
        "Nema podataka o ovom korisniku",
        textAlign: TextAlign.center,
        style: TextStyle(
            fontWeight: FontWeight.w600, fontSize: width < 800 ? 15 : 21),
      ),
    );
  }
}

TextStyle profileData(double size) {
  return TextStyle(
      fontFamily: 'Open-Sans',
      color: Boje.mainText, // surface //Colors.teal[700],
      fontSize: size,
      fontWeight: FontWeight.bold);
}
