import 'package:flutter/material.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/widgets/search/data_search.dart';
// import 'package:kotarica/widgets/search/sort_dropdownButton.dart';

class SearchAppBar extends PreferredSize {
  @override
  // AppBar().preferredSize.height provide us the height that appy on our app bar
  Size get preferredSize => Size.fromHeight(AppBar().preferredSize.height);
  static List<Product> lista = ProductModels.products;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    //filtriranje
    // lista
    //     .where((product) =>
    //         product.naziv.toLowerCase().contains("crvena".toLowerCase()))
    //     .toList();
    return SafeArea(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 8),
        width: 800,
        color: Theme.of(context).colorScheme.secondary,
        child: Column(
          children: [
            Row(
              mainAxisAlignment: width > 370
                  ? MainAxisAlignment.center
                  : MainAxisAlignment.spaceBetween,
              children: [
                SearchButton(
                  press: () async {
                    // ignore: unused_local_variable
                    final result = await showSearch<String>(
                        context: context, delegate: DataSearch());
                  },
                ),
                // width > 370 ? SizedBox(width: 30) : SizedBox(width: 0),
                // SortDropdownButton(
                //   notifyParent: notifyParent,
                // )
              ],
            ),
            // Container(color: Colors.red, height: 40, width: 100),
          ],
        ),
      ),
    );
  }
}

class SearchButton extends StatelessWidget {
  const SearchButton({
    Key key,
    this.press,
  }) : super(key: key);

  final Function press;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      // decoration: BoxDecoration(
      //   border: Border.all(
      //     color: Theme.of(context).colorScheme.secondary,
      //     width: 1,
      //   ),
      //   borderRadius: BorderRadius.circular(10),
      //   boxShadow: [
      //     BoxShadow(
      //         blurRadius: 4,
      //         offset: Offset(3, 2),
      //         color: Theme.of(context).colorScheme.secondary)
      //   ],
      // ),
      width: width < 800 ? width * 0.7 : 780,
      height: 38,
      // ignore: deprecated_member_use
      child: FlatButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Theme.of(context).colorScheme.onBackground,
        splashColor: Theme.of(context).colorScheme.secondary,
        onPressed: press,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.only(right: 7),
              margin: EdgeInsets.only(right: 10),
              decoration:
                  BoxDecoration(border: Border(right: BorderSide(width: 2))),
              alignment: Alignment.centerLeft,
              child: Icon(
                Icons.search,
                color: Theme.of(context).colorScheme.surface,
                size: 30, //MediaQuery.of(context).size.height * 0.03,
              ),
            ),
            Container(
              padding: EdgeInsets.only(right: 10),
              child: Text(
                "Pretraga",
                style: TextStyle(
                    fontSize: 18,
                    color: Theme.of(context).colorScheme.surface,
                    fontWeight: FontWeight.w600),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
