import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:kotarica/eth_models/ProductModels.dart';
// import 'package:kotarica/pages/search.dart';
// import 'package:kotarica/pages/sentReviews.dart';
// import 'package:kotarica/pages/welcome_screen.dart';
import 'package:kotarica/widgets/search/searchBody.dart';
import 'package:kotarica/widgets/search/search_app_bar.dart';
// import 'package:provider/provider.dart';

// import '../../main.dart';

/*Color text = Colors.grey[200];
Color dugme = Colors.teal[400];*/

class SortDropdownButton extends StatefulWidget {
  final Function() notifyParent;

  const SortDropdownButton({Key key, this.notifyParent}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _SortDropdownButtonState();
  }
}

class _SortDropdownButtonState extends State<SortDropdownButton> {
  int _value;
  @override
  Widget build(BuildContext context) {
    Color text = Theme.of(context).colorScheme.secondary; //Colors.grey[200];
    return Container(
      //margin: EdgeInsets.symmetric(vertical: 10.0),
      padding: EdgeInsets.only(left: 10.0, right: 10.0),
      decoration: BoxDecoration(
        //border: Border.all(color: text, width: 1),
        borderRadius: BorderRadius.circular(15),
        color: Theme.of(context).colorScheme.onBackground,
      ),
      child: DropdownButton(
          dropdownColor: Theme.of(context).colorScheme.onBackground,
          hint: Text(
            "Sortiraj po: ",
            style: TextStyle(
              fontSize: 18,
              color: Theme.of(context).colorScheme.secondary,
              fontWeight: FontWeight.bold,
              // fontSize: 26,
            ),
          ),
          underline: SizedBox(),
          icon: Icon(
            Icons.arrow_drop_down_circle,
            color: Theme.of(context).colorScheme.secondary,
          ),
          iconSize: 25,
          style: TextStyle(
            color: text,
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
          //dropdownColor: dugme,
          value: _value,
          items: [
            DropdownMenuItem(
              child: Row(
                children: [
                  Text("Naziv"),
                  Icon(Icons.arrow_upward_sharp),
                ],
              ),
              value: 1,
              onTap: () {
                SearchAppBar.lista.sort(
                    (a, b) => b.naziv.compareTo(a.naziv)); //po nazivu opadajuce
              },
            ),
            DropdownMenuItem(
              child: Row(
                children: [
                  Text("Naziv"),
                  Icon(Icons.arrow_downward),
                ],
              ),
              value: 2,
              onTap: () {
                SearchAppBar.lista.sort(
                    (a, b) => a.naziv.compareTo(b.naziv)); //po nazivu opadajuce
              },
            ),
            DropdownMenuItem(
              child: Text("Najnovije"),
              onTap: () {
                SearchAppBar.lista.sort((a, b) => b.id.compareTo(a.id));
              },
              value: 3,
            ),
            DropdownMenuItem(
              onTap: () {
                SearchAppBar.lista
                    .sort((a, b) => a.cena.compareTo(b.cena)); //najnovije
              },
              child: Row(
                children: [
                  Text("Cena"),
                  Icon(Icons.arrow_upward_sharp),
                  // IconButton(
                  //   icon: Icon(
                  //     Icons.arrow_upward_sharp,
                  //     color: text,
                  //   ),
                  // )
                ],
              ),
              value: 4,
            ),
            DropdownMenuItem(
              onTap: () {
                SearchAppBar.lista.sort(
                    (a, b) => b.cena.compareTo(a.cena)); // opadajuce po ceni
              },
              child: Row(
                children: [
                  Text("Cena"),
                  Icon(Icons.arrow_downward),
                  // IconButton(
                  //   onPressed: () {
                  //     SearchAppBar.lista.sort((a, b) =>
                  //         b.cena.compareTo(a.cena)); // opadajuce po ceni
                  //   },
                  //   icon: Icon(Icons.arrow_downward, color: text),
                  // ),
                ],
              ),
              value: 5,
            ),
            DropdownMenuItem(
                child: Row(
                  children: [
                    Text("Ocena"),
                    Icon(Icons.arrow_downward),
                  ],
                ),
                value: 6,
                onTap: () {
                  setState(() {
                    SearchAppBar.lista.sort((a, b) => b.prosecnaOcena
                        .compareTo(a.prosecnaOcena)); //po rejtingu opadajuce
                  });
                }),
            DropdownMenuItem(
                child: Row(
                  children: [
                    Text("Broj ocena"),
                    Icon(Icons.arrow_downward),
                  ],
                ),
                value: 7,
                onTap: () {
                  setState(
                    () {
                      SearchAppBar.lista
                          .sort((a, b) => b.brojOcena.compareTo(a.brojOcena));
                      //po broju ocena opadajuce
                    },
                  );
                })
          ],
          onChanged: (value) {
            setState(() {
              _value = value;
              SearchBody.refresujPriSortiranju = true;
              // ignore: unnecessary_statements
              widget.notifyParent;
              // if (!streamSubscription.isPaused)
              // controller1.add(SearchAppBar.lista);
              // Navigator.push(context,
              //     new MaterialPageRoute(builder: (context) => Search()));
            });
          }),
    );
  }
}
