// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';

// class NewCategorySelector extends StatefulWidget {
//   NewCategorySelector();

//   @override
//   _NewCategorySelectorState createState() => _NewCategorySelectorState();
// }

// class _NewCategorySelectorState extends State {
//   final _selectedCategories = List();

//   @override
//   Widget build(BuildContext context) {
//     var categories = ["pera", "mika"];
//     return Column(
//       children: [
//         Flexible(
//           child: ListView.builder(
//               itemCount: categories.length,
//               itemBuilder: (BuildContext context, int index) {
//                 return CheckboxListTile(
//                   value: _selectedCategories.contains(categories[index]),
//                   onChanged: (bool selected) {
//                     setState(() {
//                       if (selected) {
//                         _selectedCategories.add(categories[index]);
//                       } else {
//                         _selectedCategories.remove(categories[index]);
//                       }
//                     });
//                   },
//                   title: Text(categories[index]),
//                 );
//               }),
//         ),
//         RaisedButton(
//           onPressed: () {
//             Navigator.push(
//               context,
//               MaterialPageRoute(
//                   builder: (context) => SelectedCategories(
//                         categories: _selectedCategories,
//                       )),
//             );
//           },
//           child: Text("Done"),
//         )
//       ],
//     );
//   }
// }

// class SelectedCategories extends StatelessWidget {
//   final List categories;

//   const SelectedCategories({Key key, this.categories}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(title: Text("Selected categories")),
//       body: Container(
//         child: ListView.builder(
//             itemCount: categories.length,
//             itemBuilder: (BuildContext context, int index) {
//               return Padding(
//                 padding: const EdgeInsets.all(8.0),
//                 child: Text(categories[index]),
//               );
//             }),
//       ),
//     );
//   }
// }
