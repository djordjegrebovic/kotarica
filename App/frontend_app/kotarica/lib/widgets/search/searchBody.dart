import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:kotarica/AllInfoBuilder.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/utils/size_functions.dart';
import 'package:kotarica/widgets/products/productListView.dart';
import 'package:kotarica/widgets/search/search_app_bar.dart';
// import 'package:kotarica/widgets/search/sort_dropdownButton.dart';
// import 'package:numberpicker/numberpicker.dart';
import 'package:provider/provider.dart';

class SearchBody extends StatefulWidget {
  static bool refresujPriSortiranju = false;

  @override
  _SearchBodyState createState() => _SearchBodyState();
}

class _SearchBodyState extends State<SearchBody> {
  // ignore: unused_field
  var _currentValue = 1;
  var lista = SearchAppBar.lista;
  // var ind = 0;

  @override
  Widget build(BuildContext context) {
    int _numOfItems;
    Color textColor = Theme.of(context).colorScheme.secondary;
    Color iconColor = Theme.of(context).colorScheme.onPrimary;

    double width = MediaQuery.of(context).size.width;
    var listModel = Provider.of<ProductModels>(context);
    var boje = Theme.of(context).colorScheme;
    if (listModel.isLoading) {
      return Center(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 10, bottom: 20),
              child: Text(
                "Proizvodi se učitavaju",
                style: TextStyle(
                  fontSize: 20,
                  color: boje.surface,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 30),
              child: SpinKitFadingCube(size: 35, color: boje.primary),
            ),
          ],
        ),
      );
    } else {
      // SORTIRANJE LISTE
      // lista.sort(
      //     (a, b) => a.naziv.toLowerCase().compareTo(b.naziv.toLowerCase()));
      // _numOfItems = lista.length; // rastuce po nazivu
      // lista.sort(
      //     (a, b) => b.naziv.toLowerCase().compareTo(a.naziv.toLowerCase()));
      // _numOfItems = lista.length; // opadajuce po nazivu
      // lista.sort((a, b) => a.cena.compareTo(b.cena)); // rastuce po ceni
      // lista.sort((a, b) => b.cena.compareTo(a.cena)); // opadajuce po ceni
      // lista.sort((a, b) => a.kategorija
      //     .toLowerCase()
      //     .compareTo(b.kategorija.toLowerCase())); // rastuce po kategoriji
      // lista.sort((a, b) =>
      //     b.kategorija.toLowerCase().compareTo(a.kategorija.toLowerCase())); // opadajuce po kategoriji
      _numOfItems = lista.length;
      return SingleChildScrollView(
        child: Center(
          child: Container(
            width: width < 800 ? width : 800,
            color: width > 800 ? boje.onSecondary : boje.background,
            child: Column(
              children: [
                Container(
                  color: boje.secondary.withOpacity(width < 800 ? 0.7 : 0.5),
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        //margin: EdgeInsets.symmetric(vertical: 10.0),
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        height: 30,
                        decoration: BoxDecoration(
                          //border: Border.all(color: text, width: 1),
                          borderRadius: BorderRadius.circular(5),
                          color: boje.onBackground,
                        ),
                        child: DropdownButton(
                            dropdownColor: boje.onBackground,
                            hint: Text(
                              "Sortiraj po: ",
                              style: TextStyle(
                                  fontSize: 18,
                                  color: boje.secondary,
                                  fontWeight: FontWeight.bold),
                            ),
                            underline: SizedBox(),
                            icon: Icon(
                              Icons.arrow_drop_down_circle,
                              color: boje.secondary,
                            ),
                            iconSize: 25,
                            style: TextStyle(
                              color: textColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                            //dropdownColor: dugme,
                            value: AllInfos.value,
                            items: [
                              DropdownMenuItem(
                                child: Row(
                                  children: [
                                    Text("Naziv"),
                                    Icon(Icons.arrow_upward_sharp,
                                        color: iconColor),
                                  ],
                                ),
                                value: 1,
                                onTap: () {
                                  SearchAppBar.lista.sort((a, b) => b.naziv
                                      .toLowerCase()
                                      .compareTo(a.naziv
                                          .toLowerCase())); //po nazivu opadajuce
                                },
                              ),
                              DropdownMenuItem(
                                child: Row(
                                  children: [
                                    Text("Naziv"),
                                    Icon(Icons.arrow_downward,
                                        color: iconColor),
                                  ],
                                ),
                                value: 2,
                                onTap: () {
                                  SearchAppBar.lista.sort((a, b) => a.naziv
                                      .toLowerCase()
                                      .compareTo(b.naziv
                                          .toLowerCase())); //po nazivu opadajuce
                                },
                              ),
                              DropdownMenuItem(
                                child: Text("Najnovije"),
                                onTap: () {
                                  SearchAppBar.lista
                                      .sort((a, b) => b.id.compareTo(a.id));
                                },
                                value: 3,
                              ),
                              DropdownMenuItem(
                                onTap: () {
                                  SearchAppBar.lista.sort((a, b) =>
                                      a.cena.compareTo(b.cena)); //najnovije
                                },
                                child: Row(
                                  children: [
                                    Text("Cena"),
                                    Icon(Icons.arrow_upward_sharp,
                                        color: iconColor),
                                    // IconButton(
                                    //   icon: Icon(
                                    //     Icons.arrow_upward_sharp,
                                    //     color: text,
                                    //   ),
                                    // )
                                  ],
                                ),
                                value: 4,
                              ),
                              DropdownMenuItem(
                                onTap: () {
                                  SearchAppBar.lista.sort((a, b) => b.cena
                                      .compareTo(a.cena)); // opadajuce po ceni
                                },
                                child: Row(
                                  children: [
                                    Text("Cena"),
                                    Icon(Icons.arrow_downward,
                                        color: iconColor),
                                    // IconButton(
                                    //   onPressed: () {
                                    //     SearchAppBar.lista.sort((a, b) =>
                                    //         b.cena.compareTo(a.cena)); // opadajuce po ceni
                                    //   },
                                    //   icon: Icon(Icons.arrow_downward, color: text),
                                    // ),
                                  ],
                                ),
                                value: 5,
                              ),
                              DropdownMenuItem(
                                  child: Row(
                                    children: [
                                      Text("Ocena"),
                                      Icon(Icons.arrow_downward,
                                          color: iconColor),
                                    ],
                                  ),
                                  value: 6,
                                  onTap: () {
                                    setState(() {
                                      SearchAppBar.lista.sort((a, b) =>
                                          b.prosecnaOcena.compareTo(a
                                              .prosecnaOcena)); //po rejtingu opadajuce
                                    });
                                  }),
                              DropdownMenuItem(
                                  child: Row(
                                    children: [
                                      Text("Broj ocena"),
                                      Icon(Icons.arrow_downward,
                                          color: iconColor),
                                    ],
                                  ),
                                  value: 7,
                                  onTap: () {
                                    setState(
                                      () {
                                        SearchAppBar.lista.sort((a, b) =>
                                            b.brojOcena.compareTo(a.brojOcena));
                                        //po broju ocena opadajuce
                                      },
                                    );
                                  })
                            ],
                            onChanged: (value) {
                              setState(() {
                                // print("value");
                                AllInfos.value = value;
                                SearchBody.refresujPriSortiranju = true;
                                // ignore: unnecessary_statements
                                // if (!streamSubscription.isPaused)
                                // controller1.add(SearchAppBar.lista);
                                // Navigator.push(context,
                                //     new MaterialPageRoute(builder: (context) => Search()));
                              });
                            }),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 15),
                Text(
                    "Ukupan broj oglasa : " +
                        ProductModels.products.length.toString(),
                    style: TextStyle(fontWeight: FontWeight.w600)),
                SizedBox(height: 10),
                Container(
                  height: calculateHeight(context, _numOfItems, 0),
                  child: ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: lista.length,
                    itemBuilder: (BuildContext context, int index) => Padding(
                      padding: EdgeInsets.only(left: 12, right: 12, bottom: 12),
                      child: ProductList(lista[index], listModel, true, true),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
  }
}
