// import 'package:flutter/material.dart';
// import 'package:kotarica/eth_models/ProductModels.dart';
// import 'package:kotarica/widgets/products/productListView.dart';

// import 'newCategorySelector.dart';

// class SearchList extends StatefulWidget {
//   SearchList({Key key}) : super(key: key);
//   @override
//   _SearchListState createState() => new _SearchListState();
// }

// class _SearchListState extends State<SearchList> {
//   Widget appBarTitle = new Text(
//     "Search",
//     style: new TextStyle(color: Colors.white),
//   );
//   Icon actionIcon = new Icon(
//     Icons.search,
//     color: Colors.white,
//   );
//   final key = new GlobalKey<ScaffoldState>();
//   final TextEditingController _searchQuery = new TextEditingController();
//   List<Product> _list;
//   List<ChildItem> _listaPretraga;
//   bool _IsSearching;
//   String _searchText = "";
//   _SearchListState() {
//     _searchQuery.addListener(() {
//       if (_searchQuery.text.isEmpty) {
//         setState(() {
//           _IsSearching = false;
//           _searchText = "";
//         });
//       } else {
//         setState(() {
//           _IsSearching = true;
//           _searchText = _searchQuery.text;
//         });
//       }
//     });
//   }

//   @override
//   void initState() {
//     super.initState();
//     _IsSearching = false;
//     init();
//   }

//   void init() {
//     _list = ProductModels.products;
//     //_listaPretraga = _buildSearchList();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return new Scaffold(
//       key: key,
//       appBar: buildBar(context),
//       body: new ListView(
//         padding: new EdgeInsets.symmetric(vertical: 8.0),
//         children: _IsSearching ? _buildSearchList() : _buildList(),
//       ),
//       // body: ListView.builder(
//       //   physics: NeverScrollableScrollPhysics(),
//       //   itemCount: _list.length,
//       //   itemBuilder: (BuildContext context, int index) => Padding(
//       //     padding: EdgeInsets.all(16),
//       //     child: _IsSearching
//       //         ? _buildSearchList()
//       //         : ProductList(0.14, _list[index], true),
//       //   ),
//       // ),
//     );
//   }

//   List<ChildItem> _buildList() {
//     return _list
//         .map((contact) => new ChildItem(contact.naziv, contact.id))
//         .toList();
//   }

//   List<ChildItem> _buildSearchList() {
//     if (_searchText.isEmpty) {
//       return _list
//           .map((contact) => new ChildItem(contact.naziv, contact.id))
//           .toList();
//     } else {
//       List<Product> _searchList = List();
//       for (int i = 0; i < _list.length; i++) {
//         Product pr = _list.elementAt(i);
//         if (pr.naziv.toLowerCase().contains(_searchText.toLowerCase())) {
//           _searchList.add(pr);
//         }
//       }
//       return _searchList
//           .map((contact) => new ChildItem(contact.naziv, contact.id))
//           .toList();
//     }
//   }

//   Widget buildBar(BuildContext context) {
//     return new AppBar(centerTitle: true, title: appBarTitle, actions: <Widget>[
//       new IconButton(
//         icon: actionIcon,
//         onPressed: () {
//           setState(() {
//             if (this.actionIcon.icon == Icons.search) {
//               this.actionIcon = new Icon(
//                 Icons.close,
//                 color: Colors.white,
//               );
//               this.appBarTitle = new TextField(
//                 controller: _searchQuery,
//                 style: new TextStyle(
//                   color: Colors.white,
//                 ),
//                 decoration: new InputDecoration(
//                     prefixIcon: new Icon(Icons.search, color: Colors.white),
//                     hintText: "Search...",
//                     hintStyle: new TextStyle(color: Colors.white)),
//               );
//               _handleSearchStart();
//             } else {
//               _handleSearchEnd();
//             }
//           });
//         },
//       ),
//     ]);
//   }

//   void _handleSearchStart() {
//     setState(() {
//       _IsSearching = true;
//     });
//   }

//   void _handleSearchEnd() {
//     setState(() {
//       this.actionIcon = new Icon(
//         Icons.search,
//         color: Colors.white,
//       );
//       this.appBarTitle = new Text(
//         "Search",
//         style: new TextStyle(color: Colors.white),
//       );
//       _IsSearching = false;
//       _searchQuery.clear();
//     });
//   }
// }

// class ChildItem extends StatelessWidget {
//   final String name;
//   final BigInt id;
//   ChildItem(this.name, this.id);
//   @override
//   Widget build(BuildContext context) {
//     return new ListTile(
//         title: new Text(this.name),
//         onTap: () => {
//               print("Kliknuo si na " +
//                   this.name +
//                   " sa id " +
//                   this.id.toString()),
//             });
//   }
// }
