import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/utils/const.dart';
import 'package:kotarica/widgets/productDetail/product_details.dart';
import 'package:kotarica/widgets/search/search_app_bar.dart';

class DataSearch extends SearchDelegate<String> {
  String result;

  @override
  String get searchFieldLabel => "Pretražite po nazivu ili kategoriji";
  @override
  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
          icon: Icon(Icons.clear, color: Colors.teal),
          onPressed: () {
            if (query == '') close(context, result);
            query = '';
            showSuggestions(context);
          })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
        icon: AnimatedIcon(
            icon: AnimatedIcons.menu_arrow,
            color: Colors.teal,
            progress: transitionAnimation),
        onPressed: () {
          close(context, result);
        });
  }

  @override
  Widget buildResults(BuildContext context) {
    final suggestionList = query.isEmpty
        ? SearchAppBar.lista
        : SearchAppBar.lista
            .where((product) =>
                product.naziv.toLowerCase().contains(query.toLowerCase()) ||
                product.kategorija.toLowerCase().contains(query.toLowerCase()))
            .toList();
    double width = MediaQuery.of(context).size.width;
    var boje = Theme.of(context).colorScheme;
    return SingleChildScrollView(
      child: Center(
        child: Container(
          width: width < 800 ? width : 800,
          color:
              width > 800 ? boje.onSecondary.withOpacity(0.4) : boje.background,
          child: ListView.builder(
            shrinkWrap: true,
            itemCount: suggestionList.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                  onTap: () {
                    // showResults(context);
                    result = suggestionList[index].naziv;
                    close(context, result);
                    Navigator.pushNamed(context, ProductDetails.routeName,
                        arguments: ProductDetailsArguments(
                            product: suggestionList[index]));
                    // Navigator.pushNamed(
                    //   context,
                    //   ProductDetails.routeName,
                    //   arguments:
                    //       ProductDetailsArguments(product: suggestionList[index]),
                    // );
                  },
                  child: SearchItem(
                      query, index, SearchAppBar.lista, Colors.teal[600]));
            },
          ),
        ),
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestionList = query.isEmpty
        ? SearchAppBar.lista
        : SearchAppBar.lista
            .where((product) =>
                product.naziv.toLowerCase().contains(query.toLowerCase()) ||
                product.kategorija.toLowerCase().contains(query.toLowerCase()))
            .toList();
    double width = MediaQuery.of(context).size.width;
    var boje = Theme.of(context).colorScheme;
    return SingleChildScrollView(
      child: Center(
        child: Container(
          width: width < 800 ? width : 800,
          color:
              width > 800 ? boje.onSecondary.withOpacity(0.4) : boje.background,
          child: ListView.builder(
            shrinkWrap: true,
            itemCount: suggestionList.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                  onTap: () {
                    query = suggestionList[index].naziv;
                    showResults(context);
                  },
                  child: Container(
                      // color: Colors.red,
                      // width: width < 800 ? width : 800,
                      child: SearchItem(
                          query, index, SearchAppBar.lista, Colors.teal[400])));
            },
          ),
        ),
      ),
    );
  }
}

// Izgled oglasa koje pretrazujemo
// ignore: must_be_immutable
class SearchItem extends StatelessWidget {
  // ignore: non_constant_identifier_names
  String query_;
  List<Product> lista;
  // ignore: non_constant_identifier_names
  int index_;
  Color color;
  SearchItem(String query, int index, List<Product> lista, Color color) {
    this.query_ = query;
    this.index_ = index;
    this.lista = lista;
    this.color = color;
  }

  @override
  Widget build(BuildContext context) {
    final suggestionList = query_.isEmpty
        ? lista
        : lista
            .where((product) =>
                product.naziv.toLowerCase().contains(query_.toLowerCase()) ||
                product.kategorija.toLowerCase().contains(query_.toLowerCase()))
            .toList();
    // search.where((food) {
    //      final queryLower = query.toLowerCase();
    //      final foodLower = food.toLowerCase();
    //      return foodLower.contains(queryLower);
    // }).toList();
    double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height;
    return Container(
      width: width < 800 ? width : 800,
      padding: EdgeInsets.only(left: 12.0, top: 8, right: 12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            children: [
              SizedBox(
                // Siva pozadina sa slikom proizvoda
                width: width < 700 ? width * 0.18 : 130,
                child: AspectRatio(
                  aspectRatio: 1,
                  child: Container(
                      padding: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                        color: Theme.of(context).colorScheme.onBackground,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(15),
                        child: suggestionList[index_].hesh == ""
                            ? Image.asset(suggestionList[index_].images[0])
                            : Image(
                                image: NetworkImage(Const.url +
                                    "/api/Image/" +
                                    suggestionList[index_].hesh),
                                fit: BoxFit.fill,
                              ),
                      )),
                ),
              ),
              SizedBox(width: width < 600 ? width * 0.03 : 10),
              Container(
                // color: Colors.green,
                width: width < 800 ? width * 0.5 : 400,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: width * 0.50,
                      child: RichText(
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        text: TextSpan(
                          text: suggestionList[index_].naziv,
                          // .substring(0, query_.length),
                          style: TextStyle(
                            color: color,
                            fontWeight: FontWeight.bold,
                            fontSize: width < 450 ? 18 : 22,
                          ),
                          // children: [
                          //   TextSpan(
                          //       text: suggestionList[index_]
                          //           .naziv
                          //           .substring(query_.length),
                          //       style: TextStyle(
                          //           color: Colors.teal[300],
                          //           fontSize: width < 500 ? 18 : 22)),
                          // ],
                        ),
                      ),
                    ),
                    SizedBox(height: width < 450 ? 8 : 15),
                    Container(
                      padding: EdgeInsets.only(right: 13.0),
                      width: width * 0.50,
                      child: Text(
                        suggestionList[index_].kategorija,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        softWrap: false,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: width < 500 ? 16 : 20,
                            color: Theme.of(context).colorScheme.surface,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Container(
            width: width < 800 ? width * 0.2 : 180,
            // color: Colors.red,
            child: Text(
              "${suggestionList[index_].cena} RSD",
              textAlign: TextAlign.right,
              style: TextStyle(
                fontSize: width > 400
                    ? width < 700
                        ? 20
                        : 28
                    : 18,
                fontWeight: FontWeight.bold,
                color: Boje.rsd,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
