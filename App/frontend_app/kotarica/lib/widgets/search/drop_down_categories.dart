import 'package:flutter/material.dart';

// DEO SAM BIRANJEM KATEGORIJA

/*Color selected = Colors.white;
Color unselected = Colors.grey[200];
Color buttonSelected = Colors.teal[400];
Color buttonUnseleceted = Colors.teal[600];*/

// ignore: must_be_immutable
class DropDownCategories extends StatefulWidget {
  List<String> categoriesList;
  final String title;
  final List<bool> initialState;
  DropDownCategories(this.categoriesList, this.title, this.initialState);

  @override
  _DropDownCategoriesState createState() => _DropDownCategoriesState();
}

class _DropDownCategoriesState extends State<DropDownCategories> {
  List<String> categoriesListCopy = [];
  List<String> filterSearchTerms(String filter) {
    if (filter != null && filter.isNotEmpty) {
      // Reversed because we want the last added items to appear first in the UI
      return widget.categoriesList
          .where((term) => term.startsWith(filter))
          .toList();
    } else {
      return categoriesListCopy.toList();
    }
  }

  Widget build(BuildContext context) {
    categoriesListCopy = widget.categoriesList;
    return OutlinedButton(
      child: Text(widget.title, style: TextStyle(color: Colors.teal)),
      onPressed: () {
        return showDialog(
            context: context,
            builder: (context) {
              return StatefulBuilder(builder: (context, setState) {
                return AlertDialog(
                    //scrollable: true,
                    title: TextField(
                      onChanged: (String value) {
                        setState(() {
                          widget.categoriesList = filterSearchTerms(value);
                        });
                      },
                    ),
                    content: ListView.builder(
                        padding: const EdgeInsets.all(8),
                        itemCount: widget.categoriesList.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            height: 50,
                            child: CheckboxListTile(
                              title: Text(widget.categoriesList[index]),
                              value: widget.initialState[index],
                              onChanged: (bool newValue) {
                                setState(() {
                                  widget.initialState[index] = newValue;
                                });
                              },
                            ),
                          );
                        }));
              });
            });
      },
    );
  }
}

// ListView.builder(
//   padding: const EdgeInsets.all(8),
//   itemCount: entries.length,
//   itemBuilder: (BuildContext context, int index) {
//     return Container(
//       height: 50,
//       color: Colors.amber[colorCodes[index]],
//       child: Center(child: Text('Entry ${entries[index]}')),
//     );
//   }
// );

// class UserModel {
//   final String id;
//   final DateTime createdAt;
//   final String name;
//   final String avatar;

//   UserModel({this.id, this.createdAt, this.name, this.avatar});

//   factory UserModel.fromJson(Map<String, dynamic> json) {
//     if (json == null) return null;
//     return UserModel(
//       id: json["id"],
//       createdAt:
//           json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
//       name: json["name"],
//       avatar: json["avatar"],
//     );
//   }

//   static List<UserModel> fromJsonList(List list) {
//     if (list == null) return null;
//     return list.map((item) => UserModel.fromJson(item)).toList();
//   }

//   ///this method will prevent the override of toString
//   String userAsString() {
//     return '#${this.id} ${this.name}';
//   }

//   ///this method will prevent the override of toString
//   bool userFilterByCreationDate(String filter) {
//     return this?.createdAt?.toString()?.contains(filter);
//   }

//   ///custom comparing function to check if two users are equal
//   bool isEqual(UserModel model) {
//     return this?.id == model?.id;
//   }

//   @override
//   String toString() => name;
// }
