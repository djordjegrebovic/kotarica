import 'package:flutter/material.dart';

import 'complete_profile_form.dart';

class CompleteProfileBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 35),
                Text(
                  "Završite registraciju",
                  style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).colorScheme.onPrimary
                      // height: 1.5,
                      ),
                ),
                SizedBox(height: 5),
                Text(
                  "Popunite sledeća polja sa \nvašim lični podacima",
                  style:
                      TextStyle(color: Theme.of(context).colorScheme.onPrimary),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 25),
                CompleteProfileForm(),
                SizedBox(height: 15),
                Text(
                  "Nastavljajući sa registracijom, prihvatate \nnaša Pravila i uslove korišćenja",
                  textAlign: TextAlign.center,
                  style:
                      TextStyle(color: Theme.of(context).colorScheme.onPrimary),
                ),
                SizedBox(height: 20),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
