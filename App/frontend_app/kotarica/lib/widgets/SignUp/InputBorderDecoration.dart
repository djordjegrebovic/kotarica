import 'package:flutter/material.dart';
import 'package:kotarica/utils/boje.dart';

InputDecoration buildInputDecoration(
    String label, String hint, Icon icon, BuildContext context) {
  return InputDecoration(
    labelText: label,
    labelStyle: TextStyle(
        fontSize: 17,
        color: Color.fromARGB(255, 0, 122, 126),
        //Color.fromARGB(255, 0, 141, 146),
        fontWeight: FontWeight.w600),
    hintText: hint,
    hintStyle: TextStyle(color: Theme.of(context).colorScheme.secondary),
    // enabled: false,
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(28),
      borderSide:
          BorderSide(color: Color.fromARGB(255, 0, 141, 146), width: 1.3),
      gapPadding: 10,
    ),
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(28),
      borderSide: BorderSide(
          color: Theme.of(context).colorScheme.onSurface, width: 1.5),
      gapPadding: 10,
    ),
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(28),
      borderSide: BorderSide(color: Color.fromARGB(255, 0, 118, 116), width: 2),
      gapPadding: 10,
    ),
    errorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(28),
      borderSide:
          BorderSide(color: Theme.of(context).colorScheme.error, width: 1.5),
      gapPadding: 10,
    ),
    floatingLabelBehavior: FloatingLabelBehavior.always,
    suffixIcon: Padding(
      padding: const EdgeInsets.all(20.0),
      child: icon,
    ),
  );
}
