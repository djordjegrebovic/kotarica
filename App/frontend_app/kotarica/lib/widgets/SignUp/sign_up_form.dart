import 'package:flutter/material.dart';
import 'package:kotarica/AllInfoBuilder.dart';
import 'package:kotarica/eth_models/user_loginModels.dart';
import 'package:kotarica/widgets/LoginSingin/default_button.dart';
import 'package:kotarica/widgets/LoginSingin/form_error.dart';
// import 'package:kotarica/widgets/LoginSingin/sign_in_body.dart';
// import 'package:kotarica/widgets/LoginSingin/sign_in_screen.dart';
import 'package:kotarica/widgets/SignUp/InputBorderDecoration.dart';
import 'package:kotarica/widgets/SignUp/complete_profile_screen.dart';
// import 'package:kotarica/widgets/SignUp/form_fields.dart';
import 'package:provider/provider.dart';

// import '../../main.dart';
// import '../../redirektNaLogin.dart';

class SignUpForm extends StatefulWidget {
  static String savedPassword;
  static String savedUsername;
  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final _formKey = GlobalKey<FormState>();

  String email;
  String password;
  // ignore: non_constant_identifier_names
  String confirm_password;
  bool agree = false;
  final List<String> errors = [];
  double iconSize = 25;
  Color iconColor; // = Color(0xFF757575);

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  // ignore: non_constant_identifier_names
  final TextEditingController confirm_password_controller =
      TextEditingController();
  // ignore: non_constant_identifier_names
  final TextEditingController username_controller = TextEditingController();
  // ignore: non_constant_identifier_names
  final TextEditingController email_controller = TextEditingController();
  // ignore: non_constant_identifier_names
  final TextEditingController password_controller = TextEditingController();
  // ignore: non_constant_identifier_names
  final TextEditingController private_key_controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // var listModel = Provider.of<UserLoginModel>(context);
    double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height;
    return Consumer<UserLoginModel>(
        builder: (context, provider, child) => Form(
              key: _formKey,
              child: Container(
                width: width < 500 ? width : 500,
                child: Column(
                  children: [
                    buildUsernameFormField(),
                    SizedBox(height: 30),
                    buildPasswordFormField(),
                    SizedBox(height: 30),
                    buildConfirmPassFormField(),
                    SizedBox(height: 30),
                    buildPrivateKeyFormField(),
                    SizedBox(height: 30),
                    // Row(
                    //   children: [
                    //     Checkbox(
                    //       value: agree,
                    //       activeColor: Color(0xFFFF7643),
                    //       onChanged: (value) {
                    //         setState(() {
                    //           agree = value;
                    //         });
                    //       },
                    //     ),
                    //     Text("Prihvatam Pravila i uslove koriscenja"),
                    //   ],
                    // ),
                    FormError(
                        errors: errors,
                        error_color: Theme.of(context).colorScheme.error),
                    SizedBox(height: 30),
                    DefaultButton(
                      text: "Nastavi",
                      width: width < 500 ? width : 500,
                      press: () {
                        if (_formKey.currentState.validate()) {
                          _formKey.currentState.save();

                          //ispod setujem privatnji kljuc koji smo uneli pri registraciji
                          AllInfos.privateKey = private_key_controller.text;

                          if ((provider.postoji(username_controller.text,
                                  password_controller.text) ==
                              null)) {
                            SignUpForm.savedPassword = password_controller.text;
                            SignUpForm.savedUsername = username_controller.text;
                            provider.addUser(
                                username_controller.text,
                                password_controller.text,
                                private_key_controller.text);
                            // Navigator.pushNamed(
                            //     context, CompleteProfileScreen.routeName);

                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      CompleteProfileScreen()),
                            );
                          } else {
                            provider.displayDialog(context, "Desila se greška!",
                                "Registracija nije uspešna");
                          }
                        }
                      },
                    ),
                  ],
                ),
              ),
            ));
  }

  TextFormField buildConfirmPassFormField() {
    return TextFormField(
      controller: confirm_password_controller,
      obscureText: true,
      onSaved: (newValue) => confirm_password = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPassNullErrorConfirm);
        }
        if (password == confirm_password) {
          removeError(error: kMatchPassError);
        }
        confirm_password = value;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPassNullErrorConfirm);
          return "";
        } else if ((password != value)) {
          addError(error: kMatchPassError);
          return "";
        }
        return null;
      },
      decoration: buildInputDecoration(
          "Potvrda lozinke",
          "Ponovite vašu lozinku",
          Icon(Icons.lock_outline, size: iconSize, color: iconColor),
          context),
    );
  }

  TextFormField buildPasswordFormField() {
    return TextFormField(
      controller: password_controller,
      obscureText: true,
      onSaved: (newValue) => password = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPassNullError);
        }
        if (value.length >= 8) {
          removeError(error: kShortPassError);
        }
        password = value;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPassNullError);
          return "";
        } else if (value.length < 8) {
          addError(error: kShortPassError);
          return "";
        }
        return null;
      },
      decoration: buildInputDecoration("Lozinka", "Unesite minimum 8 karaktera",
          Icon(Icons.lock_outline, size: iconSize, color: iconColor), context),
    );
  }

  TextFormField buildEmailFormField() {
    return TextFormField(
      controller: email_controller,
      keyboardType: TextInputType.emailAddress,
      onSaved: (newValue) => email = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kEmailNullError);
        }
        if (emailValidatorRegExp.hasMatch(value)) {
          removeError(error: kInvalidEmailError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kEmailNullError);
          return "";
        } else if (!emailValidatorRegExp.hasMatch(value)) {
          addError(error: kInvalidEmailError);
          return "";
        }
        return null;
      },
      decoration: buildInputDecoration("Email", "korisnik123@gmail.com",
          Icon(Icons.mail_outline, size: iconSize, color: iconColor), context),
    );
  }

  TextFormField buildPrivateKeyFormField() {
    return TextFormField(
      controller: private_key_controller,
      obscureText: true,
      onSaved: (newValue) => password = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPrivateKeyNullError);
        }
        if (privateKeyValidatorRegExp.hasMatch(value)) {
          removeError(error: kInvalidPrivateKey);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPrivateKeyNullError);
          return "";
        } else if (!privateKeyValidatorRegExp.hasMatch(value)) {
          addError(error: kInvalidPrivateKey);
          return "";
        }

        return null;
      },
      decoration: buildInputDecoration(
          "Privatni ključ",
          "Unesite privatni ključ",
          Icon(Icons.privacy_tip_outlined, size: iconSize, color: iconColor),
          context),
    );
  }

  TextFormField buildUsernameFormField() {
    return TextFormField(
      controller: username_controller,
      autofocus: true,
      keyboardType: TextInputType.text,
      onSaved: (newValue) => email = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kUsernameNullError);
        }
        if (usernameValidatorRegExp.hasMatch(value)) {
          removeError(error: kUsernameWhitespacesError);
        }
        // if (value.length >= 4 && value.length <= 32) {
        //   removeError(error: kWrongLengthUsernameError);
        // }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kUsernameNullError);
          return "";
        }
        // if ((value.length < 4 || value.length > 32) &&
        //     !usernameValidatorRegExp.hasMatch(value)) {
        //   addError(error: kWrongLengthUsernameError);
        //   addError(error: kUsernameWhitespacesError);

        //   return "";
        // } else if (value.length < 4 || value.length > 32) {
        //   addError(error: kWrongLengthUsernameError);
        //   return "";
        // }
        else if (!usernameValidatorRegExp.hasMatch(value)) {
          addError(error: kUsernameWhitespacesError);
          return "";
        }

        return null;
      },
      decoration: buildInputDecoration("Korisničko ime", "korisnik_83",
          Icon(Icons.person_pin, size: iconSize, color: iconColor), context),
    );
  }
}
