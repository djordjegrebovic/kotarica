import 'package:flutter/material.dart';
import 'package:kotarica/eth_models/user_loginModels.dart';
// import 'package:kotarica/pages/welcome_screen.dart';
import 'package:kotarica/widgets/LoginSingin/default_button.dart';
// import 'package:kotarica/widgets/LoginSingin/default_button.dart';
import 'package:kotarica/widgets/LoginSingin/form_error.dart';
import 'package:kotarica/widgets/LoginSingin/registration_success_screen.dart';
import 'package:kotarica/widgets/SignUp/InputBorderDecoration.dart';
import 'package:kotarica/widgets/SignUp/sign_up_form.dart';
// import 'package:kotarica/widgets/cart/default_button.dart';
import 'package:provider/provider.dart';

import '../../main.dart';
// import 'package:kotarica/widgets/SignUp/otp_screen.dart';

class CompleteProfileForm extends StatefulWidget {
  @override
  _CompleteProfileFormState createState() => _CompleteProfileFormState();
}

class _CompleteProfileFormState extends State<CompleteProfileForm> {
  final _formKey = GlobalKey<FormState>();
  final List<String> errors = [];
  String firstName;
  String lastName;
  String phoneNumber;
  String username;
  String address;
  String city;

  double iconSize = 26;
  Color iconColor;

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  // ignore: non_constant_identifier_names
  final TextEditingController address_controller = TextEditingController();
  // ignore: non_constant_identifier_names
  final TextEditingController phone_number_controller = TextEditingController();
  // ignore: non_constant_identifier_names
  final TextEditingController last_name_controller = TextEditingController();
  // ignore: non_constant_identifier_names
  final TextEditingController first_name_controller = TextEditingController();
  // ignore: non_constant_identifier_names
  final TextEditingController email_controller = TextEditingController();
  // ignore: non_constant_identifier_names
  final TextEditingController city_controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var listModel = Provider.of<UserLoginModel>(context);
    double width = MediaQuery.of(context).size.width;

    return
        // Consumer<UserLoginModel>(
        //     builder: (context, provider, child) =>
        Form(
      key: _formKey,
      child: Container(
        width: width < 500 ? width : 500,
        child: Column(
          children: [
            buildFirstNameFormField(),
            SizedBox(height: 30),
            buildLastNameFormField(),
            SizedBox(height: 30),
            buildEmailFormField(),
            SizedBox(height: 30),
            buildPhoneNumberFormField(),
            SizedBox(height: 30),
            // buildCityFormField(),
            // SizedBox(height: 30),
            buildAddressFormField(),
            FormError(
                errors: errors,
                error_color: Theme.of(context).colorScheme.error),
            SizedBox(height: 40),
            DefaultButton(
              text: "Nastavi",
              width: width < 500 ? width : 500,
              press: () {
                if (_formKey.currentState.validate()) {
                  // Navigator.pushNamed(context, OtpScreen.routeName);

                  // print(first_name_controller.text +
                  //     " " +
                  //     last_name_controller.text);
                  var idd = listModel.vratiID(
                      SignUpForm.savedUsername, SignUpForm.savedPassword);

                  if (idd != BigInt.zero) {
                    listModel.addUserData(
                        "",
                        first_name_controller.text,
                        last_name_controller.text,
                        phone_number_controller.text,
                        address_controller.text,
                        email_controller.text,
                        idd);

                    // print("DODAO SAM USER DATA");

                    // Navigator.pushNamed(
                    //     context, RegistrationSuccessScreen.routeName);
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => RegistrationSuccessScreen()));
                  } else {
                    //   print("NISAM DODAO SAM USER DATA");
                    Navigator.pushNamed(context, MyApp.routeName);

                    // Navigator.push(context,
                    //     new MaterialPageRoute(builder: (context) => MyApp()));

                  }
                }
              },
            ),
          ],
        ),
      ),
      // ),
    );
  }

  TextFormField buildCityFormField() {
    return TextFormField(
      controller: city_controller,
      onSaved: (newValue) => city = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kCityNullError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kCityNullError);
          return "";
        }
        return null;
      },
      decoration: buildInputDecoration("Grad", "Unesite grad u kojem živite",
          Icon(Icons.location_city, size: iconSize, color: iconColor), context),
    );
  }

  TextFormField buildAddressFormField() {
    return TextFormField(
      controller: address_controller,
      onSaved: (newValue) => address = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kAddressNullError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kAddressNullError);
          return "";
        }
        return null;
      },
      decoration: buildInputDecoration(
          "Adresa i grad",
          "Unesite ulicu, broj i grad",
          Icon(Icons.location_on_outlined, size: iconSize, color: iconColor),
          context),
    );
  }

  TextFormField buildPhoneNumberFormField() {
    return TextFormField(
      controller: phone_number_controller,
      keyboardType: TextInputType.phone,
      onSaved: (newValue) => phoneNumber = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPhoneNumberNullError);
        }
        if (phoneNumberValidatorRegExp.hasMatch(value)) {
          removeError(error: kInvalidPhoneNumber);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPhoneNumberNullError);
          return "";
        } else if (!phoneNumberValidatorRegExp.hasMatch(value)) {
          addError(error: kInvalidPhoneNumber);
          return "";
        }
        return null;
      },
      decoration: buildInputDecoration(
          "Mobilni telefon",
          "06* *******",
          Icon(Icons.phone_iphone_outlined, size: iconSize, color: iconColor),
          context),
    );
  }

  TextFormField buildLastNameFormField() {
    return TextFormField(
      controller: last_name_controller,
      onSaved: (newValue) => lastName = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kNamelNullError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kNamelNullError);
          return "";
        }
        return null;
      },
      decoration: buildInputDecoration(
          "Prezime",
          "Unesite vaše prezime",
          Icon(Icons.person_outline, size: iconSize, color: iconColor),
          context),
    );
  }

  TextFormField buildFirstNameFormField() {
    return TextFormField(
      controller: first_name_controller,
      autofocus: true,
      onSaved: (newValue) => firstName = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kNamelNullError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kNamelNullError);
          return "";
        }
        return null;
      },
      decoration: buildInputDecoration(
          "Ime",
          "Unesite vaše ime",
          Icon(Icons.person_outline, size: iconSize, color: iconColor),
          context),
    );
  }

  TextFormField buildEmailFormField() {
    return TextFormField(
      controller: email_controller,
      keyboardType: TextInputType.emailAddress,
      onSaved: (newValue) => username = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kEmailNullError);
        }
        if (emailValidatorRegExp.hasMatch(value)) {
          removeError(error: kInvalidEmailError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kEmailNullError);
          return "";
        } else if (!emailValidatorRegExp.hasMatch(value)) {
          addError(error: kInvalidEmailError);
          return "";
        }
        return null;
      },
      decoration: buildInputDecoration("Email", "korisnik123@gmail.com",
          Icon(Icons.mail_outline, size: iconSize, color: iconColor), context),
    );
  }
}
