import 'package:flutter/material.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/utils/boje.dart';
import 'complete_profile_body.dart';

class CompleteProfileScreen extends StatelessWidget {
  static String routeName = "/kompletiranje_registracije";

  @override
  Widget build(BuildContext context) {
    // Color color = Theme.of(context).colorScheme.surface;

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Boje.mainText),
          onPressed: () {
            GetInfo.korpa = false;
            Navigator.pop(context);
            // Navigator.pop();
          },
        ),
        title: Text(
          'Registracija',
          style: TextStyle(color: Boje.mainText, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      body: SafeArea(child: CompleteProfileBody()),
    );
  }
}
