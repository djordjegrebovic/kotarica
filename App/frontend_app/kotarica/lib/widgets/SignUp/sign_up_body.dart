import 'package:flutter/material.dart';
// import 'package:kotarica/redirektNaLogin.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/widgets/LoginSingin/sign_in_screen.dart';
import 'package:kotarica/widgets/SignUp/sign_up_form.dart';

class SignUpBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 35),
                // 4%
                Text(
                  "Registrujte nalog",
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                    color: Theme.of(context).colorScheme.onPrimary,
                    // height: 1.5,
                  ),
                ),
                SizedBox(height: 5),
                Text("Unesite svoje podatke",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Theme.of(context).colorScheme.onPrimary)),
                SizedBox(height: 15),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Već imate nalog? ",
                      style: TextStyle(
                          fontSize: 16,
                          color: Theme.of(context).colorScheme.onPrimary),
                    ),
                    GestureDetector(
                      onTap: () {
                        // Navigator.pushNamed(context, SignInScreen.routeName);
                        // Navigator.pushNamed(context, RedirektNaLogin.routeName);

                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SignInScreen()),
                        );
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 2, horizontal: 6),
                        child: Text(
                          "Prijavite se",
                          style: TextStyle(
                            fontSize: 18,
                            color: Boje.loginRegisterButton,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 15),
                SignUpForm(),
                SizedBox(height: 15),
                Text(
                    'Registracijom na ovaj način prihvatate \nPravila i uslove korišćenja',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Theme.of(context).colorScheme.onPrimary)),
                SizedBox(height: 20),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
