import 'package:flutter/material.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/widgets/SignUp/sign_up_body.dart';

class SignUpScreen extends StatelessWidget {
  static String routeName = '/registracija';
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).colorScheme.surface;

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Boje.mainText),
          onPressed: () {
            GetInfo.korpa = false;
            Navigator.pop(context);
            // Navigator.pop();
          },
        ),
        centerTitle: true,
        title: Text(
          "Registracija",
          style: TextStyle(color: Boje.mainText, fontWeight: FontWeight.bold),
        ),
      ),
      body: SafeArea(child: SignUpBody()),
    );
  }
}
