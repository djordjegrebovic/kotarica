// import 'package:flutter/material.dart';
// import 'package:kotarica/widgets/LoginSingin/default_button.dart';
// import 'package:kotarica/widgets/LoginSingin/form_error.dart';
// import 'package:kotarica/widgets/SignUp/InputBorderDecoration.dart';
// import 'package:kotarica/widgets/SignUp/sign_up_screen.dart';

// class ForgotPasswordBody extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return SizedBox(
//       width: double.infinity,
//       child: SingleChildScrollView(
//         child: Padding(
//           padding: EdgeInsets.symmetric(horizontal: 20),
//           child: Column(
//             children: [
//               SizedBox(height: MediaQuery.of(context).size.height * 0.04),
//               Text(
//                 "Zazboravili ste lozinku?",
//                 style: TextStyle(
//                   fontSize: 28,
//                   color: Colors.black,
//                   fontWeight: FontWeight.bold,
//                 ),
//               ),
//               Text(
//                 "Molimo vas unesite vasu email adresu i poslacemo \nvam kod na email pomocu kojeg cete \ndobiti pristup vasem nalogu",
//                 textAlign: TextAlign.center,
//               ),
//               SizedBox(height: MediaQuery.of(context).size.height * 0.1),
//               ForgotPassForm(),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

// class ForgotPassForm extends StatefulWidget {
//   @override
//   _ForgotPassFormState createState() => _ForgotPassFormState();
// }

// class _ForgotPassFormState extends State<ForgotPassForm> {
//   final _formKey = GlobalKey<FormState>();
//   List<String> errors = [];
//   String email;
//   double iconSize = 26;
//   Color iconColor;
//   @override
//   Widget build(BuildContext context) {
//     final TextEditingController emailController = TextEditingController();
//     return Form(
//       key: _formKey,
//       child: Column(
//         children: [
//           TextFormField(
//             controller: emailController,
//             keyboardType: TextInputType.emailAddress,
//             onSaved: (newValue) => email = newValue,
//             onChanged: (value) {
//               if (value.isNotEmpty && errors.contains(kEmailNullError)) {
//                 setState(() {
//                   errors.remove(kEmailNullError);
//                 });
//               } else if (emailValidatorRegExp.hasMatch(value) &&
//                   errors.contains(kInvalidEmailError)) {
//                 setState(() {
//                   errors.remove(kInvalidEmailError);
//                 });
//               }
//               return null;
//             },
//             validator: (value) {
//               if (value.isEmpty && !errors.contains(kEmailNullError)) {
//                 setState(() {
//                   errors.add(kEmailNullError);
//                 });
//               } else if (!emailValidatorRegExp.hasMatch(value) &&
//                   !errors.contains(kInvalidEmailError)) {
//                 setState(() {
//                   errors.add(kInvalidEmailError);
//                 });
//               }
//               return null;
//             },
//             decoration: buildInputDecoration(
//                 "Email",
//                 "korisnik123@gmail.com",
//                 Icon(Icons.mail_outline, size: iconSize, color: iconColor),
//                 context),
//           ),
//           SizedBox(height: 30),
//           FormError(
//               errors: errors, error_color: Theme.of(context).colorScheme.error),
//           SizedBox(height: MediaQuery.of(context).size.height * 0.1),
//           DefaultButton(
//             text: "Nastavi",
//             press: () {
//               if (_formKey.currentState.validate()) {
//                 // Do what you want to do
//               }
//             },
//           ),
//           SizedBox(height: MediaQuery.of(context).size.height * 0.1),
//           Row(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               Text(
//                 "Nemate nalog?  ",
//                 style: TextStyle(fontSize: 16),
//               ),
//               GestureDetector(
//                 onTap: () {
//                   Navigator.pushNamed(context, SignUpScreen.routeName);
//                 },
//                 child: Text(
//                   "Registrujte se",
//                   style: TextStyle(fontSize: 16, color: Color(0xFFFF7643)),
//                 ),
//               ),
//             ],
//           ),
//           SizedBox(height: 20),
//         ],
//       ),
//     );
//   }
// }
