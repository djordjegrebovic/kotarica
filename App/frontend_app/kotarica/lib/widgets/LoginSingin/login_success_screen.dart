import 'package:flutter/material.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/widgets/LoginSingin/login_success_body.dart';

class LoginSuccessScreen extends StatelessWidget {
  static String routeName = "/uspešna_prijava";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: SizedBox(),
        title: Text("Uspešna prijava",
            style:
                TextStyle(color: Boje.mainText, fontWeight: FontWeight.bold)),
      ),
      body: SafeArea(child: LoginSuccessBody()),
    );
  }
}
