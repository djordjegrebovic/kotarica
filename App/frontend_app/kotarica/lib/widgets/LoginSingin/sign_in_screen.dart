import 'package:flutter/material.dart';
import 'package:kotarica/utils/boje.dart';
// import 'package:kotarica/eth_models/user_loginModels.dart';
import 'package:kotarica/widgets/LoginSingin/sign_in_body.dart';
// import 'package:provider/provider.dart';
import '../../main.dart';

class SignInScreen extends StatelessWidget {
  static String routeName = '/sign_in';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: SizedBox(
          height: 40,
          width: 40,
          // ignore: deprecated_member_use
          child: FlatButton(
            splashColor: Theme.of(context).colorScheme.onSurface,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(60),
            ),
            padding: EdgeInsets.zero,
            onPressed: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => MyApp()),
            ),
            //Navigator.pushNamed(context, MyApp.routeName),
            // Navigator.pop(context),
            child: Icon(
              Icons.arrow_back_ios_outlined,
              color: Boje.mainText,
            ),
          ),
        ),
        title: Text(
          "Prijava",
          style: TextStyle(color: Boje.mainText, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      body: SafeArea(child: SignInBody()),
      // ChangeNotifierProvider(
      //     create: (context) => UserLoginModel(),
      //     child: MaterialApp(title: 'flutter demo', home: SignInBody())),
    );
  }
}
