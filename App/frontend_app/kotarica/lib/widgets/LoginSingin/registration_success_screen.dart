import 'package:flutter/material.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/widgets/LoginSingin/registration_success_body.dart';

class RegistrationSuccessScreen extends StatelessWidget {
  static String routeName = "/uspešna_registracija";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: SizedBox(),
        title: Text("Uspešna registracija",
            style:
                TextStyle(color: Boje.mainText, fontWeight: FontWeight.bold)),
      ),
      body: SafeArea(child: RegistrationSuccessBody()),
    );
  }
}
