import 'package:flutter/material.dart';
import 'package:kotarica/AllInfoBuilder.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/eth_models/user_loginModels.dart';
import 'package:kotarica/main.dart';
import 'package:kotarica/models/getInfo.dart';
// import 'package:kotarica/pages/welcome_screen.dart';
import 'package:kotarica/widgets/LoginSingin/default_button.dart';
// import 'package:kotarica/widgets/LoginSingin/forgot_password_screen.dart';
import 'package:kotarica/widgets/LoginSingin/form_error.dart';
// import 'package:kotarica/widgets/LoginSingin/login_success_screen.dart';
import 'package:kotarica/widgets/SignUp/InputBorderDecoration.dart';
import 'package:kotarica/widgets/productDetail/add.dart';
import 'package:provider/provider.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:universal_html/html.dart' show window;
import 'package:flutter/services.dart';

class SignForm extends StatefulWidget {
  @override
  _SignFormState createState() => _SignFormState();
}

class _SignFormState extends State<SignForm> {
  // ignore: non_constant_identifier_names
  final TextEditingController password_controller = TextEditingController();
  // ignore: non_constant_identifier_names
  final TextEditingController username_controller = TextEditingController();
  // ignore: non_constant_identifier_names
  final TextEditingController private_key_controller = TextEditingController();
  // ignore: non_constant_identifier_names
  final TextEditingController email_controller = TextEditingController();
  var jwt;

  final _formKey = GlobalKey<FormState>();
  String email;
  String password;
  String username;
  bool remember = false;
  final List<String> errors = [];
  double iconSize = 26;
  Color iconColor;

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    //var listModel = Provider.of<UserLoginModel>(context);
    double width = MediaQuery.of(context).size.width;
    var addListener = Provider.of<AddModel>(context);
    var jwt;
    return Consumer<UserLoginModel>(
        builder: (context, provider, child) => Form(
              key: _formKey,
              child: Container(
                width: width < 500 ? width : 500,
                child: Column(
                  children: [
                    buildUsernameFormField(),
                    SizedBox(height: 30),
                    // buildEmailFormField(),
                    // SizedBox(height: 30),
                    buildPasswordFormField(),
                    SizedBox(height: 30),
                    // buildPrivateKeyFormField(),
                    // SizedBox(height: 30),
                    // Row(
                    //   children: [
                    //     Checkbox(
                    //       value: remember,
                    //       activeColor: Color(0xFFFF7643),
                    //       onChanged: (value) {
                    //         setState(() {
                    //           remember = value;
                    //         });
                    //       },
                    //     ),
                    //     Text("Zapamti me"),
                    //     Spacer(),
                    //     GestureDetector(
                    //       onTap: () => Navigator.pushNamed(
                    //           context, ForgotPasswordScreen.routeName),
                    //       child: Text(
                    //         "Zaboravili ste lozinku?",
                    //         style: TextStyle(
                    //             decoration: TextDecoration.underline),
                    //       ),
                    //     )
                    //   ],
                    // ),
                    // SizedBox(height: 10),
                    FormError(
                      errors: errors,
                      error_color: Theme.of(context).colorScheme.error,
                    ),
                    SizedBox(height: 20),
                    DefaultButton(
                      text: "Nastavi",
                      width: width < 500 ? width : 500,
                      press: () {
                        // print(password_controller.text);
                        if (_formKey.currentState.validate()) {
                          _formKey.currentState.save();
                          jwt = provider.postoji(username_controller.text,
                              password_controller.text);
                          // print(jwt);
                          //listModel.login(username.text, password.text),
                          if (jwt !=
                                  null /*listModel.postoji(username.text, password.text) !=null*/
                              ) {
                            AllInfos.privateKey = UserLoginModel.privKey;
                            UserLoginModel.sifra = password_controller.text;
                            if (kIsWeb) {
                              window.localStorage['csrf'] = jwt;
                            } else
                              storage.write(key: "jwt", value: jwt);
                            // Navigator.of(context).pushNamed(MyApp.routeName);
                            // Navigator.pushNamed(context, MyApp.routeName);
                            List<Product> productss = new List<Product>();
                            var len = addListener.baskets.length;
                            for (var i = 0; i < len; i++) {
                              if (addListener.baskets[i].idOglasavaca ==
                                  GetInfo.id)
                                productss.add(addListener.baskets[i]);
                            }
                            Product proizvodZabrisanje;
                            for (var j = 0; j < productss.length; j++) {
                              proizvodZabrisanje = productss[j];
                              addListener.baskets.remove(proizvodZabrisanje);
                              // addListener.baskets.length--;
                            }
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => MyApp()),
                            ); // RESITI
                          } else {
                            provider.displayDialog(context, "Desila se greška!",
                                "Kombinacija korisničkog imena i šifre ne postoji.");
                          }
                        }
                      },
                    ),
                  ],
                ),
              ),
            ));
  }

//Polja za upisivanje podataka:

  TextFormField buildPrivateKeyFormField() {
    return TextFormField(
      controller: private_key_controller,
      obscureText: true,
      onSaved: (newValue) => password = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPrivateKeyNullError);
        }
        if (privateKeyValidatorRegExp.hasMatch(value)) {
          removeError(error: kInvalidPrivateKey);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPrivateKeyNullError);
          return "";
        } else if (!privateKeyValidatorRegExp.hasMatch(value)) {
          addError(error: kInvalidPrivateKey);
          return "";
        }

        return null;
      },
      decoration: buildInputDecoration(
          "Privatni ključ",
          "Unesite privatni ključ",
          Icon(Icons.privacy_tip_outlined, size: iconSize, color: iconColor),
          context),
    );
  }

  TextFormField buildPasswordFormField() {
    return TextFormField(
      controller: password_controller,
      obscureText: true,
      onSaved: (newValue) => password = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPassNullError);
        }
        if (value.length >= 8) {
          removeError(error: kShortPassError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPassNullError);
          return "";
        } else if (value.length < 8) {
          addError(error: kShortPassError);
          return "";
        }
        return null;
      },
      decoration: buildInputDecoration("Lozinka", "Unesite vašu šifru",
          Icon(Icons.lock_outline, size: iconSize, color: iconColor), context),
    );
  }

  TextFormField buildEmailFormField() {
    return TextFormField(
      controller: email_controller,
      keyboardType: TextInputType.emailAddress,
      onSaved: (newValue) => username = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kEmailNullError);
        } else if (emailValidatorRegExp.hasMatch(value)) {
          removeError(error: kInvalidEmailError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kEmailNullError);
          return "";
        } else if (!emailValidatorRegExp.hasMatch(value)) {
          addError(error: kInvalidEmailError);
          return "";
        }
        return null;
      },
      decoration: buildInputDecoration("Email", "korisnik123@gmail.com",
          Icon(Icons.mail_outline, size: iconSize, color: iconColor), context),
    );
  }

  TextFormField buildUsernameFormField() {
    return TextFormField(
      controller: username_controller,
      autofocus: true,
      keyboardType: TextInputType.text,
      onSaved: (newValue) => email = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kUsernameNullError);
        }
        if (usernameValidatorRegExp.hasMatch(value)) {
          removeError(error: kUsernameWhitespacesError);
        }
        // if (value.length >= 4 && value.length <= 32) {
        //   removeError(error: kWrongLengthUsernameError);
        // }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kUsernameNullError);
          return "";
        }
        // if ((value.length < 4 || value.length > 32) &&
        //     !usernameValidatorRegExp.hasMatch(value)) {
        //   addError(error: kWrongLengthUsernameError);
        //   addError(error: kUsernameWhitespacesError);

        //   return "";
        // }
        // else if (value.length < 4 || value.length > 32) {
        //   addError(error: kWrongLengthUsernameError);
        //   return "";
        // }
        else if (!usernameValidatorRegExp.hasMatch(value)) {
          addError(error: kUsernameWhitespacesError);
          return "";
        }

        return null;
      },
      decoration: buildInputDecoration(
          "Korisničko ime",
          "Unesite vaše korisničko ime",
          Icon(Icons.mail_outline, size: iconSize, color: iconColor),
          context),
    );
  }
}
