import 'package:flutter/material.dart';

// Form Error
final RegExp emailValidatorRegExp =
    RegExp("^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
final RegExp usernameValidatorRegExp = RegExp(r"^[a-zA-Z][a-zA-Z0-9-_]{3,22}$");
final RegExp phoneNumberValidatorRegExp = RegExp(r"^(06)([0-9]) [\d]{6,7}$");
final RegExp privateKeyValidatorRegExp = RegExp(r"^[a-z0-9]{64}$");

const String kInvalidPhoneNumber =
    "Broj telefona mora da počinje sa 06*, zatim razmak i nakon razmaka 6 ili 7 cifara";
const String kUsernameNullError = "Molimo vas unesite korisničko ime";
// const String kInvalidUsernameError =
//     "Ne postoji nalog sa takvim korisničkim imenom";
const String kUsernameWhitespacesError =
    "Korisničko ime mora da sadrži između 4 i 22 karaktera. Mora početi slovom, ne može da sadrži razmak i specijalne karaktere osim '_'"; //(mala, velika slova abecede, brojeve i karakter _)
// const String kWrongLengthUsernameError =
//     "Korisničko ime mora da sadrži između 4 i 22 karaktera";
const String kEmailNullError = "Molimo vas uneste email adresu";
const String kInvalidEmailError = "Molimo vas uneste ispravnu email adresu";
const String kPassNullError = "Molimo vas unesite vašu lozinku";
const String kOldPassNullError = "Molimo vas unesite vašu staru lozinku";
const String kNewPassNullError = "Molimo vas unesite vašu novu lozinku";
const String kNewPassSamePasswordError =
    "Nova lozinka se mora razlikovati od stare";

const String kWrongOldPassError = "Stara lozinka je pogrešno uneta";
const String kPassNullErrorConfirm = "Molimo vas potvrdite lozinku";
const String kShortPassError = "Lozinka je prekratka";
const String kMatchPassError = "Lozinke se ne podudaraju";
const String kNamelNullError = "Molimo vas unesite vaše ime";
const String kLastNamelNullError = "Molimo vas unesite vaše prezime";

const String kPhoneNumberNullError = "Molimo vas unestie vaš broj telefona";
const String kAddressNullError = "Molimo vas unesite vašu adresu";
const String kCityNullError = "Molimo vas unesite grad u kojem živite";
const String kPrivateKeyNullError = "Molimo vas unesite privatni ključ";
const String kInvalidPrivateKey =
    "Privatni ključ mora da sadrži tačno 64 karaktera (brojeve ili mala slova abecede)";

class FormError extends StatelessWidget {
  const FormError({
    Key key,
    @required this.errors,
    // ignore: non_constant_identifier_names
    @required this.error_color,
  }) : super(key: key);

  final List<String> errors;
  // ignore: non_constant_identifier_names
  final Color error_color;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(
          errors.length,
          (index) =>
              formErrorText(error: errors[index], error_color: error_color)),
    );
  }

  // ignore: non_constant_identifier_names
  Row formErrorText({String error, Color error_color}) {
    return Row(
      children: [
        SizedBox(height: 4),
        Icon(Icons.error_outline, color: error_color),
        SizedBox(
          width: 10,
        ),
        Expanded(
          child: Text(error,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: error_color)),
        ),
      ],
    );
  }
}
