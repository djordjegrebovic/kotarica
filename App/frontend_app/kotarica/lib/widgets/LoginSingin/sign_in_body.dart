import 'package:flutter/material.dart';
// import 'package:kotarica/eth_models/user_loginModels.dart';
import 'package:kotarica/utils/boje.dart';
// import 'package:kotarica/widgets/SignUp/sign_up_body.dart';
import 'package:kotarica/widgets/SignUp/sign_up_screen.dart';
// import 'package:kotarica/widgets/login/register/login.dart';
// import 'package:provider/provider.dart';
// import '../../main.dart';
import 'sign_in_form.dart';

class SignInBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 35),
                Text(
                  "Dobrodošli",
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.onPrimary,
                    fontSize: 26,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 5),
                Text(
                  "Prijavite se pomoću korisničkog imena i lozinke",
                  textAlign: TextAlign.center,
                  style:
                      TextStyle(color: Theme.of(context).colorScheme.onPrimary),
                ),
                SizedBox(height: 35),
                SignForm(),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Nemate nalog?  ",
                      style: TextStyle(
                        fontSize: 16,
                        color: Theme.of(context).colorScheme.onPrimary,
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        // Navigator.pushNamed(context, SignUpScreen.routeName);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SignUpScreen()),
                        );
                      },
                      child: Text(
                        "Registrujte se",
                        style: TextStyle(
                          fontSize: 18,
                          color: Boje.loginRegisterButton,
                          // Colors.orange[600],
                          // Theme.of(context).colorScheme.onPrimary, //Colors.orange, //
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 30),
              ],
            ),
          ),
        ),
      ),
    ));
  }
}
