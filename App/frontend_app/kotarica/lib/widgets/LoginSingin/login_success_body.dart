import 'package:flutter/material.dart';
import 'package:kotarica/pages/welcome_screen.dart';
import 'package:kotarica/widgets/LoginSingin/default_button.dart';

// ignore: must_be_immutable
class LoginSuccessBody extends StatelessWidget {
  // ignore: non_constant_identifier_names
  final TextEditingController password_controller = TextEditingController();
  // ignore: non_constant_identifier_names
  final TextEditingController username_controller = TextEditingController();
  // ignore: non_constant_identifier_names
  final TextEditingController private_key_controller = TextEditingController();
  // ignore: non_constant_identifier_names
  final TextEditingController email_controller = TextEditingController();
  var jwt;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Center(
      child: Container(
        color: Theme.of(context).colorScheme.onBackground,
        width: width < 500 ? width : 500,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: 20),
            Image(
              height: 300,
              width: width,
              image: AssetImage("assets/images/success.png"),
            ),
            SizedBox(height: 80),
            Container(
              width: width < 500 ? width * 0.9 : 450,
              child: Text(
                "Uspešno ste se prijavili.",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: width < 700 ? 26 : 30,
                  fontWeight: FontWeight.bold,
                  color: Theme.of(context).colorScheme.onPrimary,
                ),
              ),
            ),
            Spacer(),
            DefaultButton(
              text: "Nazad na početnu",
              width: width < 500 ? width * 0.9 : 450,
              press: () {
                // Navigator.push(context,
                //     new MaterialPageRoute(builder: (context) => WelcomeScreen()));
                //     Navigator.pushNamed(context, MyApp.routeName);
                Navigator.pushNamed(context, WelcomeScreen.routeName);

                // AllInfos.privateKey = private_key_controller.text;
                // jwt = listModel.postoji(
                //     username_controller.text, password_controller.text);
                // print(jwt);
                // listModel.login(
                //     username_controller.text, password_controller.text);
                // if (jwt !=
                //         null /*listModel.postoji(
                //       username.text, password.text) !=
                //   null*/
                //     ) {
                //   if (kIsWeb) {
                //     window.localStorage['csrf'] = jwt;
                //   } else
                //     storage.write(key: "jwt", value: jwt);
                //   Navigator.push(
                //     context,
                //     MaterialPageRoute(builder: (context) => MyApp() /* Main2()*/),
                //   );
                // } else {
                //   listModel.displayDialog(context, "Desila se greska!",
                //       "Kombinacija korisnickog imena i sifre ne postoji.");
                //   print("ne postoji taj user i password");
                //   username_controller.clear();
                //   password_controller.clear();
                // }
              },
            ),
            SizedBox(height: 50),
          ],
        ),
      ),
    );
  }
}
