import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/widgets/products/productListView.dart';
import 'package:provider/provider.dart';
import 'package:kotarica/utils/size_functions.dart';

// ignore: must_be_immutable
class UserProducts extends StatelessWidget {
  int _numOfItems;

  BigInt _userId;
  UserProducts(BigInt userId) {
    _userId = userId;
  }

  @override
  Widget build(BuildContext context) {
    var listModel = Provider.of<ProductModels>(context);
    double width = MediaQuery.of(context).size.width;

    if (listModel.isLoading) {
      return Center(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 15, bottom: 20),
              child: Text(
                "Oglasi se učitavaju",
                style: TextStyle(
                    fontSize: width < 800 ? 18 : 20,
                    color: Theme.of(context).colorScheme.onPrimary),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: SpinKitFadingCube(
                  size: 35, color: Theme.of(context).colorScheme.primary),
            ),
          ],
        ),
      );
    } else {
      // var id = BigInt.parse(universal.window.localStorage["id"]);
      var lista = listModel.getProductsById(_userId);
      _numOfItems = lista.length;

      if (lista.length != 0) {
        return Container(
          width: width < 800 ? width : 800,
          height: calculateHeight(context, _numOfItems, 20),
          padding: EdgeInsets.symmetric(vertical: 10),
          child: ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            itemCount: lista.length,
            itemBuilder: (BuildContext context, int index) => Padding(
              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
              child: ProductList(lista[index], listModel, false, false),
            ),
          ),
        );
      } else {
        return Container(
          padding: EdgeInsets.symmetric(vertical: 30),
          child: Center(
              child: Text("Još uvek nema postavljenih oglasa.",
                  style: TextStyle(fontSize: 20))),
        );
      }
    }
  }
}
