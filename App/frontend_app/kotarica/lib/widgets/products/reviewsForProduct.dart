import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
// import 'package:kotarica/eth_models/KupovinaModels.dart';
// import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/eth_models/RecenzijeModels.dart';
import 'package:kotarica/utils/boje.dart';
// import 'package:kotarica/widgets/products/favouritesProductList.dart';
// import 'package:kotarica/widgets/products/reviews.dart';
import 'package:kotarica/widgets/review/reviewWidget.dart';
import 'package:provider/provider.dart';
// import 'package:universal_html/html.dart' as universal;

// ignore: must_be_immutable
class ReviewRequestsForProducts extends StatelessWidget {
  // int _numOfItems;
  // double _heightOfItemPerc;
  // double _padding;
  BigInt id1;
  ReviewRequestsForProducts(BigInt productID) {
    // _heightOfItemPerc = 0.20;
    // _padding = 16.0;
    this.id1 = productID;
  }

  @override
  Widget build(BuildContext context) {
    var listModel = Provider.of<RecenzijeModel>(context);

    //print(lista);
    if (listModel.isLoading) {
      return Center(
        child: SpinKitFadingCube(
            size: 35, color: Theme.of(context).colorScheme.primary),
      );
    } else {
      listModel.getProductReviews(id1);
      if (listModel.isLoadingProductReviews2) {
        return Center(
          child: SpinKitFadingCube(
              size: 35, color: Theme.of(context).colorScheme.primary),
        );
      } else {
        var lista = listModel.getReviewsForProduct(id1);
        //var id = BigInt.parse(universal.window.localStorage["id"]);
        // _numOfItems = lista.length;
        if (lista.length != 0) {
          return SizedBox(
            // Visina za tab na stranici profila za recenzije
            // height: calculateHeight(context),
            child: ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: lista.length,
              itemBuilder: (BuildContext context, int index) => Padding(
                padding: EdgeInsets.all(8),
                child: ShowReviewWidget(lista[index], true),
              ),
            ),
          );
        } else {
          return Container(
              padding: EdgeInsets.only(top: 20),
              child: Center(
                child: Text("Trenutno nema recenzija za dati proizvod",
                    style: TextStyle(
                        color: Boje.mainText,
                        fontSize: 16,
                        fontWeight: FontWeight.bold)),
              ));
        }
      }
    }
  }

  // double calculateHeight(BuildContext context) {
  //   return MediaQuery.of(context).size.height *
  //           _heightOfItemPerc *
  //           _numOfItems +
  //       (_numOfItems * _padding * 2);
  // }

  // Future<void> ucitajProizvode(RecenzijeModel listModel, BigInt id) async {
  //   await listModel.getProductReviews(id);
  // }
}
