import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/utils/size_functions.dart';
import 'package:kotarica/widgets/productDetail/product_card.dart';
import 'package:kotarica/widgets/productDetail/product_details.dart';
import 'package:kotarica/widgets/productDetail/rounded_icon_button.dart';
import 'package:kotarica/widgets/products/productListView.dart';
import 'package:provider/provider.dart';

class ProductListHomePage extends StatefulWidget {
  @override
  _ProductListState createState() => _ProductListState();
}

class _ProductListState extends State<ProductListHomePage> {
  var _currentValue = 1;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    var listModel = Provider.of<ProductModels>(context);

    if (listModel.isLoading) {
      return Center(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 10, bottom: 20),
              child: Text(
                "Najnoviji proizvodi se učitavaju",
                style: TextStyle(
                    fontSize: width < 800 ? 18 : 20,
                    color: Theme.of(context).colorScheme.onPrimary),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: SpinKitFadingCube(
                  size: 35, color: Theme.of(context).colorScheme.primary),
            ),
          ],
        ),
      );
    } else {
      if (listModel.isLoading3) {
        listModel.dajStranicu(BigInt.from(_currentValue));
        return Center(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 10, bottom: 20),
                child: Text(
                  "Najnoviji proizvodi se učitavaju",
                  style: TextStyle(
                      fontSize: width < 800 ? 18 : 20,
                      color: Theme.of(context).colorScheme.onPrimary),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30),
                child: SpinKitFadingCube(
                    size: 35, color: Theme.of(context).colorScheme.primary),
              ),
            ],
          ),
        );
      } else {
        var lista = listModel.vratiProizvodeZaStranicu();
        return Column(
          children: [
            Text(
              'Najnoviji',
              style: TextStyle(
                fontSize: 21,
                fontWeight: FontWeight.bold,
                color: Color.fromARGB(255, 0, 42, 42),
              ),
            ),
            SizedBox(height: 5),
            Text("Ukupan broj oglasa : " +
                ProductModels.products.length.toString()),
            width < 800
                ? SizedBox(
                    height: calculateHeight(
                        context, listModel.proizvodiStraniceDuzina(), 0),
                    child: ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: listModel.proizvodiStraniceDuzina(),
                      itemBuilder: (BuildContext context, int index) => Padding(
                        padding:
                            EdgeInsets.only(left: 12, right: 12, bottom: 12),
                        child: lista[index].id != BigInt.zero
                            ? ProductList(lista[index], listModel, true, true)
                            : SizedBox(),
                      ),
                    ),
                  )
                : Container(
                    padding: EdgeInsets.only(top: 15),
                    width: width > 800 ? 1200 : width,
                    child: GridView.builder(
                      shrinkWrap: true,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 4,
                        childAspectRatio: 120 / 155,
                        // crossAxisSpacing: 2,
                        mainAxisSpacing: 10,
                      ),
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: listModel.proizvodiStraniceDuzina(),
                      itemBuilder: (BuildContext context, int index) => Padding(
                        padding:
                            EdgeInsets.only(left: 12, right: 12, bottom: 12),
                        child: ProductCard(
                            product: lista[index],
                            listModel: listModel,
                            grid: true,
                            width: 170,
                            press: () => Navigator.pushNamed(
                                  context,
                                  ProductDetails.routeName,
                                  arguments: ProductDetailsArguments(
                                    product: lista[index],
                                  ),
                                )),
                      ),
                    ),
                  ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _currentValue == 1
                    ? SizedBox(width: 120)
                    : RoundedIconBtn(
                        iconFirst: true,
                        text: "PRETHODNA",
                        icon: Icons.arrow_back_ios_outlined,
                        width: 120,
                        height: 33,
                        size: 21,
                        buttonColor: Colors.teal[300],
                        iconColor: Colors.white,
                        showShadow: false,
                        press: () {
                          setState(() {
                            if (_currentValue != 1) {
                              final newValue = _currentValue - 1;
                              _currentValue = newValue.clamp(
                                  1, (listModel.productsCount / 12).ceil());
                              listModel.isLoading3 = true;
                            }
                          });
                        },
                      ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: listModel.productsCount > 12
                      ? Text('$_currentValue', style: TextStyle(fontSize: 22))
                      : SizedBox(),
                ),
                _currentValue == (listModel.productsCount / 12).ceil()
                    ? SizedBox(width: 120)
                    : RoundedIconBtn(
                        text: "SLEDECA",
                        icon: Icons.arrow_forward_ios_outlined,
                        width: 120,
                        height: 33,
                        size: 21,
                        buttonColor: Colors.teal[300],
                        iconColor: Colors.white,
                        showShadow: false,
                        press: () {
                          setState(() {
                            if (_currentValue <
                                ((listModel.productsCount / 12).ceil())) {
                              final newValue = _currentValue + 1;
                              _currentValue = newValue.clamp(
                                  1, (listModel.productsCount / 12).ceil());
                              listModel.isLoading3 = true;
                            }
                          });
                        },
                      ),
              ],
            ),
            SizedBox(height: 10)
          ],
        );
      }
    }
  }
}
