// import 'package:flutter/material.dart';
// import 'package:flutter_spinkit/flutter_spinkit.dart';
// import 'package:kotarica/eth_models/KupovinaModels.dart';
// // import 'package:kotarica/models/getInfo.dart';
// import 'package:kotarica/widgets/products/sentReqests.dart';
// import 'package:provider/provider.dart';
// import 'package:kotarica/utils/size_functions.dart';

// // ignore: must_be_immutable
// class RecievedRequestsProductListHomePage extends StatelessWidget {
//   int _numOfItems;

//   // RecievedRequestsProductListHomePage() {  }

//   @override
//   Widget build(BuildContext context) {
//     var listModel = Provider.of<KupovinaModels>(context);

//     if (listModel.isLoading2) {
//       return Center(
//         child: SpinKitFadingCube(
//             size: 35, color: Theme.of(context).colorScheme.primary),
//       );
//     } else {
//       // var id = GetInfo.id;
//       var lista = listModel.getMyRecievedOrderRequest();
//       _numOfItems = lista.length;
//       if (lista.length != 0) {
//         return SizedBox(
//           height: calculateHeightReview(context, _numOfItems, 5),
//           child: ListView.builder(
//             physics: NeverScrollableScrollPhysics(),
//             itemCount: lista.length,
//             shrinkWrap: true,
//             itemBuilder: (BuildContext context, int index) => Padding(
//               padding: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
//               // child: Text(" ${lista[index].id} ${lista[index].idProizvoda} \n" +
//               //     " ${lista[index].idKupca} ${lista[index].kolicina} \n" +
//               //     " ${lista[index].idOglasivaca} ${lista[index].status} \n" +
//               //     "${lista[index].poruka} ${lista[index].datum}"),
//               child: SmallOrderRepresent(lista[index], true),
//             ),
//           ),
//         );
//       } else {
//         return Container(
//           child: Center(child: Text("Nema stiglih zahteva za kupovinu")),
//         );
//       }
//     }
//   }
// }
