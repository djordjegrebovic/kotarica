import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:kotarica/eth_models/RecenzijeModels.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/widgets/review/reviewWidget.dart';
import 'package:provider/provider.dart';
//import 'package:universal_html/prefer_sdk/html.dart';

class ReviewRequestsProductListHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var listModel = Provider.of<RecenzijeModel>(context);
    double width = MediaQuery.of(context).size.width;
    if (listModel.isLoading) {
      return Center(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 15, bottom: 20),
              child: Text(
                "Recenzije se učitavaju",
                style: TextStyle(
                    fontSize: width < 800 ? 18 : 20,
                    color: Theme.of(context).colorScheme.onPrimary),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: SpinKitFadingCube(
                  size: 35, color: Theme.of(context).colorScheme.primary),
            ),
          ],
        ),
      );
    } else {
      if (listModel.isLoading1) {
        listModel.getMyRequests(GetInfo.id);
        return Center(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 15, bottom: 20),
                child: Text(
                  "Recenzije se učitavaju",
                  style: TextStyle(
                      fontSize: width < 800 ? 18 : 20,
                      color: Theme.of(context).colorScheme.onPrimary),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: SpinKitFadingCube(
                    size: 35, color: Theme.of(context).colorScheme.primary),
              ),
            ],
          ),
        );
      } else {
        // var id = BigInt.parse(universal.window.localStorage["id"]);
        var lista = listModel.getMyRequestedProducts(GetInfo.id);
        // _numOfItems = lista.length;
        if (lista.length != 0) {
          return Center(
            child: Container(
              color:
                  Theme.of(context).colorScheme.onBackground.withOpacity(0.7),
              width: width < 800 ? width : 800,
              // height: calculateHeight(context),
              child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: lista.length,
                itemBuilder: (BuildContext context, int index) => Padding(
                  padding: EdgeInsets.all(10),
                  child: ShowReviewWidget(lista[index], true),
                ),
              ),
            ),
          );
        } else {
          return Container(
            child: Center(child: Text("Nema stiglih zahteva za kupovinu")),
          );
        }
      }
    }
  }
}
