import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/pages/edit_product.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/utils/const.dart';
import 'package:kotarica/widgets/cart/snack_bar.dart';
// import 'package:kotarica/widgets/images/image_load.dart';
// import 'package:kotarica/widgets/images/image_load.dart';
import 'package:kotarica/widgets/productDetail/add.dart';
import 'package:kotarica/widgets/productDetail/product_details.dart';
import 'package:kotarica/widgets/productDetail/rounded_icon_button.dart';
import 'package:provider/provider.dart';
// import 'package:universal_html/html.dart' as universal;

// ignore: must_be_immutable
class ProductList extends StatefulWidget {
  Product product;
  ProductModels listModel;
  // ignore: non_constant_identifier_names
  bool enableFavourite = true;
  // ignore: non_constant_identifier_names
  bool enableBasket = true;

  ProductList(Product product, ProductModels listModel, bool enableFavourite,
      bool enableBasket) {
    this.product = product;
    this.listModel = listModel;
    this.enableFavourite = enableFavourite;
    this.enableBasket = enableBasket;
  }

  @override
  _ProductListState createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  @override
  Widget build(BuildContext context) {
    void displayDialog(context, title, text) => showDialog(
          context: context,
          builder: (context) =>
              AlertDialog(title: Text(title), content: Text(text)),
        );
    double width = MediaQuery.of(context).size.width;
    var cartProvider = Provider.of<AddModel>(context);
    var listModel = Provider.of<ProductModels>(context);

    if (widget.listModel.isLoading) {
      return Center(
        child: SpinKitFadingCube(
            size: 35, color: Theme.of(context).colorScheme.primary),
      );
    } else {
      return GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, ProductDetails.routeName,
              arguments: ProductDetailsArguments(product: this.widget.product));
        },
        child: Container(
          // color: Colors.red,

          //  MediaQuery.of(context).size.width > 600
          //     ? 600
          //     : MediaQuery.of(context).size.width,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: [
                  SizedBox(
                    // Siva pozadina sa slikom proizvoda
                    width: width < 600 ? width * 0.23 : 150,
                    child: AspectRatio(
                      aspectRatio: 1,
                      child: Container(
                          padding: EdgeInsets.all(5.0),
                          decoration: BoxDecoration(
                            color: Theme.of(context).colorScheme.onBackground,
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(15),
                            child: widget.product.hesh == ""
                                ? Image.asset(widget.product.images[0])
                                : Image(
                                    image: NetworkImage(Const.url +
                                        "/api/Image/" +
                                        widget.product.hesh),
                                    fit: BoxFit.fill,
                                  ),
                          )),
                    ),
                  ),
                  SizedBox(width: width < 600 ? width * 0.03 : 15),
                  Column(
                    // Text pored slike
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: width < 700 ? width * 0.45 : 380,
                        // color: Colors.green,
                        child: Text(
                          widget.product.naziv,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: width < 700 ? width * 0.043 : 30,
                            color: Theme.of(context).colorScheme.onPrimary,
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 2,
                        ),
                      ),
                      const SizedBox(height: 5.0),
                      Text.rich(
                        TextSpan(
                          text: "${widget.product.kategorija}",
                          style: TextStyle(
                            fontSize: width < 600 ? width * 0.037 : 24,
                            fontWeight: FontWeight.w700,
                            color:
                                Theme.of(context).colorScheme.secondaryVariant,
                          ),
                        ),
                      ),
                      const SizedBox(height: 5.0),
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 2, horizontal: 5),
                        decoration: BoxDecoration(
                            color: Theme.of(context).colorScheme.onBackground,
                            borderRadius: BorderRadius.all(Radius.circular(7))),
                        child: Text.rich(
                          TextSpan(
                              text: "${widget.product.cena}",
                              style: TextStyle(
                                  fontSize: width < 600 ? width * 0.043 : 30,
                                  fontWeight: FontWeight.bold,
                                  color: Theme.of(context).colorScheme.surface),
                              children: [
                                TextSpan(
                                  text: " RSD/${widget.product.mernaJedinica}",
                                  style: TextStyle(
                                    fontSize: width < 600 ? width * 0.033 : 25,
                                    fontWeight: FontWeight.bold,
                                    color: Boje.rsd,
                                  ),
                                ),
                              ]),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Row(
                children: [
                  if (this.widget.enableBasket)
                    RoundedIconBtn(
                      text: '',
                      icon: Icons.shopping_cart,
                      showShadow: false,
                      height: width > 700 ? 55 : width * 0.08,
                      width: width > 700 ? 55 : width * 0.08,
                      size: width > 700 ? 41 : width * 0.055,
                      press: () {
                        if (GetInfo.id != widget.product.idOglasavaca) {
                          // setState(() {
                          cartProvider.addOneItemToBasket(this.widget.product);
                          snackBar_basket(
                              context, cartProvider, widget.product);
                          // });
                        } else {
                          displayDialog(context, "Greška!",
                              "Ne možete kupiti vaš proizvod.");
                        }
                      },
                      buttonColor: Theme.of(context).colorScheme.onBackground,
                      iconColor: GetInfo.id != widget.product.idOglasavaca
                          ? Color.fromARGB(255, 0, 111, 109)
                          : Colors.grey,
                    ),
                  SizedBox(width: 10),
                  if (this.widget.enableFavourite && GetInfo.id != BigInt.zero)
                    RoundedIconBtn(
                        text: '',
                        icon: widget.product.isFavourite
                            ? Icons.favorite
                            : Icons.favorite_border_outlined,
                        showShadow: false,
                        height: width > 700 ? 55 : width * 0.082,
                        width: width > 700 ? 55 : width * 0.082,
                        size: width > 700 ? 52 : width * 0.08,
                        press: () {
                          favouriteButtonSetState(context, listModel);
                        },
                        buttonColor: Theme.of(context).colorScheme.onBackground,
                        iconColor: widget.product.isFavourite
                            ? Colors.red[600]
                            : Theme.of(context).colorScheme.secondary),
                  if (!this.widget.enableBasket && !this.widget.enableFavourite)
                    if (GetInfo.id != BigInt.zero &&
                        this.widget.product.idOglasavaca == GetInfo.id)
                      RoundedIconBtn(
                        text: '',
                        icon: Icons.edit,
                        showShadow: false,
                        height: width > 500 ? 50 : width * 0.1,
                        width: width > 500 ? 65 : width * 0.12,
                        size: width > 500 ? 35 : width * 0.055,
                        radius: 20,
                        press: () {
                          Navigator.pushNamed(context, EditProduct.routeName,
                              arguments: EditProductArguments(
                                  product: this.widget.product));
                          // Navigator.push(
                          //     context,
                          //     new MaterialPageRoute(
                          //         builder: (context) =>
                          //             EditProduct(this.widget.product)));
                        }, //Mogucnost da se edituje
                        buttonColor: Theme.of(context).colorScheme.onBackground,
                        iconColor: Colors.teal,
                      ),
                ],
              ),
            ],
          ),
        ),
      );
    }
  }

  void favouriteButtonSetState(BuildContext context, ProductModels listModel) {
    return setState(
      () {
        if (GetInfo.id != BigInt.zero) {
          widget.product.isFavourite = !widget.product.isFavourite;

          if (widget.product.isFavourite) {
            snackBar_add_favourite(context, listModel, widget.product);
            widget.listModel.addFavourites(widget.product.id, GetInfo.id);
          } else {
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                title: Text('Da li ste sigurni?'),
                content: Text('Da li zelite da ukloni proizvod iz omiljenih?'),
                actions: [
                  // ignore: deprecated_member_use
                  FlatButton(
                    child: Text('Ne'),
                    onPressed: () {
                      setState(() {
                        Navigator.of(context).pop(false);
                        widget.product.isFavourite = true;
                      });
                    },
                  ),
                  // ignore: deprecated_member_use
                  FlatButton(
                    child: Text('Da'),
                    onPressed: () {
                      Navigator.of(context).pop(true);
                      listModel.deleteFavourites(widget.product.id, GetInfo.id);
                    },
                  )
                ],
              ),
            );
          }
        }
      },
    );
  }
}
