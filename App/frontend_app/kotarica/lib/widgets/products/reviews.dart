import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kotarica/eth_models/KupovinaModels.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/eth_models/RecenzijeModels.dart';
import 'package:kotarica/pages/customLoading.dart';
import 'package:kotarica/widgets/add_product_widgets/resizeable_input_text.dart';
import 'package:kotarica/widgets/cart/purchaseSuccess.dart';
import 'package:kotarica/widgets/cart/text_icon_button.dart';
import 'package:provider/provider.dart';
import 'package:reviews_slider/reviews_slider.dart';

// class ReviewArguments {
//   OrderRequest order;
//   ReviewArguments({this.order});
// }

// ignore: must_be_immutable
class ReviewWidget extends StatefulWidget {
  OrderRequest order;
  ReviewWidget(OrderRequest order) {
    this.order = order;
  }

  @override
  _ReviewWidgetState createState() => _ReviewWidgetState();
}

class _ReviewWidgetState extends State<ReviewWidget> {
  int selectedValue1;
  int selectedValue2;
  bool showProgress = false;
  void onChange1(int value) {
    setState(() {
      selectedValue1 = value;
    });
  }

  void onChange2(int value) {
    setState(() {
      selectedValue2 = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    var now = new DateTime.now();
    var formatter = new DateFormat('dd.MM.yyyy.');
    String formattedDate = formatter.format(now);
    // final ReviewArguments args = ModalRoute.of(context).settings.arguments;
    // var _handleRadioValueChange1;
    // var _radioValue1;
    // var _handleRadioValueChange2;
    // var _radioValue2;
    // var _handleRadioValueChange3;
    // var _radioValue3;
    var listModel = Provider.of<RecenzijeModel>(context);
    var listModel1 = Provider.of<ProductModels>(context);
    var listModel2 = Provider.of<KupovinaModels>(context);
    double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height;
    ResizeableTextInput _komentar = ResizeableTextInput("Komentar");
    return Scaffold(
        body: showProgress == false
            ? SafeArea(
                child: Center(
                  child: Container(
                    width: width < 800 ? width : 800,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        // Column(
                        //   children: [
                        //     new Text("Predmet odgovara opisu iz oglasa ? "),
                        //     Row(
                        //         mainAxisAlignment: MainAxisAlignment.center,
                        //         children: <Widget>[
                        //           new Radio(
                        //             value: 0,
                        //             groupValue: _radioValue1,
                        //             onChanged: _handleRadioValueChange1,
                        //           ),
                        //           new Text(
                        //             'Da',
                        //             style: new TextStyle(fontSize: 16.0),
                        //           ),
                        //           new Radio(
                        //             value: 2,
                        //             groupValue: _radioValue1,
                        //             onChanged: _handleRadioValueChange1,
                        //           ),
                        //           new Text(
                        //             'Ne',
                        //             style: new TextStyle(
                        //               fontSize: 16.0,
                        //             ),
                        //           ),
                        //         ]),
                        //   ],
                        // ),
                        // Column(
                        //   children: [
                        //     new Text("Korektna komunikacija ? "),
                        //     Row(
                        //         mainAxisAlignment: MainAxisAlignment.center,
                        //         children: <Widget>[
                        //           new Radio(
                        //             value: 0,
                        //             groupValue: _radioValue2,
                        //             onChanged: _handleRadioValueChange2,
                        //           ),
                        //           new Text(
                        //             'Da',
                        //             style: new TextStyle(fontSize: 16.0),
                        //           ),
                        //           new Radio(
                        //             value: 2,
                        //             groupValue: _radioValue2,
                        //             onChanged: _handleRadioValueChange2,
                        //           ),
                        //           new Text(
                        //             'Ne',
                        //             style: new TextStyle(
                        //               fontSize: 16.0,
                        //             ),
                        //           ),
                        //         ]),
                        //   ],
                        // ),
                        // Column(
                        //   children: [
                        //     new Text("Ispoštovan dogovor ? "),
                        //     Row(
                        //         mainAxisAlignment: MainAxisAlignment.center,
                        //         children: <Widget>[
                        //           new Radio(
                        //             value: 0,
                        //             groupValue: _radioValue3,
                        //             onChanged: _handleRadioValueChange3,
                        //           ),
                        //           new Text(
                        //             'Da',
                        //             style: new TextStyle(fontSize: 16.0),
                        //           ),
                        //           new Radio(
                        //             value: 2,
                        //             groupValue: _radioValue3,
                        //             onChanged: _handleRadioValueChange3,
                        //           ),
                        //           new Text(
                        //             'Ne',
                        //             style: new TextStyle(
                        //               fontSize: 16.0,
                        //             ),
                        //           ),
                        //         ]),
                        //   ],
                        // ),
                        SizedBox(height: 50),
                        Text(
                          'Molimo, ocenite proizvod nekom od ocena ispod.',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Theme.of(context).colorScheme.surface,
                              fontSize: 18),
                        ),
                        SizedBox(height: 20),
                        Padding(
                            padding: EdgeInsets.symmetric(horizontal: 0),
                            child: ReviewSlider(onChange: onChange1)),
                        //Text(selectedValue1.toString()),
                        SizedBox(height: 50),
                        Text(
                          "Unesite tekst kojim bliže dočaravate Vaše zadovoljstvo kupljenim proizvodom.",
                          style: TextStyle(
                              color: Theme.of(context).colorScheme.surface,
                              fontSize: 18),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: 20),
                        Padding(
                            padding: const EdgeInsets.only(left: 30, right: 30),
                            child: _komentar),
                        SizedBox(height: 50),
                        TextAndIconButton(
                          press: () async => {
                            showProgress = true,
                            await listModel.createReview(
                                widget.order.idProizvoda,
                                widget.order.idKupca,
                                widget.order.idOglasivaca,
                                // args.order.idProizvoda,
                                // args.order.idKupca,
                                // args.order.idOglasivaca,
                                BigInt.from(selectedValue1 + 1),
                                _komentar.textEditingController.text,
                                formattedDate.toString()),
                            await listModel1.updateOcenu(
                                widget.order.idProizvoda,
                                BigInt.from(selectedValue1 + 1)),
                            await listModel2.ocenjenTrue(widget.order.id),
                            // Navigator.pushNamed(
                            //     context, PurchaseSuccessBody.routeName,
                            //     arguments: PurchaseSuccessArguments(
                            //         text: "Uspešno ste napisali recenziju."))
                            Navigator.push(
                                context,
                                new MaterialPageRoute(
                                    builder: (context) => PurchaseSuccessBody(
                                        'Uspešno ste napisali recenziju.')))
                          },
                          text: "Oceni",
                          textColor: Colors.white,
                          icon: Icons.rate_review,
                          buttonColor: Theme.of(context).colorScheme.primary,
                          buttonWidth: width * 0.35,
                          buttonHeight: width > 850 ? 45 : 37,
                          fontSize: width > 850 ? 24 : 20,
                          iconSize: width > 850 ? 25 : 23,
                        )
                      ],
                    ),
                  ),
                ),
              )
            : CustomProgressIndicator(
                'Molimo, sačekajte. Recenzija se postavlja'));
  }
}
