import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/utils/const.dart';
import 'package:kotarica/widgets/cart/snack_bar.dart';
// import 'package:kotarica/widgets/images/image_load.dart';
import 'package:kotarica/widgets/productDetail/product_details.dart';
import 'package:provider/provider.dart';
import 'package:kotarica/widgets/productDetail/add.dart';
import 'package:kotarica/widgets/productDetail/rounded_icon_button.dart';

// ignore: must_be_immutable
class FavouritesProductList extends StatefulWidget {
  // double _heightOfContainer;
  Product product;
  // ignore: unused_field
  bool _favourite;

  FavouritesProductList(Product product, bool favourite) {
    // _heightOfContainer = heightOfContainer;
    this.product = product;
    _favourite = favourite;
  }

  @override
  _FavouritesProductListState createState() => _FavouritesProductListState();
}

class _FavouritesProductListState extends State<FavouritesProductList> {
  @override
  Widget build(BuildContext context) {
    void displayDialog(context, title, text) => showDialog(
          context: context,
          builder: (context) =>
              AlertDialog(title: Text(title), content: Text(text)),
        );
    var listModel = Provider.of<ProductModels>(context);
    var addListener = Provider.of<AddModel>(context);
    double width = MediaQuery.of(context).size.width;

    if (listModel.isLoading) {
      return Center(
        child: SpinKitFadingCube(
            size: 35, color: Theme.of(context).colorScheme.primary),
      );
    } else
      return GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, ProductDetails.routeName,
              arguments: ProductDetailsArguments(product: this.widget.product));
        },
        child: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: [
                  SizedBox(
                    // Siva pozadina sa slikom proizvoda
                    width: width < 600 ? width * 0.23 : 150,
                    child: AspectRatio(
                      aspectRatio: 1,
                      child: Container(
                        padding: EdgeInsets.all(5.0),
                        decoration: BoxDecoration(
                          color: Theme.of(context).colorScheme.onBackground,
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(15),
                          child: widget.product.hesh == ""
                              ? Image.asset(widget.product.images[0])
                              : Image(
                                  image: NetworkImage(Const.url +
                                      "/api/Image/" +
                                      widget.product.hesh),
                                  fit: BoxFit.fill,
                                ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: width < 600 ? width * 0.03 : 15),
                  Column(
                    // Text pored slike
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: width < 700 ? width * 0.45 : 380,
                        child: Text(
                          widget.product.naziv,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: width < 700 ? width * 0.043 : 30,
                            color: Theme.of(context).colorScheme.onPrimary,
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 2,
                        ),
                      ),
                      const SizedBox(height: 5.0),
                      Text.rich(
                        TextSpan(
                          text: "${widget.product.kategorija}",
                          style: TextStyle(
                            fontSize: width < 600 ? width * 0.037 : 24,
                            fontWeight: FontWeight.w700,
                            color:
                                Theme.of(context).colorScheme.secondaryVariant,
                          ),
                        ),
                      ),
                      const SizedBox(height: 5.0),
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 2, horizontal: 5),
                        decoration: BoxDecoration(
                            color: Theme.of(context).colorScheme.onBackground,
                            borderRadius: BorderRadius.all(Radius.circular(7))),
                        child: Text.rich(
                          TextSpan(
                              text: "${widget.product.cena}",
                              style: TextStyle(
                                  fontSize: width < 600 ? width * 0.043 : 30,
                                  fontWeight: FontWeight.bold,
                                  color: Theme.of(context).colorScheme.surface),
                              children: [
                                TextSpan(
                                  text: " RSD/${widget.product.mernaJedinica}",
                                  style: TextStyle(
                                    fontSize: width < 600 ? width * 0.033 : 25,
                                    fontWeight: FontWeight.bold,
                                    color: Boje.rsd,
                                  ),
                                ),
                              ]),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Row(
                children: [
                  RoundedIconBtn(
                    text: '',
                    icon: Icons.shopping_cart,
                    showShadow: false,
                    height: width > 700 ? 55 : width * 0.08,
                    width: width > 700 ? 55 : width * 0.08,
                    size: width > 700 ? 41 : width * 0.055,
                    press: () {
                      // setState(() {
                      if (GetInfo.id != widget.product.idOglasavaca) {
                        addListener.addOneItemToBasket(this.widget.product);
                        snackBar_basket(
                            context, addListener, this.widget.product);
                      } else {
                        displayDialog(context, "Greška!",
                            "Ne možete kupiti vaš proizvod.");
                      }
                      // });
                    },
                    buttonColor: Theme.of(context).colorScheme.onBackground,
                    iconColor: GetInfo.id != widget.product.idOglasavaca
                        ? Color.fromARGB(255, 0, 111, 109)
                        : Colors.grey,
                  ),
                  SizedBox(width: 10),
                  GetInfo.id != BigInt.zero
                      ? RoundedIconBtn(
                          text: '',
                          icon: this.widget.product.isFavourite
                              ? Icons.favorite
                              : Icons.favorite_border_outlined,
                          showShadow: false,
                          height: width > 700 ? 55 : width * 0.082,
                          width: width > 700 ? 55 : width * 0.082,
                          size: width > 700 ? 52 : width * 0.08,
                          press: () {
                            favouriteButtonSetState(context, listModel);
                          },
                          buttonColor:
                              Theme.of(context).colorScheme.onBackground,
                          iconColor: this.widget.product.isFavourite
                              ? Colors.red[600]
                              : Theme.of(context).colorScheme.secondary)
                      : SizedBox(),
                ],
              ),
            ],
          ),
        ),
      );
  }

  void favouriteButtonSetState(BuildContext context, ProductModels listModel) {
    return setState(
      () {
        widget.product.isFavourite = !widget.product.isFavourite;
        if (GetInfo.id != BigInt.zero) {
          if (widget.product.isFavourite) {
            snackBar_add_favourite(context, listModel, widget.product);
            listModel.addFavourites(widget.product.id, GetInfo.id);
          } else {
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                title: Text('Da li ste sigurni?'),
                content: Text('Da li zelite da ukloni proizvod iz omiljenih?'),
                actions: [
                  // ignore: deprecated_member_use
                  FlatButton(
                    child: Text('Ne'),
                    onPressed: () {
                      setState(() {
                        Navigator.of(context).pop(false);
                        widget.product.isFavourite = true;
                      });
                    },
                  ),
                  // ignore: deprecated_member_use
                  FlatButton(
                    child: Text('Da'),
                    onPressed: () {
                      Navigator.of(context).pop(true);
                      listModel.deleteFavourites(widget.product.id, GetInfo.id);
                    },
                  )
                ],
              ),
            );
          }
        }
      },
    );
  }
}
