import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/utils/size_functions.dart';
import 'package:kotarica/widgets/products/favouritesProductList.dart';
import 'package:provider/provider.dart';

class FavProductListHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var listModel = Provider.of<ProductModels>(context);
    double width = MediaQuery.of(context).size.width;

    if (listModel.isLoading1 || listModel.isLoading) {
      return Center(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 15, bottom: 20),
              child: Text(
                "Omiljeni proizvodi se učitavaju",
                style: TextStyle(
                    fontSize: width < 800 ? 18 : 20,
                    color: Theme.of(context).colorScheme.onPrimary),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: SpinKitFadingCube(
                  size: 35, color: Theme.of(context).colorScheme.primary),
            ),
          ],
        ),
      );
    } else {
      var lista = listModel.getFavouritesProducts(GetInfo.id);
      if (lista.length > 0 ) {
        return Center(
          child: Container(
            height: calculateHeight(context, lista.length, 0),
            // padding: EdgeInsets.only(top: 15),
            width: width < 800 ? width : 800,
            child: ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              itemCount: lista.length,
              itemBuilder: (BuildContext context, int index) => Padding(
                padding: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                child: FavouritesProductList(lista[index], true),
              ),
            ),
          ),
        );
      } else
        return Center(
          child: Container(
            child: Text("Još uvek niste dodali omiljene proizvode"),
          ),
        );
    }
  }
}
