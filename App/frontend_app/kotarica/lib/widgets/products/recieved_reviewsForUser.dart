import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:kotarica/eth_models/RecenzijeModels.dart';
// import 'package:kotarica/utils/size_functions.dart';
import 'package:kotarica/widgets/review/reviewWidget.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class RecievedReviewsForUser extends StatefulWidget {
  BigInt id1;
  RecievedReviewsForUser(BigInt productID) {
    this.id1 = productID;
  }

  @override
  _RecievedReviewsForUserState createState() => _RecievedReviewsForUserState();
}

class _RecievedReviewsForUserState extends State<RecievedReviewsForUser> {
  // ValueNotifier<Size> _size = ValueNotifier<Size>(Size(0, 0)); // Za trazenje visine child-a
  @override
  Widget build(BuildContext context) {
    var listModel = Provider.of<RecenzijeModel>(context);
    double width = MediaQuery.of(context).size.width;

    if (listModel.isLoading) {
      return Center(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 15, bottom: 20),
              child: Text(
                "Recenzije se učitavaju",
                style: TextStyle(
                    fontSize: width < 800 ? 18 : 20,
                    color: Theme.of(context).colorScheme.onPrimary),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: SpinKitFadingCube(
                  size: 35, color: Theme.of(context).colorScheme.primary),
            ),
          ],
        ),
      );
    } else {
      listModel.getRecievedReviewsForUser(widget.id1);
      if (listModel.isLoading2) {
        return Center(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 15, bottom: 20),
                child: Text(
                  "Recenzije se učitavaju",
                  style: TextStyle(
                      fontSize: width < 800 ? 18 : 20,
                      color: Theme.of(context).colorScheme.onPrimary),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: SpinKitFadingCube(
                    size: 35, color: Theme.of(context).colorScheme.primary),
              ),
            ],
          ),
        );
      } else {
        var lista = listModel.getMyRecievedReviews(widget.id1);
        //var id = BigInt.parse(universal.window.localStorage["id"]);
        if (lista.length != 0) {
          return SizedBox(
            // VISINA RECENZIJE
            // height: calculateHeight(context, lista.length, -(lista.length * 8)),
            child: ListView.builder(
              // key: _reviewKey,
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: lista.length,
              itemBuilder: (BuildContext context, int index) => Padding(
                padding: EdgeInsets.all(8),
                child: ShowReviewWidget(lista[index], true),
                //     ValueListenableBuilder( // Za trazenje visine child-a
                //   builder: (BuildContext context, Size value, Widget child) {
                //     return Column(
                //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                //       children: <Widget>[
                //         Text('Height of the child: ${value.height}'),
                //         Text('Width of the child: ${value.width}'),
                //         SizeTrackingWidget(
                //           sizeValueNotifier: _size,
                //         )
                //       ],
                //     );
                //   },
                //   valueListenable: _size,
                //   child: null,
                // ),
              ),
            ),
          );
        } else {
          return Container(
            padding: EdgeInsets.symmetric(vertical: 30),
            child: Center(
                child: Text("Korisnik još uvek nije dobio nijednu ocenu.",
                    style: TextStyle(fontSize: 20))),
          );
        }
      }
    }
  }
}
// Za trazenje visine child-a
// class SizeTrackingWidget extends StatefulWidget {
//   Widget child;
//   ValueNotifier<Size> sizeValueNotifier;
//   SizeTrackingWidget({Key key, @required this.sizeValueNotifier})
//       : super(key: key);
//   @override
//   _SizeTrackingWidgetState createState() => _SizeTrackingWidgetState();
// }

// class _SizeTrackingWidgetState extends State<SizeTrackingWidget> {
//   @override
//   void initState() {
//     super.initState();
//     WidgetsBinding.instance.addPostFrameCallback((_) {
//       _getSize();
//     });
//   }

//   _getSize() {
//     RenderBox renderBox = context.findRenderObject();
//     widget.sizeValueNotifier.value = renderBox.size;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//         height: MediaQuery.of(context).size.width * 0.4,
//         width: MediaQuery.of(context).size.width * 0.3,
//         color: Colors.red);
//   }
// }
