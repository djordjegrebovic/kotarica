// import 'package:flutter/material.dart';
// import 'package:flutter_spinkit/flutter_spinkit.dart';
// // import 'package:intl/intl.dart';
// import 'package:kotarica/eth_models/KupovinaModels.dart';
// import 'package:kotarica/eth_models/ProductModels.dart';
// import 'package:kotarica/eth_models/user_loginModels.dart';
// // import 'package:kotarica/models/getInfo.dart';
// import 'package:kotarica/utils/size_functions.dart';
// import 'package:kotarica/widgets/cart/text_icon_button.dart';
// // import 'package:kotarica/widgets/productDetail/add.dart';
// import 'package:kotarica/widgets/productDetail/product_details.dart';
// // import 'package:kotarica/widgets/productDetail/rounded_icon_button.dart';
// import 'package:provider/provider.dart';

// // ignore: must_be_immutable
// class SentRequestsProductListHomePage extends StatelessWidget {
//   int _numOfItems;

//   // SentRequestsProductListHomePage() {  }

//   @override
//   Widget build(BuildContext context) {
//     var listModel = Provider.of<KupovinaModels>(context);

//     if (listModel.isLoading1) {
//       return Center(
//         child: SpinKitFadingCube(
//             size: 35, color: Theme.of(context).colorScheme.primary),
//       );
//     } else {
//       // var id = GetInfo.id;
//       var lista = listModel.getMyRequestedOrderRequest();
//       _numOfItems = lista.length;
//       if (lista.length != 0) {
//         return SizedBox(
//           height: calculateHeightReview(context, _numOfItems, 5),
//           child: ListView.builder(
//             physics: NeverScrollableScrollPhysics(),
//             itemCount: lista.length,
//             itemBuilder: (BuildContext context, int index) => Padding(
//               padding: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
//               child: SmallOrderRepresent(lista[index], false),
//             ),
//           ),
//         );
//       } else {
//         return Container(
//           child: Center(child: Text("Nema poslatih zahteva za kupovinu")),
//         );
//       }
//     }
//   }

//   // child: Text(" ${lista[index].id} ${lista[index].idProizvoda} \n" +
//   //     " ${lista[index].idKupca} ${lista[index].kolicina} \n" +
//   //     " ${lista[index].idOglasivaca} ${lista[index].status} \n" +
//   //     "${lista[index].poruka} ${lista[index].datum}"),
// }

// // ignore: must_be_immutable
// class SmallOrderRepresent extends StatefulWidget {
//   OrderRequest order;
//   bool buttons;
//   SmallOrderRepresent(
//     this.order,
//     this.buttons,
//   );
//   @override
//   _SmallOrderRepresentState createState() => _SmallOrderRepresentState();
// }

// class _SmallOrderRepresentState extends State<SmallOrderRepresent> {
//   @override
//   Widget build(BuildContext context) {
//     var providerProizvodi = Provider.of<ProductModels>(context);
//     var providerUsers = Provider.of<UserLoginModel>(context);
//     // var kupovinaListener = Provider.of<KupovinaModels>(context);
//     // var addListener = Provider.of<AddModel>(context);
//     var user = providerUsers.getUserById(widget.order.idOglasivaca);
//     var product = providerProizvodi.getProductById(widget.order.idProizvoda);
//     double width = MediaQuery.of(context).size.width;
//     double height = MediaQuery.of(context).size.height;

//     // var now = new DateTime.now();
//     // var formatter = new DateFormat('dd.MM.yyyy.');
//     // String formattedDate = formatter.format(now);

//     if (providerProizvodi.isLoading && providerUsers.isLoading) {
//       return Center(
//         child: SpinKitFadingCube(size: 35, color: Colors.orange),
//       );
//     } else {
//       var status;
//       if (widget.order.status == BigInt.from(0))
//         status = "Na cekanju";
//       else if (widget.order.status == BigInt.from(1))
//         status = "Prihvacen";
//       else
//         status = "Odbijen";
//       return GestureDetector(
//         onTap: () {
//           Navigator.pushNamed(context, ProductDetails.routeName,
//               arguments: ProductDetailsArguments(
//                   product: ProductModels.products[product.id.toInt() - 1]));
//         },
//         child: Container(
//           height: MediaQuery.of(context).size.height * 0.28, // Proveriti visinu
//           padding: EdgeInsets.all(7),
//           decoration: BoxDecoration(
//               color: Colors.white, borderRadius: BorderRadius.circular(14)),
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: [
//               Container(
//                 //Naziv i datum
//                 child: Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     children: [
//                       Text(
//                         "${product.naziv}",
//                         overflow: TextOverflow.ellipsis,
//                         style: TextStyle(
//                           fontSize: MediaQuery.of(context).size.width * 0.043,
//                           color: Theme.of(context).colorScheme.onPrimary,
//                           fontWeight: FontWeight.bold,
//                         ),
//                         maxLines: 2,
//                       ),
//                       Text("${widget.order.datum}")
//                     ]),
//               ),
//               const SizedBox(height: 5.0),
//               //Kolicina i merna jedinica
//               Text.rich(
//                 TextSpan(
//                   text:
//                       "Narucena kolicina ${widget.order.kolicina} ${product.mernaJedinica}",
//                   style: TextStyle(
//                     fontSize: MediaQuery.of(context).size.width * 0.037,
//                     fontWeight: FontWeight.w700,
//                     color: Theme.of(context).colorScheme.secondaryVariant,
//                   ),
//                 ),
//               ),
//               const SizedBox(height: 5.0),
//               //Status, korisnicko ime, cena
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   Container(
//                     padding: EdgeInsets.symmetric(vertical: 2, horizontal: 5),
//                     decoration: BoxDecoration(
//                         color: Theme.of(context).colorScheme.onBackground,
//                         borderRadius: BorderRadius.all(Radius.circular(7))),
//                     child: Text.rich(
//                       TextSpan(
//                         text: "",
//                         style: TextStyle(
//                           fontSize: MediaQuery.of(context).size.width * 0.043,
//                           fontWeight: FontWeight.bold,
//                           color: Colors.red,
//                         ),
//                         children: [
//                           TextSpan(
//                             text: "Status : $status ",
//                             style: TextStyle(
//                                 fontSize:
//                                     MediaQuery.of(context).size.width * 0.043,
//                                 fontWeight: FontWeight.bold,
//                                 color: Theme.of(context).colorScheme.surface),
//                           ),
//                           TextSpan(
//                             text: "\nProdavac: ${user.username} ",
//                             style: TextStyle(
//                                 fontSize:
//                                     MediaQuery.of(context).size.width * 0.043,
//                                 fontWeight: FontWeight.bold,
//                                 color: Theme.of(context).colorScheme.surface),
//                           ),
//                           TextSpan(
//                             text:
//                                 "\nUkupna cena: ${product.cena * widget.order.kolicina} ",
//                             style: TextStyle(
//                                 fontSize:
//                                     MediaQuery.of(context).size.width * 0.043,
//                                 fontWeight: FontWeight.bold,
//                                 color: Theme.of(context).colorScheme.surface),
//                           ),
//                         ],
//                       ),
//                     ),
//                   ),
//                   //Dugmici za prihvatanje i odbijanje
//                 ],
//               ),
//               SizedBox(height: 8),
//               if (this.widget.buttons)
//                 Container(
//                   child: Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                     children: [
//                       TextAndIconButton(
//                         text: "Odbij",
//                         textColor: Colors.white,
//                         icon: Icons.dangerous,
//                         buttonColor: Colors.red[700],
//                         press: () {
//                           setState(() {
//                             this.widget.order.status = BigInt.from(2);
//                             // kupovinaListener.changeReqStatus(
//                             //     product.id,
//                             //     Status.DECLINED,
//                             //     "Trenutno nemamo zalihe ovo proizvoda",
//                             //     formattedDate);
//                           });
//                         },
//                         buttonWidth: width * 0.35,
//                         buttonHeight: width > 850 ? 45 : 37,
//                         fontSize: width > 850 ? 24 : 20,
//                         iconSize: width > 850 ? 25 : 23,
//                       ),
//                       SizedBox(width: 10),
//                       TextAndIconButton(
//                         text: "Prihvati",
//                         textColor: Colors.white,
//                         icon: Icons.verified,
//                         buttonColor: Colors.green[700],
//                         press: () {
//                           setState(() {
//                             this.widget.order.status = BigInt.from(1);
//                             // kupovinaListener.changeReqStatus(
//                             //     product.id,
//                             //     Status.CONFIRMED,
//                             //     "Bice isporuceno u roku od 2 dana",
//                             //     formattedDate);
//                           });
//                         },
//                         buttonWidth: width * 0.35,
//                         buttonHeight: width > 850 ? 45 : 37,
//                         fontSize: width > 850 ? 24 : 20,
//                         iconSize: width > 850 ? 25 : 23,
//                       ),
//                     ],
//                   ),
//                 )
//             ],
//           ),
//         ),
//       );
//     }
//   }
// }
