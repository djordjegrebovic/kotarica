import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:kotarica/eth_models/KupovinaModels.dart';
// import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:provider/provider.dart';
// import '../eth_models/user_loginModels.dart';

class UsersList extends StatelessWidget {
  final TextEditingController t1 = TextEditingController();
  final TextEditingController t2 = TextEditingController();
  @override
  Widget build(BuildContext context) {
    var listModel = Provider.of<KupovinaModels>(context);
    //var omiljeni = listModel.getFavourites(BigInt.from(1));
    return Scaffold(
      appBar: AppBar(
        title: Text("TODOLIST"),
      ),
      body: listModel.isLoading
          ? Center(
              child: SpinKitFadingCube(
                  size: 35, color: Theme.of(context).colorScheme.primary),
            )
          : Column(
              children: [
                Expanded(
                  flex: 4,
                  child: Text(listModel.mojiPristigliZahtevi[0][0].toString()),
                ),
                Expanded(
                    flex: 1,
                    child: Row(
                      children: [
                        Expanded(
                          child: TextField(
                            controller: t1,
                          ),
                          flex: 5,
                        ),
                        Expanded(
                          child: TextField(
                            controller: t2,
                          ),
                          flex: 5,
                        ),
                        Expanded(
                            flex: 1,
                            child: ElevatedButton(
                              onPressed: () {
                                //print(t1.text + " " + t2.text);
                                //  listModel.getFavourites(BigInt.from(1));

                                t1.clear();
                                t2.clear();
                              },
                              child: Text("ADD user"),
                            ))
                      ],
                    )),
              ],
            ),
    );
  }
}
