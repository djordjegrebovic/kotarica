import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:kotarica/eth_models/KupovinaModels.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/eth_models/user_loginModels.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/widgets/cart/text_icon_button.dart';
import 'package:kotarica/widgets/productDetail/product_details.dart';
import 'package:kotarica/widgets/products/reviews.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class SmallOrderRecivedRepresent extends StatefulWidget {
  OrderRequest order;

  SmallOrderRecivedRepresent(this.order);
  @override
  _SmallOrderRecivedRepresentState createState() =>
      _SmallOrderRecivedRepresentState();
}

double fontSize800 = 21;
double fontSize400 = 13;
double fontSizeWidth = 0.037;

class _SmallOrderRecivedRepresentState
    extends State<SmallOrderRecivedRepresent> {
  @override
  Widget build(BuildContext context) {
    var kupovina = Provider.of<KupovinaModels>(context);
    var providerProizvodi = Provider.of<ProductModels>(context);
    var providerUsers = Provider.of<UserLoginModel>(context);
    var user = providerUsers.getUserNameAndSurname(widget.order.idKupca);
    var product = providerProizvodi.getProductById(widget.order.idProizvoda);
    double width = MediaQuery.of(context).size.width;
    double textWidth = (width < 400
        ? fontSize400
        : width >= 600
            ? fontSize800
            : width * fontSizeWidth);
    var boje = Theme.of(context).colorScheme;

    // double height = MediaQuery.of(context).size.height;
    var datumSlanjaPorudzbine;
    var datumKupovine;
    if (user == null) user = "Ne postoje informacije";
    if (providerProizvodi.isLoading && providerUsers.isLoading) {
      return Center(child: SpinKitFadingCube(size: 35, color: Colors.orange));
    } else {
      var status;
      if (widget.order.status == BigInt.from(0))
        status = "Na čekanju";
      else if (widget.order.status == BigInt.from(1)) {
        status = "Prihvaćen";
        var datum = widget.order.datum.toString();
        List<String> listaDatuma = datum.split("!");
        datumSlanjaPorudzbine = listaDatuma[0].toString();
        datumKupovine = listaDatuma[1].toString();
      } else {
        status = "Odbijen";
        var datum = widget.order.datum.toString();
        List<String> listaDatuma = datum.split("!");
        datumSlanjaPorudzbine = listaDatuma[0].toString();
        datumKupovine = listaDatuma[1].toString();
      }
      return GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, ProductDetails.routeName,
              arguments: ProductDetailsArguments(
                  product: providerProizvodi
                      .getProductById(widget.order.idProizvoda),
                  order: widget.order,
                  imaLiZahtevaZaKupovinu: 2));
        },
        child: Container(
          padding: EdgeInsets.only(
              top: 7,
              bottom: 7,
              left: 7,
              right: width < 580
                  ? 7
                  : width < 850
                      ? 18
                      : 20),
          decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.8),
              borderRadius: BorderRadius.circular(14)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                padding: EdgeInsets.symmetric(
                    vertical: 0, horizontal: width < 600 ? 0 : 10),
                decoration: BoxDecoration(
                    border: Border.all(width: 2, color: boje.secondary),
                    color: boje.onBackground,
                    borderRadius: BorderRadius.all(Radius.circular(7))),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(
                          vertical: 5, horizontal: width < 850 ? 5 : 10),
                      decoration: BoxDecoration(
                          color: boje.background, //Colors.green[200],
                          borderRadius: BorderRadius.all(Radius.circular(7))),

                      //Naziv i datum
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            constraints: BoxConstraints(minWidth: 121),
                            width: width < 850
                                ? (width -
                                    80 -
                                    textWidth -
                                    (width < 342
                                        ? 105
                                        : width < 400
                                            ? 110
                                            : width < 580
                                                ? 165
                                                : width < 800
                                                    ? 195
                                                    : width < 820
                                                        ? 215
                                                        : 245))
                                : 450,
                            decoration: BoxDecoration(
                                color: boje.onBackground,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4))),
                            padding: EdgeInsets.symmetric(
                                vertical: 3, horizontal: 5),
                            child: Text(
                              "${product.naziv}",
                              textAlign: TextAlign.center,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                fontSize: textWidth,
                                color: Color.fromARGB(255, 101, 155, 149),
                                //boje.secondary,
                                fontWeight: FontWeight.bold,
                              ),
                              maxLines: 2,
                            ),
                          ),
                          SizedBox(width: width < 400 ? 5 : 10),
                          Container(
                            decoration: BoxDecoration(
                                color: boje.onBackground,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4))),
                            padding: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 5),
                            child: Text(
                              "${widget.order.kolicina} ${product.mernaJedinica}",
                              style: TextStyle(
                                fontSize: textWidth,
                                fontWeight: FontWeight.w700,
                                color: boje.secondaryVariant,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    SizedBox(height: 5.0),
                    //Status, korisnicko ime, cena
                    Container(
                      padding: EdgeInsets.symmetric(
                          vertical: 5, horizontal: width < 530 ? 0 : 7),
                      decoration: BoxDecoration(
                          color: boje.background,
                          borderRadius: BorderRadius.all(Radius.circular(7))),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 4, horizontal: 5),
                            decoration: BoxDecoration(
                                color: boje.onBackground,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(7))),
                            child: Row(
                              children: [
                                Text(
                                  "Status: ",
                                  style: TextStyle(
                                      fontSize: textWidth,
                                      fontWeight: FontWeight.bold,
                                      color: boje.surface),
                                ),
                                Text(
                                  "$status",
                                  style: TextStyle(
                                    fontSize: textWidth,
                                    fontWeight: FontWeight.bold,
                                    color: widget.order.status == BigInt.from(0)
                                        ? Colors.orange[400]
                                        : widget.order.status == BigInt.from(1)
                                            ? Colors.green
                                            : Colors.red,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 5.0),
                          Container(
                            // width: width < 405
                            //     ? width * 0.53
                            //     : width < 585
                            //         ? width * 0.6
                            //         : null,
                            constraints: BoxConstraints(
                              minWidth: 169,
                              maxWidth: width < 405
                                  ? width * 0.53
                                  : width < 585
                                      ? width * 0.6
                                      : width * 0.6,
                            ),
                            margin: EdgeInsets.symmetric(horizontal: 4),

                            padding: EdgeInsets.symmetric(
                                vertical: 4, horizontal: 5),
                            decoration: BoxDecoration(
                                color: boje.onBackground,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(7))),
                            child: Text(
                              "Naručilac: $user",
                              maxLines: 2,
                              textAlign: TextAlign.center,
                              overflow: TextOverflow.ellipsis,
                              softWrap: true,
                              style: TextStyle(
                                  fontSize: textWidth,
                                  fontWeight: FontWeight.bold,
                                  color: boje.surface),
                            ),
                          ),
                          SizedBox(height: 5.0),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 4, horizontal: 5),
                            decoration: BoxDecoration(
                                color: boje.onBackground,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(7))),
                            child: Text(
                              "Ukupna cena: ${product.cena * widget.order.kolicina} RSD",
                              style: TextStyle(
                                  fontSize: textWidth,
                                  fontWeight: FontWeight.bold,
                                  color: boje.surface),
                            ),
                          ),
                          SizedBox(height: 5.0),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 4, horizontal: 5),
                            decoration: BoxDecoration(
                                color: boje.onBackground,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(7))),
                            child: Text(
                              "Datum porudžbine: " +
                                  (widget.order.status != BigInt.zero
                                      ? datumSlanjaPorudzbine.toString()
                                      : widget.order.datum),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                              style: TextStyle(
                                  fontSize: textWidth,
                                  fontWeight: FontWeight.bold,
                                  color: boje.surface),
                            ),
                          ),
                          SizedBox(height: 5.0),
                          widget.order.status != BigInt.zero
                              ? Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 4, horizontal: 5),
                                  decoration: BoxDecoration(
                                      color: boje.onBackground,
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(7))),
                                  child: Text(
                                    (widget.order.status == BigInt.from(1)
                                            ? "Datum kupovine: "
                                            : "Datum odbijanja: ") +
                                        (widget.order.status != BigInt.zero
                                            ? datumKupovine
                                                .toString()
                                                .toString()
                                            : widget.order.datum),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                    style: TextStyle(
                                        fontSize: textWidth,
                                        fontWeight: FontWeight.bold,
                                        color: boje.surface),
                                  ),
                                )
                              : SizedBox(),
                          SizedBox(
                              height:
                                  widget.order.status != BigInt.zero ? 5 : 0),
                        ],
                      ),
                    ),
                    // SizedBox(height: 3),
                  ],
                ),
              ),
              SizedBox(width: 5),
              (this.widget.order.status == BigInt.zero)
                  ? Container(
                      decoration: BoxDecoration(
                          color: boje.background,
                          borderRadius: BorderRadius.circular(5)),
                      padding: EdgeInsets.symmetric(
                          vertical: width < 400 ? 10 : 16,
                          horizontal: width < 400 ? 5 : 8),
                      // color: Colors.blue,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          TextAndIconButton(
                            text: "Potvrdi",
                            press: () async {
                              var now = new DateTime.now();
                              var formatter = new DateFormat('dd.MM.yyyy.');
                              String formattedDate = formatter.format(now);

                              await kupovina.changeReqStatus(
                                widget.order.id,
                                BigInt.from(1),
                                "",
                                formattedDate,
                              );
                              setState(() {
                                widget.order.status = BigInt.from(1);
                              });
                            },
                            icon: Icons.verified,
                            buttonColor: Color.fromARGB(255, 101, 155, 149),
                            textColor: Colors.white,
                            buttonWidth: width < 400
                                ? 85
                                : width > 850
                                    ? 140
                                    : 111,
                            buttonHeight: width > 850
                                ? 45
                                : width < 400
                                    ? 30
                                    : 40,
                            fontSize: width > 850
                                ? 24
                                : width < 400
                                    ? 13
                                    : 18,
                            iconSize: width > 850
                                ? 25
                                : width < 400
                                    ? 14
                                    : 20,
                            radius: 7,
                          ),
                          SizedBox(height: width < 400 ? 12 : 20),
                          TextAndIconButton(
                            text: "Otkaži",
                            press: () async {
                              var now = new DateTime.now();
                              var formatter = new DateFormat('dd.MM.yyyy.');
                              String formattedDate = formatter.format(now);

                              await kupovina.changeReqStatus(
                                widget.order.id,
                                BigInt.from(2),
                                "",
                                formattedDate,
                              );
                              setState(() {
                                widget.order.status = BigInt.from(2);
                              });
                            },
                            icon: Icons.dangerous,
                            buttonColor: Colors.red[300],
                            textColor: Colors.white,
                            buttonWidth: width < 400
                                ? 85
                                : width > 850
                                    ? 140
                                    : 111,
                            buttonHeight: width > 850
                                ? 45
                                : width < 400
                                    ? 30
                                    : 40,
                            fontSize: width > 850
                                ? 24
                                : width < 400
                                    ? 13
                                    : 18,
                            iconSize: width > 850
                                ? 25
                                : width < 400
                                    ? 14
                                    : 20,
                            radius: 7,
                          ),
                        ],
                      ),
                    )
                  : SizedBox()
            ],
          ),
        ),
      );
    }
  }
}

// ignore: must_be_immutable
class SmallOrderSentRepresent extends StatefulWidget {
  OrderRequest order;
  SmallOrderSentRepresent(this.order);
  @override
  _SmallOrderSentRepresentState createState() =>
      _SmallOrderSentRepresentState();
}

class _SmallOrderSentRepresentState extends State<SmallOrderSentRepresent> {
  @override
  Widget build(BuildContext context) {
    var kupovina = Provider.of<KupovinaModels>(context);
    var providerProizvodi = Provider.of<ProductModels>(context);
    var providerUsers = Provider.of<UserLoginModel>(context);
    var user = providerUsers.getUserNameAndSurname(widget.order.idOglasivaca);
    var product = providerProizvodi.getProductById(widget.order.idProizvoda);
    double width = MediaQuery.of(context).size.width;
    var boje = Theme.of(context).colorScheme;
    double textWidth = (width < 400
        ? fontSize400
        : width >= 600
            ? fontSize800
            : width * fontSizeWidth);
    var datumSlanjaPorudzbine;
    var datumKupovine;
    if (user == null) user = "Ne postoje informacije";
    if (providerProizvodi.isLoading &&
        providerUsers.isLoading &&
        kupovina.isLoading) {
      return Center(child: SpinKitFadingCube(size: 35, color: Colors.orange));
    } else {
      var status;
      if (widget.order.status == BigInt.from(0))
        status = "Na čekanju";
      else if (widget.order.status == BigInt.from(1)) {
        status = "Prihvaćen";
        var datum = widget.order.datum.toString();
        List<String> listaDatuma = datum.split("!");
        datumSlanjaPorudzbine = listaDatuma[0].toString();
        datumKupovine = listaDatuma[1].toString();
      } else {
        status = "Odbijen";
        var datum = widget.order.datum.toString();
        List<String> listaDatuma = datum.split("!");
        datumSlanjaPorudzbine = listaDatuma[0].toString();
        datumKupovine = listaDatuma[1].toString();
      }
      return GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, ProductDetails.routeName,
              arguments: ProductDetailsArguments(
                  product: providerProizvodi
                      .getProductById(widget.order.idProizvoda),
                  order: widget.order,
                  imaLiZahtevaZaKupovinu: 1));
        },
        child: Container(
          padding: EdgeInsets.all(7),
          decoration: BoxDecoration(
              color: boje.secondary, borderRadius: BorderRadius.circular(14)),
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 8),
            decoration: BoxDecoration(
                color: boje.onBackground,
                borderRadius: BorderRadius.circular(14)),
            child: Column(
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                  decoration: BoxDecoration(
                      color: boje.background, //Colors.green[200],
                      borderRadius: BorderRadius.all(Radius.circular(7))),
                  // color: Colors.green[200],
                  //Naziv i datum
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        constraints: BoxConstraints(
                          minWidth: width < 400
                              ? 160
                              : width < 500
                                  ? 220
                                  : 280,
                          maxWidth: width < 800
                              ? (width -
                                  30 -
                                  textWidth -
                                  (width < 400 ? 90 : 130))
                              : 530,
                        ),
                        // width: width < 800
                        //     ? (width -
                        //         30 -
                        //         textWidth -
                        //         (width < 400 ? 90 : 130))
                        //     : 530,
                        decoration: BoxDecoration(
                            color: boje.onBackground,
                            borderRadius: BorderRadius.all(Radius.circular(4))),
                        padding:
                            EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                        child: Text(
                          "${product.naziv}",
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: textWidth,
                            color: Color.fromARGB(255, 101, 155, 149),
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 2,
                        ),
                      ),
                      SizedBox(width: 10),
                      Container(
                        decoration: BoxDecoration(
                            color: boje.onBackground,
                            borderRadius: BorderRadius.all(Radius.circular(4))),
                        padding:
                            EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                        child: Text(
                          "${widget.order.kolicina} ${product.mernaJedinica}",
                          style: TextStyle(
                            fontSize: textWidth,
                            fontWeight: FontWeight.w700,
                            color: boje.secondaryVariant,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

                SizedBox(height: 5.0),
                //Status, korisnicko ime, cena
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  decoration: BoxDecoration(
                      color: boje.background,
                      borderRadius: BorderRadius.all(Radius.circular(7))),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                        decoration: BoxDecoration(
                            color: boje.onBackground,
                            borderRadius: BorderRadius.all(Radius.circular(7))),
                        child: Text.rich(
                          TextSpan(
                            text: "Status: ",
                            style: TextStyle(
                                fontSize: textWidth,
                                fontWeight: FontWeight.bold,
                                color: Theme.of(context).colorScheme.surface),
                            children: [
                              TextSpan(
                                text: "$status",
                                style: TextStyle(
                                  fontSize: textWidth,
                                  fontWeight: FontWeight.bold,
                                  color: widget.order.status == BigInt.from(0)
                                      ? Colors.orange[400]
                                      : widget.order.status == BigInt.from(1)
                                          ? Colors.green
                                          : Colors.red,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 5.0),
                      Container(
                        // alignment: Alignment.center,
                        constraints: BoxConstraints(
                          minWidth: 169,
                          maxWidth: width < 405
                              ? width * 0.53
                              : width < 650
                                  ? width * 0.6
                                  : width * 0.5,
                        ),
                        padding:
                            EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                        decoration: BoxDecoration(
                            color: boje.onBackground,
                            borderRadius: BorderRadius.all(Radius.circular(7))),
                        child: Text(
                          "Oglašavač: $user",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: textWidth,
                              fontWeight: FontWeight.bold,
                              color: boje.surface),
                        ),
                      ),
                      SizedBox(height: 5.0),
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                        decoration: BoxDecoration(
                            color: boje.onBackground,
                            borderRadius: BorderRadius.all(Radius.circular(7))),
                        child: Text(
                          "Ukupna cena: ${product.cena * widget.order.kolicina} RSD",
                          style: TextStyle(
                              fontSize: textWidth,
                              fontWeight: FontWeight.bold,
                              color: boje.surface),
                        ),
                      ),
                      SizedBox(height: 5.0),
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                        decoration: BoxDecoration(
                            color: boje.onBackground,
                            borderRadius: BorderRadius.all(Radius.circular(7))),
                        child: Text(
                          "Datum porudžbine: " +
                              (widget.order.status != BigInt.zero
                                  ? datumSlanjaPorudzbine.toString()
                                  : widget.order.datum),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: TextStyle(
                              fontSize: textWidth,
                              fontWeight: FontWeight.bold,
                              color: boje.surface),
                        ),
                      ),
                      SizedBox(
                          height: widget.order.status != BigInt.zero ? 5 : 0),
                      widget.order.status != BigInt.zero
                          ? Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 4, horizontal: 8),
                              decoration: BoxDecoration(
                                  color: boje.onBackground,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(7))),
                              child: Text(
                                (widget.order.status == BigInt.from(1)
                                        ? "Datum kupovine: "
                                        : "Datum odbijanja: ") +
                                    (widget.order.status != BigInt.zero
                                        ? datumKupovine.toString().toString()
                                        : widget.order.datum),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                                style: TextStyle(
                                    fontSize: textWidth,
                                    fontWeight: FontWeight.bold,
                                    color: boje.surface),
                              ),
                            )
                          : SizedBox(),
                      // SizedBox(
                      //     height: widget.order.status != BigInt.zero ? 5 : 0),
                    ],
                  ),
                ),
                SizedBox(height: 5),
                (this.widget.order.status == BigInt.zero)
                    ? Container(
                        child: TextAndIconButton(
                          text: "Otkaži",
                          press: () {
                            setState(() {
                              kupovina.deleteReq(widget.order.id);
                            });
                          },
                          icon: Icons.dangerous,
                          buttonColor: Colors.red[400],
                          textColor: Colors.white,
                          buttonWidth: width < 400
                              ? 100
                              : width > 600
                                  ? 140
                                  : 110,
                          buttonHeight: width > 600
                              ? 40
                              : width < 400
                                  ? 33
                                  : 35,
                          fontSize: width > 600
                              ? 22
                              : width < 400
                                  ? 15
                                  : 18,
                          iconSize: width > 600
                              ? 23
                              : width < 400
                                  ? 17
                                  : 20,
                          radius: 7,
                        ),
                      )
                    : SizedBox(),
                (widget.order.status == BigInt.from(1) &&
                        widget.order.ocenjen == false)
                    ? Container(
                        child: TextAndIconButton(
                          text: "Ocenite proizvod",
                          press: () {
                            Navigator.push(
                                context,
                                new MaterialPageRoute(
                                    builder: (context) => Scaffold(
                                        appBar: AppBar(
                                          centerTitle: true,
                                          title: Text("Ocenite proizvod",
                                              style: TextStyle(
                                                  color: Boje.mainText,
                                                  fontWeight: FontWeight.bold)),
                                        ),
                                        body: ReviewWidget(widget.order))));
                          },
                          icon: Icons.rate_review,
                          buttonColor: Colors.orange[300],
                          textColor: Colors.white,
                          buttonWidth: width < 400
                              ? 180
                              : width > 600
                                  ? 240
                                  : 210,
                          buttonHeight: width > 600
                              ? 40
                              : width < 400
                                  ? 33
                                  : 35,
                          fontSize: width > 600
                              ? 22
                              : width < 400
                                  ? 15
                                  : 18,
                          iconSize: width > 600
                              ? 23
                              : width < 400
                                  ? 17
                                  : 20,
                          radius: 7,
                        ),
                      )
                    : SizedBox()
              ],
            ),
          ),
          // (this.widget.order.status == BigInt.zero)
          //     ? Container(
          //         child: Column(
          //           mainAxisAlignment: MainAxisAlignment.spaceAround,
          //           children: [
          //             TextAndIconButton(
          //               text: "Otkaži",
          //               press: () {
          //                 setState(() {
          //                   kupovina.deleteReq(widget.order.id);
          //                 });
          //               },
          //               icon: Icons.dangerous,
          //               buttonColor: Colors.red[700],
          //               textColor: Colors.white,
          //               buttonWidth: width < 400
          //                   ? 100
          //                   : width > 850
          //                       ? 140
          //                       : 110,
          //               buttonHeight: width > 850
          //                   ? 40
          //                   : width < 525
          //                       ? 30
          //                       : 35,
          //               fontSize: width > 850
          //                   ? 24
          //                   : width < 400
          //                       ? 13
          //                       : 18,
          //               iconSize: width > 850
          //                   ? 25
          //                   : width < 400
          //                       ? 14
          //                       : 20,
          //               radius: 7,
          //             ),
          //           ],
          //         ),
          //       )
          //     : SizedBox(),
          // (widget.order.status == BigInt.from(1) &&
          //         widget.order.ocenjen == false)
          //     ? Container(
          //         child: Row(
          //           mainAxisAlignment: MainAxisAlignment.spaceAround,
          //           children: [
          //             TextAndIconButton(
          //               text: "Ocenite proizvod",
          //               press: () {
          //                 Navigator.push(
          //                     context,
          //                     new MaterialPageRoute(
          //                         builder: (context) => Scaffold(
          //                             appBar: AppBar(
          //                               centerTitle: true,
          //                               title: Text("Ocenite proizvod"),
          //                             ),
          //                             body: ReviewWidget(widget.order))));
          //               },
          //               icon: Icons.rate_review,
          //               buttonColor: Colors.orange[300],
          //               textColor: Colors.white,
          //               buttonWidth: width < 400
          //                   ? 180
          //                   : width > 850
          //                       ? 260
          //                       : 210,
          //               buttonHeight: width > 850
          //                   ? 40
          //                   : width < 525
          //                       ? 30
          //                       : 35,
          //               fontSize: width > 850
          //                   ? 24
          //                   : width < 400
          //                       ? 13
          //                       : 18,
          //               iconSize: width > 850
          //                   ? 25
          //                   : width < 400
          //                       ? 14
          //                       : 20,
          //               radius: 7,
          //             )
          //           ],
          //         ),
          //       )
          //     : SizedBox()
          //   ],
          // ),
        ),
      );
    }
  }
}
