import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:kotarica/eth_models/KupovinaModels.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/widgets/orders/small_order_preview.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class SentRequestsProductListHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var listModel = Provider.of<KupovinaModels>(context);
    double width = MediaQuery.of(context).size.width;

    if (listModel.isLoading) {
      return Center(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 15, bottom: 20),
              child: Text(
                "Poslati zahtevi za kupovinu se učitavaju",
                style: TextStyle(
                    fontSize: width < 800 ? 18 : 20,
                    color: Theme.of(context).colorScheme.onPrimary),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: SpinKitFadingCube(
                  size: 35, color: Theme.of(context).colorScheme.primary),
            ),
          ],
        ),
      );
    } else {
      if (listModel.isLoading1) {
        listModel.getMyRequests(GetInfo.id);
        return Center(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 15, bottom: 20),
                child: Text(
                  "Poslati zahtevi za kupovinu se učitavaju",
                  style: TextStyle(
                      fontSize: width < 800 ? 18 : 20,
                      color: Theme.of(context).colorScheme.onPrimary),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: SpinKitFadingCube(
                    size: 35, color: Theme.of(context).colorScheme.primary),
              ),
            ],
          ),
        );
      } else {
        listModel.isLoading1 = true;

        var lista = listModel.getMyRequestedOrderRequest();

        if (lista.length != 0) {
          return SizedBox(
            width: width >= 800 ? 800 : width,
            child: ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: lista.length,
              itemBuilder: (BuildContext context, int index) => Padding(
                padding: EdgeInsets.symmetric(horizontal: 6, vertical: 6),
                child: SmallOrderSentRepresent(lista[index]),
              ),
            ),
          );
        } else {
          return Container(
            child: Center(child: Text("Nema poslatih zahteva za kupovinu")),
          );
        }
      }
    }
  }

  // child: Text(" ${lista[index].id} ${lista[index].idProizvoda} \n" +
  //     " ${lista[index].idKupca} ${lista[index].kolicina} \n" +
  //     " ${lista[index].idOglasivaca} ${lista[index].status} \n" +
  //     "${lista[index].poruka} ${lista[index].datum}"),
}
