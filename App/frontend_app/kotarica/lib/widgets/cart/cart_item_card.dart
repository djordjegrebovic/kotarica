import 'package:flutter/material.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/utils/const.dart';
import 'package:kotarica/widgets/cart/custom_stepper.dart';
// import 'package:kotarica/widgets/images/image_load.dart';
import 'package:kotarica/widgets/productDetail/add.dart';
import 'package:kotarica/widgets/productDetail/product_details.dart';
import 'package:provider/provider.dart';

// KARTICA PORIZVODA U KORPI
// ignore: must_be_immutable
class CartItemCard extends StatelessWidget {
  CartItemCard({
    Key key,
    @required this.product,
    @required this.index,
    this.stepper = true,
  }) : super(key: key);

  final int index;
  final int value = 1;
  final Product product;
  bool stepper;
  @override
  Widget build(BuildContext context) {
    var addListener = Provider.of<AddModel>(context);
    double width = MediaQuery.of(context).size.width;

    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, ProductDetails.routeName,
            arguments: ProductDetailsArguments(product: this.product));
      },
      child: Container(
        // color: Colors.yellow,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: [
                SizedBox(
                  // Siva pozadina sa slikom proizvoda
                  width: width < 515 ? width * 0.23 : 120,
                  child: AspectRatio(
                    aspectRatio: 1,
                    child: Container(
                      padding: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                        color: Theme.of(context).colorScheme.onBackground,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(15),
                          child: this.product.hesh == ""
                              ? Image.asset(this.product.images[0])
                              : Image(
                                  image: NetworkImage(
                                      Const.url + "/api/Image/" + product.hesh),
                                  fit: BoxFit.fill,
                                )),
                    ),
                  ),
                ),
                SizedBox(width: width < 600 ? width * 0.03 : 15),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // NAZIV PROIZVODA
                    Container(
                      // color: Colors.red,
                      constraints: BoxConstraints(
                          maxWidth: stepper == true
                              ? (width < 506
                                  ? width * 0.403
                                  : width < 550
                                      ? width * 0.45
                                      : width <= 612
                                          ? width * 0.48
                                          : width <= 655
                                              ? width * 0.54
                                              : width <= 850
                                                  ? width * 0.57
                                                  : width <= 1000
                                                      ? width * 0.35
                                                      : 394)
                              : (width < 540
                                  ? width * 0.675
                                  : width < 620
                                      ? width * 0.71
                                      : width < 850
                                          ? width * 0.75
                                          : width < 928
                                              ? width * 0.49
                                              : width < 1000
                                                  ? width * 0.54
                                                  : 540)),
                      child: Text(
                        product.naziv,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: width < 600
                              ? width * 0.035
                              : width > 800
                                  ? 23
                                  : 21,
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).colorScheme.onPrimary,
                        ),
                        maxLines: 2,
                      ),
                    ),
                    SizedBox(height: 10.0),
                    //RED SA CENOM KOLICINOM I UKUPNOM CENOM ZA PROIZVOD
                    Container(
                      // color: Colors.green,
                      child: Row(
                        children: [
                          Text(
                            "${product.cena} rsd/${product.mernaJedinica}",
                            style: TextStyle(
                                fontSize: width < 600
                                    ? width * 0.035
                                    : width <= 1000
                                        ? 20
                                        : 22,
                                fontWeight: FontWeight.bold,
                                color: Boje.loginRegisterButton),
                          ),
                          Text(
                            " x${product.quantity}",
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.surface,
                              fontSize: width < 600
                                  ? width * 0.035
                                  : width <= 1000
                                      ? 18
                                      : 20,
                            ),
                          ),
                          width >= 600 //|| stepper == false
                              ? Container(
                                  constraints: BoxConstraints(
                                      maxWidth: stepper == true ? 210 : 280),
                                  // color: stepper == true
                                  //     ? Colors.red
                                  //     : Colors.green,
                                  width: width < 630
                                      ? width * 0.27
                                      : width <= 850
                                          ? width * 0.25
                                          : width < 900
                                              ? width * 0.17
                                              : width <= 1000
                                                  ? 195
                                                  : 210,
                                  child: Text(
                                    " = ${addListener.getProductSubtotal(product)} RSD",
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontSize: width <= 1000 ? 22 : 24,
                                        fontWeight: FontWeight.bold,
                                        color: Boje.loginRegisterButton),
                                  ),
                                )
                              : SizedBox()
                        ],
                      ),
                    ),
                    width < 600 //&& stepper == true
                        ? Container(
                            constraints:
                                BoxConstraints(maxWidth: width * 0.404),
                            margin: EdgeInsets.only(top: 4),
                            child: Text(
                              "Ukupno: ${addListener.getProductSubtotal(product)} RSD",
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: width * 0.04,
                                  fontWeight: FontWeight.bold,
                                  color: Boje.loginRegisterButton),
                            ),
                          )
                        : SizedBox()
                  ],
                ),
              ],
            ),
            stepper
                ? CustomStepper(
                    product: product,
                    index: index,
                    value: this.value,
                    onChange: (value) {
                      addListener.setProductQty(product, value);
                    },
                  )
                : SizedBox()
          ],
        ),
      ),
    );
  }
}
