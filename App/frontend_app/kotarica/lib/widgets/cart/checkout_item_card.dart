// import 'package:flutter/material.dart';
// import 'package:kotarica/eth_models/ProductModels.dart';
// import 'package:kotarica/utils/boje.dart';
// import 'package:kotarica/utils/const.dart';
// import 'package:kotarica/widgets/productDetail/add.dart';
// import 'package:provider/provider.dart';

// Color text = Colors.teal[400];

// class CheckoutItemCard extends StatelessWidget {
//   const CheckoutItemCard({
//     Key key,
//     // ignore: non_constant_identifier_names
//     @required this.product_,
//     @required this.index,
//   }) : super(key: key);

//   final int index;
//   final int value = 1;
//   // ignore: non_constant_identifier_names
//   final Product product_;

// // Kartica proizvoda u korpi
//   @override
//   Widget build(BuildContext context) {
//     // var test_listener = Provider.of<CartFunctions>(context);
//     var addListener = Provider.of<AddModel>(context);
//     // test_listener.setActiveProduct(this.product_);
//     return Row(
//       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//       children: <Widget>[
//         Row(
//           children: [
//             SizedBox(
//               // Siva pozadina sa slikom proizvoda
//               width: MediaQuery.of(context).size.width * 0.23,
//               child: AspectRatio(
//                 aspectRatio: 1,
//                 child: Container(
//                     padding: EdgeInsets.all(5.0),
//                     decoration: BoxDecoration(
//                       color: Boje.textColor,
//                       borderRadius: BorderRadius.circular(15),
//                     ),
//                     child: ClipRRect(
//                       borderRadius: BorderRadius.circular(15),
//                       child: this.product_.hesh == ""
//                           ? Image.asset(this.product_.images[0])
//                           : Image(
//                               image: NetworkImage(
//                                   Const.url + "/api/Image/" + product_.hesh),
//                               fit: BoxFit.fill,
//                             ),
//                     )),
//               ),
//             ),
//             SizedBox(
//               width: MediaQuery.of(context).size.width * 0.03,
//             ),
//             Column(
//               // Text pored slike
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 Container(
//                   width: MediaQuery.of(context).size.width * 0.37,
//                   child: Text(
//                     product_.naziv,
//                     style: TextStyle(
//                       fontSize: MediaQuery.of(context).size.width * 0.035,
//                       color: Boje.textColor,
//                     ),
//                     maxLines: 2,
//                   ),
//                 ),
//                 const SizedBox(height: 10.0),
//                 Text.rich(
//                   TextSpan(
//                     text: "\$${product_.cena}",
//                     style: TextStyle(
//                         fontSize: MediaQuery.of(context).size.width * 0.035,
//                         fontWeight: FontWeight.bold,
//                         color: Colors.orange),
//                     children: [
//                       TextSpan(
//                         text: " x${product_.quantity} = ",
//                         style: TextStyle(
//                             color: Colors.grey,
//                             fontSize:
//                                 MediaQuery.of(context).size.width * 0.035),
//                       ),
//                       TextSpan(
//                         // text: "\$${test_listener.getProductSubtotal(product_)}",
//                         text: "\$${addListener.getProductSubtotal(product_)}",
//                         style: TextStyle(
//                             fontSize: MediaQuery.of(context).size.width * 0.04,
//                             fontWeight: FontWeight.bold,
//                             color: Colors.orange),
//                       )
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//           ],
//         ),
//       ],
//     );
//   }
// }
