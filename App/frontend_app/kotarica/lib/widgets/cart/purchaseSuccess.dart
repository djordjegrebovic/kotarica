import 'package:flutter/material.dart';
import 'package:kotarica/main.dart';
import 'package:kotarica/widgets/LoginSingin/default_button.dart';

class PurchaseSuccessArguments {
  String text;

  PurchaseSuccessArguments({this.text});
}

// ignore: must_be_immutable
class PurchaseSuccessBody extends StatelessWidget {
  // static String routeName = '/uspesno_odradjen_dogadjaj';

  String text;
  PurchaseSuccessBody(String text) {
    this.text = text;
  }
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    // final PurchaseSuccessArguments args =
    //     ModalRoute.of(context).settings.arguments;

    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Container(
            color: Theme.of(context).colorScheme.onBackground,
            width: width < 500 ? width : 500,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: 20),
                Image(
                  height: 300,
                  width: width,
                  image: AssetImage("assets/images/success.png"),
                ),
                SizedBox(height: 80),
                Container(
                  width: width < 500 ? width * 0.9 : 450,
                  child: Text(
                    text,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: width < 700 ? 26 : 30,
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).colorScheme.onPrimary,
                    ),
                  ),
                ),
                // SizedBox(height: 90),
                Spacer(),
                DefaultButton(
                  width: width < 500 ? width * 0.9 : 400,
                  text: "Nazad na početnu",
                  press: () {
                    Navigator.pushNamed(context, MyApp.routeName);

                    // Navigator.push(context,
                    //     new MaterialPageRoute(builder: (context) => MyApp()));
                  },
                ),
                SizedBox(height: 50),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
