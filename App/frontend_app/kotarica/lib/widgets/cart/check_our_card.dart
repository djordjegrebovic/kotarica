import 'package:flutter/material.dart';
import 'package:kotarica/eth_models/KupovinaModels.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/pages/checkoutPage.dart';
import 'package:kotarica/pages/customLoading.dart';
import 'package:kotarica/pages/welcome_screen.dart';
import 'package:kotarica/widgets/cart/purchaseSuccess.dart';
import 'package:kotarica/widgets/productDetail/add.dart';
// import 'package:kotarica/widgets/cart/checkout/checkout_page.dart';
import 'package:intl/intl.dart';
import 'package:kotarica/widgets/user/profileDescription.dart';
import 'package:provider/provider.dart';
import '../../main.dart';
import '../../redirektNaLogin.dart';
import 'text_icon_button.dart';

Color text = Colors.teal[400];
Color appBarColor = Colors.grey[300];

class CheckOurCard extends StatefulWidget {
  const CheckOurCard({
    Key key,
    @required this.text,
    @required this.icon,
  }) : super(key: key);

  final String text;
  final IconData icon;
  @override
  _CheckOurCardState createState() => _CheckOurCardState();
}

class _CheckOurCardState extends State<CheckOurCard> {
  bool isLoadingKupovina = false;
  bool nesto = false;
  int nesto2 = 0;

  @override
  Widget build(BuildContext context) {
    // var testListener = Provider.of<CartFunctions>(context);
    var add_Listener = Provider.of<AddModel>(context);
    var provider = Provider.of<KupovinaModels>(context);
    double width = MediaQuery.of(context).size.width;

    uradiNesto();

    return isLoadingKupovina == false
        ? Container(
            // BottomMeni
            padding: EdgeInsets.symmetric(
              vertical: 20,
              horizontal: width < 500 ? 15 : 20,
            ),
            decoration: BoxDecoration(
              color: Theme.of(context)
                  .colorScheme
                  .onBackground, //Color.fromARGB(255, 219, 207,186),
              //Theme.of(context).colorScheme.primary, //Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30), topRight: Radius.circular(30)),
              boxShadow: [
                BoxShadow(
                  offset: Offset(0, -5),
                  blurRadius: 5,
                  color: Theme.of(context).colorScheme.surface.withOpacity(0.4),
                ),
              ],
            ),
            child: SafeArea(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    // (Total: i Dugme)
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      // Text.rich(
                      //   TextSpan(
                      //     text: "Ukupno za placanje:\n",
                      //     style: TextStyle(fontSize: 18),
                      //     children: [
                      //       TextSpan(
                      //         text:
                      //             '\$${add_Listener.baskets != null ? add_Listener.getBasketSum() : 0}',
                      //         style: TextStyle(
                      //           fontWeight: FontWeight.bold,
                      //           fontSize: 22,
                      //           color: Colors.orange[800],
                      //         ),
                      //       )
                      //     ],
                      //   ),
                      // ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text("Ukupno za plaćanje:",
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Theme.of(context).colorScheme.surface,
                                  fontWeight: FontWeight.w700)),
                          SizedBox(height: 5),
                          Container(
                            child: Text(
                              '${add_Listener.baskets != null ? add_Listener.getBasketSum() : 0} RSD',
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 22,
                                color: Color.fromARGB(255, 193, 76, 47),
                              ),
                            ),
                          ),
                        ],
                      ),
                      TextAndIconButton(
                        text: this.widget.text,
                        textColor: Theme.of(context).colorScheme.onBackground,
                        icon: this.widget.icon,
                        // buttonColor: Color.fromARGB(255, 101, 155, 140),
                        buttonColor: Color.fromARGB(255, 101, 155, 149),
                        buttonWidth: width < 400
                            ? 110
                            : width > 850
                                ? 130
                                : 115,
                        buttonHeight: width > 850 ? 45 : 37,
                        fontSize: width > 850 ? 24 : 20,
                        iconSize: width > 850 ? 25 : 23,
                        press: () {
                          if (GetInfo.korpa == false) {
                            if (add_Listener.baskets.length == 0)
                              showWarning(context);
                            else {
                              if (MyApp.postojiJWT) {
                                GetInfo.korpa = true;
                                Navigator.pushNamed(
                                    context, Checkout.routeName);
                                // Navigator.push(
                                //     context,
                                //     new MaterialPageRoute(
                                //         builder: (context) => Checkout()));
                              } else {
                                Navigator.pushNamed(
                                    context, RedirektNaLogin.routeName);
                                // Navigator.push(
                                //     context,
                                //     new MaterialPageRoute(
                                //         builder: (context) =>
                                //             RedirektNaLogin()));
                              }
                            }
                          } else {
                            showAlert(BuildContext context) {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text('Pošaljite zahtev.'),
                                    content: Text(
                                        "Da li ste sigurni da želite da pošaljete zahtev za kupovinu ? Vaš račun iznosi : " +
                                            add_Listener
                                                .getBasketSum()
                                                .toString() +
                                            ' RSD'),
                                    actions: <Widget>[
                                      FlatButton(
                                        child: Text("DA"),
                                        onPressed: () async {
                                          setState(() {
                                            nesto = true;
                                            nesto2 = 1;
                                            Navigator.of(context).pop();
                                          });
                                        },
                                      ),
                                      FlatButton(
                                        child: Text("NE"),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                    ],
                                  );
                                },
                              );
                            }

                            if (add_Listener.baskets.length != 0)
                              showAlert(context);
                            else
                              showWarning(context);
                          }
                        },
                      ),
                    ],
                  )
                ],
              ),
            ),
          )
        : CustomProgressIndicator(
            'Molimo, sačekajte. Vaš zahtev za kupovinu se šalje.');
  }

  void uradiNesto() async {
    if (nesto == true && nesto2 == 1) {
      nesto2 = 0;
      var add_Listener = Provider.of<AddModel>(context);
      var provider = Provider.of<KupovinaModels>(context);
      var now = new DateTime.now();
      var formatter = new DateFormat('dd.MM.yyyy.');
      String formattedDate = formatter.format(now);

      /*UKOLIKO JE PROMENJEN USER DATA ONDA GA MENJAM*/
      GetInfo.korpa = false;
      setState(() {
        isLoadingKupovina = true;
      });

      print(
          "******************** SALJEM ZAHTEV ZA KUPOVINU ********************");
      //ovde pravim zahtev za kupovinu
      for (var i = 0; i < add_Listener.baskets.length; i++) {
        await provider.createRequest(
          add_Listener.baskets[i].id,
          GetInfo.id,
          add_Listener.baskets[i].cena,
          DataToBeShownCheckout.adresa1 == null
              ? DataToBeShownCheckout.adresa
              : DataToBeShownCheckout.adresa1,
          DataToBeShownCheckout.brojTelefona1 == null
              ? DataToBeShownCheckout.brojTelefona
              : DataToBeShownCheckout.brojTelefona1,
          BigInt.from(add_Listener.baskets[i].quantity),
          add_Listener.baskets[i].idOglasavaca,
          formattedDate.toString(),
        );
        print(
            "${add_Listener.baskets[i].id}  ${GetInfo.id}  ${add_Listener.baskets[i].quantity} ${formattedDate.toString()} ${add_Listener.baskets[i].cena} ${DataToBeShownCheckout.adresa1 == null ? DataToBeShownCheckout.adresa : DataToBeShownCheckout.adresa1} ${DataToBeShownCheckout.brojTelefona1 == null ? DataToBeShownCheckout.brojTelefona : DataToBeShownCheckout.brojTelefona1} ");
      }

      setState(() {
        DataToBeShownCheckout.brojTelefona1 = null;
        DataToBeShownCheckout.adresa1 = null;

        print(
            "******************** ZAVRSAVAM ZAHTEV ZA KUPOVINU ********************\n");
        //GetInfo.isLoadingKupovina = false;

        add_Listener.baskets.clear();
        // Navigator.pushNamed(context, PurchaseSuccessBody.routeName,
        //     arguments: PurchaseSuccessArguments(
        //         text: "Uspešno ste poslali zahteve za kupovinu."));
        Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (context) => PurchaseSuccessBody(
                    "Uspešno ste poslali zahteve za kupovinu.")));
      });
    }
  }

  showWarning(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Korpa je prazna.'),
          content: Text(
              "Pre kupovine korpa ne moze biti prazna. Vrati me na pocetnu stranicu ? "),
          actions: <Widget>[
            FlatButton(
              child: Text("Da"),
              onPressed: () {
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => WelcomeScreen()));
              },
            ),
            FlatButton(
              child: Text("Otkaži"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
