import 'package:flutter/material.dart';
import 'package:kotarica/eth_models/KupovinaModels.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/pages/checkoutPage.dart';
import 'package:kotarica/pages/customLoading.dart';
import 'package:kotarica/pages/welcome_screen.dart';
import 'package:kotarica/widgets/cart/purchaseSuccess.dart';
import 'package:kotarica/widgets/productDetail/add.dart';
import 'package:intl/intl.dart';
import 'package:kotarica/widgets/user/profileDescription.dart';
import 'package:provider/provider.dart';
import '../../main.dart';
import '../../redirektNaLogin.dart';
import 'text_icon_button.dart';

class TotalAndCheckoutButton extends StatefulWidget {
  const TotalAndCheckoutButton({
    Key key,
    @required this.text,
    @required this.icon,
  }) : super(key: key);

  final String text;
  final IconData icon;
  @override
  _TotalAndCheckoutButtonState createState() => _TotalAndCheckoutButtonState();
}

class _TotalAndCheckoutButtonState extends State<TotalAndCheckoutButton> {
  bool isLoadingKupovina = false;
  bool nesto = false;
  int nesto2 = 0;

  @override
  Widget build(BuildContext context) {
    // var testListener = Provider.of<CartFunctions>(context);
    var addListener = Provider.of<AddModel>(context);
    // var provider = Provider.of<KupovinaModels>(context);
    uradiNesto();
    double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height;

    return isLoadingKupovina == false
        ? Container(
            alignment: Alignment.topRight,
            // color: Colors.green,
            height: 210,
            width: 235,
            margin: EdgeInsets.only(
                top: 20, left: width < 1000 ? 10 : 35, right: 10),
            padding: EdgeInsets.symmetric(
              vertical: 25,
              horizontal: 15,
            ),
            decoration: BoxDecoration(
              color: Theme.of(context)
                  .colorScheme
                  .onBackground, //Color.fromARGB(255, 219, 207,186),
              //Theme.of(context).colorScheme.primary, //Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  "Ukupno za plaćanje:",
                  style: TextStyle(
                      fontSize: 21,
                      color: Theme.of(context).colorScheme.secondaryVariant,
                      fontWeight: FontWeight.w700),
                ),
                SizedBox(height: 10),
                Text(
                  '${addListener.baskets != null ? addListener.getBasketSum() : 0} RSD',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 22,
                    color: Color.fromARGB(255, 193, 76, 47),
                  ),
                ),
                SizedBox(height: 34),
                TextAndIconButton(
                  text: this.widget.text,
                  textColor: Theme.of(context).colorScheme.onBackground,
                  icon: this.widget.icon,
                  buttonColor: Theme.of(context).colorScheme.primary,
                  buttonHeight: width > 850 ? 45 : 37,
                  fontSize: width > 850 ? 24 : 20,
                  iconSize: width > 850 ? 25 : 23,
                  buttonWidth: width * 0.35,
                  press: () {
                    if (GetInfo.korpa == false) {
                      if (addListener.baskets.length == 0)
                        showWarning(context);
                      else {
                        if (MyApp.postojiJWT) {
                          GetInfo.korpa = true;
                          Navigator.pushNamed(context, Checkout.routeName);
                          // Navigator.push(
                          //     context,
                          //     new MaterialPageRoute(
                          //         builder: (context) => Checkout()));
                        } else {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => RedirektNaLogin()));
                        }
                      }
                    } else {
                      showAlert(BuildContext context) {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: Text('Pošaljite zahtev.'),
                              content: Text(
                                  "Da li ste sigurni da želite da pošaljete zahtev za kupovinu ? Vaš račun iznosi: " +
                                      addListener.getBasketSum().toString() +
                                      " RSD"),
                              actions: <Widget>[
                                // ignore: deprecated_member_use
                                FlatButton(
                                  child: Text("DA"),
                                  onPressed: () async {
                                    setState(() {
                                      nesto = true;
                                      nesto2 = 1;
                                      Navigator.of(context).pop();
                                    });
                                  },
                                ),
                                // ignore: deprecated_member_use
                                FlatButton(
                                  child: Text("NE"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      }

                      if (addListener.baskets.length != 0)
                        showAlert(context);
                      else
                        showWarning(context);
                    }
                  },
                ),
              ],
            ),
          )
        : CustomProgressIndicator(
            'Molimo, sačekajte. Vaš zahtev za kupovinu se šalje.');
  }

  void uradiNesto() async {
    if (nesto == true && nesto2 == 1) {
      nesto2 = 0;
      var addListener = Provider.of<AddModel>(context);
      var provider = Provider.of<KupovinaModels>(context);
      var now = new DateTime.now();
      var formatter = new DateFormat('dd.MM.yyyy.');
      String formattedDate = formatter.format(now);

      /*UKOLIKO JE PROMENJEN USER DATA ONDA GA MENJAM*/
      GetInfo.korpa = false;
      setState(() {
        isLoadingKupovina = true;
      });

      print(
          "******************** SALJEM ZAHTEV ZA KUPOVINU ********************");
      //ovde pravim zahtev za kupovinu
      for (var i = 0; i < addListener.baskets.length; i++) {
        await provider.createRequest(
          addListener.baskets[i].id,
          GetInfo.id,
          addListener.baskets[i].cena,
          DataToBeShownCheckout.adresa1 == null
              ? DataToBeShownCheckout.adresa
              : DataToBeShownCheckout.adresa1,
          DataToBeShownCheckout.brojTelefona1 == null
              ? DataToBeShownCheckout.brojTelefona
              : DataToBeShownCheckout.brojTelefona1,
          BigInt.from(addListener.baskets[i].quantity),
          addListener.baskets[i].idOglasavaca,
          formattedDate.toString(),
        );
        print(
            "${addListener.baskets[i].id}  ${GetInfo.id}  ${addListener.baskets[i].quantity} ${formattedDate.toString()} ${addListener.baskets[i].cena} ${DataToBeShownCheckout.adresa1 == null ? DataToBeShownCheckout.adresa : DataToBeShownCheckout.adresa1} ${DataToBeShownCheckout.brojTelefona1 == null ? DataToBeShownCheckout.brojTelefona : DataToBeShownCheckout.brojTelefona1} ");
      }

      setState(() {
        DataToBeShownCheckout.brojTelefona1 = null;
        DataToBeShownCheckout.adresa1 = null;

        print(
            "******************** ZAVRSAVAM ZAHTEV ZA KUPOVINU ********************\n");
        //GetInfo.isLoadingKupovina = false;

        addListener.baskets.clear();
        // Navigator.pushNamed(context, PurchaseSuccessBody.routeName,
        //     arguments: PurchaseSuccessArguments(
        //         text: "Uspešno ste poslali zahteve za kupovinu."));
        Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (context) => PurchaseSuccessBody(
                    "Uspešno ste poslali zahteve za kupovinu.")));
      });
    }
  }

  showWarning(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Korpa je prazna.'),
          content: Text(
              "Pre kupovine korpa ne može biti prazna. Vrati me na početnu stranicu ? "),
          actions: <Widget>[
            // ignore: deprecated_member_use
            FlatButton(
              child: Text("Da"),
              onPressed: () {
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => WelcomeScreen()));
              },
            ),
            // ignore: deprecated_member_use
            FlatButton(
              child: Text("Otkaži"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
