import 'package:flutter/material.dart';

class TextAndIconButton extends StatelessWidget {
  const TextAndIconButton({
    Key key,
    @required this.text,
    @required this.press,
    @required this.icon,
    @required this.buttonColor,
    @required this.textColor,
    @required this.fontSize,
    this.buttonWidth,
    @required this.buttonHeight,
    @required this.iconSize,
    this.radius = 15,
  }) : super(key: key);
  final String text;
  final Function press;
  final IconData icon;
  final Color buttonColor;
  final Color textColor;
  final double fontSize;
  final double iconSize;
  final double buttonWidth;
  final double buttonHeight;
  final double radius;
  @override
  Widget build(BuildContext context) {
    // double height = MediaQuery.of(context).size.height;
    // double width = MediaQuery.of(context).size.width;

    return Container(
      width: buttonWidth, //width * 0.35,
      height: buttonHeight, //width > 850 ? 45 : height * 0.06,
      // ignore: deprecated_member_use
      child: FlatButton(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(radius)),
        color: buttonColor, //Theme.of(context).colorScheme.secondaryVariant,
        splashColor: Theme.of(context).colorScheme.secondary,
        onPressed: press,
        child: Wrap(
          spacing: 5,
          alignment: WrapAlignment.center,
          crossAxisAlignment: WrapCrossAlignment.center,
          children: [
            Text(
              text,
              textAlign: TextAlign.center,
              style: TextStyle(
                  letterSpacing: 1.2,
                  fontSize: fontSize, //width > 850 ? 24 : height * 0.025,
                  color:
                      textColor, //Theme.of(context).colorScheme.onBackground,
                  fontWeight: FontWeight.w600),
            ),
            Icon(
              icon,
              color: textColor, //Theme.of(context).colorScheme.onBackground,
              size: iconSize, // width > 850 ? 25 : height * 0.03,
            ),
          ],
        ),
      ),
    );
  }
}
