import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/widgets/productDetail/add.dart';
import 'package:provider/provider.dart';
import 'package:kotarica/widgets/productDetail/rounded_icon_button.dart';

// Dugmici za povecavanje i smanjivanje broja proizvoda u korpi
class CustomStepper extends StatefulWidget {
  @override
  CustomStepper({
    @required this.product,
    @required this.value,
    @required this.onChange,
    @required this.index,
  });

  final Product product;
  final int index;
  final int value;
  final ValueChanged<dynamic> onChange;

  State<StatefulWidget> createState() {
    return _CustomStepperState();
  }
}

class _CustomStepperState extends State<CustomStepper> {
  @override
  Widget build(BuildContext context) {
    var addListener = Provider.of<AddModel>(context);
    double width = MediaQuery.of(context).size.width;
    double width600 = 500;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 6.0),
      child: Container(
        height: width > width600
            ? width <= 1000
                ? 43
                : 50
            : width * 0.09,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(30))),
        child: Row(
          children: [
            RoundedIconBtn(
              text: '',
              icon: Icons.remove,
              showShadow: false,
              height: width > width600
                  ? width <= 1000
                      ? 40
                      : 48
                  : width * 0.08,
              width: width > width600
                  ? width <= 1000
                      ? 40
                      : 48
                  : width * 0.08,
              size: width > width600
                  ? width <= 1000
                      ? 30
                      : 37
                  : width * 0.065,
              iconColor: Colors.white,
              buttonColor: Theme.of(context).colorScheme.primary,
              press: () {
                addListener.removeOneItemFromBasket(this.widget.product);
              },
            ),
            SizedBox(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 3),
                child: Text(
                  '${addListener.baskets[this.widget.index].quantity}',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.secondary,
                    fontSize: width > width600
                        ? width <= 1000
                            ? 25
                            : 27
                        : width * 0.05,
                  ),
                ),
              ),
            ),
            RoundedIconBtn(
              text: '',
              icon: Icons.add,
              showShadow: false,
              height: width > width600
                  ? width <= 1000
                      ? 40
                      : 48
                  : width * 0.08,
              width: width > width600
                  ? width <= 1000
                      ? 40
                      : 48
                  : width * 0.08,
              size: width > width600
                  ? width <= 1000
                      ? 30
                      : 37
                  : width * 0.065,
              iconColor: Colors.white,
              buttonColor: Theme.of(context).colorScheme.primary,
              press: () {
                // setState(() {
                addListener.addOneItemToBasket(this.widget.product);
                //});
              },
            ),
          ],
        ),
      ),
    );
  }
}
