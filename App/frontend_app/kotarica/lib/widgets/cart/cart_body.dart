import 'package:flutter/material.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/pages/welcome_screen.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/widgets/cart/cart_item_card.dart';
import 'package:kotarica/widgets/cart/totalAndCheckoutButton.dart';
import 'package:kotarica/widgets/productDetail/add.dart';
import 'package:provider/provider.dart';

class CartBody extends StatefulWidget {
  @override
  _CartBodyState createState() => _CartBodyState();
}

//Definisanje dela za brisanje kartice kad skrolujemo sa desna na levo
class _CartBodyState extends State<CartBody> {
  Color color = Colors.green;

  @override
  Widget build(BuildContext context) {
    var addListener = Provider.of<AddModel>(context);
    double width = MediaQuery.of(context).size.width;
    int br = addListener.baskets.length;
    return addListener.baskets.length > 0
        ? Center(
            //Glavni kontejner
            child: Container(
              alignment: Alignment.topCenter,
              width: width < 1000
                  ? width > 850
                      ? width
                      : width
                  : 1000,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    decoration: width > 850
                        ? BoxDecoration(
                            border: Border.all(
                              color: Colors.white,
                            ),
                            borderRadius: BorderRadius.circular(10),
                            color: Theme.of(context)
                                .colorScheme
                                .onBackground
                                .withOpacity(0.7),
                          )
                        : null,
                    margin: EdgeInsets.only(
                      top: width > 850 ? 20 : 0,
                      bottom: width > 850 ? 20 : 0,
                      left: width > 850
                          ? width < 1000
                              ? 10
                              : 20
                          : 0,
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    // color: Colors.blue,
                    width: width > 850
                        ? width < 1000
                            ? width - 270
                            : 700
                        : width,
                    height: width <= 850
                        ? null
                        : br == 1
                            ? 144
                            : br == 2
                                ? 144 * 2 - 12
                                : br == 3
                                    ? 144 * 3 - 20
                                    : 144 * 4,
                    // height: width > 850
                    //     ? 500
                    //     //add_Listener.baskets.length * width < 600
                    //     //     ? width * 0.23
                    //     //     : 120 + (add_Listener.baskets.length * 12)
                    //     : null,
                    child: ListView.builder(
                      itemCount: addListener.baskets.length,
                      scrollDirection: Axis.vertical,
                      itemBuilder: (context, index) => Padding(
                        padding: EdgeInsets.only(top: 12.0),
                        child: Dismissible(
                            key: Key(addListener.baskets[index].id.toString()),
                            direction: DismissDirection.endToStart,
                            background: Container(
                              padding: EdgeInsets.symmetric(horizontal: 20.0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15.0),
                                color: Colors.red[600],
                              ),
                              child: Row(
                                children: [
                                  Spacer(),
                                  Icon(
                                    Icons.delete,
                                    size: MediaQuery.of(context).size.height *
                                        0.07,
                                    color: Colors.white,
                                  )
                                ],
                              ),
                            ),
                            onDismissed: (direction) {
                              setState(() {
                                addListener.removeItemFromBasket(
                                    addListener.baskets[index]);
                              });
                            },
                            confirmDismiss: (direction) {
                              return showDialog(
                                context: context,
                                builder: (context) => AlertDialog(
                                  title: Text('Da li ste sigurni?'),
                                  content: Text(
                                      'Da li zelite da uklonite ovaj proizvod iz korpe?'),
                                  actions: [
                                    // ignore: deprecated_member_use
                                    FlatButton(
                                      child: Text('Ne'),
                                      onPressed: () {
                                        Navigator.of(context).pop(false);
                                      },
                                    ),
                                    // ignore: deprecated_member_use
                                    FlatButton(
                                      child: Text('Da'),
                                      onPressed: () {
                                        Navigator.of(context).pop(true);
                                      },
                                    )
                                  ],
                                ),
                              );
                            },
                            child: CartItemCard(
                                product: addListener.baskets[index],
                                index: index)),
                      ),
                    ),
                  ),
                  width > 850
                      ? TotalAndCheckoutButton(
                          text: "Račun", icon: Icons.fact_check_outlined)
                      : SizedBox(),
                ],
              ),
            ),
          )
        //Ako je korpa prazna
        : emptyCart(width, context);
  }

  Center emptyCart(double width, BuildContext context) {
    return Center(
      child: Container(
          width: width > 800 ? 800 : width,
          padding: EdgeInsets.only(top: 35),
          child: Column(
            children: [
              Icon(Icons.shopping_cart_outlined,
                  size: width > 500 ? 250 : width * 0.5,
                  color: Colors.grey[700]),
              Text(
                "Vaša korpa je prazna",
                style: TextStyle(
                    fontSize: width > 500 ? 25 : 20, color: Colors.grey[800]),
              ),
              SizedBox(height: 20),
              width > 500
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Nemate proizvode u korpi ",
                            style: TextStyle(
                                fontSize: width > 800 ? 20 : 18,
                                color: Colors.grey[800])),
                        InkWell(
                          onTap: () {
                            setState(() {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => WelcomeScreen()),
                              );
                            });
                          },
                          child: Text(
                            "Vratite se na početnu stranicu",
                            style: TextStyle(
                              fontSize: width > 800 ? 18 : 16,
                              color: Boje.loginRegisterButton,
                              decoration: TextDecoration.underline,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                      ],
                    )
                  : Column(
                      // mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Nemate proizvode u korpi ",
                            style: TextStyle(fontSize: width > 800 ? 20 : 16)),
                        SizedBox(height: 2),
                        InkWell(
                          onTap: () {
                            setState(() {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => WelcomeScreen()),
                              );
                            });
                          },
                          child: Text(
                            "Vratite se na početnu stranicu",
                            style: TextStyle(
                              fontSize: width > 800 ? 18 : 16,
                              color: Boje.loginRegisterButton,
                              decoration: TextDecoration.underline,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                      ],
                    ),
            ],
          )),
    );
  }
}
