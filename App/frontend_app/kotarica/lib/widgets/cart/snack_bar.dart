import 'package:flutter/material.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/widgets/productDetail/add.dart';

// ignore: non_constant_identifier_names
void snackBar_basket(
    BuildContext context, AddModel cartProvider, Product product) {
  double width = MediaQuery.of(context).size.width;
  ScaffoldMessenger.of(context).hideCurrentSnackBar();
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      backgroundColor: Colors.teal,
      elevation: width < 800 ? null : 6,
      width: width < 800 ? null : 800,
      behavior: width < 800 ? null : SnackBarBehavior.floating,
      content: Text(
          'Dodali ste 1${product.mernaJedinica} proizvoda: "${product.naziv}" u korpu!',
          style: TextStyle(
            color: Theme.of(context).colorScheme.onBackground,
            fontSize: width < 800 ? 15 : 18,
          )),
      duration: Duration(seconds: 2),
      action: SnackBarAction(
        label: 'PONIŠTI',
        textColor: Theme.of(context).colorScheme.onBackground,
        onPressed: () {
          cartProvider.removeOneItemFromBasket(product);
        },
      ),
    ),
  );
}

void snackBarMessage(BuildContext context) {
  double width = MediaQuery.of(context).size.width;

  ScaffoldMessenger.of(context).hideCurrentSnackBar();
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      width: width < 800 ? null : 600,
      behavior: width < 800 ? null : SnackBarBehavior.floating,
      backgroundColor: Colors.teal,
      content: Text('Pitanje je uspešno postavljeno.',
          style: TextStyle(
            color: Theme.of(context).colorScheme.onBackground,
            fontSize: width < 800 ? 15 : 18,
          )),
      duration: Duration(seconds: 2),
    ),
  );
}

// ignore: non_constant_identifier_names
void snackBar_qty_basket(
    BuildContext context, AddModel cartProvider, Product product, int qty) {
  double width = MediaQuery.of(context).size.width;

  ScaffoldMessenger.of(context).hideCurrentSnackBar();
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      width: width < 800 ? null : 800,
      behavior: width < 800 ? null : SnackBarBehavior.floating,
      backgroundColor: Colors.teal,
      content: Text(
          'Dodali ste $qty${product.mernaJedinica} proizvoda: "${product.naziv}" u korpu!',
          style: TextStyle(
            color: Theme.of(context).colorScheme.onBackground,
            fontSize: width < 800 ? 15 : 18,
          )),
      duration: Duration(seconds: 2),
      action: SnackBarAction(
        label: 'PONIŠTI',
        textColor: Theme.of(context).colorScheme.onBackground,
        onPressed: () {
          cartProvider.removeQty(product, qty);
        },
      ),
    ),
  );
}

// ignore: non_constant_identifier_names
void snackBar_add_favourite(
    BuildContext context, ProductModels listModel, Product product) {
  double width = MediaQuery.of(context).size.width;

  ScaffoldMessenger.of(context).hideCurrentSnackBar();
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      // elevation: width < 800 ? null : 156,
      width: width < 800 ? null : 600,
      behavior: width < 800 ? null : SnackBarBehavior.floating,

      // margin: EdgeInsets.only(bottom: width < 800 ? 0 : 10),

      backgroundColor: Colors.red[600],
      content: Text('Dodali ste proizvod u omiljene!',
          style: TextStyle(
            color: Theme.of(context).colorScheme.onBackground,
            fontSize: width < 800 ? 15 : 18,
          )),
      duration: Duration(seconds: 2),
      action: SnackBarAction(
        label: 'PONIŠTI',
        textColor: Colors.white,
        onPressed: () {
          product.isFavourite = !product.isFavourite;
          listModel.deleteFavourites(product.id, GetInfo.id);
        },
      ),
    ),
  );
}
