import 'package:flutter/material.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/eth_models/RecenzijeModels.dart';
import 'package:kotarica/eth_models/user_loginModels.dart';
import 'package:kotarica/utils/const.dart';
import 'package:kotarica/widgets/productDetail/product_details.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class ShowReviewWidget extends StatefulWidget {
  Recenzije product;
  // ignore: unused_field
  bool _favourite;

  ShowReviewWidget(Recenzije product, bool favourite) {
    this.product = product;
    _favourite = favourite;
  }

  @override
  _ShowReviewWidgetState createState() => _ShowReviewWidgetState();
}

class _ShowReviewWidgetState extends State<ShowReviewWidget> {
  // DOBIJANJE VISINE WIDGETA PREKO GLOBALNOG KLJUCA !!!
  // final GlobalKey _reviewKey = GlobalKey();
  // Size reviewSize;
  // Offset reviewPosition;
  // @override
  // void initState() {
  //   super.initState();
  //   WidgetsBinding.instance.addPostFrameCallback((_) => getSizeOfReview());
  //   // getSizeOfReview();
  // }

  // getSizeOfReview() {
  //   RenderBox _reviewBox = _reviewKey.currentContext.findRenderObject();
  //   reviewSize = _reviewBox.size;
  //   // reviewPosition = _reviewBox.localToGlobal(Offset.zero); // POZICIJA WIDGETA
  //   // print("REVIEW REVIEW REVIEW REVIEW REVIEW    ");
  //   // print(reviewSize);
  //   // print(reviewSize.width);
  //   // print(reviewSize.height);
  //   setState(() {});
  // }

  @override
  Widget build(BuildContext context) {
    var providerUsers = Provider.of<UserLoginModel>(context);

    var listModel = Provider.of<ProductModels>(context);
    double width = MediaQuery.of(context).size.width;
    var user = providerUsers.getUserNameAndSurname(widget.product.idKupca);
    var boje = Theme.of(context).colorScheme;
    UserData data = providerUsers.vratiInformacijeOKorisniku(widget.product.idKupca);
    if (listModel.isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else
      return GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, ProductDetails.routeName,
              arguments: ProductDetailsArguments(
                  product:
                      listModel.getProductById(widget.product.idProizvoda)));
        },
        child: Container(
          // key: _reviewKey, // Postavljamo Globalni kljuc u widget ciju visunu hocemo da uzmemo
          padding: EdgeInsets.all(7),
          decoration: BoxDecoration(
              border: Border.all(
                color: Theme.of(context).colorScheme.secondary,
                width: 2,
              ),
              color: Theme.of(context).colorScheme.background,
              borderRadius: BorderRadius.circular(3)),
          child: Column(
            // crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // DODATI SLIKU I IME KO JE POSTAVIO KOMENTAR, OCENU, DATUM KOMENTARISANJA,
              // NAZIV PROIZVODA
              // KOMENTAR
              //Red sa slikom i imenom i datumom
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  //Red sa slikom i imenom
                  Row(
                    children: [
                      SizedBox(
                        height: 50,
                        width: 50,
                        child: Center(
                          child: Stack(
                            clipBehavior: Clip.none,
                            children: [
                              data.profilePictureHash != ""
                                  ? CircleAvatar(
                                      radius: 75,
                                      child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          child: listModel.isLoading3 == true
                                              ? CircularProgressIndicator()
                                              : Image(
                                                  image: NetworkImage(
                                                      Const.url +
                                                          "/api/Image/" +
                                                          data.profilePictureHash
                                                              ),
                                                          width: 800,
                                                          height: 800,
                                                  fit: BoxFit.fill,
                                                )),
                                    )
                                  : CircleAvatar(
                                      radius: 75,
                                      backgroundImage: NetworkImage(
                                          "https://images.megapixl.com/1796/17965459.jpg"),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(73),
                                      ),
                                    ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(width: 5),
                      Text(
                        "$user",
                        style: TextStyle(
                            fontSize: width < 500 ? width * 0.043 : 21,
                            fontWeight: FontWeight.bold,
                            color: boje.surface),
                      ),
                    ],
                  ),
                  Text(
                    widget.product.datum,
                    style: TextStyle(
                        fontSize: width < 500 ? width * 0.043 : 21,
                        fontWeight: FontWeight.bold,
                        color: boje.surface),
                  ),
                ],
              ),
              SizedBox(height: 3),
              // Ime Proizvoda
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    // color: Colors.red,
                    width: width < 800 ? width - 110 : 680,
                    child: Text(
                      widget.product.nazivProizvoda,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize: width < 500 ? width * 0.043 : 21,
                        color: Theme.of(context).colorScheme.onPrimary,
                        fontWeight: FontWeight.bold,
                      ),
                      maxLines: 2,
                    ),
                  ),
                  Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 14, vertical: 5),
                    decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.onBackground,
                      borderRadius: BorderRadius.circular(14),
                    ),
                    child: Row(
                      children: [
                        Text(
                          "${widget.product.ocena}",
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.surface,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(width: 5),
                        Icon(Icons.star,
                            color: Theme.of(context).colorScheme.primary)
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: 5.0),
              // // Kategorija
              // Container(
              //   alignment: Alignment.centerLeft,
              //   child: Text(
              //     "${widget.product.kategorijaProizvoda}",
              //     style: TextStyle(
              //       fontSize: width < 500 ? width * 0.037 : 19,
              //       fontWeight: FontWeight.w700,
              //       color: Theme.of(context).colorScheme.secondaryVariant,
              //     ),
              //   ),
              // ),
              // SizedBox(height: 5.0),
              // Ocena i komentar
              Container(
                padding: EdgeInsets.symmetric(vertical: 2, horizontal: 5),
                decoration: BoxDecoration(
                    color: Theme.of(context).colorScheme.onBackground,
                    borderRadius: BorderRadius.all(Radius.circular(7))),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // Text(
                    //   "Proizvod je ocenjen ocenom : ${widget.product.ocena}",
                    //   style: TextStyle(
                    //       fontSize: width < 500 ? width * 0.043 : 21,
                    //       fontWeight: FontWeight.bold,
                    //       color: Theme.of(context).colorScheme.surface),
                    // ),
                    Container(
                      width: width < 400
                          ? width - 30
                          : width < 800
                              ? width - 30
                              : 770,
                      padding:
                          EdgeInsets.symmetric(vertical: 10, horizontal: 7),
                      child: Text(
                        "${widget.product.komentar}",
                        maxLines: 16,
                        textAlign: TextAlign.justify,
                        overflow: TextOverflow.ellipsis,
                        softWrap: true,
                        style: TextStyle(
                            fontSize: width < 500 ? width * 0.039 : 19,
                            fontWeight: FontWeight.bold,
                            color: Theme.of(context).colorScheme.surface),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
  }
}
