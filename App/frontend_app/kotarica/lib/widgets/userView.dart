import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';

class UserView extends StatefulWidget {
  UserView({Key key}) : super(key: key);

  @override
  _UserViewState createState() => _UserViewState();
}

class _UserViewState extends State<UserView> {
  Widget build(BuildContext context) {
    return SizedBox(
      width: 100,
      height: 100,
      child: Container(
        child: Center(
            child: Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  children: <Widget>[
                    Icon(Icons.person),
                    Text(
                      "Ime korisnika",
                      textAlign: TextAlign.center,
                    ),
                  ],
                ))),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor, //Colors.blue,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10),
              topRight: Radius.circular(10),
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3),
            ),
          ],
        ),
      ),
    ).onTap(() {
      return showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: new Text("Stranica korisnika."),
              actions: <Widget>[
                // ignore: deprecated_member_use
                FlatButton(
                  child: new Text("OK"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          });
    });
  }
}
