import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:kotarica/eth_models/PorukeModels.dart';
import 'package:kotarica/eth_models/user_loginModels.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/utils/const.dart';
import 'package:provider/provider.dart';

import 'conversationsList.dart';

class RecievedMsgs extends StatefulWidget {
  @override
  _RecievedMsgsState createState() => _RecievedMsgsState();
}

class _RecievedMsgsState extends State<RecievedMsgs> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    var porukeMODEL = Provider.of<PorukeModel>(context);
    var userModel = Provider.of<UserLoginModel>(context);
    var boje = Theme.of(context).colorScheme;

    var listaKorisnika;
    if (porukeMODEL.isLoading) {
      return SpinKitFadingCube(size: 35, color: boje.primary);
    } else {
      if (porukeMODEL.isLoadingDajChatHeads) {
        porukeMODEL.getChatHeads(GetInfo.id);
        return SpinKitFadingCube(size: 35, color: boje.primary);
      } else {
        porukeMODEL.isLoadingDajChatHeads = true;
        if (PorukeModel.vrtiPonovo == true) {
          porukeMODEL.izLoadingCelaKonverzacija = true;
          PorukeModel.vrtiPonovo = false;
        }

        listaKorisnika = porukeMODEL.dajMiKorisnikeKojiSuMmiPoslaliPoruku();
        print(listaKorisnika);
        if (porukeMODEL.isLoadingZadnjePoruke && listaKorisnika != null) {
          if (listaKorisnika != null)
            porukeMODEL.getLastMsgs(
                GetInfo.id,
                listaKorisnika,
                listaKorisnika != null
                    ? BigInt.from(listaKorisnika.length)
                    : BigInt.zero);
          else
            porukeMODEL.isLoadingZadnjePoruke = false;
          return SpinKitFadingCube(size: 35, color: boje.primary);
        } else {
          List<ZadnjePoruke> zadnjePoruke;
          // print(listaKorisnika);
          if (listaKorisnika != null)
            zadnjePoruke = porukeMODEL.dajMiZadnjePorukeUListu();

          return Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: Icon(Icons.arrow_back_ios, color: Boje.mainText),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              title: Text("Poruke",
                  style: TextStyle(
                      color: Boje.mainText, fontWeight: FontWeight.bold)),
              centerTitle: true,
            ),
            body: Center(
              child: Container(
                color: boje.onBackground,
                alignment: Alignment.topCenter,
                width: width < 800 ? width : 800,
                child: ListView.builder(
                  itemCount: listaKorisnika != null ? listaKorisnika.length : 0,
                  shrinkWrap: true,
                  padding: EdgeInsets.only(top: 16),
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) {
                    return ConversationList(
                      name: UserLoginModel
                              .usersDataList[listaKorisnika[index].toInt() - 1]
                              .name +
                          " " +
                          UserLoginModel
                              .usersDataList[listaKorisnika[index].toInt() - 1]
                              .surname,
                      isMessageRead: false,
                      messageText: zadnjePoruke.length > 0
                          ? zadnjePoruke[index].poruka
                          : "",
                      time: zadnjePoruke.length > 0
                          ? zadnjePoruke[index].datum
                          : "",
                      idKorisnika: listaKorisnika[index],
                      imageUrl: UserLoginModel
                                  .usersDataList[
                                      listaKorisnika[index].toInt() - 1]
                                  .profilePictureHash ==
                              ""
                          ? "https://connectnigeria.com/articles/wp-content/uploads/2016/03/work-from-home-on-the-internet.jpg"
                          : Const.url +
                              "/api/Image/" +
                              UserLoginModel
                                  .usersDataList[
                                      listaKorisnika[index].toInt() - 1]
                                  .profilePictureHash,
                    );
                  },
                ),
              ),
            ),
            // body: ListView.builder(
            //   shrinkWrap: true,
            //   physics: NeverScrollableScrollPhysics(),
            //   itemCount: listaKorisnika.length,
            //   itemBuilder: (BuildContext context, int index) => Padding(
            //       padding: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
            //       child: Column(
            //         children: [
            //           ChatHeadPreview(
            //               imeIPrezimeKorisnika: UserLoginModel
            //                       .usersDataList[
            //                           listaKorisnika[index].toInt() - 1]
            //                       .name +
            //                   " " +
            //                   UserLoginModel
            //                       .usersDataList[
            //                           listaKorisnika[index].toInt() - 1]
            //                       .surname,
            //               idKorisnika: listaKorisnika[index]),
            //         ],
            //       )),
            // ),
          );
        }
      }
    }
  }
}
