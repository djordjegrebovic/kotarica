import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:kotarica/eth_models/PorukeModels.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:provider/provider.dart';

import '../../AllInfoBuilder.dart';

class ChatFeed extends StatefulWidget {
  @override
  _ChatFeed_state createState() => _ChatFeed_state();
  BigInt id;
  String imeIPrezime;
  ChatFeed({this.id, this.imeIPrezime});
}

class PrikaziDatumPoruke {
  int idPoruke;
  bool prikazi;
  PrikaziDatumPoruke({this.idPoruke, this.prikazi});
}

class _ChatFeed_state extends State<ChatFeed> {
  final _controller = ScrollController();
  final TextEditingController _poruka = TextEditingController();
  List<PrikaziDatumPoruke> prikaziDatumZaPoruku =
      // ignore: deprecated_member_use
      new List<PrikaziDatumPoruke>();
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    var now = new DateTime.now();
    var formatter = new DateFormat('dd.MM.yyyy.');
    var boje = Theme.of(context).colorScheme;

    String formattedDate = formatter.format(now);
    Timer(
      Duration(seconds: 1),
      () => _controller.animateTo(
        _controller.position.maxScrollExtent,
        duration: Duration(seconds: 1),
        curve: Curves.fastOutSlowIn,
      ),
    );

    var porukeMODEL = Provider.of<PorukeModel>(context);
    List<Poruke> listaKorisnika;
    if (porukeMODEL.isLoading) {
      return SpinKitFadingCube(size: 35, color: boje.primary);
    } else {
      if (porukeMODEL.izLoadingCelaKonverzacija) {
        porukeMODEL.getWholeConversation(widget.id, GetInfo.id);
        return AllInfos.uCetu == false
            ? SpinKitFadingCube(size: 35, color: boje.primary)
            : SizedBox();
      } else {
        if (PorukeModel.vrtiPonovo == true) {
          porukeMODEL.izLoadingCelaKonverzacija = true;
          PorukeModel.vrtiPonovo = false;
        }
        listaKorisnika = porukeMODEL.getWholeConverationInList(GetInfo.id);
        if (listaKorisnika != null)
          for (var i = 0; i < listaKorisnika.length; i++) {
            prikaziDatumZaPoruku
                .add(new PrikaziDatumPoruke(idPoruke: i, prikazi: false));
          }
        // porukeMODEL.izLoadingCelaKonverzacija = false;
        return Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios, color: Boje.mainText),
              onPressed: () {
                porukeMODEL.izLoadingCelaKonverzacija = true;
                porukeMODEL.isLoadingDajChatHeads = true;
                AllInfos.uCetu = false;
                porukeMODEL.isLoadingZadnjePoruke = true;
                Navigator.pop(context);
              },
            ),
            title: Text(widget.imeIPrezime,
                style: TextStyle(
                    color: Boje.mainText, fontWeight: FontWeight.bold)),
            centerTitle: true,
          ),
          body: Center(
            child: Container(
              color: Theme.of(context).colorScheme.onBackground,
              width: width < 800 ? width : 800,
              child: Stack(
                children: <Widget>[
                  SingleChildScrollView(
                    controller: _controller,
                    padding: EdgeInsets.only(bottom: 60),
                    child: ListView.builder(
                      itemCount: listaKorisnika.length,
                      shrinkWrap: true,
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return Container(
                          padding: EdgeInsets.only(
                              left: 14, right: 14, top: 3, bottom: 3),
                          child: Align(
                            alignment: (listaKorisnika[index].oglasavac == false
                                ? Alignment.topLeft
                                : Alignment.topRight),
                            child: GestureDetector(
                              onTap: () {
                                if (prikaziDatumZaPoruku[index].prikazi ==
                                    false) {
                                  setState(() {
                                    prikaziDatumZaPoruku[index].prikazi = true;
                                  });
                                } else
                                  setState(() {
                                    prikaziDatumZaPoruku[index].prikazi = false;
                                  });
                              },
                              child: Column(
                                crossAxisAlignment:
                                    listaKorisnika[index].oglasavac == false
                                        ? CrossAxisAlignment.start
                                        : CrossAxisAlignment.end,
                                children: [
                                  Container(
                                    constraints: BoxConstraints(
                                        maxWidth:
                                            width < 800 ? width * 0.7 : 600),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20),
                                      color: (listaKorisnika[index].oglasavac ==
                                              true
                                          ? boje.background
                                          : Color.fromARGB(255, 101, 155, 149)),
                                    ),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 14, vertical: 10),
                                    child: Text(
                                      listaKorisnika[index].poruka,
                                      style: TextStyle(
                                          fontSize: 15,
                                          color:
                                              listaKorisnika[index].oglasavac ==
                                                      true
                                                  ? Colors.black
                                                  : Colors.white,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  prikaziDatumZaPoruku != null &&
                                          prikaziDatumZaPoruku[index].prikazi ==
                                              true
                                      ? SizedBox(height: 3)
                                      : SizedBox(),
                                  prikaziDatumZaPoruku != null &&
                                          prikaziDatumZaPoruku[index].prikazi ==
                                              true
                                      ? Container(
                                          alignment:
                                              listaKorisnika[index].oglasavac ==
                                                      false
                                                  ? Alignment.topLeft
                                                  : Alignment.topRight,
                                          child:
                                              Text(listaKorisnika[index].datum))
                                      : SizedBox()
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Container(
                      padding: EdgeInsets.only(left: 10, bottom: 10, top: 10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20)),
                        color: boje.onSecondary,
                      ),
                      height: 60,
                      width: double.infinity,
                      child: Row(
                        children: <Widget>[
                          SizedBox(width: 5),
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: boje.onBackground,
                              ),
                              padding: EdgeInsets.only(
                                  left: 15, right: 15, bottom: 9),
                              child: TextField(
                                controller: _poruka,
                                autofocus: true,
                                decoration: InputDecoration(
                                    hintText: "Napiši poruku...",
                                    hintStyle: TextStyle(
                                      color: Colors.grey[600],
                                      //fromARGB(255, 101, 155, 149),
                                    ),
                                    border: InputBorder.none),
                              ),
                            ),
                          ),
                          SizedBox(width: 10),
                          FloatingActionButton(
                            onPressed: () async {
                              AllInfos.uCetu = true;
                              await porukeMODEL.createMsg(
                                  BigInt.zero,
                                  widget.id,
                                  GetInfo.id,
                                  _poruka.text,
                                  formattedDate.toString());

                              listaKorisnika.add(new Poruke(
                                  idProizvoda: BigInt.zero,
                                  idKupca: widget.id,
                                  idOglasavaca: GetInfo.id,
                                  poruka: _poruka.text,
                                  nazivProizvoda: ""));
                              _poruka.clear();
                              porukeMODEL.izLoadingCelaKonverzacija = true;
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          super.widget));
                            },
                            child:
                                Icon(Icons.send, color: Colors.white, size: 18),
                            backgroundColor: Color.fromARGB(255, 101, 155, 149),
                            elevation: 0,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      }
    }
  }
}
