import 'package:flutter/material.dart';
import 'package:kotarica/eth_models/PorukeModels.dart';
import 'package:kotarica/widgets/poruke/chatFeed.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class ChatHeadPreview extends StatefulWidget {
  @override
  _ChatHeadPreviewState createState() => _ChatHeadPreviewState();
  final String imeIPrezimeKorisnika;
  final BigInt idKorisnika;
  ChatHeadPreview({this.imeIPrezimeKorisnika, this.idKorisnika});
}

class _ChatHeadPreviewState extends State<ChatHeadPreview> {
  @override
  Widget build(BuildContext context) {
   var porukeMODEL = Provider.of<PorukeModel>(context);
    double width = MediaQuery.of(context).size.width;

    return GestureDetector(
      onTap: () {
        print("kliknuo sam na  " + widget.idKorisnika.toString());
        PorukeModel.vrtiPonovo = true;
        if (PorukeModel.vrtiPonovo == true) {
          porukeMODEL.izLoadingCelaKonverzacija = true;
          PorukeModel.vrtiPonovo = false;
        }
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ChatFeed(
                    id: widget.idKorisnika,
                    imeIPrezime: widget.imeIPrezimeKorisnika)));
      },
      child: Container(
        // key: _reviewKey, // Postavljamo Globalni kljuc u widget ciju visunu hocemo da uzmemo
        padding: EdgeInsets.all(7),
        decoration: BoxDecoration(
            border: Border.all(
              color: Theme.of(context).colorScheme.secondary,
              width: 2,
            ),
            color: Theme.of(context).colorScheme.background,
            borderRadius: BorderRadius.circular(3)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Text(
                        widget.imeIPrezimeKorisnika,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: width < 500 ? width * 0.043 : 21,
                          color: Theme.of(context).colorScheme.onPrimary,
                          fontWeight: FontWeight.bold,
                        ),
                        maxLines: 2,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
