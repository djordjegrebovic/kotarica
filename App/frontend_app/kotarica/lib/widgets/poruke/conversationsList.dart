import 'package:flutter/material.dart';
import 'package:kotarica/widgets/poruke/chatFeed.dart';

class ConversationList extends StatefulWidget {
  String name;
  String messageText;
  String imageUrl;
  String time;
  bool isMessageRead;
  BigInt idKorisnika;
  ConversationList(
      {@required this.name,
      @required this.messageText,
      @required this.imageUrl,
      @required this.time,
      @required this.isMessageRead,
      @required this.idKorisnika});
  @override
  _ConversationListState createState() => _ConversationListState();
}

class _ConversationListState extends State<ConversationList> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    var boje = Theme.of(context).colorScheme;

    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ChatFeed(
                    id: widget.idKorisnika, imeIPrezime: widget.name)));
      },
      child: Container(
        //  padding: EdgeInsets.all(7),
        decoration: BoxDecoration(
            // border: Border.all(
            //   color: Theme.of(context).colorScheme.secondary,
            //   width: 2,
            // ),
            color: Theme.of(context).colorScheme.background,
            borderRadius: BorderRadius.circular(10)),
        margin: EdgeInsets.symmetric(
            horizontal: width < 600 ? 10 : 16, vertical: 6),

        padding: EdgeInsets.only(
            left: 10, right: width < 600 ? 10 : 16, top: 10, bottom: 10),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Row(
                children: <Widget>[
                  CircleAvatar(
                    backgroundImage: NetworkImage(widget.imageUrl),
                    maxRadius: 30,
                  ),
                  SizedBox(width: 12),
                  Expanded(
                    child: Container(
                      color: Colors.transparent,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(widget.name, style: TextStyle(fontSize: 16)),
                          SizedBox(height: 6),
                          Container(
                            child: Text(
                              widget.messageText,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 13,
                                  color: widget.isMessageRead
                                      ? Color.fromARGB(255, 101, 155, 149)
                                      : Colors.grey[600],
                                  fontWeight: widget.isMessageRead
                                      ? FontWeight.bold
                                      : FontWeight.normal),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Column(
              children: [
                Text(
                  widget.time,
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: widget.isMessageRead
                          ? FontWeight.bold
                          : FontWeight.normal),
                ),
                SizedBox(height: 5.0),
                widget.isMessageRead
                    ? Container(
                        // width: 40.0,
                        padding:
                            EdgeInsets.symmetric(horizontal: 5, vertical: 3),
                        height: 20.0,
                        decoration: BoxDecoration(
                          color: boje.primary,
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        alignment: Alignment.center,
                        child: Text(
                          'Nepročitano',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      )
                    : Text(''),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
