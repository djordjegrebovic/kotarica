import 'package:flutter/material.dart';
import 'package:kotarica/pages/product.dart';
import 'package:toggle_switch/toggle_switch.dart';
import 'package:velocity_x/velocity_x.dart';

class ChangeView extends StatefulWidget {
  ChangeView({Key key}) : super(key: key);

  @override
  _ChangeViewState createState() => _ChangeViewState();
}

//za primer samo klasa proizvod
class Proizvod extends Object {
  String ime;
  double cena;
  String opis;

  Proizvod(this.ime, this.cena, this.opis);
}

class _ChangeViewState extends State<ChangeView> {
  num countValue = 2;
  num aspectWidth = 2;
  num aspectHeight = 1;

  List<Proizvod> gridItems = [
    new Proizvod('Jabuka', 150, 'Neki opis za proizvod Jabuka'),
    new Proizvod('Kruska', 100, 'Neki opis za proizvod Kruska'),
    new Proizvod('Malina', 200, 'Neki opis za proizvod Malina'),
    new Proizvod('Kotarica', 500, 'Neki opis za proizvod Kotarica'),
    new Proizvod('Limun', 90, 'Neki opis za proizvod Limun'),
    new Proizvod('Kravlji sir', 400, 'Neki opis za proizvod Kravlji sir'),
    new Proizvod('Lesnik', 900, 'Neki opis za proizvod Lesnik'),
  ];

  changeMode() {
    if (countValue == 2) {
      setState(() {
        countValue = 1;
        aspectWidth = 2.9;
        aspectHeight = 1;
      });
    } else {
      setState(() {
        countValue = 2;
        aspectWidth = 2;
        aspectHeight = 1.5;
      });
    }
  }

  getGridViewSelectedItem(BuildContext context, String gridItem) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(gridItem),
          actions: <Widget>[
            // ignore: deprecated_member_use
            FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(children: [
      Align(
          alignment: Alignment.topRight,
          child: Container(
              padding: EdgeInsets.all(35),
              child: ToggleSwitch(
                initialLabelIndex: countValue,
                labels: ['', ' '],
                icons: [Icons.grid_view, Icons.list],
                minWidth: 35.0,
                minHeight: 20.0,
                cornerRadius: 10.0,
                activeBgColor: Colors.deepPurple,
                activeFgColor: Colors.white,
                inactiveBgColor: Colors.grey,
                inactiveFgColor: Colors.white,
                onToggle: (index) {
                  changeMode();
                },
              ))),
      Expanded(
        child: GridView.count(
          crossAxisCount: countValue,
          childAspectRatio: (aspectWidth / aspectHeight),
          children: gridItems
              .map((data) => GestureDetector(
                  // onTap: () {
                  //   getGridViewSelectedItem(context, data.ime);
                  // },
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Product()),
                    );
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                    margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                    child: Center(
                        child: Container(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(30),
                            decoration: BoxDecoration(color: Vx.gray200),
                          ),
                          Padding(
                              padding: const EdgeInsets.symmetric(vertical: 1),
                              child: Text(
                                data.ime,
                                style: TextStyle(color: Colors.black),
                              )),
                          Text('\$' + data.cena.toString(),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold))
                        ],
                      ),
                    )),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10),
                          bottomLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10)),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                  )))
              .toList(),
        ),
      ),
    ]));
  }
}
