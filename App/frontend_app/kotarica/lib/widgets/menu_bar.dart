import 'package:badges/badges.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:kotarica/eth_models/user_loginModels.dart';
import 'package:kotarica/main.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/pages/homePage.dart';
import 'package:kotarica/pages/my_acc.dart';
import 'package:kotarica/pages/my_cart.dart';
import 'package:kotarica/pages/search.dart';
import 'package:kotarica/redirektNaLogin.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/utils/const.dart';
// import 'package:kotarica/widgets/app_bar.dart';
import 'package:kotarica/widgets/drawer.dart';
import 'package:kotarica/widgets/productDetail/add.dart';
// import 'package:kotarica/widgets/products/reviews.dart';
// import 'package:kotarica/widgets/reviews.dart';
import 'package:provider/provider.dart';
import 'package:universal_html/html.dart' as html;

class MenuBar extends StatefulWidget {
  static String routeName = '/meni';

  MenuBar({Key key}) : super(key: key);

  @override
  _MenuBarState createState() => _MenuBarState();
}

class _MenuBarState extends State<MenuBar> {
  int selectedIndex = 0;
  final widgetOptions = [
    html.Text('Početna'),
    html.Text('Pretraži'),
    html.Text('Korpa'),
    html.Text('Moj nalog'),
    html.Text('Još'),
  ];

  final List<Widget> _children = [
    HomePage(),
    Search(),
    MyCart(meni: true),
    MyApp.postojiJWT
        ? MyAcc(id: GetInfo.id, oglasavac: false)
        : RedirektNaLogin(),
    SideMenu(),
  ];

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  // ignore: unused_element
  void _closeDrawer() {
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    TabController _tabController;
    var addListener = Provider.of<AddModel>(context);
    double width = MediaQuery.of(context).size.width;
    var listModel = Provider.of<UserLoginModel>(context);
    return DefaultTabController(
      length: 5,
      child: Scaffold(
        key: _scaffoldKey,
        //Pomocni meni koji izlazi sa desne strane kad se klikne na dugme more
        endDrawer: SideMenu(),
        body: _children[selectedIndex],
        appBar: width >= 800
            ?
            // MainAppBar(press: onItemTapped)
            AppBar(
                actions: [new Container()],
                bottom: TabBar(
                  indicatorWeight: 4,
                  indicatorSize: TabBarIndicatorSize.label,
                  indicatorColor: Theme.of(context).colorScheme.onBackground,
                  indicatorPadding: EdgeInsets.only(bottom: 2),
                  onTap: onItemTapped,
                  controller: _tabController,
                  // labelPadding: EdgeInsets.symmetric(horizontal: 20.0),
                  tabs: [
                    Container(
                      width: 150,
                      child: Tab(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.home, color: Boje.mainText),
                            SizedBox(width: 7),
                            Text('Početna', style: appBarTextStyle()),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      width: 150,
                      child: Tab(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.search, color: Boje.mainText),
                            SizedBox(width: 7),
                            Text('Pretraga', style: appBarTextStyle()),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      width: 150,
                      child: Tab(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Badge(
                              shape: BadgeShape.circle,
                              // badgeColor: Theme.of(context).colorScheme.secondaryVariant,
                              badgeColor:
                                  Theme.of(context).colorScheme.onBackground,
                              badgeContent: Text(
                                addListener.getBasketQty().toString(),
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Color.fromARGB(255, 0, 111, 109),
                                    fontWeight: FontWeight.bold),
                              ),
                              child: Icon(Icons.shopping_cart,
                                  color: Boje.mainText),
                              position:
                                  BadgePosition.topEnd(top: (-13), end: -10),
                              animationType: BadgeAnimationType.scale,
                            ),
                            SizedBox(width: 7),
                            Text('Korpa', style: appBarTextStyle()),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      width: 150,
                      child: Tab(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            GetInfo.id != BigInt.zero
                                ? SizedBox(
                                    height: 35,
                                    width: 35,
                                    child: Center(
                                      child: Stack(
                                        clipBehavior: Clip.none,
                                        children: [
                                          listModel.hashProfile != ""
                                              ? CircleAvatar(
                                                  radius: 75,
                                                  child: ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              50),
                                                      child: listModel
                                                                  .isLoading3 ==
                                                              true
                                                          ? CircularProgressIndicator()
                                                          : Image(
                                                              image: NetworkImage(Const
                                                                      .url +
                                                                  "/api/Image/" +
                                                                  listModel
                                                                      .hashProfile),
                                                                  height: 200,
                                                                  width: 200,
                                                              fit: BoxFit.fill,
                                                            )),
                                                )
                                              : CircleAvatar(
                                                  radius: 75,
                                                  backgroundImage: NetworkImage(
                                                      "https://images.megapixl.com/1796/17965459.jpg"),
                                                  child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            73),
                                                  )),
                                        ],
                                      ),
                                    ),
                                  )
                                : Icon(Icons.person, color: Boje.mainText),
                            SizedBox(width: 7),
                            Text('Profil', style: appBarTextStyle()),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      // alignment: Alignment.center,
                      // color: Colors.red,
                      width: 150,
                      child: Tab(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.menu, color: Boje.mainText),
                            SizedBox(width: 7),
                            Text('Meni', style: appBarTextStyle()),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                centerTitle: true,
                title: Container(
                  child: Text('Kotarica',
                      style: TextStyle(
                          fontSize: 24,
                          color: Boje.mainText,
                          fontWeight: FontWeight.w500)),
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 6),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Theme.of(context).colorScheme.onBackground,
                  ),
                ),
              )
            : null,

        bottomNavigationBar: width < 800
            ? BottomNavigationBar(
                items: <BottomNavigationBarItem>[
                  BottomNavigationBarItem(
                      icon: Icon(Icons.home), label: 'Početna'),
                  BottomNavigationBarItem(
                      icon: Icon(Icons.search), label: 'Pretraži'),
                  BottomNavigationBarItem(
                    icon: new Badge(
                      shape: BadgeShape.circle,
                      // badgeColor: Theme.of(context).colorScheme.secondaryVariant,
                      badgeColor: Color.fromARGB(255, 0, 107, 111),
                      badgeContent: Text(
                        addListener.getBasketQty().toString(),
                        style: TextStyle(
                            fontSize: 11,
                            color: Theme.of(context).colorScheme.onBackground,
                            fontWeight: FontWeight.bold),
                      ),
                      child: Icon(Icons.shopping_cart),
                      position: BadgePosition.topEnd(top: (-10)),
                      animationType: BadgeAnimationType.scale,
                    ),
                    label: 'Korpa',
                  ),
                  BottomNavigationBarItem(
                      icon: GetInfo.id != BigInt.zero
                          ? SizedBox(
                              height: 35,
                              width: 35,
                              child: Center(
                                child: Stack(
                                  clipBehavior: Clip.none,
                                  children: [
                                    listModel.hashProfile != ""
                                        ? CircleAvatar(
                                            radius: 75,
                                            child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(50),
                                                child: listModel.isLoading3 ==
                                                        true
                                                    ? CircularProgressIndicator()
                                                    : Image(
                                                        image: NetworkImage(Const
                                                                .url +
                                                            "/api/Image/" +
                                                            listModel
                                                                .hashProfile),
                                                                height: 200,
                                                                  width: 200,
                                                        fit: BoxFit.fill,
                                                      )),
                                          )
                                        : CircleAvatar(
                                            radius: 75,
                                            backgroundImage: NetworkImage(
                                                "https://images.megapixl.com/1796/17965459.jpg"),
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(73),
                                            )),
                                  ],
                                ),
                              ),
                            )
                          : Icon(Icons.person),
                      label: 'Moj nalog'),
                  BottomNavigationBarItem(
                      icon: Icon(Icons.menu_rounded), label: 'Još'),
                ],
                currentIndex: selectedIndex,
                onTap: onItemTapped,
                selectedItemColor: Color.fromARGB(255, 0, 111, 109),
                unselectedItemColor: Boje.mainText.withOpacity(0.6),
                selectedLabelStyle: TextStyle(
                    color: Boje.mainText, fontWeight: FontWeight.w600),
                // unselectedLabelStyle: ,
              )
            : null,
      ),
    );
  }

  void onItemTapped(int index) {
    // index == 0
    //     ? Navigator.pushNamed(context, HomePage.routeName)
    //     : index == 1
    //         ? Navigator.pushNamed(context, Search.routeName)
    //         : index == 2
    //             ? Navigator.pushNamed(context, MyCart.routeName)
    // :
    (!MyApp.postojiJWT && index == 3)
        ?
        // Navigator.pushNamed(context, RedirektNaLogin.routeName)
        Navigator.push(context,
            new MaterialPageRoute(builder: (context) => RedirektNaLogin()))
        :
        // : (MyApp.postojiJWT && index == 3)
        //     ? Navigator.pushNamed(context, MyAcc.routeName)
        //     :
        index == 4
            ? _scaffoldKey.currentState.openEndDrawer()
            : setState(() {
                selectedIndex = index;
              });
  }
}

TextStyle appBarTextStyle() {
  return TextStyle(
      fontFamily: 'Open-Sans',
      color: Boje.mainText, // surface //Colors.teal[700],
      fontSize: 18,
      fontWeight: FontWeight.bold);
}
