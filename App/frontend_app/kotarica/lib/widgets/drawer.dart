import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:kotarica/AllInfoBuilder.dart';
import 'package:kotarica/eth_models/user_loginModels.dart';
import 'package:kotarica/main.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/pages/addProduct.dart';
import 'package:kotarica/pages/favourites.dart';
import 'package:kotarica/pages/recievedBuyRequests.dart';
import 'package:kotarica/pages/sentBuyRequests.dart';
import 'package:kotarica/pages/sentReviews.dart';
import 'package:kotarica/pages/welcome_screen.dart';
import 'package:kotarica/redirektNaLogin.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/utils/const.dart';
import 'package:kotarica/widgets/poruke/recievedMsgsHeads.dart';
import 'package:provider/provider.dart';
import 'package:kotarica/widgets/user/options/changePassword.dart';
import 'package:kotarica/widgets/user/options/changePrivateKey.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:universal_html/html.dart' as universal;

class SideMenu extends StatelessWidget {
  const SideMenu({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var listModel = Provider.of<UserLoginModel>(context);

    Future<String> _loadPreferences(String dataNeeded) async {
      SharedPreferences pref = await SharedPreferences.getInstance();
      return pref.getString(dataNeeded);
    }

    if (MyApp.postojiJWT) {
      return SafeArea(
        child: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              DrawerHeader(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: <Color>[
                      Theme.of(context).colorScheme.primary,
                      Color.fromARGB(255, 160, 216, 209),
                    ],
                  ),
                ),
                child: Container(
                    child: Column(children: [
                  // Material(
                  //     borderRadius: BorderRadius.all(Radius.circular(50)),
                  //     child: Padding(
                  //       padding: EdgeInsets.all(8),
                  //       child: Image.asset(
                  //         'images/ps4_console_white_1.png',
                  //         width: 80,
                  //         height: 80,
                  //       ),
                  //     )),
                  SizedBox(
                    height: 100,
                    width: 100,
                    child: Center(
                      child: Stack(
                        clipBehavior: Clip.none,
                        children: [
                          listModel.hashProfile != ""
                              ? CircleAvatar(
                                  radius: 75,
                                  child: ClipRRect(
                                      borderRadius: BorderRadius.circular(50),
                                      child: listModel.isLoading3 == true
                                          ? CircularProgressIndicator()
                                          : Image(
                                              image: NetworkImage(Const.url +
                                                  "/api/Image/" +
                                                  listModel.hashProfile)
                                                
                                                  ,
                                                  height: 800,
                                                  width: 800,
                                              fit: BoxFit.fill,
                                            )),
                                )
                              : CircleAvatar(
                                  radius: 75,
                                  backgroundImage: NetworkImage(
                                      "https://images.megapixl.com/1796/17965459.jpg"),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(73),
                                  )),
                        ],
                      ),
                    ),
                  ),
                  // SizedBox(height: 4),
                  // Container(
                  //   width: width < 800 ? 170 : 170,
                  //   alignment: Alignment.center,
                  //   // alignment: width < 800
                  //   //     ? Alignment.center
                  //   //     : Alignment.centerLeft,
                  //   padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  //   decoration: BoxDecoration(
                  //     // color: width < 800
                  //     //     ? Theme.of(context)
                  //     //         .colorScheme
                  //     //         .onBackground
                  //     //         .withOpacity(0.4)
                  //     //     : null,

                  //     color: Theme.of(context)
                  //         .colorScheme
                  //         .onBackground
                  //         .withOpacity(0.4),
                  //     borderRadius: BorderRadius.circular(10),
                  //   ),
                  //   margin: width < 800 ? null : EdgeInsets.only(left: 15),
                  //   child: Text(
                  //     universal.window.localStorage["username"],
                  //     style: TextStyle(
                  //       // color: width < 800
                  //       //     ? Theme.of(context).colorScheme.surface
                  //       //     : Theme.of(context).colorScheme.onBackground,
                  //       color: Theme.of(context).colorScheme.surface,
                  //       fontSize: 20,
                  //       fontWeight: FontWeight.bold,
                  //     ),
                  //   ),
                  // ),
                  Padding(
                      padding: EdgeInsets.all(5),
                      child: kIsWeb
                          ? Text(
                              universal.window.localStorage["username"],
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                              ),
                            )
                          : FutureBuilder<String>(
                              future: _loadPreferences("username"),
                              builder: (BuildContext context,
                                  AsyncSnapshot<String> snapshot) {
                                switch (snapshot.connectionState) {
                                  case ConnectionState.waiting:
                                    return SpinKitFadingCube(
                                        size: 35,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary);
                                  default:
                                    if (snapshot.hasError) {
                                      return Text('Error: ${snapshot.error}');
                                    } else {
                                      return Text(
                                        snapshot.data,
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                        ),
                                      );
                                    }
                                }
                              },
                            ))
                ])),
              ),
              // CustomListTile(Icons.home, "Početna", () {
              //   Navigator.of(context).pop();
              //   Navigator.push(context,
              //       new MaterialPageRoute(builder: (context) => WelcomeScreen()));
              // }),
              // CustomListTile(Icons.person, "Profil", () {
              //   Navigator.of(context).pop();
              //   Navigator.push(context,
              //       new MaterialPageRoute(builder: (context) => MyAcc()));
              // }),
              CustomListTile(Icons.favorite, "Omiljeni proizvodi", () {
                Navigator.of(context).pop();
                Navigator.pushNamed(context, MyFavourites.routeName);
                // Navigator.push(context,
                //     new MaterialPageRoute(builder: (context) => MyFavourites()));
              }),
              CustomListTile(
                Icons.folder_special_outlined,
                "Poslati zahtevi za kupovinu",
                () {
                  Navigator.of(context).pop();
                  Navigator.pushNamed(context, MySentBuyRequests.routeName);

                  // Navigator.push(
                  //     context,
                  //     new MaterialPageRoute(
                  //         builder: (context) => MySentBuyRequests()));
                },
              ),
              CustomListTile(
                  Icons.folder_open_outlined, "Primljeni zahtevi za kupovinu",
                  () {
                Navigator.of(context).pop();
                Navigator.pushNamed(context, MyRecievedBuyRequests.routeName);

                // Navigator.push(
                //     context,
                //     new MaterialPageRoute(
                //         builder: (context) => MyRecievedBuyRequests()));
              }),
              CustomListTile(Icons.star_half_sharp, "Napisane recenzije", () {
                Navigator.of(context).pop();
                Navigator.pushNamed(context, MySentReviews.routeName);

                // Navigator.push(context,
                //     new MaterialPageRoute(builder: (context) => MySentReviews()));
              }),
              CustomListTile(Icons.rate_review, "Poruke", () {
                Navigator.of(context).pop();
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => RecievedMsgs()));
                // Navigator.push(context,
                //     new MaterialPageRoute(builder: (context) => MySentReviews()));
              }),
              Divider(
                  //color: Colors.white,
                  //thickness: 2,
                  ),
              CustomListTile(Icons.add_business_rounded, "Dodaj oglas", () {
                Navigator.of(context).pop();
                Navigator.pushNamed(context, AddProductPage.routeName);

                // Navigator.push(
                //     context,
                //     new MaterialPageRoute(
                //         builder: (context) => AddProductPage()));
              }),

              CustomListTile(
                  Icons.admin_panel_settings_sharp, "Promeni lozinku", () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ChangePassword()));
              }),
              CustomListTile(Icons.account_balance_wallet_rounded,
                  "Promeni Etherium novčanik", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ChangePrivateKey()));
              }),
              CustomListTile(Icons.login, "Odjavi me", () {
                if (kIsWeb) {
                  universal.window.localStorage.clear();
                } else {
                  FlutterSecureStorage storage1 = FlutterSecureStorage();
                  storage.delete(key: "jwt");
                  storage.deleteAll();

                  storage1.deleteAll();
                }

                AllInfos.privateKey =
                    "6749cd466f2098265162693f8408658b4a5b1d8cd766f2da22020ec32ff01cb2";
                GetInfo.id = BigInt.zero;
                Navigator.pushNamed(context, MyApp.routeName);

                // Navigator.push(context,
                //     new MaterialPageRoute(builder: (context) => MyApp()));
              }),
            ],
          ),
        ),
      );
    } else {
      return Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: <Color>[
                    Theme.of(context).colorScheme.primary,
                    Color.fromARGB(255, 73, 219, 202)
                  ],
                ),
              ),
              child: null,
            ),
            CustomListTile(Icons.home, "Početna", () {
              Navigator.of(context).pop();
              Navigator.pushNamed(context, WelcomeScreen.routeName);

              // Navigator.push(context,
              //     new MaterialPageRoute(builder: (context) => WelcomeScreen()));
            }),
            // CustomListTile(Icons.settings_applications_sharp, "Opcije", () {}),
            CustomListTile(Icons.login, "Prijava/Registracija", () {
              Navigator.pushNamed(context, RedirektNaLogin.routeName);

              // Navigator.push(
              //     context,
              //     new MaterialPageRoute(
              //         builder: (context) => RedirektNaLogin()));
            }),
          ],
        ),
      );
    }
  }
}

class CustomListTile extends StatefulWidget {
  final IconData icon;
  final String text;
  final Function onTap;

  CustomListTile(this.icon, this.text, this.onTap);

  @override
  _CustomListTileState createState() => _CustomListTileState();
}

class _CustomListTileState extends State<CustomListTile> {
  bool isHovering = false;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8.0),
      child: Container(
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
                    color: Theme.of(context).colorScheme.secondary))),
        child: InkWell(
          splashColor: Theme.of(context).colorScheme.secondary,
          hoverColor: Theme.of(context).colorScheme.secondary,
          onTap: widget.onTap,
          child: Container(
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Icon(widget.icon,
                        color: Theme.of(context).colorScheme.onPrimary),
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(widget.text,
                          style: TextStyle(
                              fontSize: 16.0,
                              color: Boje.mainText.withOpacity(0.8))),
                    ),
                  ],
                ),
                Icon(Icons.arrow_right,
                    color: Theme.of(context).colorScheme.secondary),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
