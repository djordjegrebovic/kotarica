import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kotarica/AllInfoBuilder.dart';
import 'package:kotarica/eth_models/user_loginModels.dart';
import 'package:provider/provider.dart';
import '../../../main.dart';
// import '../../../pages/welcome_screen_novi.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:universal_html/html.dart' show window;

enum FormContent {
  SIGN_IN,
  SIGN_UP,
}

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var _formContent = FormContent.SIGN_IN;

  bool _islogin = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                width: double.infinity,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Colors.grey[600],
                      Colors.grey[700],
                      Colors.grey[800],
                      Colors.grey[850],
                    ],
                    stops: [0.1, 0.4, 0.7, 0.9],
                  ),
                ),
              ),
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 40.0,
                    vertical: 120.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Prijava/Registracija',
                        style: TextStyle(
                          color: Colors.grey[900],
                          letterSpacing: 1.5,
                          fontSize: 24.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'OpenSans',
                        ),
                      ),
                      Switch(
                        value: _islogin,
                        onChanged: (isChecked) {
                          setState(() {
                            _islogin = isChecked;
                            _formContent = isChecked
                                ? FormContent.SIGN_UP
                                : FormContent.SIGN_IN;
                          });
                        },
                      ),
                      _formContent == FormContent.SIGN_IN
                          ? SignInScreen()
                          : SignUpScreen(),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class BuildEmailTF extends StatelessWidget {
  final TextEditingController email = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Email',
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: Colors.blue,
            borderRadius: BorderRadius.circular(30),
            boxShadow: [BoxShadow(blurRadius: 10, offset: Offset(4, 8))],
            border: Border.all(
              color: Colors.blue,
              width: 3,
            ),
          ),
          height: 60.0,
          child: TextField(
            controller: email,
            keyboardType: TextInputType.emailAddress,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.email,
                color: Colors.red,
              ),
              hintText: 'Unesite e-mail adresu',
              hintStyle: TextStyle(color: Colors.red),
            ),
          ),
        ),
      ],
    );
  }
}

class UsernameTF extends StatelessWidget {
  final TextEditingController username = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Username',
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: Colors.blue,
            borderRadius: BorderRadius.circular(30),
            boxShadow: [BoxShadow(blurRadius: 10, offset: Offset(4, 8))],
            border: Border.all(
              color: Colors.blue,
              width: 3,
            ),
          ),
          height: 60.0,
          child: TextField(
            controller: username,
            keyboardType: TextInputType.emailAddress,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.account_circle,
                color: Colors.white,
              ),
              hintText: 'Unesite svoj username',
            ),
          ),
        ),
      ],
    );
  }
}

class BuildPasswordTF extends StatelessWidget {
  final TextEditingController password = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Lozinka',
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          height: 60.0,
          decoration: BoxDecoration(
            color: Colors.blue,
            borderRadius: BorderRadius.circular(30),
            boxShadow: [BoxShadow(blurRadius: 10, offset: Offset(4, 8))],
            border: Border.all(
              color: Colors.blue,
              width: 3,
            ),
          ),
          child: TextField(
            controller: password,
            obscureText: true,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.lock,
                color: Colors.white,
              ),
              hintText: 'Unesite lozinku',
            ),
          ),
        ),
      ],
    );
  }
}

class BuildForgotPasswordBtn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerRight,
      // ignore: deprecated_member_use
      child: FlatButton(
        onPressed: () => print('Forgot Password Button Pressed'),
        padding: EdgeInsets.only(right: 0.0),
        child: Text(
          'Zaboravili ste lozinku?',
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class BuildLoginBtn extends StatelessWidget {
  var textInButton;

  BuildLoginBtn(String text) {
    textInButton = text;
  }

  @override
  Widget build(Object context) {
    //var listModel = Provider.of<userLoginModel>(context);

    return Container(
      padding: EdgeInsets.symmetric(vertical: 25.0),
      width: double.infinity,
      // ignore: deprecated_member_use
      child: RaisedButton(
        elevation: 5.0,
        onPressed: () => print(""),
        padding: EdgeInsets.all(15.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        color: Colors.white,
        child: Text(
          '$textInButton',
          style: TextStyle(
            color: Colors.black,
            letterSpacing: 1.5,
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class SignInScreen extends StatelessWidget {
  final TextEditingController password = TextEditingController();
  final TextEditingController username = TextEditingController();
  final TextEditingController privateKey = TextEditingController();
  var jwt;

  @override
  Widget build(BuildContext context) {
    var listModel = Provider.of<UserLoginModel>(context);

    return Column(children: [
      SizedBox(height: 30.0),
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Username',
          ),
          SizedBox(height: 10.0),
          Container(
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              boxShadow: [BoxShadow(blurRadius: 10, offset: Offset(4, 8))],
              border: Border.all(
                color: Colors.white,
                width: 3,
              ),
            ),
            height: 60.0,
            child: TextField(
              controller: username,
              keyboardType: TextInputType.emailAddress,
              style: TextStyle(
                color: Colors.black,
                fontFamily: 'OpenSans',
              ),
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.only(top: 14.0),
                prefixIcon: Icon(
                  Icons.email,
                  color: Colors.white,
                ),
                hintText: 'Unesite username ',
              ),
            ),
          ),
        ],
      ),
      SizedBox(
        height: 30.0,
      ),
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Lozinka',
          ),
          SizedBox(height: 10.0),
          Container(
            alignment: Alignment.centerLeft,
            height: 60.0,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              boxShadow: [BoxShadow(blurRadius: 10, offset: Offset(4, 8))],
              border: Border.all(
                color: Colors.white,
                width: 3,
              ),
            ),
            child: TextField(
              controller: password,
              obscureText: true,
              style: TextStyle(
                color: Colors.black,
                fontFamily: 'OpenSans',
              ),
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.only(top: 14.0),
                prefixIcon: Icon(
                  Icons.lock,
                  color: Colors.white,
                ),
                hintText: 'Unesite lozinku',
              ),
            ),
          ),
        ],
      ),
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Private key',
          ),
          SizedBox(height: 10.0),
          Container(
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              boxShadow: [BoxShadow(blurRadius: 10, offset: Offset(4, 8))],
              border: Border.all(
                color: Colors.white,
                width: 3,
              ),
            ),
            height: 60.0,
            child: TextField(
              controller: privateKey,
              keyboardType: TextInputType.emailAddress,
              style: TextStyle(
                color: Colors.black,
                fontFamily: 'OpenSans',
              ),
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.only(top: 14.0),
                prefixIcon: Icon(
                  Icons.account_circle,
                  color: Colors.white,
                ),
                hintText: 'Unesite svoj private key',
              ),
            ),
          ),
        ],
      ),
      BuildForgotPasswordBtn(),
      Container(
        padding: EdgeInsets.symmetric(vertical: 25.0),
        width: double.infinity,
        // ignore: deprecated_member_use
        child: RaisedButton(
          elevation: 5.0,
          onPressed: () => {
            AllInfos.privateKey = privateKey.text,
            jwt = listModel.postoji(username.text, password.text),
            // print(jwt),
            //listModel.login(username.text, password.text),
            if (jwt !=
                null /*listModel.postoji(
                    username.text, password.text) !=
                null*/
            )
              {
                if (kIsWeb)
                  {
                    window.localStorage['csrf'] = jwt,
                  }
                else
                  storage.write(key: "jwt", value: jwt),
                Navigator.pushNamed(context, MyApp.routeName),

                // Navigator.push(
                //   context,
                //   MaterialPageRoute(builder: (context) => MyApp() /* Main2()*/),
                // ),
              }
            else
              {
                listModel.displayDialog(context, "Desila se greška!",
                    "Kombinacija korisničkog imena i šifre ne postoji."),
                // print("ne postoji taj user i password"),
                username.clear(),
                password.clear(),
              }
          },
          padding: EdgeInsets.all(15.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          color: Colors.white,
          child: Text(
            'Prijavi se',
            style: TextStyle(
              color: Colors.black,
              letterSpacing: 1.5,
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
              fontFamily: 'OpenSans',
            ),
          ),
        ),
      ),
    ]);
  }
}

class SignUpScreen extends StatelessWidget {
  final TextEditingController emailRegistracija = TextEditingController();
  final TextEditingController passwordRegistracija1 = TextEditingController();
  final TextEditingController passwordRegistracija2 = TextEditingController();
  final TextEditingController usernameRegistracija = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var listModel = Provider.of<UserLoginModel>(context);

    return Column(children: [
      SizedBox(height: 30.0),
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Email',
          ),
          SizedBox(height: 10.0),
          Container(
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              boxShadow: [BoxShadow(blurRadius: 10, offset: Offset(4, 8))],
              border: Border.all(
                color: Colors.white,
                width: 3,
              ),
            ),
            height: 60.0,
            child: TextField(
              controller: emailRegistracija,
              keyboardType: TextInputType.emailAddress,
              style: TextStyle(
                color: Colors.black,
                fontFamily: 'OpenSans',
              ),
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.only(top: 14.0),
                prefixIcon: Icon(
                  Icons.email,
                  color: Colors.white,
                ),
                hintText: 'Unesite e-mail adresu',
              ),
            ),
          ),
        ],
      ),
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Username',
          ),
          SizedBox(height: 10.0),
          Container(
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              boxShadow: [BoxShadow(blurRadius: 10, offset: Offset(4, 8))],
              border: Border.all(
                color: Colors.white,
                width: 3,
              ),
            ),
            height: 60.0,
            child: TextField(
              controller: usernameRegistracija,
              keyboardType: TextInputType.emailAddress,
              style: TextStyle(
                color: Colors.black,
                fontFamily: 'OpenSans',
              ),
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.only(top: 14.0),
                prefixIcon: Icon(
                  Icons.account_circle,
                  color: Colors.white,
                ),
                hintText: 'Unesite svoj username',
              ),
            ),
          ),
        ],
      ),
      SizedBox(height: 25.0),
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Lozinka',
          ),
          SizedBox(height: 10.0),
          Container(
            alignment: Alignment.centerLeft,
            height: 60.0,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              boxShadow: [BoxShadow(blurRadius: 10, offset: Offset(4, 8))],
              border: Border.all(
                color: Colors.white,
                width: 3,
              ),
            ),
            child: TextField(
              controller: passwordRegistracija1,
              obscureText: true,
              style: TextStyle(
                color: Colors.black,
                fontFamily: 'OpenSans',
              ),
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.only(top: 14.0),
                prefixIcon: Icon(
                  Icons.lock,
                  color: Colors.white,
                ),
                hintText: 'Unesite lozinku',
              ),
            ),
          ),
        ],
      ),
      SizedBox(height: 25.0),
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Ponovi lozinku',
          ),
          SizedBox(height: 10.0),
          Container(
            alignment: Alignment.centerLeft,
            height: 60.0,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              boxShadow: [BoxShadow(blurRadius: 10, offset: Offset(4, 8))],
              border: Border.all(
                color: Colors.white,
                width: 3,
              ),
            ),
            child: TextField(
              controller: passwordRegistracija2,
              obscureText: true,
              style: TextStyle(
                color: Colors.black,
                fontFamily: 'OpenSans',
              ),
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.only(top: 14.0),
                prefixIcon: Icon(
                  Icons.lock,
                  color: Colors.white,
                ),
                hintText: 'Unesite lozinku',
              ),
            ),
          ),
        ],
      ),
      SizedBox(height: 25.0),
      Container(
        padding: EdgeInsets.symmetric(vertical: 25.0),
        width: double.infinity,
        // ignore: deprecated_member_use
        child: RaisedButton(
          elevation: 5.0,
          onPressed: () => {
            if (passwordRegistracija1.text == passwordRegistracija2.text &&
                (listModel.postoji(usernameRegistracija.text,
                        passwordRegistracija1.text)) ==
                    null &&
                listModel.validateEmail(emailRegistracija.text) &&
                listModel.validatePassword(passwordRegistracija1.text))
              {
                listModel.addUser(
                    usernameRegistracija.text, passwordRegistracija1.text, ""),
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),
                )
              }
            else
              {
                // print("Ovaj user vec postoji"),
                usernameRegistracija.clear(),
                passwordRegistracija1.clear(),
                passwordRegistracija2.clear(),
                emailRegistracija.clear()
              }
          },
          padding: EdgeInsets.all(15.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          color: Colors.white,
          child: Text(
            'Registruj se',
            style: TextStyle(
              color: Colors.black,
              letterSpacing: 1.5,
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
              fontFamily: 'OpenSans',
            ),
          ),
        ),
      ),
    ]);
  }
}
