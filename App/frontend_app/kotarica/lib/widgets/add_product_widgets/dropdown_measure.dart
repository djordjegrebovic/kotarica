import 'package:flutter/material.dart';

// ignore: must_be_immutable
class MeasureDropDown extends StatefulWidget {
  String _chosenValue = "";
  String get chosenValue => _chosenValue;
  @override
  _MeasureDropDownState createState() => _MeasureDropDownState();
}

class _MeasureDropDownState extends State<MeasureDropDown> {
  var _chosenValue;

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      focusColor: Colors.white,
      value: _chosenValue,
      isExpanded: true,
      //elevation: 5,
      style: TextStyle(color: Colors.white),
      iconEnabledColor: Colors.black,
      items: <String>[
        'kg (kilogram)',
        'l (litar)',
        'kom (komad)',
      ].map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(
            value,
            style: TextStyle(color: Colors.black),
          ),
        );
      }).toList(),
      hint: Text("Merna jedinica"),
      onChanged: (String value) {
        setState(() {
          _chosenValue = value;
          widget._chosenValue = value;
        });
      },
    );
  }

  String get chosenValue {
    return _chosenValue;
  }
}
