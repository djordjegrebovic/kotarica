import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ResizeableTextInput extends StatefulWidget {
  final TextEditingController textEditingController = TextEditingController();
  String text;
  ResizeableTextInput(String text) {
    this.text = text;
  }
  TextEditingController get controler => textEditingController;
  @override
  _ResizeableTextInputState createState() => _ResizeableTextInputState();
}

class _ResizeableTextInputState extends State<ResizeableTextInput> {
  double _inputHeight = 50;

  @override
  void initState() {
    super.initState();
    widget.textEditingController.addListener(_checkInputHeight);
  }

  @override
  void dispose() {
    widget.textEditingController.dispose();
    super.dispose();
  }

  void _checkInputHeight() async {
    int count = widget.textEditingController.text.split('\n').length;

    if (count == 0 && _inputHeight == 50.0) {
      return;
    }
    if (count <= 5) {
      // use a maximum height of 6 rows
      // height values can be adapted based on the font size
      var newHeight = count == 0 ? 50.0 : 28.0 + (count * 18.0);
      setState(() {
        _inputHeight = newHeight;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(labelText: widget.text),
      controller: widget.textEditingController,
      textInputAction: TextInputAction.newline,
      keyboardType: TextInputType.multiline,
      maxLines: null,
    );
  }
}
