// import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:kotarica/models/type_of_product.dart';

// ignore: must_be_immutable
class CategoryDropDown extends StatefulWidget {
  String _category = "";
  String _type = "";
  String get category => _category;
  String get type => _type;
  @override
  _CategoryDropDownState createState() => _CategoryDropDownState();
}

class _CategoryDropDownState extends State<CategoryDropDown> {
  var cat = 0;
  var category = "";
  var type = "";
  Widget test = Text("");
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Column(
          children: <Widget>[
            Dropdown(
              'Kategorija',
              [
                "Voće",
                "Povrće",
                "Domaća pića",
                "Alkoholna pića",
                "Stočarstvo",
                "Nekategorisano"
              ],
              update: _update,
              settest: _setTest,
            ),
            test
          ],
        ),
      ],
    );
  }

  void _setTest() {
    test = Text("");
  }

  void _settype(String type) {
    widget._type = type;
    // print(type);
  }

  void _update(int b, String category) {
    widget._category = category;
    widget._type = "";
    setState(() {
      cat = b;
      (cat == 1)
          ? test = Dropdown('Voće', TypeOfProduct().voce, settype: _settype)
          : (cat == 2)
              ? test =
                  Dropdown('Povrće', TypeOfProduct().povrce, settype: _settype)
              : (cat == 3)
                  ? test = Dropdown('Domaća pića', TypeOfProduct().domacaPica,
                      settype: _settype)
                  : (cat == 4)
                      ? test = Dropdown(
                          'Alkoholna pića', TypeOfProduct().alkoholnaPica,
                          settype: _settype)
                      : (cat == 5)
                          ? test = Dropdown(
                              'Stočarstvo', TypeOfProduct().stocarstvo,
                              settype: _settype)
                          : test = Text("");
    });
  }
}

class Dropdown extends StatefulWidget {
  final String _key;
  final List<String> _values;
  final Function update;
  final Function settype;
  final Function settest;

  // ignore: avoid_init_to_null
  Dropdown(this._key, this._values,
      {this.update = null, this.settype = null, this.settest = null});

  @override
  _DropdownState createState() => _DropdownState();
}

class _DropdownState extends State<Dropdown> {
  var _chosenValue;

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      hint: Text(widget._key),
      value: _chosenValue,
      icon: Icon(Icons.arrow_drop_down),
      iconSize: 24,
      isExpanded: true,
      items: widget._values.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
      onTap: () {
        widget._key == 'Kategorija';
        widget.update(0, "Nekategorisano");
      },
      onChanged: (String value) {
        if (widget.update != null && widget._key == 'Kategorija') {
          (value == 'Voće')
              ? widget.update(1, 'Voće')
              : (value == 'Povrće')
                  ? widget.update(2, 'Povrće')
                  : (value == 'Domaća pića')
                      ? widget.update(3, 'Domaća pića')
                      : (value == 'Alkoholna pića')
                          ? widget.update(4, 'Alkoholna pića')
                          : (value == 'Stočarstvo')
                              ? widget.update(5, 'Stočarstvo')
                              : widget.update(0, "Nekategorisano");
        }
        setState(() {
          _chosenValue = value;
        });
        if (widget.settype != null) {
          widget.settype(value);
        }
      },
    );
  }
}
