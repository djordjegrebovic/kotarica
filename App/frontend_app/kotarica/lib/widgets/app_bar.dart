// import 'package:badges/badges.dart';
// import 'package:flutter/material.dart';
// import 'package:kotarica/AllInfoBuilder.dart';
// import 'package:kotarica/eth_models/ProductModels.dart';
// import 'package:kotarica/eth_models/user_loginModels.dart';
// import 'package:kotarica/main.dart';
// import 'package:kotarica/models/getInfo.dart';
// import 'package:kotarica/pages/edit_product.dart';
// import 'package:kotarica/pages/my_cart.dart';
// import 'package:kotarica/redirektNaLogin.dart';
// import 'package:kotarica/utils/boje.dart';
// import 'package:kotarica/widgets/productDetail/add.dart';
// import 'package:kotarica/widgets/productDetail/basketButton.dart';
// import 'package:kotarica/widgets/productDetail/rounded_icon_button.dart';
// import 'package:provider/provider.dart';

// class MainAppBar extends PreferredSize {
//   // final double rating;
//   // final Product product;

//   MainAppBar({@required this.press});

//   final Function press;
//   @override
//   // AppBar().preferredSize.height provide us the height that appy on our app bar
//   Size get preferredSize => Size.fromHeight(AppBar().preferredSize.height);

//   @override
//   Widget build(BuildContext context) {

//     void onItemTapped(int index) {
//     (!MyApp.postojiJWT && index == 3)
//         ? Navigator.push(context,
//             new MaterialPageRoute(builder: (context) => RedirektNaLogin()))
//         : index == 4
//             ? _scaffoldKey.currentState.openEndDrawer()
//             : setState(() {
//                 selectedIndex = index;
//               });
//   }
//     var add_listener = Provider.of<AddModel>(context);
//     double width = MediaQuery.of(context).size.width;
//     double height = MediaQuery.of(context).size.height;

//     return SafeArea(
//       child: Container(
//           width: 800,
//           child: TabBar(
//                       indicatorSize: TabBarIndicatorSize.label,
//                       onTap: onItemTapped,
//                       controller: _tabController,
//                       labelPadding: EdgeInsets.symmetric(horizontal: 20.0),
//                       tabs: [
//                         Container(
//                           width: 70,
//                           child: Tab(icon: Icon(Icons.home), text: 'Početna'),
//                         ),
//                         Tab(icon: Icon(Icons.search), text: 'Pretraži'),
//                         Tab(
//                           icon: new Badge(
//                             shape: BadgeShape.circle,
//                             // badgeColor: Theme.of(context).colorScheme.secondaryVariant,
//                             badgeColor: Boje.selectedMenuItemColor,
//                             badgeContent: Text(
//                               add_listener.getBasketQty().toString(),
//                               style: TextStyle(
//                                   fontSize: 11,
//                                   color: Theme.of(context)
//                                       .colorScheme
//                                       .onBackground,
//                                   fontWeight: FontWeight.bold),
//                             ),
//                             child: Icon(Icons.shopping_cart),
//                             position: BadgePosition.topEnd(top: (-10)),
//                             animationType: BadgeAnimationType.scale,
//                           ),
//                           text: 'Korpa',
//                         ),
//                         Tab(icon: Icon(Icons.person), text: 'Moj nalog'),
//                         // Tab(),
//                       ],
//                     ),
//           // Row(
//           //   mainAxisAlignment: MainAxisAlignment.center,
//           //   children: [
//           //     RoundedIconBtn(
//           //       text: '',
//           //       icon: Icons.edit,
//           //       showShadow: false,
//           //       height: 35,
//           //       width: 55,
//           //       size: 25,
//           //       press: () {
//           //         // Navigator.push(
//           //         //     context,
//           //         //     new MaterialPageRoute(
//           //         //         builder: (context) => EditProduct(product)));
//           //       }, //Mogucnost da se edituje
//           //       buttonColor: Theme.of(context).colorScheme.onBackground,
//           //       iconColor: Colors.teal,
//           //     ),
//           //     RoundedIconBtn(
//           //       text: '',
//           //       icon: Icons.edit,
//           //       showShadow: false,
//           //       height: 35,
//           //       width: 55,
//           //       size: 25,
//           //       press: () {
//           //         // Navigator.push(
//           //         //     context,
//           //         //     new MaterialPageRoute(
//           //         //         builder: (context) => EditProduct(product)));
//           //       }, //Mogucnost da se edituje
//           //       buttonColor: Theme.of(context).colorScheme.onBackground,
//           //       iconColor: Colors.teal,
//           //     ),
//           //     RoundedIconBtn(
//           //       text: '',
//           //       icon: Icons.edit,
//           //       showShadow: false,
//           //       height: 35,
//           //       width: 55,
//           //       size: 25,
//           //       press: () {
//           //         // Navigator.push(
//           //         //     context,
//           //         //     new MaterialPageRoute(
//           //         //         builder: (context) => EditProduct(product)));
//           //       }, //Mogucnost da se edituje
//           //       buttonColor: Theme.of(context).colorScheme.onBackground,
//           //       iconColor: Colors.teal,
//           //     ),
//           //     RoundedIconBtn(
//           //       text: '',
//           //       icon: Icons.edit,
//           //       showShadow: false,
//           //       height: 35,
//           //       width: 55,
//           //       size: 25,
//           //       press: () {
//           //         // Navigator.push(
//           //         //     context,
//           //         //     new MaterialPageRoute(
//           //         //         builder: (context) => EditProduct(product)));
//           //       }, //Mogucnost da se edituje
//           //       buttonColor: Theme.of(context).colorScheme.onBackground,
//           //       iconColor: Colors.teal,
//           //     ),
//           //     RoundedIconBtn(
//           //       text: '',
//           //       icon: Icons.edit,
//           //       showShadow: false,
//           //       height: 35,
//           //       width: 55,
//           //       size: 25,
//           //       press: () {
//           //         // Navigator.push(
//           //         //     context,
//           //         //     new MaterialPageRoute(
//           //         //         builder: (context) => EditProduct(product)));
//           //       }, //Mogucnost da se edituje
//           //       buttonColor: Theme.of(context).colorScheme.onBackground,
//           //       iconColor: Colors.teal,
//           //     )
//           //   ],
//           )),
//     );

//     // SafeArea(
//     //   child: Container(
//     //     margin: EdgeInsets.symmetric(horizontal: 100, vertical: 5),
//     //     padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
//     //     width: double.infinity,
//     //     color: Theme.of(context).colorScheme.background,
//     //     child: Row(
//     //       children: [
//     //         SizedBox(
//     //           height: 40,
//     //           width: 40,
//     //           child: FlatButton(
//     //             splashColor: Theme.of(context).colorScheme.onSurface,
//     //             shape: RoundedRectangleBorder(
//     //               borderRadius: BorderRadius.circular(60),
//     //             ),
//     //             padding: EdgeInsets.zero,
//     //             onPressed: () => Navigator.pop(context),
//     //             child: Icon(
//     //               Icons.arrow_back_ios_outlined,
//     //               color: Theme.of(context).colorScheme.surface,
//     //             ),
//     //           ),
//     //         ),
//     //         Spacer(),
//     //         // Polje za rejting
//     //         Container(
//     //           padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 5),
//     //           decoration: BoxDecoration(
//     //             color: Theme.of(context).colorScheme.onBackground,
//     //             borderRadius: BorderRadius.circular(14),
//     //           ),
//     //           child: Row(
//     //             children: [
//     //               rating != 0.0
//     //                   ? Text(
//     //                       "$rating",
//     //                       overflow: TextOverflow.ellipsis,
//     //                       style: TextStyle(
//     //                         color: Theme.of(context).colorScheme.surface,
//     //                         fontSize: 16,
//     //                         fontWeight: FontWeight.bold,
//     //                       ),
//     //                     )
//     //                   : Icon(Icons.not_interested,
//     //                       color: Theme.of(context).colorScheme.error),
//     //               const SizedBox(width: 5),
//     //               Icon(Icons.star, color: Theme.of(context).colorScheme.primary)
//     //             ],
//     //           ),
//     //         ),
//     //         SizedBox(width: 5),
//     //         // Korpa
//     //         BasketButton(
//     //             add_listener: add_listener,
//     //             color: Theme.of(context).colorScheme.onBackground,
//     //             margin: 0),
//     //         // Dugme za izmenu
//     //         SizedBox(width: 5),
//     //         GetInfo.id != BigInt.zero && product.idOglasavaca == GetInfo.id
//     //             ? RoundedIconBtn(
//     //                 text: '',
//     //                 icon: Icons.edit,
//     //                 showShadow: false,
//     //                 height: 35,
//     //                 width: 55,
//     //                 size: 25,
//     //                 press: () {
//     //                   Navigator.push(
//     //                       context,
//     //                       new MaterialPageRoute(
//     //                           builder: (context) => EditProduct(product)));
//     //                 }, //Mogucnost da se edituje
//     //                 buttonColor: Theme.of(context).colorScheme.onBackground,
//     //                 iconColor: Colors.teal,
//     //               )
//     //             : SizedBox()
//     //       ],
//     //     ),
//     //   ),
//     // );
//   }
// }
