import 'package:flutter/material.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/utils/const.dart';
import 'package:kotarica/widgets/cart/snack_bar.dart';
// import 'package:kotarica/widgets/images/image_load.dart';
import 'package:kotarica/widgets/productDetail/add.dart';
import 'package:provider/provider.dart';

//Kartica kod najpoularnijih proizvoda
class ProductCard extends StatefulWidget {
  const ProductCard({
    Key key,
    this.width = 140,
    // this.height,
    this.grid = false,
    this.aspectRetio = 1.02,
    @required this.product,
    @required this.press,
    this.listModel,
  }) : super(key: key);

  final double width, aspectRetio;
  // final double height;
  final bool grid;
  final Product product;
  final GestureTapCallback press;
  final ProductModels listModel;

  @override
  _ProductCardState createState() => _ProductCardState();
}

class _ProductCardState extends State<ProductCard> {
  @override
  Widget build(BuildContext context) {
    void displayDialog(context, title, text) => showDialog(
          context: context,
          builder: (context) =>
              AlertDialog(title: Text(title), content: Text(text)),
        );
    var cartProvider = Provider.of<AddModel>(context);
    var listModel = Provider.of<ProductModels>(context);
    // double height = MediaQuery.of(context).size.height;
    // double width = MediaQuery.of(context).size.width;
    return Padding(
      padding: this.widget.grid == false
          ? EdgeInsets.symmetric(horizontal: 8)
          : EdgeInsets.symmetric(horizontal: 0),
      child: GestureDetector(
        onTap: widget.press,
        child: SizedBox(
          width: 140,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AspectRatio(
                aspectRatio: 1.02,
                //Slika i siva pozadina iza slike
                child: Container(
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.onBackground,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(15),
                      child: widget.product.hesh == ""
                          ? Image.asset(widget.product.images[0])
                          : Image(
                              image: NetworkImage(Const.url +
                                  "/api/Image/" +
                                  widget.product.hesh),
                              fit: BoxFit.fill,
                            ),
                    )),
              ),
              const SizedBox(height: 10),
              // Naziv proizvoda
              Text(
                widget.product.naziv,
                overflow: TextOverflow.ellipsis,
                style: this.widget.grid == false
                    ? TextStyle(
                        color: Boje.mainText,
                        fontWeight: FontWeight.bold,
                        fontSize: 16)
                    : TextStyle(
                        color: Theme.of(context).colorScheme.surface,
                        fontWeight: FontWeight.bold,
                        fontSize: 19),
                maxLines: 2,
              ),
              SizedBox(height: 2),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  // CENA
                  Expanded(
                    child: RawScrollbar(
                      thumbColor: Colors.teal,
                      thickness: 3,
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: [
                            Text(
                              "${widget.product.cena}",
                              overflow: TextOverflow.clip,
                              maxLines: 1,
                              style: this.widget.grid == false
                                  ? TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Theme.of(context)
                                          .colorScheme
                                          .onBackground,
                                    )
                                  : TextStyle(
                                      fontSize: 21,
                                      fontWeight: FontWeight.bold,
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary),
                            ),
                            Text(
                              " RSD",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.orange[300],
                                //Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  // Korpa na kartici popularnih proizvoda
                  Row(
                    children: [
                      InkWell(
                        borderRadius: BorderRadius.circular(50),
                        onTap: () {
                          setState(() {
                            if (GetInfo.id != widget.product.idOglasavaca) {
                              cartProvider.addOneItemToBasket(widget.product);
                              snackBar_basket(
                                  context, cartProvider, widget.product);
                            } else
                              displayDialog(context, "Greška!",
                                  "Ne možete kupiti vaš proizvod.");
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.all(3),
                          height: 28,
                          width: 28,
                          decoration: BoxDecoration(
                            color: Theme.of(context).colorScheme.onBackground,
                            shape: BoxShape.circle,
                          ),
                          child: Icon(Icons.shopping_cart,
                              color: GetInfo.id != widget.product.idOglasavaca
                                  ? Color.fromARGB(255, 0, 111, 109)
                                  : Colors.grey,
                              size: 22),
                        ),
                      ),
                      SizedBox(width: 5),
                      // Srce na kartici popularnih proizvoda
                      InkWell(
                        borderRadius: BorderRadius.circular(50),
                        onTap: () {
                          // setState(() {
                          //   widget.product.isFavourite =
                          //       !widget.product.isFavourite;
                          //   if (GetInfo.id != BigInt.zero)
                          //     listModel.addFavourites(
                          //         widget.product.id, GetInfo.id);
                          // });
                          favouriteButtonSetState(context, listModel);
                        },
                        child: GetInfo.id != BigInt.zero
                            ? Container(
                                padding: EdgeInsets.all(3),
                                height: 28,
                                width: 28,
                                decoration: BoxDecoration(
                                  color: Theme.of(context)
                                      .colorScheme
                                      .onBackground,
                                  shape: BoxShape.circle,
                                ),
                                child: Icon(
                                  widget.product.isFavourite
                                      ? Icons.favorite
                                      : Icons.favorite_border,
                                  color: widget.product.isFavourite
                                      ? Colors.red[700]
                                      : Theme.of(context)
                                          .colorScheme
                                          .secondaryVariant,
                                ),
                              )
                            : SizedBox(),
                      ),
                    ],
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  void favouriteButtonSetState(BuildContext context, ProductModels listModel) {
    return setState(
      () {
        widget.product.isFavourite = !widget.product.isFavourite;
        if (GetInfo.id != BigInt.zero) {
          if (widget.product.isFavourite) {
            snackBar_add_favourite(context, listModel, widget.product);
            listModel.addFavourites(widget.product.id, GetInfo.id);
          } else {
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                title: Text('Da li ste sigurni?'),
                content: Text('Da li zelite da ukloni proizvod iz omiljenih?'),
                actions: [
                  // ignore: deprecated_member_use
                  FlatButton(
                    child: Text('Ne'),
                    onPressed: () {
                      setState(() {
                        Navigator.of(context).pop(false);
                        widget.product.isFavourite = true;
                      });
                    },
                  ),
                  // ignore: deprecated_member_use
                  FlatButton(
                    child: Text('Da'),
                    onPressed: () {
                      Navigator.of(context).pop(true);
                      listModel.deleteFavourites(widget.product.id, GetInfo.id);
                    },
                  )
                ],
              ),
            );
          }
        }
      },
    );
  }
}
