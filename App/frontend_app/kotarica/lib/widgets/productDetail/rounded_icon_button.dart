import 'package:flutter/material.dart';

class RoundedIconBtn extends StatelessWidget {
  const RoundedIconBtn({
    Key key,
    @required this.text,
    @required this.icon,
    @required this.press,
    @required this.width,
    @required this.height,
    @required this.size,
    this.radius = 50,
    @required this.buttonColor,
    @required this.iconColor,
    this.showShadow = false,
    this.iconFirst = false,
  }) : super(key: key);

  final IconData icon;
  final GestureTapCancelCallback press;
  final bool showShadow;
  final double height;
  final double width;
  final Color buttonColor;
  final Color iconColor;
  final double size;
  final String text;
  final bool iconFirst;
  final double radius;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        boxShadow: [
          if (showShadow)
            BoxShadow(
              offset: Offset(0, 6),
              blurRadius: 10,
              color: Color(0xFFB0B0B0).withOpacity(0.7),
            ),
        ],
      ),
      // ignore: deprecated_member_use
      child: FlatButton(
        padding: EdgeInsets.zero,
        color: buttonColor,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(radius)),
        onPressed: press,
        child: text == ''
            ? Icon(icon, color: iconColor, size: size)
            : Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  iconFirst
                      ? Icon(icon, color: iconColor, size: size)
                      : Text(text, style: TextStyle(color: iconColor)),
                  SizedBox(width: 2),
                  iconFirst
                      ? Text(text, style: TextStyle(color: iconColor))
                      : Icon(icon, color: iconColor, size: size),
                ],
              ),
      ),
    );
  }
}
