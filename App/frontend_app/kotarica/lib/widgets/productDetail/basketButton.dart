import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:kotarica/pages/my_cart.dart';
import 'package:kotarica/widgets/productDetail/add.dart';

class BasketButton extends StatelessWidget {
  const BasketButton({
    Key key,
    @required this.addListener,
    @required this.color,
    @required this.margin,
  }) : super(key: key);

  final AddModel addListener;
  final Color color;
  final double margin;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 5),
      margin: margin > 0
          ? EdgeInsets.symmetric(vertical: margin)
          : EdgeInsets.symmetric(vertical: 0),
      height: 35,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(14),
      ),
      child: InkWell(
        onTap: () {
          //KADA SE KLIKNE NA TAJ DEO PREBACUJE NAS NA STRANICU KORPE
          // move to basket page
          Navigator.pushNamed(context, MyCart.routeName, arguments: MyCart());
          // Navigator.push(
          //     context, MaterialPageRoute(builder: (context) => MyCart()));
        },
        borderRadius: BorderRadius.circular(30),
        //PRIKAZUJE KOLICINU PROIZVODA
        child: Badge(
          shape: BadgeShape.circle,
          badgeColor: Color.fromARGB(255, 0, 148, 146),
          badgeContent: Text(
            addListener.getBasketQty().toString(),
            // Text(test_listener.getBasketQty().toString()),
            style: TextStyle(
                fontSize: 11, color: Colors.white, fontWeight: FontWeight.bold),
          ),
          child: Icon(Icons.shopping_cart,
              size: 25, color: Color.fromARGB(255, 0, 111, 109)),
          position: BadgePosition.topEnd(end: -10),
          animationType: BadgeAnimationType.scale,
        ),
      ),
    );
  }
}
