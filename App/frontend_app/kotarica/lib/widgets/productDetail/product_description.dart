import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kotarica/eth_models/KupovinaModels.dart';
// import 'package:kotarica/models/Products.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/eth_models/user_loginModels.dart';
// import 'package:kotarica/eth_models/RecenzijeModels.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/utils/boje.dart';
// import 'package:kotarica/pages/customLoading.dart';
// import 'package:kotarica/pages/my_acc.dart';
import 'package:kotarica/widgets/cart/snack_bar.dart';
import 'package:kotarica/widgets/cart/text_icon_button.dart';
import 'package:kotarica/widgets/productDetail/add.dart';
// import 'package:kotarica/widgets/productDetail/expanded_text.dart';
//import 'package:kotarica/widgets/productDetail/reviewTab.dart';
import 'package:kotarica/widgets/productDetail/rounded_icon_button.dart';
// import 'package:kotarica/widgets/productDetail/showProfile.dart';
import 'package:kotarica/widgets/products/reviews.dart';
// import 'package:kotarica/widgets/products/reviewsForProduct.dart';
import 'package:kotarica/widgets/user/profileDescription.dart';
import 'package:provider/provider.dart';

class ProductDescription extends StatefulWidget {
  const ProductDescription(
      {Key key,
      @required this.product,
      this.pressOnSeeMore,
      this.order,
      this.imaLiZahtevaZaKupovinu})
      : super(key: key);

  final Product product;
  final int imaLiZahtevaZaKupovinu;
  final OrderRequest order;
  final GestureTapCallback pressOnSeeMore;

  @override
  _ProductDescriptionState createState() => _ProductDescriptionState();
}

class _ProductDescriptionState extends State<ProductDescription> {
  var quantity = 1;
  bool uradioSiNesto = false;

  @override
  Widget build(BuildContext context) {
    void displayDialog(context, title, text) => showDialog(
          context: context,
          builder: (context) =>
              AlertDialog(title: Text(title), content: Text(text)),
        );
    String status;
    var providerUsers;
    var userOglasivac;
    var userKupac;
    var kupovina;
    var datumSladnjaPorudzbine;
    var datumKupovine;
    //kreira ove provajdere ako se widget koristi za prikaz detaljnih informacija o kupovini
    if (widget.order != null) {
      kupovina = Provider.of<KupovinaModels>(context);
      providerUsers = Provider.of<UserLoginModel>(context);
      userOglasivac =
          providerUsers.getUserNameAndSurname(widget.order.idOglasivaca);
      userKupac = providerUsers.getUserNameAndSurname(widget.order.idKupca);
      if (widget.order.status == BigInt.zero) {
        status = "Na čekanju";
      } else if (widget.order.status == BigInt.one) {
        status = "Prihvaćeno";
        var datum = widget.order.datum.toString();
        List<String> listaDatuma = datum.split("!");
        datumSladnjaPorudzbine = listaDatuma[0].toString();
        datumKupovine = listaDatuma[1].toString();
      } else {
        status = "Odbijen";
        var datum = widget.order.datum.toString();
        List<String> listaDatuma = datum.split("!");
        datumSladnjaPorudzbine = listaDatuma[0].toString();
        datumKupovine = listaDatuma[1].toString();
      }
    }
    var addListener = Provider.of<AddModel>(context);
    var listModel = Provider.of<ProductModels>(context);
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    Color dataBoxColor = Theme.of(context).colorScheme.secondary;
    double fontSizeWeb = 20;
    double fontSizeApp = 16;

    return uradioSiNesto == false
        ? Center(
            child: Container(
              // margin: EdgeInsets.only(top: 20),
              padding: EdgeInsets.only(top: 15, bottom: 15),
              width: width < 800 ? width : 800,
              decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.background,
                // borderRadius: BorderRadius.only(
                //   topLeft: Radius.circular(40),
                //   topRight: Radius.circular(40),
                // ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Center(
                    child: Container(
                      width: 50,
                      height: 6,
                      decoration: BoxDecoration(
                          color: Theme.of(context).colorScheme.surface,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          boxShadow: [
                            BoxShadow(
                                blurRadius: 30,
                                color: Theme.of(context).colorScheme.secondary)
                          ]),
                    ),
                  ),
                  SizedBox(height: 5),
                  // Red sa imenom prozivoda i cenom
                  //pitam da li postoji order ako ne postoji ja onda radim sve po starom a dole u else dizajn za order
                  widget.order == null
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            //Naziv proizvoda
                            Expanded(
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20),
                                child: Text(
                                  widget.product.naziv,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  softWrap: true,
                                  style: TextStyle(
                                    fontSize: width < 800 ? 26 : 32,
                                    color:
                                        Theme.of(context).colorScheme.surface,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            //Cena proizvoda
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20.0),
                              child: Row(
                                children: [
                                  Text(
                                    "${widget.product.cena}",
                                    style: TextStyle(
                                      fontSize: width < 800 ? 26 : 32,
                                      color:
                                          Theme.of(context).colorScheme.surface,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text(
                                    " rsd/${widget.product.mernaJedinica}",
                                    style: TextStyle(
                                      fontSize: width < 800 ? 23 : 28,
                                      color: Boje.rsd,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ) //else deo za order
                      : orderScreen(
                          height,
                          context,
                          width,
                          dataBoxColor,
                          userKupac,
                          fontSizeApp,
                          fontSizeWeb,
                          userOglasivac,
                          status,
                          datumSladnjaPorudzbine,
                          datumKupovine),
                  SizedBox(height: 5),
                  //Red sa dugmicima za dodavanje u korpu i sa ikonicom za favorite
                  //pitam da li postoji ordder ako ne postoji ja onda radim sve po starom a dole u else dizajn za order

                  widget.order == null
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            //Dugme remove, kolicina, dugme add
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10.0),
                              child: Row(
                                children: [
                                  RoundedIconBtn(
                                    text: '',
                                    icon: Icons.remove,
                                    showShadow: true,
                                    height: width > 500 ? 55 : width * 0.1,
                                    width: width > 500 ? 55 : width * 0.1,
                                    size: width > 500 ? 35 : width * 0.065,
                                    buttonColor: Theme.of(context)
                                        .colorScheme
                                        .onBackground,
                                    iconColor:
                                        Theme.of(context).colorScheme.surface,
                                    press: () {
                                      setState(() {
                                        if (quantity > 1) quantity -= 1;
                                      });
                                    },
                                  ),
                                  SizedBox(
                                    child: Padding(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 4),
                                      child: Text(
                                        '$quantity/${widget.product.mernaJedinica}',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .surface,
                                          fontSize:
                                              width < 500 ? width * 0.055 : 26,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ),
                                  RoundedIconBtn(
                                    text: '',
                                    icon: Icons.add,
                                    showShadow: true,
                                    height: width > 500 ? 55 : width * 0.1,
                                    width: width > 500 ? 55 : width * 0.1,
                                    size: width > 500 ? 35 : width * 0.065,
                                    buttonColor: Theme.of(context)
                                        .colorScheme
                                        .onBackground,
                                    iconColor:
                                        Theme.of(context).colorScheme.surface,
                                    press: () {
                                      setState(() {
                                        if (quantity < 30) quantity += 1;
                                      });
                                    },
                                  ),
                                ],
                              ),
                            ),
                            //Veliko dugme za dodavanje u korpu
                            Flexible(
                              child: Container(
                                constraints: BoxConstraints(
                                    maxHeight: 57,
                                    minHeight: 52,
                                    maxWidth: 250),
                                padding: EdgeInsets.symmetric(horizontal: 15),
                                height: width < 500 ? height * 0.06 : 57,
                                width: width < 500 ? width * 0.8 : 250,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  // shape: BoxShape.circle,
                                  // boxShadow: [
                                  //   BoxShadow(
                                  //     offset: Offset(0, -10),
                                  //     blurRadius: 10,
                                  //     color: Color(0xFFB0B0B0).withOpacity(0.3),
                                  //   ),
                                  // ],
                                ),
                                // ignore: deprecated_member_use
                                child: FlatButton(
                                  padding: EdgeInsets.symmetric(horizontal: 15),
                                  color:
                                      GetInfo.id != widget.product.idOglasavaca
                                          ? Color.fromARGB(255, 129, 192, 185)
                                          : Colors.grey,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20)),
                                  onPressed: () {
                                    setState(() {
                                      if (GetInfo.id !=
                                          widget.product.idOglasavaca) {
                                        addListener.addOneItemToBasket(
                                            this.widget.product);
                                        addListener.subQty(
                                            this.widget.product, quantity);
                                        snackBar_qty_basket(
                                            context,
                                            addListener,
                                            widget.product,
                                            quantity);
                                      } else {
                                        displayDialog(context, "Greška!",
                                            "Ne možete kupiti vaš proizvod.");
                                      }
                                      quantity = 1;
                                    });
                                  },
                                  //Text u velikom dugmetu i ikonica
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        child: Text(
                                          width > 360
                                              ? "Dodaj u korpu "
                                              : 'Dodaj ',
                                          maxLines: 1,
                                          overflow: TextOverflow.clip,
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .onBackground,
                                            fontWeight: FontWeight.bold,
                                            fontSize: width <= 360
                                                ? 20
                                                : width >= 500
                                                    ? 22
                                                    : width * 0.04,
                                          ),
                                        ),
                                      ),
                                      Flexible(
                                        child: Icon(
                                          Icons.add_shopping_cart,
                                          color: Theme.of(context)
                                              .colorScheme
                                              .onBackground,
                                          size: width <= 360
                                              ? 20
                                              : width > 600
                                                  ? 30
                                                  : width * 0.06,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            //Favourite deo
                            if (GetInfo.id != BigInt.zero)
                              Align(
                                alignment: Alignment.centerRight,
                                child: InkWell(
                                  onTap: () {
                                    favouriteButtonSetState(context, listModel);
                                  },
                                  child: Container(
                                    padding: EdgeInsets.all(15),
                                    width: 64,
                                    decoration: BoxDecoration(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .onBackground,
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(20),
                                        bottomLeft: Radius.circular(20),
                                      ),
                                    ),
                                    child: Icon(
                                      widget.product.isFavourite
                                          ? Icons.favorite
                                          : Icons.favorite_border_outlined,
                                      color: widget.product.isFavourite
                                          ? Colors.red[400]
                                          : Theme.of(context)
                                              .colorScheme
                                              .secondary,
                                    ),
                                  ),
                                ),
                              ),
                          ],
                        )
                      : SizedBox(),
                  // prikazi mi tabove samo ako je prikaz proizvoda u pitanju a ne prikaz infromacija o kupovoini
                  widget.order == null
                      ? SizedBox()
                      : Column(
                          children: [
                            SizedBox(height: 20),
                            Container(
                              child: widget.imaLiZahtevaZaKupovinu == 1 &&
                                      widget.imaLiZahtevaZaKupovinu != null
                                  ? Column(
                                      children: [
                                        (this.widget.order.status ==
                                                BigInt.zero)
                                            ? Container(
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceAround,
                                                  children: [
                                                    TextAndIconButton(
                                                      text: "Obriši zahtev",
                                                      press: () async {
                                                        uradioSiNesto = true;

                                                        await kupovina
                                                            .deleteReq(widget
                                                                .order.id);
                                                      },
                                                      icon: Icons.dangerous,
                                                      buttonColor:
                                                          Colors.red[700],
                                                      textColor: Colors.white,
                                                      buttonWidth: width > 850
                                                          ? 235
                                                          : 185,
                                                      buttonHeight:
                                                          width > 850 ? 40 : 35,
                                                      fontSize:
                                                          width > 850 ? 24 : 18,
                                                      iconSize:
                                                          width > 850 ? 25 : 20,
                                                    ),
                                                  ],
                                                ),
                                              )
                                            : SizedBox(),
                                        (widget.order.status ==
                                                    BigInt.from(1) &&
                                                widget.order.ocenjen == false)
                                            ? Container(
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceAround,
                                                  children: [
                                                    TextAndIconButton(
                                                      text: "Ocenite proizvod",
                                                      press: () {
                                                        Navigator.push(
                                                            context,
                                                            new MaterialPageRoute(
                                                                builder: (context) =>
                                                                    Scaffold(
                                                                        appBar:
                                                                            AppBar(
                                                                          centerTitle:
                                                                              true,
                                                                          title: Text(
                                                                              "Ocenite proizvod",
                                                                              style: TextStyle(color: Boje.mainText, fontWeight: FontWeight.bold)),
                                                                        ),
                                                                        body: ReviewWidget(
                                                                            widget.order))));
                                                      },
                                                      icon: Icons.rate_review,
                                                      buttonColor:
                                                          Colors.orange[300],
                                                      textColor: Colors.white,
                                                      buttonWidth: width > 850
                                                          ? 260
                                                          : 210,
                                                      buttonHeight:
                                                          width > 850 ? 40 : 35,
                                                      fontSize:
                                                          width > 850 ? 24 : 18,
                                                      iconSize:
                                                          width > 850 ? 25 : 20,
                                                    ),
                                                  ],
                                                ),
                                              )
                                            : SizedBox()
                                      ],
                                    )
                                  : SizedBox(),
                            ),
                            widget.order.status == BigInt.zero &&
                                    widget.imaLiZahtevaZaKupovinu == 2 &&
                                    widget.imaLiZahtevaZaKupovinu != null
                                ? Container(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 20.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: [
                                        TextAndIconButton(
                                          text: "Otkaži",
                                          press: () async {
                                            var now = new DateTime.now();
                                            var formatter =
                                                new DateFormat('dd.MM.yyyy.');
                                            String formattedDate =
                                                formatter.format(now);
                                            uradioSiNesto = true;

                                            await kupovina.changeReqStatus(
                                              widget.order.id,
                                              BigInt.from(2),
                                              "",
                                              formattedDate,
                                            );
                                            // setState(() {
                                            //   widget.order.status = BigInt.from(2);
                                            // });
                                            Navigator.pop(context);
                                          },
                                          icon: Icons.dangerous,
                                          buttonColor: Colors.red[700],
                                          textColor: Colors.white,
                                          buttonWidth: width > 850 ? 150 : 115,
                                          buttonHeight: width > 850 ? 45 : 35,
                                          fontSize: width > 850 ? 24 : 18,
                                          iconSize: width > 850 ? 25 : 20,
                                        ),
                                        SizedBox(width: 10),
                                        TextAndIconButton(
                                          text: "Potvrdi",
                                          press: () async {
                                            var now = new DateTime.now();
                                            var formatter =
                                                new DateFormat('dd.MM.yyyy.');
                                            String formattedDate =
                                                formatter.format(now);
                                            uradioSiNesto = true;

                                            await kupovina.changeReqStatus(
                                              widget.order.id,
                                              BigInt.from(1),
                                              "",
                                              formattedDate,
                                            );
                                            // setState(() {
                                            //   widget.order.status = BigInt.from(1);
                                            // });
                                            Navigator.pop(context);
                                          },
                                          icon: Icons.verified,
                                          buttonColor: Colors.green[700],
                                          textColor: Colors.white,
                                          buttonWidth: width > 850 ? 150 : 115,
                                          buttonHeight: width > 850 ? 45 : 35,
                                          fontSize: width > 850 ? 24 : 18,
                                          iconSize: width > 850 ? 25 : 20,
                                        ),
                                      ],
                                    ),
                                  )
                                : SizedBox(),
                            SizedBox(height: 20)
                          ],
                        ),
                ],
              ),
            ),
          )
        : CircularProgressIndicator();

    // );
    // }); DraggableScrollableSheet
  }

  Container orderScreen(
      double height,
      BuildContext context,
      double width,
      Color dataBoxColor,
      userKupac,
      double fontSizeApp,
      double fontSizeWeb,
      userOglasivac,
      String status,
      datumSladnjaPorudzbine,
      datumKupovine) {
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                //Naziv proizvoda
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Text(
                      widget.product.naziv,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      softWrap: true,
                      style: TextStyle(
                        fontSize: width < 800 ? 26 : 32,
                        color: Theme.of(context).colorScheme.surface,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                //Cena proizvoda
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Row(
                    children: [
                      Text(
                        "${widget.product.cena}",
                        style: TextStyle(
                          fontSize: width < 800 ? 26 : 32,
                          color: Theme.of(context).colorScheme.surface,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        " rsd/${widget.product.mernaJedinica}",
                        style: TextStyle(
                          fontSize: width < 800 ? 23 : 28,
                          color: Boje.rsd,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 20),
          Container(
            width: width < 800 ? width * 0.87 : 800,
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              boxShadow: [BoxShadow(blurRadius: 10)],
              color:
                  Theme.of(context).primaryColor, //Boje.userPageContainerColor,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  decoration: BoxDecoration(
                    color: dataBoxColor,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Row(
                    children: [
                      Text("Naručilac: "),
                      Flexible(
                        child: Text(userKupac,
                            style: width < 800
                                ? profileData(fontSizeApp)
                                : profileData(fontSizeWeb),
                            overflow: TextOverflow.ellipsis),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 3),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  decoration: BoxDecoration(
                    color: dataBoxColor,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Row(
                    children: [
                      Text("Oglašavač: "),
                      Flexible(
                        child: Text(userOglasivac,
                            style: width < 800
                                ? profileData(fontSizeApp)
                                : profileData(fontSizeWeb),
                            overflow: TextOverflow.ellipsis),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 3),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  decoration: BoxDecoration(
                    color: dataBoxColor,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Row(
                    children: [
                      Text("Naručena količina: "),
                      Flexible(
                        child: Text(widget.order.kolicina.toString(),
                            style: width < 800
                                ? profileData(fontSizeApp)
                                : profileData(fontSizeWeb),
                            overflow: TextOverflow.ellipsis),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 3),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  decoration: BoxDecoration(
                    color: dataBoxColor,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Row(
                    children: [
                      Text("Račun: "),
                      Flexible(
                        child: Text(widget.order.cena.toString(),
                            style: width < 800
                                ? profileData(fontSizeApp)
                                : profileData(fontSizeWeb),
                            overflow: TextOverflow.ellipsis),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 3),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  decoration: BoxDecoration(
                    color: dataBoxColor,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Row(
                    children: [
                      Text("Status porudžbine: "),
                      Flexible(
                        child: Text(status,
                            style: width < 800
                                ? profileData(fontSizeApp)
                                : profileData(fontSizeWeb),
                            overflow: TextOverflow.ellipsis),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 3),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  decoration: BoxDecoration(
                    color: dataBoxColor,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Row(
                    children: [
                      Text("Datum porudžbine: "),
                      Flexible(
                        child: Text(
                            widget.order.status != BigInt.zero
                                ? datumSladnjaPorudzbine.toString()
                                : widget.order.datum,
                            style: width < 800
                                ? profileData(fontSizeApp)
                                : profileData(fontSizeWeb),
                            overflow: TextOverflow.ellipsis),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 3),
                widget.order.status != BigInt.zero
                    ? Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                        decoration: BoxDecoration(
                          color: dataBoxColor,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Row(
                          children: [
                            Text("Datum kupovine: "),
                            Flexible(
                              child: Text(
                                  widget.order.status != BigInt.zero
                                      ? datumKupovine.toString().toString()
                                      : widget.order.datum,
                                  style: width < 800
                                      ? profileData(fontSizeApp)
                                      : profileData(fontSizeWeb),
                                  overflow: TextOverflow.ellipsis),
                            ),
                          ],
                        ),
                      )
                    : SizedBox(),
                widget.order.status != BigInt.zero
                    ? SizedBox(height: 3)
                    : SizedBox(),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  decoration: BoxDecoration(
                    color: dataBoxColor,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Row(
                    children: [
                      Text("Broj telefona kupca: "),
                      Flexible(
                        child: Text(widget.order.brojTelefona.toString(),
                            style: width < 800
                                ? profileData(fontSizeApp)
                                : profileData(fontSizeWeb),
                            overflow: TextOverflow.ellipsis),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 3),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  decoration: BoxDecoration(
                    color: dataBoxColor,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Row(
                    children: [
                      Text("Adresa i grad kupca: "),
                      Flexible(
                        child: Text(widget.order.adresa.toString(),
                            style: width < 800
                                ? profileData(fontSizeApp)
                                : profileData(fontSizeWeb),
                            overflow: TextOverflow.ellipsis),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 3),
                // Container(
                //   padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                //   decoration: BoxDecoration(
                //     color: dataBoxColor,
                //     borderRadius: BorderRadius.circular(10),
                //   ),
                //   child: Row(
                //     children: [
                //       Text("Poruka: "),
                //       Flexible(
                //         child: Text(widget.order.poruka.toString(),
                //             style: width < 800
                //                 ? profileData(fontSizeApp)
                //                 : profileData(fontSizeWeb),
                //             overflow: TextOverflow.ellipsis),
                //       ),
                //     ],
                //   ),
                // ),
                // SizedBox(height: 3),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void favouriteButtonSetState(BuildContext context, ProductModels listModel) {
    return setState(
      () {
        widget.product.isFavourite = !widget.product.isFavourite;
        if (GetInfo.id != BigInt.zero) {
          if (widget.product.isFavourite) {
            snackBar_add_favourite(context, listModel, widget.product);
            listModel.addFavourites(widget.product.id, GetInfo.id);
          } else {
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                title: Text('Da li ste sigurni?'),
                content: Text('Da li zelite da ukloni proizvod iz omiljenih?'),
                actions: [
                  // ignore: deprecated_member_use
                  FlatButton(
                    child: Text('Ne'),
                    onPressed: () {
                      setState(() {
                        Navigator.of(context).pop(false);
                        widget.product.isFavourite = true;
                      });
                    },
                  ),
                  // ignore: deprecated_member_use
                  FlatButton(
                    child: Text('Da'),
                    onPressed: () {
                      Navigator.of(context).pop(true);
                      listModel.deleteFavourites(widget.product.id, GetInfo.id);
                    },
                  )
                ],
              ),
            );
          }
        }
      },
    );
  }
}
