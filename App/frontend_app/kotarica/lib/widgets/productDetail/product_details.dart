import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:kotarica/eth_models/KupovinaModels.dart';
import 'package:kotarica/eth_models/PorukeModels.dart';
// import 'package:kotarica/models/Products.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/eth_models/RecenzijeModels.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/widgets/productDetail/custom_app_bar.dart';
// import 'package:kotarica/widgets/productDetail/expanded_text.dart';
// import 'package:kotarica/widgets/productDetail/product_body.dart';
import 'package:kotarica/widgets/productDetail/product_description.dart';
import 'package:kotarica/widgets/productDetail/product_images.dart';
import 'package:kotarica/widgets/productDetail/showProfile.dart';
import 'package:kotarica/widgets/products/reviewsForProduct.dart';
import 'package:provider/provider.dart';

class ProductDetails extends StatefulWidget {
  static String routeName = '/proizvod';

  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  _handleTabSelection() {
    if (_tabController.indexIsChanging) {
      setState(() {});
    }
  }

  @override
  void initState() {
    _tabController = new TabController(length: 3, vsync: this);
    _tabController.addListener(_handleTabSelection);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height;

    final ProductDetailsArguments args =
        ModalRoute.of(context).settings.arguments;
    var model = Provider.of<RecenzijeModel>(context);
    var porukeMODEL = Provider.of<PorukeModel>(context);

    if (model.isLoading) {
      return SpinKitFadingCube(
          size: 35, color: Theme.of(context).colorScheme.primary);
    } else {
      if (model.isLoadingProductReviews2) {
        model.getProductReviews(args.product.id);
        return SpinKitFadingCube(
            size: 35, color: Theme.of(context).colorScheme.primary);
      } else {
        //double ocena = model.dajOcenuProizvoda(agrs.product.id);
        return Scaffold(
          backgroundColor: Theme.of(context).colorScheme.onBackground,
          appBar:
              // width < 800 ?
              args.order == null
                  ? CustomAppBar(
                      rating: !args.product.prosecnaOcena.isNaN
                          ? args.product.prosecnaOcena
                          : 0.0,
                      product: args.product,
                      porukaModel: porukeMODEL,
                    )
                  : AppBar(
                      leading: IconButton(
                        icon: Icon(Icons.arrow_back_ios, color: Boje.mainText),
                        onPressed: () {
                          GetInfo.korpa = false;
                          Navigator.pop(context);
                          // Navigator.push(context,
                          //     MaterialPageRoute(builder: (context) => WelcomeScreen()));
                        },
                      ),
                      title: Text("Porudžbina",
                          style: TextStyle(
                              color: Boje.mainText,
                              fontWeight: FontWeight.bold)),
                      centerTitle: true,
                    ),
          // : null,
          body: Center(
            child: Container(
              width: width < 800 ? width : 800,
              child: ListView(
                children: [
                  ProductImages(product: args.product),
                  // order != null ? Spacer() : SizedBox(),
                  ProductDescription(
                    product: args.product,
                    order: args.order,
                    imaLiZahtevaZaKupovinu: args.imaLiZahtevaZaKupovinu,
                    pressOnSeeMore: () {},
                  ),
                  // IZGLED TABBARA
                  args.order == null
                      ? Container(
                          padding: EdgeInsets.only(top: 10),
                          color: Theme.of(context).colorScheme.secondary,
                          width: width,
                          height: width > 450 ? 60 : 50,
                          child: TabBar(
                            controller: _tabController,
                            indicatorColor: Boje.mainText,
                            // indicatorSize: TabBarIndicatorSize.tab,
                            indicatorWeight: 4.5,
                            labelColor: Boje.mainText,
                            labelStyle: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 17),
                            unselectedLabelColor:
                                Theme.of(context).colorScheme.onBackground,
                            isScrollable: false,
                            tabs: [
                              Text(
                                "Detalji",
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontSize: width < 335
                                        ? 15.5
                                        : width > 450
                                            ? 18.5
                                            : 17),
                              ), // Tab(text: "O proizvodu", ),
                              Text(
                                "Recenzije",
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontSize: width < 335
                                        ? 15.5
                                        : width > 450
                                            ? 18.5
                                            : 17),
                              ),
                              Text(
                                "Oglašavač",
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontSize: width < 335
                                        ? 15.5
                                        : width > 450
                                            ? 18.5
                                            : 17),
                              ),
                              // Tab(text: "Oglašavač"),
                            ],
                          ),
                        )
                      : SizedBox(),
                  // TABBAR STRANICE
                  args.order == null
                      ? Center(
                          child: [
                            // Container(
                            //     padding: EdgeInsets.only(bottom: 15),
                            //     decoration: BoxDecoration(
                            //       borderRadius: BorderRadius.only(
                            //           bottomLeft: Radius.circular(10),
                            //           bottomRight: Radius.circular(10)),
                            //       color:
                            //           Theme.of(context).colorScheme.background,
                            //     ),
                            //     child: ExpandText(
                            //       labelHeader: "Detalji proizvoda",
                            //       shortDesc: args.product.description,
                            //       desc: args.product.description +
                            //           " Vise Vise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise dVise detalja VVise detalja Vise detalja Vise detalja Vise ddetalja VVise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja ise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja Vise detalja",
                            //     )),
                            Container(
                              width: width < 800 ? width : 800,
                              padding: EdgeInsets.only(
                                  bottom: 15, top: 10, left: 15),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(10),
                                    bottomRight: Radius.circular(10)),
                                color: Theme.of(context).colorScheme.background,
                              ),
                              // margin: EdgeInsets.only(
                              //     left: 20.0, right: 20.0, top: 5),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Detalji proizvoda",
                                    style: TextStyle(
                                        fontSize: 22,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .onPrimary,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(height: 15),
                                  Row(
                                    children: [
                                      Text(
                                        "Kategorija: ",
                                        style: TextStyle(
                                            fontSize: 18,
                                            color: Theme.of(context)
                                                .colorScheme
                                                .onPrimary
                                                .withOpacity(0.8),
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Container(
                                        // padding:
                                        //     EdgeInsets.symmetric(vertical: 5),
                                        child: Text(
                                          args.product.kategorija,
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .onPrimary,
                                            fontSize: 16,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 10),
                                  args.product.kategorija != "Nekategorisano"
                                      ? Row(
                                          children: [
                                            Text(
                                              "Vrsta: ",
                                              style: TextStyle(
                                                  fontSize: 18,
                                                  color: Theme.of(context)
                                                      .colorScheme
                                                      .onPrimary
                                                      .withOpacity(0.8),
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Container(
                                              // padding:
                                              //     EdgeInsets.symmetric(vertical: 5),
                                              child: Text(
                                                args.product.vrsta,
                                                style: TextStyle(
                                                  color: Theme.of(context)
                                                      .colorScheme
                                                      .onPrimary,
                                                  fontSize: 16,
                                                ),
                                              ),
                                            ),
                                          ],
                                        )
                                      : SizedBox(),
                                  SizedBox(
                                      height: args.product.kategorija !=
                                              "Nekategorisano"
                                          ? 10
                                          : 0),
                                  Text(
                                    "Opis proizvoda:",
                                    style: TextStyle(
                                        fontSize: 18,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .onPrimary
                                            .withOpacity(0.8),
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Container(
                                    padding: EdgeInsets.symmetric(vertical: 5),
                                    child: Text(
                                      args.product.description,
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .onPrimary,
                                        fontSize: 16,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                                // height: MediaQuery.of(context).size.height,
                                child:
                                    ReviewRequestsForProducts(args.product.id)),
                            //Text("recenzije"),
                            Container(
                              // height: MediaQuery.of(context).size.height * 0.5,
                              child: ShowProfile(args.product.idOglasavaca),
                            ),
                          ][_tabController.index],
                        )
                      : SizedBox(),
                  SizedBox(height: 15)
                ],
              ),
            ),
          ),

          // ProductBody(
          //   product: agrs.product,
          //   order: agrs.order,
          //   imaLiZahtevaZaKupovinu: agrs.imaLiZahtevaZaKupovinu,
          // ),
        );
      }
    }
  }
}

class ProductDetailsArguments {
  final Product product;
  final OrderRequest order;
  final int imaLiZahtevaZaKupovinu;
  ProductDetailsArguments(
      {@required this.product, this.order, this.imaLiZahtevaZaKupovinu});
}
