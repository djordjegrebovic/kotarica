import 'package:flutter/material.dart';
//import 'package:flutter_html/flutter_html.dart';
//import 'package:flutter_html/style.dart';

// ignore: must_be_immutable
class ExpandText extends StatefulWidget {
  ExpandText({
    this.labelHeader,
    this.desc,
    this.shortDesc,
  });

  String labelHeader;
  String desc;
  String shortDesc;

  @override
  _ExpandTextState createState() {
    return _ExpandTextState();
  }
}

class _ExpandTextState extends State<ExpandText> {
  bool descTextShowFlag = false;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(left: 20.0, right: 20.0, top: 5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              this.widget.labelHeader,
              style: TextStyle(
                  fontSize: 15,
                  color: Theme.of(context).colorScheme.surface,
                  fontWeight: FontWeight.bold),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: Text(
                descTextShowFlag ? this.widget.desc : this.widget.shortDesc,
                style: TextStyle(
                  color: Theme.of(context).colorScheme.surface,
                  fontSize: 15,
                ),
              ),
            ),
            //Dugme za prikazivanje vise i manje detalja proizvoda
            Align(
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    descTextShowFlag = !descTextShowFlag;
                  });
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      descTextShowFlag ? "Prikazi manje" : "Prikazi vise",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).colorScheme.surface,
                      ),
                    ),
                    SizedBox(width: 5),
                    Icon(
                      Icons.arrow_forward_ios,
                      size: 15,
                      color: Theme.of(context).colorScheme.surface,
                    ),
                  ],
                ),
              ),
            )
          ],
        ));
  }
}
