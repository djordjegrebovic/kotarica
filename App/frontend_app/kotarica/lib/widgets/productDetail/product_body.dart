import 'package:flutter/material.dart';
import 'package:kotarica/eth_models/KupovinaModels.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/widgets/productDetail/product_description.dart';
import 'package:kotarica/widgets/productDetail/product_images.dart';

class ProductBody extends StatelessWidget {
  final Product product;
  final OrderRequest order;
  final int imaLiZahtevaZaKupovinu;
  const ProductBody(
      {Key key,
      @required this.product,
      this.order,
      this.imaLiZahtevaZaKupovinu})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // double width = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        // mainAxisAlignment: order != null
        //     ? MainAxisAlignment.spaceBetween
        //     : MainAxisAlignment.start,
        children: [
          // width > 800
          //     ? Container(height: 100, width: 800, color: Colors.green)
          //     : SizedBox(),
          ProductImages(product: product),
          // order != null ? Spacer() : SizedBox(),
          ProductDescription(
            product: product,
            order: order,
            imaLiZahtevaZaKupovinu: imaLiZahtevaZaKupovinu,
            pressOnSeeMore: () {},
          ),
        ],
      ),
    );
  }
}
