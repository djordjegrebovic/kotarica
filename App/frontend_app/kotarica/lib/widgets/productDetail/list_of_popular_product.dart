import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/widgets/productDetail/product_card.dart';
import 'package:kotarica/widgets/productDetail/product_details.dart';
import 'package:provider/provider.dart';

class PopularProducts extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var listModel = Provider.of<ProductModels>(context);
    double width = MediaQuery.of(context).size.width;
    if (listModel.isLoading) {
      return Center(
        child: Padding(
          padding: const EdgeInsets.only(top: 20),
          child: Column(children: [
            Padding(
              padding: EdgeInsets.only(top: 10, bottom: 20),
              child: Text(
                "Najpopularniji proizvodi se učitavaju",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: width < 800 ? 18 : 20,
                    color: Theme.of(context).colorScheme.onPrimary),
              ),
            ),
            SpinKitFadingCube(
                size: 35, color: Theme.of(context).colorScheme.primary),
          ]),
        ),
      );
    } else {
      var lista = listModel.dajNajpopularnije();
      return Container(
        width: MediaQuery.of(context).size.width > 800
            ? 1200
            : MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          //Color.fromARGB(255, 156, 183, 180),
          color: Color.fromARGB(255, 143, 166, 163),
          // Color.fromARGB(255, 172, 186, 183),
          //Color.fromARGB(255, 183, 181, 166),
          //Color.fromARGB(255, 192, 190, 176),
          //Color.fromARGB(255, 173, 210, 198),
          // Color.fromARGB(255, 89, 133, 129),
        ),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Padding(
                padding: EdgeInsets.only(top: 10, bottom: 15),
                child: Text(
                  "Najpopularnije",
                  style: TextStyle(
                    fontSize: 21,
                    color: Colors.white, //Color.fromARGB(255, 0, 42, 42),
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  ...List.generate(
                    lista.length,
                    (index) {
                      // if (ProductModels.products[index].isFavourite)
                      if (lista.length > 0)
                        return ProductCard(
                            product: lista[index],
                            press: () => Navigator.pushNamed(
                                  context,
                                  ProductDetails.routeName,
                                  arguments: ProductDetailsArguments(
                                      product: lista[index]),
                                ));

                      return SizedBox
                          .shrink(); // here by default width and height is 0
                    },
                  ),
                  SizedBox(width: 10),
                ],
              ),
            ),
            SizedBox(height: 15),
          ],
        ),
      );
    }
  }
}
