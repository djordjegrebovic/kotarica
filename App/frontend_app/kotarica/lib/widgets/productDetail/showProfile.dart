import 'package:flutter/material.dart';
import 'package:kotarica/eth_models/user_loginModels.dart';
import 'package:kotarica/pages/my_acc.dart';
import 'package:kotarica/utils/const.dart';
// import 'package:kotarica/utils/const.dart';
import 'package:kotarica/widgets/cart/text_icon_button.dart';
// import 'package:kotarica/widgets/user/profilePictureAndUsername.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class ShowProfile extends StatefulWidget {
  BigInt id;

  ShowProfile(BigInt id1) {
    this.id = id1;
  }

  @override
  ShowProfileState createState() => ShowProfileState();
}

// ignore: camel_case_types
class ShowProfileState extends State<ShowProfile> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height;

    var listModel = Provider.of<UserLoginModel>(context);
    if (listModel.isLoading1) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      UserData data = listModel.vratiInformacijeOKorisniku(widget.id);

      return GestureDetector(
        onTap: () {},
        child: Container(
          // height: 50,
          padding: EdgeInsets.all(7),
          decoration: BoxDecoration(
              // border: Border.all(
              //   color: Colors.red,
              //   width: 2,
              // ),
              color: Theme.of(context).colorScheme.background,
              borderRadius: BorderRadius.circular(14)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: [
                  SizedBox(
                    width: width < 500 ? width * 0.25 : 150,
                    child: AspectRatio(
                      aspectRatio: 1,
                      child: Container(
                        padding: EdgeInsets.all(5.0),
                        decoration: BoxDecoration(
                          color: Theme.of(context).colorScheme.secondary,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: SizedBox(
                          // Siva pozadina sa slikom proizvoda
                          width: width < 500 ? width * 0.25 : 150,
                          child: AspectRatio(
                            aspectRatio: 1,
                            child: Container(
                              padding: EdgeInsets.all(5.0),
                              decoration: BoxDecoration(
                                color: Theme.of(context).colorScheme.background,
                                borderRadius: BorderRadius.circular(70),
                              ),
                              // child: this.product.hesh == "" ?
                              child: SizedBox(
                                  height: 150,
                                  width: 150,
                                  child: Center(
                                      child: CircleAvatar(
                                          radius: 75,
                                          backgroundImage: data
                                                      .profilePictureHash ==
                                                  ""
                                              ? NetworkImage(
                                                  "https://images.megapixl.com/1796/17965459.jpg")
                                              : NetworkImage(Const.url +
                                                  "/api/Image/" +
                                                  data.profilePictureHash),
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(73),
                                          ))
                                      // Image(
                                      //   image: snapshot.data,
                                      //   width: 150,
                                      //   height: 150,
                                      //   fit: BoxFit.fill,
                                      // );

                                      )),
                            ),
                            //Image.asset(data.profilePictureHash)
                            // : ShowIpfsImage(this.product.hesh),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: width < 600 ? width * 0.03 : 15),
                  Column(
                    // Text pored slike
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        // width: MediaQuery.of(context).size.width * 0.60,
                        child: Text(
                          data.name +
                              " " +
                              data.surname, //prikaz imena i prezimena
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: width < 500
                                  ? width * 0.035
                                  : width > 800
                                      ? 25
                                      : 23,
                              color: Theme.of(context).colorScheme.surface,
                              fontWeight: FontWeight.bold),
                          maxLines: 2,
                        ),
                      ),
                      SizedBox(height: 5.0),
                      Text.rich(
                        TextSpan(
                          text:
                              "Broj telefona : ${data.phoneNumber}\nEmail : " +
                                  data.email +
                                  "\nAdresa : " +
                                  data.homeAddress, //prikaz broja telefona
                          style: TextStyle(
                            fontSize: width < 500
                                ? width * 0.037
                                : width <= 1000
                                    ? 20
                                    : 21,
                            fontWeight: FontWeight.w700,
                            color:
                                Theme.of(context).colorScheme.secondaryVariant,
                          ),
                        ),
                      ),
                      SizedBox(height: 3),
                      TextAndIconButton(
                        buttonColor: Theme.of(context).colorScheme.secondary,
                        press: () {
                          // print("widget.id" + widget.id.toString());
                          // Navigator.pushNamed(context, MyAcc.routeName,
                          //     arguments:
                          //         MyAcc(id: ShowProfile.id, oglasavac: true));
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) =>
                                      MyAcc(id: widget.id, oglasavac: true)));
                        },
                        textColor: Theme.of(context).colorScheme.onBackground,
                        icon: Icons.person_pin,
                        text: 'Pogledaj ceo profil',
                        buttonWidth: width >= 500 ? 300 : width - width * 0.4,
                        buttonHeight: width >= 700
                            ? 40
                            : width < 410
                                ? 22
                                : width < 500
                                    ? 30
                                    : 35,
                        fontSize: width >= 700
                            ? 20
                            : width < 410
                                ? 12
                                : width < 500
                                    ? 14
                                    : 17,
                        iconSize: width >= 700
                            ? 21
                            : width < 450
                                ? 15
                                : 18,
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    }
  }
}
