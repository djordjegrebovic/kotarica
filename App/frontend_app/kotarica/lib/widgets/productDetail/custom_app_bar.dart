// import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kotarica/eth_models/PorukeModels.dart';
// import 'package:kotarica/AllInfoBuilder.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
// import 'package:kotarica/eth_models/user_loginModels.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/pages/edit_product.dart';
import 'package:kotarica/utils/boje.dart';
// import 'package:kotarica/pages/my_cart.dart';
import 'package:kotarica/widgets/productDetail/add.dart';
import 'package:kotarica/widgets/productDetail/basketButton.dart';
import 'package:kotarica/widgets/productDetail/rounded_icon_button.dart';
import 'package:provider/provider.dart';

class CustomAppBar extends PreferredSize {
  final double rating;
  final Product product;
  final PorukeModel porukaModel;

  CustomAppBar(
      {@required this.rating, @required this.product, this.porukaModel});

  @override
  // AppBar().preferredSize.height provide us the height that appy on our app bar
  Size get preferredSize => Size.fromHeight(AppBar().preferredSize.height);
  @override
  Widget build(BuildContext context) {
    var now = new DateTime.now();
    var formatter = new DateFormat('dd.MM.yyyy.');
    String formattedDate = formatter.format(now);
    var addListener = Provider.of<AddModel>(context);
    double width = MediaQuery.of(context).size.width;
    var boje = Theme.of(context).colorScheme;
    Color myColor = Color(0xff00bfa5);
    final TextEditingController porukacontroller = TextEditingController();

    openAlertBox() {
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(32.0))),
              contentPadding: EdgeInsets.only(top: 10.0),
              content: Container(
                width: width < 600 ? width * 0.90 : 600,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 30.0, right: 30.0),
                      child: TextField(
                        controller: porukacontroller,
                        decoration: InputDecoration(
                          hintText: "Postavite pitanje.",
                          border: InputBorder.none,
                        ),
                        maxLines: 8,
                      ),
                    ),
                    InkWell(
                      child: Container(
                        padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                        decoration: BoxDecoration(
                          color: myColor,
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(32.0),
                              bottomRight: Radius.circular(32.0)),
                        ),
                        child: Text(
                          "Pošalji pitanje",
                          style: TextStyle(color: Colors.white),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      onTap: () async {
                        // print(porukacontroller.text);
                        Navigator.pop(context);

                        // ovde saljem poruku
                        await porukaModel.createMsg(
                            product.id,
                            product.idOglasavaca,
                            GetInfo.id,
                            porukacontroller.text,
                            formattedDate.toString());
                        porukacontroller.clear();
                        // snackBarMessage(context);
                      },
                    ),
                  ],
                ),
              ),
            );
          });
    }

    return SafeArea(
      child: Container(
        // margin: EdgeInsets.symmetric(horizontal: 100, vertical: 5),
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        // width: double.infinity,
        // width: 200,
        color: Theme.of(context).colorScheme.primary,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              height: 40,
              width: 40,
              // ignore: deprecated_member_use
              child: FlatButton(
                splashColor: Theme.of(context).colorScheme.onSurface,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(60),
                ),
                padding: EdgeInsets.zero,
                onPressed: () => Navigator.pop(context),
                child:
                    Icon(Icons.arrow_back_ios_outlined, color: Boje.mainText),
              ),
            ),

            // Polje za rejting
            Row(
              children: [
                Tooltip(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  height: 35,
                  textStyle: TextStyle(
                      fontSize: 15,
                      color: Colors.white,
                      fontWeight: FontWeight.w600),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: width > 600 ? boje.onPrimary : boje.secondary,
                  ),
                  message: rating != 0.0
                      ? 'Prosečna ocena proizvoda'
                      : 'Proizvod još nije ocenjen',
                  child: Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 14, vertical: 5),
                    decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.onBackground,
                      borderRadius: BorderRadius.circular(14),
                    ),
                    child: Row(
                      children: [
                        rating != 0.0
                            ? Text(
                                "$rating",
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.surface,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ),
                              )
                            : Icon(Icons.not_interested,
                                color: Theme.of(context).colorScheme.error),
                        const SizedBox(width: 5),
                        Icon(Icons.star,
                            color: Theme.of(context).colorScheme.primary)
                      ],
                    ),
                  ),
                ),
                SizedBox(width: 5),
                // Korpa
                BasketButton(
                    addListener: addListener,
                    color: Theme.of(context).colorScheme.onBackground,
                    margin: 0),
                // Dugme za izmenu
                SizedBox(width: 5),
                GetInfo.id != BigInt.zero && product.idOglasavaca == GetInfo.id
                    ? RoundedIconBtn(
                        text: '',
                        icon: Icons.edit,
                        showShadow: false,
                        height: 35,
                        width: 55,
                        size: 25,
                        press: () {
                          Navigator.pushNamed(context, EditProduct.routeName,
                              arguments:
                                  EditProductArguments(product: this.product));
                          // Navigator.push(
                          //     context,
                          //     new MaterialPageRoute(
                          //         builder: (context) => EditProduct(product)));
                        }, //Mogucnost da se edituje
                        buttonColor: Theme.of(context).colorScheme.onBackground,
                        iconColor: Colors.teal,
                      )
                    : SizedBox(),
                GetInfo.id != BigInt.zero && product.idOglasavaca != GetInfo.id
                    ? Tooltip(
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                        height: 35,
                        textStyle: TextStyle(
                            fontSize: 15,
                            color: Colors.white,
                            fontWeight: FontWeight.w600),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: width > 600 ? boje.onPrimary : boje.secondary,
                        ),
                        message: 'Pošaljite poruku oglašavaču',
                        child: RoundedIconBtn(
                          text: '',
                          icon: Icons.message_rounded,
                          showShadow: false,
                          height: 35,
                          width: 55,
                          size: 25,
                          press: () {
                            openAlertBox();
                          }, //posalji poruku
                          buttonColor:
                              Theme.of(context).colorScheme.onBackground,
                          iconColor: Colors.teal,
                        ),
                      )
                    : SizedBox(),
              ],
            )
          ],
        ),
      ),
    );
  }
}
