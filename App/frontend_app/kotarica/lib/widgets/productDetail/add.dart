import 'package:flutter/material.dart';
import 'package:kotarica/eth_models/ProductModels.dart';

class AddModel extends ChangeNotifier {
  // Global variables
  // List<Product> _products = [];
  static List<Product> korpa = [];
  Product _activeProduct;

  // Constructor to initialize variables
  // AddModel() {
  //   // _products = [];

  //   // _baskets = [];

  //   //send notification to the provider
  //   // whoever subscribed to a subscriber or provider which is everything inside
  //   // your aplication you get notifcations
  //   notifyListeners();
  // }

  //in order for outside world of the UI to have access to this globl variables
  // that will be done through use of GETTER function
  // List<Product> get products => _products;
  // List<Product> get products() {
  //   return _products;
  // }
  //
  List<Product> get baskets => korpa;
  Product get activeProduct => _activeProduct;

  getProductSubtotal(Product p) {
    int subtotal = p.quantity * p.cena.toInt();

    return subtotal;
  }

  // setActiveProduct(Product p) {
  //   _activeProduct = p;
  // }

  setProductQty(Product p, int qty) {
    p.quantity = qty;
    notifyListeners();
  }

  subQty(Product p, int qty) {
    p.quantity += (qty - 1);
    notifyListeners();
  }

  removeQty(Product p, int qty) {
    p.quantity -= (qty);
    notifyListeners();
  }

  addOneItemToBasket(Product p) {
    // find if the product(p) alredy in the baskets
    // if that is the case just increment the qty property of product(p)

    Product found = baskets.firstWhere((i) => i.id == p.id,
        orElse: () =>
            null); // i ide kroz sve proizvode u _baskets i pokusava da nadje isti id kao item koji je prosledjen tj p
    if (found != null) {
      if (p.quantity < 30) p.quantity += 1;
    } else
      baskets.add(p);

    notifyListeners();
  }

  removeItemFromBasket(Product p) {
    baskets.remove(p);
    notifyListeners();
  }

  removeOneItemFromBasket(Product p) {
    // find if the product(p) alredy in the baskets
    // if that is the case just increment the qty property of product(p)

    Product found = baskets.firstWhere((i) => i.id == p.id,
        orElse: () =>
            null); // i ide kroz sve proizvode u _baskets i pokusava da nadje isti id kao item koji je prosledjen tj p
    if (found != null && found.quantity == 1)
      baskets.remove(p);
    else
      p.quantity -= 1;

    notifyListeners();
  }

  getBasketQty() {
    int total = 0;
    for (int i = 0; i < baskets.length; i++) {
      total += baskets[i].quantity;
    }
    //notifyListeners();
    return total;
  }

  getProductQty(Product p) {
    Product found = baskets.firstWhere((i) => i.id == p.id, orElse: () => null);
    if (found != null)
      return p.quantity;
    else
      return 0;

    // notifyListeners();
  }

  getBasketSum() {
    int sum = 0;
    for (int i = 0; i < baskets.length; i++) {
      sum += (baskets[i].quantity * baskets[i].cena.toInt());
      //sum += getProductSubtotal(products[i]);
    }
    //notifyListeners();
    return sum;
  } //Dobra je funkcija

  // void clearBasket() {
  //   _baskets.clear();
  //   notifyListeners();
  // }
}
