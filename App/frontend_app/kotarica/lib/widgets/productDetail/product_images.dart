import 'package:flutter/material.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/utils/const.dart';
// import 'package:kotarica/widgets/images/image_load.dart';

class ProductImages extends StatefulWidget {
  const ProductImages({
    Key key,
    @required this.product,
  }) : super(key: key);

  final Product product;

  @override
  _ProductImagesState createState() => _ProductImagesState();
}

class _ProductImagesState extends State<ProductImages> {
  int selectedImage = 0;

  @override
  Widget build(BuildContext context) {
    // double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Container(
      decoration: width > 600
          ? BoxDecoration(
              // color: Theme.of(context).colorScheme.background,
              color: Color.fromARGB(255, 143, 166, 163),
              // borderRadius: BorderRadius.only(
              //   bottomLeft: Radius.circular(40),
              //   bottomRight: Radius.circular(40),
              // ),
            )
          : null,
      padding: EdgeInsets.only(bottom: 20, top: 15),
      width: width < 600 ? width : 600,
      child: Column(
        children: [
          SizedBox(
            width: 238,
            child: AspectRatio(
              aspectRatio: 1,
              child: widget.product.hesh == ""
                  ? Hero(
                      tag: widget.product.id.toString(),
                      child: Image.asset(widget.product.images[0]))
                  : ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: Image(
                        image: NetworkImage(
                            Const.url + "/api/Image/" + widget.product.hesh),
                        fit: BoxFit.fill,
                      ),
                  ),
            ),
          ),
        
        ],
      ),
    );
  }

  GestureDetector buildSmallProductPreview(int index) {
    double width = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedImage = index;
        });
      },
      child: AnimatedContainer(
        duration: Duration(milliseconds: 250),
        margin: EdgeInsets.symmetric(horizontal: 7),
        padding: EdgeInsets.all(8),
        height: width < 500 ? 48 : 60,
        width: width < 500 ? 48 : 60,
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.background,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
              color: Theme.of(context)
                  .colorScheme
                  .secondaryVariant
                  .withOpacity(selectedImage == index ? 1 : 0)),
        ),
        child: Image.asset(widget.product.images[index]),
      ),
    );
  }
}
