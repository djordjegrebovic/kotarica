import 'package:flutter/material.dart';
import 'package:kotarica/pages/welcome_screen.dart';
import 'utils/themes.dart';

void main() {
  runApp(Main2());
}

class Main2 extends StatefulWidget {
  // This widget is the root of your application.

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<Main2> {
  ThemeData theme;

  @override
  void initState() {
    theme = Themes.dark; // To-do: Load from user settings
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Kotarica',
      theme: theme,
      home: WelcomeScreen(),
    );
  }
}

/*
import 'package:flutter/material.dart';
import 'package:kotarica/widgets/login.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'eth_models/user_loginModels.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    /*
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: TodoList(),
    );
    */

    //ovako ispod se poziva model za eth
    return ChangeNotifierProvider(
        create: (context) => userLoginModel(),
        child: MaterialApp(title: 'flutter demo', home: LoginScreen()));
  }
}
*/
