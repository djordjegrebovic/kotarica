import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart';
import 'package:kotarica/AllInfoBuilder.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:web3dart/web3dart.dart';
import 'package:web_socket_channel/io.dart';
// import 'package:universal_html/html.dart' as universal;

class PorukeModel extends ChangeNotifier {
  var mojePoruke;
  var porukeProizvoda;
  var stiglePoruke;
  var chatHeads;
  var celaKonverzacija;
  var zadnjePoruke;

  bool isLoading = true;
  bool isLoading1 = true;
  bool isLoading2 = true;
  bool isLoadingDajChatHeads = true;
  bool isLoadingDajPoruke = true;
  bool izLoadingCelaKonverzacija = true;
  bool isLoadingZadnjePoruke = true;
  static bool vrtiPonovo = false;
  static bool vrtiPonovoPoruke = false;

  final String _rpcUrl = "http://147.91.204.116:11070";
  final String _wsUrl = "ws://147.91.204.116:11070/";

  final String _privateKey = AllInfos.privateKey;

  int msgCount = 0;
  Web3Client _client;
  String _abiCode;
  Credentials _credentials;
  EthereumAddress _contractAddress;
  // ignore: unused_field
  EthereumAddress _ownAddress;
  DeployedContract _contract;
  ContractFunction _chatHeads;
  ContractFunction _msgCount;
  ContractFunction _getMsgsForProduct;
  ContractFunction _getMySendMsgs;
  ContractFunction _vratiCeluKonverzaciju;
  ContractFunction _createnewMsg;
  ContractFunction _zadnjePoruke;

  // ignore: unused_field
  ContractEvent _msgCreatedEvent;
  ContractFunction _getRecievedMsgs;

  PorukeModel() {
    initiateSetup();
  }

  Future<void> initiateSetup() async {
    _client = Web3Client(_rpcUrl, Client(), socketConnector: () {
      return IOWebSocketChannel.connect(_wsUrl).cast<String>();
    });

    await getAbi();
    await getCredentials();
    await getDeployedContract();
  }

  Future<void> getAbi() async {
    String abiStringFile =
        await rootBundle.loadString("src/abis/poruke_ugovor.json");
    var jsonAbi = jsonDecode(abiStringFile);
    _abiCode = jsonEncode(jsonAbi["abi"]);
    //print(jsonAbi["networks"]);
    _contractAddress =
        EthereumAddress.fromHex(jsonAbi["networks"]["5777"]["address"]);
    //print("Contract address " + _contractAddress);
    // print("Privatni kljuc " + _privateKey);
  }

  Future<void> getCredentials() async {
    _credentials = await _client.credentialsFromPrivateKey(_privateKey);
    _ownAddress = await _credentials.extractAddress();
  }

  Future<void> getDeployedContract() async {
    _contract = DeployedContract(
        ContractAbi.fromJson(_abiCode, "poruke_ugovor"), _contractAddress);

    _msgCount = _contract.function("brojPoruka");
    _createnewMsg = _contract.function("novaPoruka");

    _getMySendMsgs = _contract.function("vratiMojePoslatePoruke");
    _getRecievedMsgs = _contract.function("vratiMojePrimljeneRecenzije");
    _getMsgsForProduct = _contract.function("vratiPorukeZaProizvod");
    _msgCreatedEvent = _contract.event("kreiranaPoruka");
    _vratiCeluKonverzaciju = _contract.function("celaKonverzacija");
    _chatHeads = _contract.function("vratiLjudeSaKojimaSeDopisujem");
    _zadnjePoruke = _contract.function("vratiZadnjePoruke");

    getMsgsCount();

    // if (UserLoginModel.id != null) getMySentMsgs(UserLoginModel.id);
  }

  getMsgsCount() async {
    isLoading = true;
    List totalProductsList = await _client
        .call(contract: _contract, function: _msgCount, params: []);

    BigInt totalUsers = totalProductsList[0];
    msgCount = totalUsers.toInt();
    isLoading = false;
    notifyListeners();
  }

  createMsg(BigInt _idProizvoda, BigInt _idKupca, BigInt _idOglasavaca,
      String _poruka, String _datum) async {
    // isLoading = true;
    notifyListeners();
    await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            maxGas: 6721975,
            gasPrice: EtherAmount.inWei(BigInt.one),
            contract: _contract,
            function: _createnewMsg,
            parameters: [
              _idProizvoda,
              _idKupca,
              _idOglasavaca,
              _poruka,
              _datum
            ]));
    // isLoading = false;
    //getReviewCount();
  }

  getMySentMsgs(BigInt _userID) async {
    // print("uso u poruke");
    mojePoruke = await _client.call(
        sender: _contractAddress,
        contract: _contract,
        function: _getMySendMsgs,
        params: [_userID]);

    //print("moji poslati zahtevi su");
    // print(mojePoruke);
    isLoading1 = false;
    notifyListeners();
  }

  getWholeConversation(BigInt _kupac, BigInt _prodavac) async {
    izLoadingCelaKonverzacija = true;

    celaKonverzacija = await _client.call(
        sender: _contractAddress,
        contract: _contract,
        function: _vratiCeluKonverzaciju,
        params: [_kupac, _prodavac]);

    //print("moji poslati zahtevi su");
    // print(celaKonverzacija);
    izLoadingCelaKonverzacija = false;
    isLoading1 = false;
    notifyListeners();
    // izLoadingCelaKonverzacija = true;
  }

  getChatHeads(BigInt _userID) async {
    isLoadingDajChatHeads = true;

    // print("uso u poruke");
    chatHeads = await _client.call(
        sender: _contractAddress,
        contract: _contract,
        function: _chatHeads,
        params: [_userID]);

    //print("moji poslati zahtevi su");
    // print(chatHeads);
    // isLoading1 = false;
    isLoadingDajChatHeads = false;

    notifyListeners();
  }

  getLastMsgs(BigInt _userID, List<BigInt> _ljudi, BigInt _duzina) async {
    isLoadingZadnjePoruke = true;

    // print("uso u poruke");
    zadnjePoruke = await _client.call(
        sender: _contractAddress,
        contract: _contract,
        function: _zadnjePoruke,
        params: [_userID, _ljudi, _duzina]);

    //print("moji poslati zahtevi su");
    // print(chatHeads);
    // isLoading1 = false;
    isLoadingZadnjePoruke = false;

    notifyListeners();
  }

  getRecievedMsgs(BigInt _userID) async {
    isLoadingDajPoruke = true;
    stiglePoruke = await _client.call(
        sender: _contractAddress,
        contract: _contract,
        function: _getRecievedMsgs,
        params: [_userID]);

    //print("moji poslati zahtevi su");
    // print(stiglePoruke);
    isLoading2 = false;
    isLoadingDajPoruke = false;
    notifyListeners();
  }

  getProductMsgs(BigInt _productID) async {
    porukeProizvoda = await _client.call(
        sender: _contractAddress,
        contract: _contract,
        function: _getMsgsForProduct,
        params: [_productID]);

    notifyListeners();
  }

  List<Poruke> getMyRecievedMsgsInList(BigInt _userId) {
    /*
      guide
        uint256 idProizvoda = mojiZahtev[0][i][0]
        uint256 idKupca = mojiZahtev[0][i][1]
        uint256 idOglasavaca = mojiZahtev[0][i][2]
        uint256 ocena = mojiZahtev[0][i][3]
        string komentar = mojiZahtev[0][i][4]
    */

    //getFavourites(_userId);
    // ignore: deprecated_member_use
    List<Poruke> productsById = new List<Poruke>();
    // int br = 0;
    // print(stiglePoruke);
    for (var i = 0; i < stiglePoruke[0].length; i++) {
      //print(mojiOmiljeniInt[i].toInt().toString() + "omiljeni");
      for (var j = 0; j < ProductModels.products.length; j++) {
        if (stiglePoruke[0][i][0] == ProductModels.products[j].id &&
            stiglePoruke[0][i][2] == _userId) {
          productsById.add(new Poruke(
            idProizvoda: stiglePoruke[0][i][0],
            idKupca: stiglePoruke[0][i][1],
            idOglasavaca: stiglePoruke[0][i][2],
            poruka: stiglePoruke[0][i][3],
            nazivProizvoda: ProductModels.products[j].naziv,
          ));
        }
      }
    }

    return productsById;
  }

  List<Poruke> getSentMsgsInList(BigInt _userId) {
    /*
      guide
        uint256 idProizvoda = mojiZahtev[0][i][0]
        uint256 idKupca = mojiZahtev[0][i][1]
        uint256 idOglasavaca = mojiZahtev[0][i][2]
        uint256 ocena = mojiZahtev[0][i][3]
        string komentar = mojiZahtev[0][i][4]
    */

    //getFavourites(_userId);
    // ignore: deprecated_member_use
    List<Poruke> productsById = new List<Poruke>();
    // int br = 0;
    for (var i = 0; i < mojePoruke[0].length; i++) {
      //print(mojiOmiljeniInt[i].toInt().toString() + "omiljeni");
      for (var j = 0; j < ProductModels.products.length; j++) {
        if (mojePoruke[0][i][0] == ProductModels.products[j].id &&
            mojePoruke[0][i][1] == _userId) {
          productsById.add(new Poruke(
            idProizvoda: mojePoruke[0][i][0],
            idKupca: mojePoruke[0][i][1],
            idOglasavaca: mojePoruke[0][i][2],
            poruka: mojePoruke[0][i][3],
            nazivProizvoda: ProductModels.products[j].naziv,
          ));
        }
      }
    }

    return productsById;
  }

  List<Poruke> getWholeConverationInList(BigInt _oglasavac) {
    /*
      guide
        uint256 idProizvoda = mojiZahtev[0][i][0]
        uint256 idKupca = mojiZahtev[0][i][1]
        uint256 idOglasavaca = mojiZahtev[0][i][2]
        uint256 ocena = mojiZahtev[0][i][3]
        string komentar = mojiZahtev[0][i][4]
    */

    //getFavourites(_userId);
    // ignore: deprecated_member_use
    List<Poruke> productsById = new List<Poruke>();
    // int br = 0;
    for (var i = 0; i < celaKonverzacija[0].length; i++) {
      //print(mojiOmiljeniInt[i].toInt().toString() + "omiljeni");

      productsById.add(new Poruke(
          idProizvoda: celaKonverzacija[0][i][0],
          idKupca: celaKonverzacija[0][i][1],
          idOglasavaca: celaKonverzacija[0][i][2],
          poruka: celaKonverzacija[0][i][3],
          nazivProizvoda: "",
          datum: celaKonverzacija[0][i][4],
          prikaziDatum: false,
          oglasavac: celaKonverzacija[0][i][2] == _oglasavac ? true : false));
    }

    return productsById;
  }

  List<Poruke> getMsgsForProductInList(BigInt _productID) {
    /*
      guide
        uint256 idProizvoda = mojiZahtev[0][i][0]
        uint256 idKupca = mojiZahtev[0][i][1]
        uint256 idOglasavaca = mojiZahtev[0][i][2]
        uint256 ocena = mojiZahtev[0][i][3]
        string komentar = mojiZahtev[0][i][4]
    */

    //getFavourites(_userId);
    // ignore: deprecated_member_use
    List<Poruke> productsById = new List<Poruke>();
    // int br = 0;
    for (var i = 0; i < porukeProizvoda[0].length; i++) {
      //print(mojiOmiljeniInt[i].toInt().toString() + "omiljeni");
      for (var j = 0; j < ProductModels.products.length; j++) {
        if (porukeProizvoda[0][i][0] == ProductModels.products[j].id &&
            porukeProizvoda[0][i][0] == _productID) {
          // print(_productID);
          productsById.add(new Poruke(
            idProizvoda: porukeProizvoda[0][i][0],
            idKupca: porukeProizvoda[0][i][1],
            idOglasavaca: porukeProizvoda[0][i][2],
            poruka: porukeProizvoda[0][i][3],
            nazivProizvoda: ProductModels.products[j].naziv,
          ));
        }
      }
    }

    return productsById;
  }

  List<BigInt> dajMiKorisnikeKojiSuMmiPoslaliPoruku() {
    // print("chat heads");

    // print(chatHeads);
    // ignore: deprecated_member_use
    List<BigInt> korisnici = new List<BigInt>();
    if (chatHeads[0].length > 0)
      for (var i = 0; i < chatHeads[0].length; i++) {
        if (chatHeads[0][i] != BigInt.zero && chatHeads[0][i] != GetInfo.id)
          korisnici.add(chatHeads[0][i]);
      }
    else
      return null;
    return korisnici;
  }

  List<ZadnjePoruke> dajMiZadnjePorukeUListu() {
    // ignore: deprecated_member_use
    List<ZadnjePoruke> poruke = new List<ZadnjePoruke>();
    for (var i = 0; i < zadnjePoruke[0].length; i++) {
      poruke.add(new ZadnjePoruke(
          poruka: zadnjePoruke[0][i][0], datum: zadnjePoruke[0][i][1]));
    }
    return poruke;
  }
}

class ZadnjePoruke {
  String poruka;
  String datum;
  ZadnjePoruke({this.poruka, this.datum});
}

class Poruke {
  BigInt idProizvoda;
  BigInt idKupca;
  BigInt idOglasavaca;
  String poruka;
  String nazivProizvoda;
  bool oglasavac;
  String datum;
  bool prikaziDatum;

  Poruke(
      {this.idProizvoda,
      this.idKupca,
      this.idOglasavaca,
      this.poruka,
      this.nazivProizvoda,
      this.oglasavac,
      this.datum,
      this.prikaziDatum});
}
