import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart';
import 'package:kotarica/AllInfoBuilder.dart';
import 'package:kotarica/models/TestModel.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:universal_html/html.dart' as universal;
import 'package:web3dart/web3dart.dart';
import 'package:web_socket_channel/io.dart';
import 'package:corsac_jwt/corsac_jwt.dart';

class UserLoginModel extends ChangeNotifier {
  List<User> users = [];
  List<UserData> userData = [];
  bool isLoading = true;
  bool isLoading1 = true;
  bool isLoading2 = true;
  bool isLoading3 = true;
  String hashProfile;
  final String _rpcUrl = "http://147.91.204.116:11070";
  final String _wsUrl = "ws://147.91.204.116:11070/";
  static BigInt id;
  final String _privateKey = AllInfos.privateKey;
  int usersCount = 0;
  int usersDataCount = 0;
  Web3Client _client;
  String _abiCode;
  Credentials _credentials;
  EthereumAddress _contractAddress;
  // ignore: unused_field
  EthereumAddress _ownAddress;
  DeployedContract _contract;
  ContractFunction _usersCount;
  ContractFunction _usersDataCount;
  ContractFunction _users;
  ContractFunction _usersData;
  ContractFunction _createUser;
  ContractFunction _createUserData;
  ContractFunction _getProfilePicture;
  // ignore: unused_field
  ContractEvent _userCreatedEvent;
  // ignore: unused_field
  ContractEvent _userDataCreatedEvent;
  ContractFunction _findUserAndLogin;
  ContractFunction _findUserAndLogout;
  ContractFunction _changePassword;
  ContractFunction _changePrivateKey;

  //ContractFunction _findUserAndLogoutWithID;
  static String sifra;
  static String privKey;
  static int index;
  static List<UserData> usersDataList;
  UserLoginModel() {
    initiateSetup();
  }

  Future<void> initiateSetup() async {
    _client = Web3Client(_rpcUrl, Client(), socketConnector: () {
      return IOWebSocketChannel.connect(_wsUrl).cast<String>();
    });

    await getAbi();
    await getCredentials();
    await getDeployedContract();
  }

  Future<void> getAbi() async {
    String abiStringFile =
        await rootBundle.loadString("src/abis/users_login.json");
    var jsonAbi = jsonDecode(abiStringFile);
    _abiCode = jsonEncode(jsonAbi["abi"]);
    _contractAddress =
        EthereumAddress.fromHex(jsonAbi["networks"]["5777"]["address"]);
    // print(_contractAddress);
  }

  Future<void> getCredentials() async {
    _credentials = await _client.credentialsFromPrivateKey(_privateKey);
    _ownAddress = await _credentials.extractAddress();
  }

  Future<void> getDeployedContract() async {
    _contract = DeployedContract(
        ContractAbi.fromJson(_abiCode, "users_login"), _contractAddress);

    _usersCount = _contract.function("usersCount");
    _usersDataCount = _contract.function("usersDataCount");

    _createUser = _contract.function("createUser");
    _createUserData = _contract.function("createUserData");
    _getProfilePicture = _contract.function("returnHashCode");
    _changePrivateKey = _contract.function("changePrivateKey");
    _changePassword = _contract.function("changePassword");

    _findUserAndLogin = _contract.function("findUserAndLogin");
    _findUserAndLogout = _contract.function("findUserAndLogout");
    // _findUserAndLogoutWithID = _contract.function("findUserAndLogoutWithID");
    _users = _contract.function("users");
    _usersData = _contract.function("users_data");

    _userCreatedEvent = _contract.event("UserCreated");
    _userDataCreatedEvent = _contract.event("UserDataCreated");

    getUsers();
    getUsersData();
    if (GetInfo.id != BigInt.zero) getPrefilePitureHash(GetInfo.id);
    // print("");
  }

  getUsers() async {
    List totalUsersList = await _client
        .call(contract: _contract, function: _usersCount, params: []);

    BigInt totalUsers = totalUsersList[0];
    usersCount = totalUsers.toInt();
    // print(totalUsers);
    users.clear();
    for (var i = 0; i < totalUsers.toInt(); i++) {
      var temp = await _client.call(
          contract: _contract, function: _users, params: [BigInt.from(i)]);
      users.add(User(
          username: temp[0],
          password: temp[1],
          privateKey: temp[2],
          userID: temp[3],
          isLogged: temp[4]));
    }

    isLoading = false;
    notifyListeners();
  }

  getUsersData() async {
    isLoading1 = true;
    List totalUsersList = await _client
        .call(contract: _contract, function: _usersDataCount, params: []);

    BigInt totalUsers = totalUsersList[0];
    usersDataCount = totalUsers.toInt();
    // print(totalUsers);
    userData.clear();
    for (var i = 0; i < totalUsers.toInt(); i++) {
      var temp = await _client.call(
          contract: _contract, function: _usersData, params: [BigInt.from(i)]);
      userData.add(UserData(
        profilePictureHash: temp[0],
        name: temp[1],
        surname: temp[2],
        phoneNumber: temp[3],
        homeAddress: temp[4],
        email: temp[5],
        userID: temp[6],
      ));
      usersDataList = userData;
      // print("UsersDataCount : " + usersDataCount.toString());
    }

    isLoading1 = false;
    notifyListeners();
  }

  String postoji(String username, String password) {
    TestModel test = new TestModel(username, password);

    var builder = new JWTBuilder();
    var jsonEncode2 = jsonEncode(test.toJson());
    // ignore: unused_local_variable
    var token = builder
      ..issuer = 'https://api.foobar.com'
      ..expiresAt = new DateTime.now().add(new Duration(days: 60))
      ..setClaim("data", jsonEncode2)
      ..getToken(); // returns token without signature

    var signer = new JWTHmacSha256Signer('sharedSecret');
    var signedToken = builder.getSignedToken(signer);
    // print(signedToken); // prints encoded JWT
    var stringToken = signedToken.toString();

    // ignore: unused_local_variable
    var decodedToken = new JWT.parse(stringToken);
    // Verify signature:
    // print(decodedToken.verify(signer)); // true

    // print(username + password);
    for (var i = 0; i < usersCount; i++) {
      if (users[i].username == username && users[i].password == password) {
        // print(username + password);
        universal.window.localStorage.clear();
        universal.window.localStorage["id"] = users[i].userID.toString();
        id = users[i].userID;
        dodajusername(username);
        dodajID(users[i].userID.toInt());
        universal.window.localStorage["username"] = username;
        AllInfos.id = users[i].userID;
        GetInfo.id = users[i].userID;
        privKey = users[i].privateKey;

        return stringToken;
      }
    }
    return null;
  }

  BigInt vratiID(String username, String password) {
    BigInt id;
    for (var i = 0; i < usersCount; i++) {
      if (users[i].username == username && users[i].password == password) {
        id = users[i].userID;
        return id;
      }
    }
    return BigInt.zero;
  }

  dodajusername(String username) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('username', username);
  }

  dodajID(int id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('id', id);
  }

  int dajIndex() {
    return index;
  }

  bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return (!regex.hasMatch(value)) ? false : true;
  }

  bool validatePassword(String password, [int minLength = 6]) {
    if (password == null || password.isEmpty) {
      return false;
    }

    bool hasUppercase = password.contains(new RegExp(r'[A-Z]'));
    bool hasDigits = password.contains(new RegExp(r'[0-9]'));
    bool hasLowercase = password.contains(new RegExp(r'[a-z]'));
    bool hasMinLength = password.length > minLength;

    return hasDigits & hasUppercase & hasLowercase & hasMinLength;
  }

  addUser(String username, String password, String key) async {
    isLoading = true;
    notifyListeners();
    await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            maxGas: 6721975,
            gasPrice: EtherAmount.inWei(BigInt.one),
            contract: _contract,
            function: _createUser,
            parameters: [username, password, key]));

    getUsers();
    getUsersData();
  }

  changePassword(BigInt id, String password) async {
    isLoading = true;
    notifyListeners();
    await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            maxGas: 6721975,
            gasPrice: EtherAmount.inWei(BigInt.one),
            contract: _contract,
            function: _changePassword,
            parameters: [id, password]));

    getUsers();
    getUsersData();
    isLoading = false;
  }

  changePrivateKey(BigInt id, String key) async {
    isLoading = true;
    notifyListeners();
    await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            maxGas: 6721975,
            gasPrice: EtherAmount.inWei(BigInt.one),
            contract: _contract,
            function: _changePrivateKey,
            parameters: [id, key]));

    getUsers();
    getUsersData();
    isLoading = false;
  }

  addUserData(
      String profilePictureHash,
      String name,
      String surname,
      String phoneNumber,
      String homeAddress,
      String email,
      BigInt userID) async {
    isLoading = true;
    notifyListeners();
    await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            gasPrice: EtherAmount.inWei(BigInt.one),
            contract: _contract,
            function: _createUserData,
            maxGas: 6721975,
            parameters: [
              profilePictureHash,
              name,
              surname,
              phoneNumber,
              homeAddress,
              email,
              userID
            ]));
    getPrefilePitureHash(GetInfo.id);
    getUsers();
    getUsersData();
  }

  login(String username, String password) async {
    isLoading = true;
    notifyListeners();
    await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            gasPrice: EtherAmount.inWei(BigInt.one),
            contract: _contract,
            function: _findUserAndLogin,
            parameters: [username, password]));

    getUsers();
    getUsersData();
  }

  logoutWithUsernamePassword(String username) async {
    isLoading = true;
    notifyListeners();
    await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            gasPrice: EtherAmount.inWei(BigInt.one),
            contract: _contract,
            function: _findUserAndLogout,
            parameters: [username]));

    getUsers();
  }

  Future<String> getPrefilePitureHash(BigInt _userID) async {
    isLoading3 = true;
    var hashProfilne = await _client.call(
        sender: _contractAddress,
        contract: _contract,
        function: _getProfilePicture,
        params: [_userID]);

    hashProfile = hashProfilne[0];

    isLoading3 = false;
    notifyListeners();
    return hashProfile;
  }

  void displayDialog(context, title, text) => showDialog(
        context: context,
        builder: (context) =>
            AlertDialog(title: Text(title), content: Text(text)),
      );

  UserData giveDataForUser(BigInt id) {
    // print(userData.length.toString() + " user data count");
    if (userData.length > 0)
      for (var i = 0; i < userData.length; i++) {
        // print(userData[i]);
        if (userData[i].userID == id) {
          return userData[i];
        }
      }

    return null;
  }

  Future<UserData> giveDataForUser1(BigInt id) async {
    
    for (var i = 0; i < usersDataCount; i++) {
      if (userData[i].userID == id) {
        return userData[i];
      }
    }
    return null;
  }

  UserData vratiInformacijeOKorisniku(BigInt id) {
    isLoading3 = true;
    for (var i = 0; i < usersDataCount; i++) {
      if (userData[i].userID == id) {
        isLoading3 = false;
        return userData[i];
      }
      
    }

    isLoading3 = false;
    return new UserData(
        name: "Ime i prezime nisu uneti",
        surname: "",
        phoneNumber: "Broj telefona nije unet",
        homeAddress: "Adresa stranovanje nije uneta",
        email: "Email nije unet",
        userID: id);
  }

  User getUserById(BigInt _userId) {
    isLoading = true;
    User oneUser;
    for (var i = 0; i < users.length; i++)
      if (users[i].userID == _userId) {
        oneUser = users[i];
        break;
      }
    isLoading = false;
    return oneUser;
  }

  // ignore: missing_return
  String getUserNameAndSurname(BigInt _userID) {
    isLoading = true;
    for (var i = 0; i < userData.length; i++)
      if (userData[i].userID == _userID) {
        return userData[i].name + " " + userData[i].surname;
      }
    isLoading = false;
  }

/*
  logoutWithID(String username, String password) async {
    isLoading = true;
    notifyListeners();
    await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            gasPrice: EtherAmount.inWei(BigInt.one),
            contract: _contract,
            function: _findUserAndLogoutWithID,
            parameters: [username, password]));

    getUsers();
  }
  */
}

class User {
  String username;
  String password;
  String privateKey;
  BigInt userID;
  bool isLogged;
  User(
      {this.username,
      this.password,
      this.privateKey,
      this.userID,
      this.isLogged});
}

class UserData {
  String profilePictureHash;
  String name;
  String surname;
  String phoneNumber;
  String homeAddress;
  String email;
  BigInt userID;

  UserData(
      {this.profilePictureHash,
      this.name,
      this.surname,
      this.phoneNumber,
      this.homeAddress,
      this.email,
      this.userID});
}
