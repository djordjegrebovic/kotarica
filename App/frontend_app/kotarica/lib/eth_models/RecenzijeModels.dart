import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart';
import 'package:kotarica/AllInfoBuilder.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/eth_models/user_loginModels.dart';
import 'package:web3dart/web3dart.dart';
import 'package:web_socket_channel/io.dart';
// import 'package:universal_html/html.dart' as universal;

class RecenzijeModel extends ChangeNotifier {
  var mojiZahtevi;
  var oceneProizvoda;
  var stigleRecenzije;

  bool isLoading = true;
  bool isLoading1 = true;
  bool isLoading2 = true;
  bool isLoadingProductReviews = true;
  bool isLoadingProductReviews1 = true;
  bool isLoadingProductReviews2 = true;

  final String _rpcUrl = "http://147.91.204.116:11070";
  final String _wsUrl = "ws://147.91.204.116:11070/";

  final String _privateKey = AllInfos.privateKey;

  int reqCount = 0;
  Web3Client _client;
  String _abiCode;
  Credentials _credentials;
  EthereumAddress _contractAddress;
  // ignore: unused_field
  EthereumAddress _ownAddress;
  DeployedContract _contract;

  ContractFunction _reviewsCount;
  ContractFunction _deleteReview;
  ContractFunction _getReviewsForProduct;
  ContractFunction _getMySendReviews;
  ContractFunction _createnewReview;
  // ignore: unused_field
  ContractEvent _reviewCreatedEvent;
  ContractFunction _getRecievedReviews;

  RecenzijeModel() {
    initiateSetup();
  }

  Future<void> initiateSetup() async {
    _client = Web3Client(_rpcUrl, Client(), socketConnector: () {
      return IOWebSocketChannel.connect(_wsUrl).cast<String>();
    });

    await getAbi();
    await getCredentials();
    await getDeployedContract();
  }

  Future<void> getAbi() async {
    String abiStringFile =
        await rootBundle.loadString("src/abis/recenzije.json");
    var jsonAbi = jsonDecode(abiStringFile);
    _abiCode = jsonEncode(jsonAbi["abi"]);
    //print(jsonAbi["networks"]);
    _contractAddress =
        EthereumAddress.fromHex(jsonAbi["networks"]["5777"]["address"]);
    //print("Contract address " + _contractAddress);
    // print("Privatni kljuc " + _privateKey);
  }

  Future<void> getCredentials() async {
    _credentials = await _client.credentialsFromPrivateKey(_privateKey);
    _ownAddress = await _credentials.extractAddress();
  }

  Future<void> getDeployedContract() async {
    _contract = DeployedContract(
        ContractAbi.fromJson(_abiCode, "recenzije"), _contractAddress);

    _reviewsCount = _contract.function("brojRecenzija");
    _createnewReview = _contract.function("novaRecenzija");

    _getMySendReviews = _contract.function("vratiMojePoslateRecenzije");
    _getRecievedReviews = _contract.function("vratiMojePrimljeneRecenzije");
    _getReviewsForProduct = _contract.function("vratiRecenzijeZaProizvod");

    _deleteReview = _contract.function("obrisiZahtev");
    _reviewCreatedEvent = _contract.event("kreiranZahtevZaRecenzijom");

    getReviewCount();

    if (UserLoginModel.id != null) getMyRequests(UserLoginModel.id);
  }

  getReviewCount() async {
    List totalProductsList = await _client
        .call(contract: _contract, function: _reviewsCount, params: []);

    BigInt totalUsers = totalProductsList[0];
    reqCount = totalUsers.toInt();
    isLoading = false;
    notifyListeners();
  }

  createReview(BigInt _idProizvoda, BigInt _idKupca, BigInt _idOglasavaca,
      BigInt _ocena, String _komentar, String _datum) async {
    isLoading = true;
    notifyListeners();
    await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            maxGas: 6721975,
            gasPrice: EtherAmount.inWei(BigInt.one),
            contract: _contract,
            function: _createnewReview,
            parameters: [
              _idProizvoda,
              _idKupca,
              _idOglasavaca,
              _ocena,
              _komentar,
              _datum
            ]));

    //getReviewCount();
  }

  getMyRequests(BigInt _userID) async {
    mojiZahtevi = await _client.call(
        sender: _contractAddress,
        contract: _contract,
        function: _getMySendReviews,
        params: [_userID]);

    //print("moji poslati zahtevi su");
    //print(mojiZahtevi[0][0][4]);
    isLoading1 = false;
    notifyListeners();
  }

  getRecievedReviewsForUser(BigInt _userID) async {
    stigleRecenzije = await _client.call(
        sender: _contractAddress,
        contract: _contract,
        function: _getRecievedReviews,
        params: [_userID]);

    //print("moji poslati zahtevi su");
    // print(stigleRecenzije[0][0][4]);
    isLoading2 = false;
    notifyListeners();
  }

  getProductReviews(BigInt _productID) async {
    // isLoadingProductReviews = true;
    // isLoadingProductReviews2 = true;
    oceneProizvoda = await _client.call(
        sender: _contractAddress,
        contract: _contract,
        function: _getReviewsForProduct,
        params: [_productID]);

    isLoadingProductReviews = false;
    isLoadingProductReviews2 = false;
    notifyListeners();
  }

  double dajOcenuProizvoda(BigInt _productID) {
    BigInt suma = BigInt.zero;
    double srednjaOcena = 0.0;
    if (oceneProizvoda != null) {
      //print("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
      for (var i = 0; i < oceneProizvoda[0].length; i++) {
        suma += oceneProizvoda[0][i][3];
      }
      srednjaOcena = suma.toDouble() / (oceneProizvoda[0].length).toDouble();
      return srednjaOcena;
    } else
      return 0.0;
  }

  deleteReq(BigInt _idProizvoda, BigInt _idKupca, BigInt _idOglasavaca) async {
    isLoading = true;
    notifyListeners();
    await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            maxGas: 6721975,
            gasPrice: EtherAmount.inWei(BigInt.one),
            contract: _contract,
            function: _deleteReview,
            parameters: [_idProizvoda, _idKupca, _idOglasavaca]));

    //print("Obrisan item iz korpe sa id " + _productID.toString());
  }

  List<Recenzije> getMyRecievedReviews(BigInt _userId) {
    /*
      guide
        uint256 idProizvoda = mojiZahtev[0][i][0]
        uint256 idKupca = mojiZahtev[0][i][1]
        uint256 idOglasavaca = mojiZahtev[0][i][2]
        uint256 ocena = mojiZahtev[0][i][3]
        string komentar = mojiZahtev[0][i][4]
    */

    //getFavourites(_userId);
    // ignore: deprecated_member_use
    List<Recenzije> productsById = new List<Recenzije>();
    // int br = 0;
    for (var i = 0; i < stigleRecenzije[0].length; i++) {
      //print(mojiOmiljeniInt[i].toInt().toString() + "omiljeni");
      for (var j = 0; j < ProductModels.products.length; j++) {
        if (stigleRecenzije[0][i][0] == ProductModels.products[j].id &&
            stigleRecenzije[0][i][2] == _userId) {
          productsById.add(new Recenzije(
              idProizvoda: stigleRecenzije[0][i][0],
              idKupca: stigleRecenzije[0][i][1],
              idOglasavaca: stigleRecenzije[0][i][2],
              ocena: stigleRecenzije[0][i][3],
              komentar: stigleRecenzije[0][i][4],
              datum: stigleRecenzije[0][i][5],
              nazivProizvoda: ProductModels.products[j].naziv,
              kategorijaProizvoda: ProductModels.products[j].kategorija,
              cenaProizvoda: ProductModels.products[j].cena));
        }
      }
    }

    return productsById;
  }

  List<Recenzije> getMyRequestedProducts(BigInt _userId) {
    /*
      guide
        uint256 idProizvoda = mojiZahtev[0][i][0]
        uint256 idKupca = mojiZahtev[0][i][1]
        uint256 idOglasavaca = mojiZahtev[0][i][2]
        uint256 ocena = mojiZahtev[0][i][3]
        string komentar = mojiZahtev[0][i][4]
    */

    //getFavourites(_userId);
    // ignore: deprecated_member_use
    List<Recenzije> productsById = new List<Recenzije>();
    // int br = 0;
    for (var i = 0; i < mojiZahtevi[0].length; i++) {
      //print(mojiOmiljeniInt[i].toInt().toString() + "omiljeni");
      for (var j = 0; j < ProductModels.products.length; j++) {
        if (mojiZahtevi[0][i][0] == ProductModels.products[j].id &&
            mojiZahtevi[0][i][1] == _userId) {
          productsById.add(new Recenzije(
              idProizvoda: mojiZahtevi[0][i][0],
              idKupca: mojiZahtevi[0][i][1],
              idOglasavaca: mojiZahtevi[0][i][2],
              ocena: mojiZahtevi[0][i][3],
              komentar: mojiZahtevi[0][i][4],
              datum: mojiZahtevi[0][i][5],
              nazivProizvoda: ProductModels.products[j].naziv,
              kategorijaProizvoda: ProductModels.products[j].kategorija,
              cenaProizvoda: ProductModels.products[j].cena));
        }
      }
    }

    return productsById;
  }

  List<Recenzije> getReviewsForProduct(BigInt _productID) {
    /*
      guide
        uint256 idProizvoda = mojiZahtev[0][i][0]
        uint256 idKupca = mojiZahtev[0][i][1]
        uint256 idOglasavaca = mojiZahtev[0][i][2]
        uint256 ocena = mojiZahtev[0][i][3]
        string komentar = mojiZahtev[0][i][4]
    */

    //getFavourites(_userId);
    // ignore: deprecated_member_use
    List<Recenzije> productsById = new List<Recenzije>();
    // int br = 0;
    for (var i = 0; i < oceneProizvoda[0].length; i++) {
      //print(mojiOmiljeniInt[i].toInt().toString() + "omiljeni");
      for (var j = 0; j < ProductModels.products.length; j++) {
        if (oceneProizvoda[0][i][0] == ProductModels.products[j].id &&
            oceneProizvoda[0][i][0] == _productID) {
          // print(_productID);
          productsById.add(new Recenzije(
              idProizvoda: oceneProizvoda[0][i][0],
              idKupca: oceneProizvoda[0][i][1],
              idOglasavaca: oceneProizvoda[0][i][2],
              ocena: oceneProizvoda[0][i][3],
              komentar: oceneProizvoda[0][i][4],
              datum: oceneProizvoda[0][i][5],
              nazivProizvoda: ProductModels.products[j].naziv,
              kategorijaProizvoda: ProductModels.products[j].kategorija,
              cenaProizvoda: ProductModels.products[j].cena));
        }
      }
    }

    return productsById;
  }
}

class Recenzije {
  BigInt idProizvoda;
  BigInt idKupca;
  BigInt idOglasavaca;
  BigInt ocena;
  String komentar;
  String nazivProizvoda;
  String kategorijaProizvoda;
  BigInt cenaProizvoda;
  String datum;

  Recenzije(
      {this.idProizvoda,
      this.idKupca,
      this.idOglasavaca,
      this.ocena,
      this.komentar,
      this.nazivProizvoda,
      this.kategorijaProizvoda,
      this.cenaProizvoda,
      this.datum});
}
