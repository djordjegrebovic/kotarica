import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart';
import 'package:kotarica/AllInfoBuilder.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:web3dart/web3dart.dart';
import 'package:web_socket_channel/io.dart';

class KupovinaModels extends ChangeNotifier {
  var mojiZahtevi;
  var mojiPristigliZahtevi;

  bool isLoading = true;
  bool isLoading1 = true;
  bool isLoading2 = true;

  final String _rpcUrl = "http://147.91.204.116:11070";
  final String _wsUrl = "ws://147.91.204.116:11070/";

  final String _privateKey = AllInfos.privateKey;

  int reqCount = 0;
  Web3Client _client;
  String _abiCode;
  Credentials _credentials;
  EthereumAddress _contractAddress;
  // ignore: unused_field
  EthereumAddress _ownAddress;
  DeployedContract _contract;

  ContractFunction _purchaseReqCount;
  ContractFunction _deleteReq;
  ContractFunction _getMyReqs;
  ContractFunction _createPurchaseReq;
  // ignore: unused_field
  ContractEvent _reqCreatedEvent;
  ContractFunction _getRecievedReq;
  ContractFunction _changeReqStatus;
  ContractFunction _dajOcenu;

  KupovinaModels() {
    initiateSetup();
  }

  Future<void> initiateSetup() async {
    _client = Web3Client(_rpcUrl, Client(), socketConnector: () {
      return IOWebSocketChannel.connect(_wsUrl).cast<String>();
    });

    await getAbi();
    await getCredentials();
    await getDeployedContract();
  }

  Future<void> getAbi() async {
    String abiStringFile =
        await rootBundle.loadString("src/abis/kupovina.json");
    var jsonAbi = jsonDecode(abiStringFile);
    _abiCode = jsonEncode(jsonAbi["abi"]);
    _contractAddress =
        EthereumAddress.fromHex(jsonAbi["networks"]["5777"]["address"]);
    // print("Privatni kljuc " + _privateKey);
  }

  Future<void> getCredentials() async {
    _credentials = await _client.credentialsFromPrivateKey(_privateKey);
    _ownAddress = await _credentials.extractAddress();
  }

  Future<void> getDeployedContract() async {
    _contract = DeployedContract(
        ContractAbi.fromJson(_abiCode, "kupovina"), _contractAddress);

    _purchaseReqCount = _contract.function("brojZahtevaZaKupovinu");
    _createPurchaseReq = _contract.function("noviZahtevZaKupovinu");
    _dajOcenu = _contract.function("dajOcenu");
    _getMyReqs = _contract.function("vratiMojePoslateZahteveZaKupovinu");
    _getRecievedReq = _contract.function("vratiMojePristigleZahteveZaKupovinu");
    _deleteReq = _contract.function("obrisiZahtev");
    _reqCreatedEvent = _contract.event("kreiranZahtevZaKupovinu");
    _changeReqStatus = _contract.function("promeniStatusZahtevaZaKupovinu");

    getRequestsCount();
    //getMyRequests(GetInfo.id);
    if (GetInfo.id != BigInt.zero) getRecievedRequests(GetInfo.id);
  }

  getRequestsCount() async {
    List totalProductsList = await _client
        .call(contract: _contract, function: _purchaseReqCount, params: []);

    BigInt totalUsers = totalProductsList[0];
    reqCount = totalUsers.toInt();
    isLoading = false;
    notifyListeners();
  }

  ocenjenTrue(
    BigInt _id,
  ) async {
    isLoading = true;
    await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            maxGas: 6721975,
            gasPrice: EtherAmount.inWei(BigInt.one),
            contract: _contract,
            function: _dajOcenu,
            parameters: [_id]));

    getRequestsCount();
    //getMyRequests(GetInfo.id);
    //getRecievedRequests(GetInfo.id);
    isLoading = false;
    notifyListeners();
  }

  createRequest(
      BigInt _idProizvoda,
      BigInt _idKupca,
      BigInt _cena,
      String _adresa,
      String _brojTelefona,
      BigInt _kolicina,
      BigInt _idOglasavaca,
      String _datum) async {
    await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            maxGas: 6721975,
            gasPrice: EtherAmount.inWei(BigInt.one),
            contract: _contract,
            function: _createPurchaseReq,
            parameters: [
              _idProizvoda,
              _idKupca,
              _kolicina,
              _idOglasavaca,
              _datum,
              _cena,
              _adresa,
              _brojTelefona
            ]));

    getRequestsCount();
    //getMyRequests(GetInfo.id);
    //getRecievedRequests(GetInfo.id);
    isLoading = false;
    notifyListeners();
  }

  getMyRequests(BigInt _userID) async {
    isLoading1 = true;
    mojiZahtevi = await _client.call(
        sender: _contractAddress,
        contract: _contract,
        function: _getMyReqs,
        params: [_userID]);
    // print("moji poslati zahtevi su");
    //print(mojiZahtevi);
    isLoading1 = false;
    notifyListeners();
  }

  getRecievedRequests(BigInt _userID) async {
    isLoading2 = true;
    mojiPristigliZahtevi = await _client.call(
        sender: _contractAddress,
        contract: _contract,
        function: _getRecievedReq,
        params: [_userID]);

    //print("moji pristigli zahtevi su");
    //print(mojiPristigliZahtevi);
    isLoading2 = false;
    notifyListeners();
  }

  deleteReq(BigInt _id) async {
    isLoading = true;
    await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            maxGas: 6721975,
            gasPrice: EtherAmount.inWei(BigInt.one),
            contract: _contract,
            function: _deleteReq,
            parameters: [_id]));

    await getMyRequests(GetInfo.id);
    await getRecievedRequests(GetInfo.id);
    isLoading = false;
    notifyListeners();

    //print("Obrisan item iz korpe sa id " + _productID.toString());
  }

  changeReqStatus(
      BigInt _id, BigInt _status, String _poruka, String _datum) async {
    await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
          maxGas: 6721975,
          gasPrice: EtherAmount.inWei(BigInt.one),
          contract: _contract,
          function: _changeReqStatus,
          parameters: [_id, _status, _poruka, _datum],
        ));
    getMyRequests(GetInfo.id);
    getRecievedRequests(GetInfo.id);
    isLoading = false;
    notifyListeners();
  }

  List<OrderRequest> getMyRequestedOrderRequest() {
    // ignore: deprecated_member_use
    List<OrderRequest> myRequestedOrders = new List<OrderRequest>();
    // print("asdasdasdasdasdasdsd " + mojiZahtevi[0].length.toString());
    for (var i = mojiZahtevi[0].length - 1; i >= 0; i--) {
      for (var j = 0; j < ProductModels.products.length; j++)
        if (mojiZahtevi[0][i][1] == ProductModels.products[j].id) {
          myRequestedOrders.add(new OrderRequest(
            id: mojiZahtevi[0][i][0],
            idProizvoda: mojiZahtevi[0][i][1],
            idKupca: mojiZahtevi[0][i][2],
            cena: mojiZahtevi[0][i][3],
            adresa: mojiZahtevi[0][i][4],
            ocenjen: mojiZahtevi[0][i][5],
            brojTelefona: mojiZahtevi[0][i][6],
            kolicina: mojiZahtevi[0][i][7],
            idOglasivaca: mojiZahtevi[0][i][8],
            status: mojiZahtevi[0][i][9],
            poruka: mojiZahtevi[0][i][10],
            datum: mojiZahtevi[0][i][11],
          ));
          // print("OSAOOSMASODASDASDASDASDASDASD");
        }
    }

    return myRequestedOrders;
  }

  List<OrderRequest> getMyRecievedOrderRequest() {
    //print(mojiPristigliZahtevi);
    // ignore: deprecated_member_use
    List<OrderRequest> myRecivedOrderRequest1 = new List<OrderRequest>();

    for (var i = 0; i < mojiPristigliZahtevi[0].length; i++) {
      for (var j = 0; j < ProductModels.products.length; j++)
        if (mojiPristigliZahtevi[0][i][1] == ProductModels.products[j].id) {
          myRecivedOrderRequest1.add(new OrderRequest(
            id: mojiPristigliZahtevi[0][i][0],
            idProizvoda: mojiPristigliZahtevi[0][i][1],
            idKupca: mojiPristigliZahtevi[0][i][2],
            cena: mojiPristigliZahtevi[0][i][3],
            adresa: mojiPristigliZahtevi[0][i][4],
            ocenjen: mojiPristigliZahtevi[0][i][5],
            brojTelefona: mojiPristigliZahtevi[0][i][6],
            kolicina: mojiPristigliZahtevi[0][i][7],
            idOglasivaca: mojiPristigliZahtevi[0][i][8],
            status: mojiPristigliZahtevi[0][i][9],
            poruka: mojiPristigliZahtevi[0][i][10],
            datum: mojiPristigliZahtevi[0][i][11],
          ));
        }
    }
    return myRecivedOrderRequest1;
  }
}

enum Status { WAITING, CONFIRMED, DECLINED }

class OrderRequest {
  BigInt id;
  BigInt idProizvoda;
  BigInt idKupca;
  BigInt cena;
  String adresa;
  bool ocenjen;
  String brojTelefona;
  BigInt kolicina;
  BigInt idOglasivaca;
  BigInt status;
  String poruka;
  String datum;

  OrderRequest({
    this.id,
    this.idProizvoda,
    this.idKupca,
    this.cena,
    this.adresa,
    this.ocenjen,
    this.brojTelefona,
    this.kolicina,
    this.idOglasivaca,
    this.status,
    this.poruka,
    this.datum,
  });
}
