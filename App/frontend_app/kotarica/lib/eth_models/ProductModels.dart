// import 'dart:collection';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart';
import 'package:kotarica/AllInfoBuilder.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:web3dart/web3dart.dart';
import 'package:web_socket_channel/io.dart';
// import 'package:universal_html/html.dart' as universal;

class ProductModels extends ChangeNotifier {
  // ignore: non_constant_identifier_names
  var proizvodi_stranice;
  static int ucitao = 0;
  static List<Product> products = [];
  List<Omiljeni> omiljeni = [];
  static var mojiOmiljeni;
  static var mojSadrzajKorpe;
  static List<BigInt> mojiOmiljeniInt;
  bool isLoading = true;
  bool isLoading1 = true;
  bool isLoadingMyProducts = true;
  bool isLoading3 = true;
  final String _rpcUrl = "http://147.91.204.116:11070";
  final String _wsUrl = "ws://147.91.204.116:11070/";

  final String _privateKey = AllInfos.privateKey;

  int productsCount = 0;
  int favouritesCount = 0;
  Web3Client _client;
  String _abiCode;
  Credentials _credentials;
  EthereumAddress _contractAddress;
  // ignore: unused_field
  EthereumAddress _ownAddress;
  DeployedContract _contract;
  static int duzinaListe = 0;

  ContractFunction _productsCount;
  ContractFunction _products;
  ContractFunction _createProduct;
  // ignore: unused_field
  ContractEvent _productCreatedEvent;
  // ignore: non_constant_identifier_names
  ContractFunction _obrisi_proizvod;
  // ignore: non_constant_identifier_names
  ContractFunction _izmeni_proizvod;
  // ignore: non_constant_identifier_names
  ContractFunction _daj_stranicu;
  ContractFunction _updateOcenu;

  ContractFunction _favouritesCount;
  ContractFunction _favourites;
  // ignore: non_constant_identifier_names
  ContractFunction _obrisi_omiljeni;
  ContractFunction _createFavourites;
  // ignore: unused_field
  ContractEvent _favouritesCreatedEvent;
  ContractFunction _getFavourites;

  // ignore: unused_field
  ContractFunction _cartCount;
  // ignore: unused_field
  ContractFunction _cart;

  // ignore: non_constant_identifier_names
  ContractFunction _obrisi_cart;
  ContractFunction _createCart;
  // ignore: unused_field
  ContractEvent _cartCreatedEvent;
  ContractFunction _getCart;

  ProductModels() {
    initiateSetup();
  }

  Future<void> initiateSetup() async {
    _client = Web3Client(_rpcUrl, Client(), socketConnector: () {
      return IOWebSocketChannel.connect(_wsUrl).cast<String>();
    });

    await getAbi();
    await getCredentials();
    await getDeployedContract();
  }

  Future<void> getAbi() async {
    String abiStringFile =
        await rootBundle.loadString("src/abis/proizvodi.json");
    var jsonAbi = jsonDecode(abiStringFile);
    _abiCode = jsonEncode(jsonAbi["abi"]);
    //print(jsonAbi["networks"]);
    _contractAddress =
        EthereumAddress.fromHex(jsonAbi["networks"]["5777"]["address"]);
    //print("Contract address " + _contractAddress);
    // print("Privatni kljuc " + _privateKey);
  }

  Future<void> getCredentials() async {
    _credentials = await _client.credentialsFromPrivateKey(_privateKey);
    _ownAddress = await _credentials.extractAddress();
  }

  Future<void> getDeployedContract() async {
    _contract = DeployedContract(
        ContractAbi.fromJson(_abiCode, "proizvodi"), _contractAddress);

    _productsCount = _contract.function("brojProizvoda");
    _favouritesCount = _contract.function("brojOmiljeninih");
    _cartCount = _contract.function("brojUKorpi");

    _createProduct = _contract.function("dodajProizvod");
    _createFavourites = _contract.function("dodajOmiljeni");
    _createCart = _contract.function("dodajUKorpu");

    _getFavourites = _contract.function("vratiOmiljeneProizvodeKorisnika");
    _getCart = _contract.function("vratiSadrzajKorpeZaKorisnika");
    _updateOcenu = _contract.function("updateOcenu");
    _favourites = _contract.function("omiljeni");
    _obrisi_omiljeni = _contract.function("obrisiOmiljeni");
    _obrisi_cart = _contract.function("obrisiIzKorpe");
    _obrisi_proizvod = _contract.function("obrisiProizvod");
    _daj_stranicu = _contract.function("izlistajProizvodePoStranicama");
    _izmeni_proizvod = _contract.function("izmeniProizvod");

    _cart = _contract.function("korpa");

    _products = _contract.function("proizvod");
    _productCreatedEvent = _contract.event("ProizvodKreiran");
    _favouritesCreatedEvent = _contract.event("OmiljeniKreiran");
    _cartCreatedEvent = _contract.event("DodatUKorpu");

    getProducts();
    // getFavs();
    dajStranicu(BigInt.from(1));

    if (GetInfo.id != BigInt.zero) getFavourites(GetInfo.id);
    //getCartItems(BigInt.from(1));
    // print("");
  }

  getProducts() async {
    List totalProductsList = await _client
        .call(contract: _contract, function: _productsCount, params: []);

    BigInt totalUsers = totalProductsList[0];
    productsCount = totalUsers.toInt();
    products.clear();

    for (var i = 0; i < totalUsers.toInt(); i++) {
      var temp = await _client.call(
          contract: _contract, function: _products, params: [BigInt.from(i)]);
      if (temp[0] != BigInt.zero) {
        products.add(Product(
          id: temp[0],
          idOglasavaca: temp[1],
          cena: temp[2],
          naziv: temp[3],
          kategorija: temp[4],
          vrsta: temp[5],
          mernaJedinica: temp[6],
          hesh: temp[8],
          zbirOcena: temp[9],
          brojOcena: temp[10],
          prosecnaOcena: temp[10] != BigInt.zero ? temp[9] / temp[10] : 0.0,
          isFavourite: false, //true,
          quantity: 1,
          images: [
            "assets/images/unnamed.png",
            "assets/images/ps4_console_white_2.png",
          ],
          description: temp[7],
        ));
      } else
        productsCount--;
    }

    isLoading = false;
    notifyListeners();
  }

  getFavs() async {
    List totalProductsList = await _client
        .call(contract: _contract, function: _favouritesCount, params: []);

    BigInt totalFavs = totalProductsList[0];
    favouritesCount = totalFavs.toInt();
    //print(totalFavs);
    omiljeni.clear();
    for (var i = 0; i < totalFavs.toInt(); i++) {
      var temp = await _client.call(
          contract: _contract, function: _favourites, params: [BigInt.from(i)]);
      omiljeni.add(Omiljeni(idProizvoda: temp[0], idKorisnika: temp[1]));
    }
    isLoading = false;
    notifyListeners();
  }

  dajStranicu(BigInt _stranica) async {
    isLoading3 = true;
    proizvodi_stranice = await _client.call(
        sender: _contractAddress,
        contract: _contract,
        function: _daj_stranicu,
        params: [_stranica]);

    //print(proizvodi_stranice);
    //await getProducts();
    isLoading3 = false;
    notifyListeners();
  }

  int proizvodiStraniceDuzina() {
    return proizvodi_stranice[0].length;
  }

  List<Product> vratiProizvodeZaStranicu() {
    bool omiljeni = false;

    // ignore: deprecated_member_use
    List<Product> lista = new List<Product>();
    for (var i = 0; i <proizvodi_stranice[0].length; i++) {
      omiljeni = false;
      // print("vratiProizvodeZaStranicu");
      // print(mojiOmiljeni);
      if (GetInfo.id != BigInt.zero)
        for (var j = 0; j < mojiOmiljeni[0].length; j++) {
          if (mojiOmiljeni[0][j] == proizvodi_stranice[0][i][0] &&
              mojiOmiljeni[0][j] != BigInt.zero) {
            // print("products[i].id" + products[i].id.toString());
            omiljeni = true;
            break;
          }
        }

      if (proizvodi_stranice[0][i][0] != BigInt.zero)
        lista.add(Product(
          id: proizvodi_stranice[0][i][0],
          idOglasavaca: proizvodi_stranice[0][i][1],
          cena: proizvodi_stranice[0][i][2],
          naziv: proizvodi_stranice[0][i][3],
          kategorija: proizvodi_stranice[0][i][4],
          vrsta: proizvodi_stranice[0][i][5],
          mernaJedinica: proizvodi_stranice[0][i][6],
          description: proizvodi_stranice[0][i][7],
          hesh: proizvodi_stranice[0][i][8],
          zbirOcena: proizvodi_stranice[0][i][9],
          brojOcena: proizvodi_stranice[0][i][10],
          prosecnaOcena:
              proizvodi_stranice[0][i][9] / proizvodi_stranice[0][i][10],
          isFavourite: omiljeni,
          quantity: 1,
          images: [
            "assets/images/unnamed.png",
            "assets/images/ps4_console_white_2.png",
            "assets/images/ps4_console_white_3.png",
            "assets/images/ps4_console_white_4.png",
          ],
        ));
    }
    duzinaListe = lista.length;
    return lista;
  }

  addProduct(
      String naziv,
      String kategorija,
      BigInt idOglasavaca,
      BigInt cena,
      String vrstaProzivoda,
      String mernaJedinica,
      String opis,
      String hesh) async {
    isLoading = true;
    //CircularProgressIndicator();

    await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            maxGas: 6721975,
            gasPrice: EtherAmount.inWei(BigInt.one),
            contract: _contract,
            function: _createProduct,
            parameters: [
              naziv,
              kategorija,
              vrstaProzivoda,
              mernaJedinica,
              opis,
              idOglasavaca,
              cena,
              hesh
            ]));

    //await getProducts();
    isLoading = false;

    notifyListeners();
  }

  getFavourites(BigInt _userID) async {
    mojiOmiljeni = await _client.call(
        sender: _contractAddress,
        contract: _contract,
        function: _getFavourites,
        params: [_userID]);

    mojiOmiljeniInt = mojiOmiljeni.cast<BigInt>();
    // print(mojiOmiljeni);
    // print(mojiOmiljeni[0][0].toString() +
    //     " prvi omiljeni proizvod a duzina omiljenih : " +
    //     mojiOmiljeni[0].length.toString());
    isLoading1 = false;
    notifyListeners();
  }

  getCartItems(BigInt _userID) async {
    isLoading = true;
    notifyListeners();
    mojSadrzajKorpe = await _client.call(
        sender: _contractAddress,
        contract: _contract,
        function: _getCart,
        params: [_userID]);

    // mojiOmiljeniInt = mojiOmiljeni.cast<BigInt>();
    // print(mojSadrzajKorpe);
    // print(mojSadrzajKorpe[0][0].toString() +
    //     " prvi item u korpi  a duzina omiljenih : " +
    //     mojSadrzajKorpe[0].length.toString());
  }

  deleteFavourites(BigInt _productID, BigInt _userID) async {
    await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            maxGas: 6721975,
            gasPrice: EtherAmount.inWei(BigInt.one),
            contract: _contract,
            function: _obrisi_omiljeni,
            parameters: [_productID, _userID]));
    getFavourites(_userID);
    //print("Obrisan omiljeni sa id " + _productID.toString());
    getFavs();

    isLoading = false;
    notifyListeners();
  }

  deleteProduct(BigInt _productID, BigInt _userID) async {
    isLoading = true;

    await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            maxGas: 6721975,
            gasPrice: EtherAmount.inWei(BigInt.one),
            contract: _contract,
            function: _obrisi_proizvod,
            parameters: [_productID, _userID]));

    getFavourites(GetInfo.id);
    isLoading = false;
    notifyListeners();
  }

  editProduct(
      BigInt _productID, BigInt _userID, BigInt _cena, String _opis) async {
    await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            maxGas: 6721975,
            gasPrice: EtherAmount.inWei(BigInt.one),
            contract: _contract,
            function: _izmeni_proizvod,
            parameters: [_productID, _userID, _cena, _opis]));

    getFavourites(GetInfo.id);
    isLoading = false;
    notifyListeners();
  }

  deleteCartItem(BigInt _productID, BigInt _userID) async {
    isLoading = true;
    notifyListeners();
    await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            maxGas: 6721975,
            gasPrice: EtherAmount.inWei(BigInt.one),
            contract: _contract,
            function: _obrisi_cart,
            parameters: [_productID, _userID]));

    //print("Obrisan item iz korpe sa id " + _productID.toString());
  }

  updateOcenu(BigInt _productID, BigInt ocena) async {
    isLoading = true;
    await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            maxGas: 6721975,
            gasPrice: EtherAmount.inWei(BigInt.one),
            contract: _contract,
            function: _updateOcenu,
            parameters: [_productID, ocena]));
    isLoading = false;
    notifyListeners();
  }

  addFavourites(BigInt _productID, BigInt _userID) async {
    await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            maxGas: 6721975,
            gasPrice: EtherAmount.inWei(BigInt.one),
            contract: _contract,
            function: _createFavourites,
            parameters: [_productID, _userID]));

    getFavourites(GetInfo.id);
    isLoading = false;
    getFavs();
    notifyListeners();
  }

  addCartItem(BigInt _productID, BigInt _userID) async {
    isLoading = true;
    notifyListeners();
    await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            maxGas: 6721975,
            gasPrice: EtherAmount.inWei(BigInt.one),
            contract: _contract,
            function: _createCart,
            parameters: [_productID, _userID]));

    getCartItems(BigInt.from(1));
  }

  List<Product> getProductsById(BigInt userId) {
    // ignore: deprecated_member_use
    List<Product> productsById = new List<Product>();
    // print("UKUPAN BROJ PROIZVODA JE :" + productsCount.toString());
    for (var i = 0; i < productsCount; i++) {
      if (products[i].idOglasavaca == userId) {
        //  print("USO");
        productsById.add(Product(
            id: products[i].id,
            idOglasavaca: products[i].idOglasavaca,
            cena: products[i].cena,
            naziv: products[i].naziv,
            kategorija: products[i].kategorija,
            vrsta: products[i].vrsta,
            mernaJedinica: products[i].mernaJedinica,
            zbirOcena: products[i].zbirOcena,
            brojOcena: products[i].brojOcena,
            prosecnaOcena: products[i].zbirOcena / products[i].brojOcena,
            isFavourite: false,
            hesh: products[i].hesh,
            quantity: 1,
            images: [
              "assets/images/unnamed.png",
              "assets/images/ps4_console_white_2.png",
              "assets/images/ps4_console_white_3.png",
              "assets/images/ps4_console_white_4.png",
            ],
            description: products[i].description));
      }
    }

    isLoadingMyProducts = false;
    return productsById;
  }

  List<Product> getFavouritesProducts(BigInt _userId) {
    // ignore: deprecated_member_use
    List<Product> list = new List<Product>();
    Product product;

    for (var i = 0; i < mojiOmiljeni[0].length; i++) {
      for (var j = 0; j < productsCount; j++) {
        if (mojiOmiljeni[0][i] == products[j].id) {
          product = products[j];
          product.isFavourite = true;
          // print(product);
          list.add(product);
        }
      }
    }
    return list;
  }

  List<Product> getCartItemsByUserId(BigInt _userId) {
    //getFavourites(_userId);
    // ignore: deprecated_member_use
    List<Product> productsById = new List<Product>();
    // int br = 0;
    for (var i = 0; i < mojSadrzajKorpe[0].length; i++) {
      //print(mojiOmiljeniInt[i].toInt().toString() + "omiljeni");
      for (var j = 0; j < productsCount; j++) {
        if (mojSadrzajKorpe[0][i] == products[j].id) {
          productsById.add(products[j]);
        }
      }
    }

    // print(productsById.length.toString() + " je broj itema u korpi");
    return productsById;
  }

  Product getProductById(BigInt _productId) {
    isLoading = true;
    Product oneProduct;
    for (var i = 0; i < products.length; i++)
      if (products[i].id == _productId) {
        oneProduct = products[i];
        break;
      }

    isLoading = false;
    return oneProduct;
  }

  List<Product> dajNajpopularnije() {
   

    // ignore: deprecated_member_use
    List<Product> lista = new List<Product>();
    for (var i = 0; i < products.length; i++) {
      if (products[i].prosecnaOcena != 0.0) lista.add(products[i]);
    }

    // ignore: deprecated_member_use
    List<Product> listaPoBrojuOcena = new List<Product>();
    //sortiram nasu listu po broju ocena
    lista.sort(
        (a, b) => b.brojOcena.compareTo(a.brojOcena)); // opadajuce po ceni

    int n;
    if (lista.length < 15)
      n = lista.length;
    else
      n = 15;

    // var lista1;
    bool omiljeni = false;
    // if (GetInfo.id != BigInt.zero) lista1 = getFavouritesProducts(GetInfo.id);
    for (var i = 0; i < n; i++) {
      omiljeni = false;
      // print("USO");
      if (GetInfo.id != BigInt.zero)
        for (var j = 0; j < mojiOmiljeni[0].length; j++) {
          if (mojiOmiljeni[0][j] == lista[i].id &&
              mojiOmiljeni[0][j] != BigInt.zero) {
            omiljeni = true;
            break;
          }
        }
      listaPoBrojuOcena.add(Product(
          id: lista[i].id,
          idOglasavaca: lista[i].idOglasavaca,
          cena: lista[i].cena,
          naziv: lista[i].naziv,
          kategorija: lista[i].kategorija,
          vrsta: lista[i].vrsta,
          mernaJedinica: lista[i].mernaJedinica,
          zbirOcena: lista[i].zbirOcena,
          brojOcena: lista[i].brojOcena,
          prosecnaOcena: lista[i].zbirOcena / lista[i].brojOcena,
          isFavourite: omiljeni,
          hesh: lista[i].hesh,
          quantity: 1,
          images: [
            "assets/images/unnamed.png",
            "assets/images/ps4_console_white_2.png",
            "assets/images/ps4_console_white_3.png",
            "assets/images/ps4_console_white_4.png",
          ],
          description: lista[i].description));
    }

    listaPoBrojuOcena
        .sort((a, b) => b.prosecnaOcena.compareTo(a.prosecnaOcena));

    return listaPoBrojuOcena;
  }
}

class Product {
  BigInt id;
  BigInt idOglasavaca;
  BigInt cena;
  String naziv;
  String kategorija;
  String vrsta;
  String mernaJedinica;
  bool isFavourite;
  List<String> images;
  String description;
  String hesh;
  int quantity = 1;
  BigInt zbirOcena;
  BigInt brojOcena;
  double prosecnaOcena;

  Product(
      {this.id,
      this.idOglasavaca,
      this.cena,
      this.naziv,
      this.kategorija,
      this.vrsta,
      this.mernaJedinica,
      this.isFavourite,
      this.images,
      this.description,
      this.hesh,
      this.quantity,
      this.zbirOcena,
      this.brojOcena,
      this.prosecnaOcena});
}

class Omiljeni {
  BigInt idProizvoda;
  BigInt idKorisnika;

  Omiljeni({this.idProizvoda, this.idKorisnika});
}
