import 'package:flutter/material.dart';
import 'package:kotarica/eth_models/user_loginModels.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/utils/boje.dart';
// import 'package:kotarica/widgets/images/image_load.dart';
//import 'package:kotarica/widgets/images/android/image_upload.dart';
//import 'package:kotarica/widgets/images/android/profile_image_mob.dart';
import 'package:kotarica/widgets/images/web/image_upload_web.dart';
import 'package:kotarica/widgets/images/web/profile_image_web.dart';
import 'package:kotarica/widgets/user/descriptionWeb.dart';
import 'package:kotarica/widgets/user/profileDescription.dart';
import 'package:provider/provider.dart';

class EditInfo extends StatefulWidget {
  static String routeName = '/izmena_profila';

  // EditInfo(@required this.id);
  // BigInt id;
  @override
  // _EditInfoState createState() => _EditInfoState(id);
  _EditInfoState createState() => _EditInfoState();
}

class EditInfoArguments {
  BigInt id;

  EditInfoArguments({@required this.id});
}

class _EditInfoState extends State<EditInfo> {
  bool showPassword = false;
  bool showProgress = false;
  // _EditInfoState(@required this.id);
  // BigInt id;

  @override
  Widget build(BuildContext context) {
    final TextEditingController name = TextEditingController();
    final TextEditingController surname = TextEditingController();
    final TextEditingController phoneNumber = TextEditingController();
    final TextEditingController homeAddress = TextEditingController();
    final TextEditingController email = TextEditingController();
    // ProfileImageMob profileImageMob = new ProfileImageMob();
    ProfileImage profileImageWeb = new ProfileImage();
    UploadImageIpfs uploadimage = new UploadImageIpfs();
    var listModel = Provider.of<UserLoginModel>(context);

    final EditInfoArguments args = ModalRoute.of(context).settings.arguments;

    double width = MediaQuery.of(context).size.width;
    double iconSize = 25;
    Color iconColor;
    name.text = ProfileDescription.dataExists == 1 || DescriptionWeb.web == 1
        ? ProfileDescription.userData.name.toString()
        : "Nema podataka";

    surname.text = ProfileDescription.dataExists == 1 || DescriptionWeb.web == 1
        ? ProfileDescription.userData.surname.toString()
        : "Nema podataka";

    phoneNumber.text =
        ProfileDescription.dataExists == 1 || DescriptionWeb.web == 1
            ? ProfileDescription.userData.phoneNumber.toString()
            : "Nema podataka";

    homeAddress.text =
        ProfileDescription.dataExists == 1 || DescriptionWeb.web == 1
            ? ProfileDescription.userData.homeAddress.toString()
            : "Nema podataka";

    email.text = ProfileDescription.dataExists == 1 || DescriptionWeb.web == 1
        ? ProfileDescription.userData.email.toString()
        : "Nema podataka";
    // bool web = width < 800 ? false : true;

    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios, color: Boje.mainText),
            onPressed: () {
              GetInfo.korpa = false;
              Navigator.pop(context);
              // Navigator.push(context,
              //     MaterialPageRoute(builder: (context) => WelcomeScreen()));
            },
          ),
          title: Text(
            "Izmena korisničkih podataka",
            style: TextStyle(color: Boje.mainText, fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Center(
            child: Container(
              width: width < 600 ? width : 600,
              child: Column(
                children: <Widget>[
                  SizedBox(height: 32),
                  Center(child: profileImageWeb),
                  SizedBox(height: 35),
                  Center(
                    child: SizedBox(
                      // width: MediaQuery.of(context).size.width * 0.75,
                      // height: MediaQuery.of(context).size.height * 0.80,
                      child: Column(
                        children: [
                          ProfileDescription.dataExists == 1 ||
                                  DescriptionWeb.web == 1
                              ? buildTextField(
                                  "Ime",
                                  ProfileDescription.userData.name.toString(),
                                  false,
                                  name,
                                  Icon(Icons.person,
                                      size: iconSize, color: iconColor),
                                  TextInputType.text,
                                )
                              : buildTextField(
                                  "Ime",
                                  "Nema podataka",
                                  false,
                                  name,
                                  Icon(Icons.person,
                                      size: iconSize, color: iconColor),
                                  TextInputType.name,
                                ),
                          ProfileDescription.dataExists == 1 ||
                                  DescriptionWeb.web == 1
                              ? buildTextField(
                                  "Prezime",
                                  ProfileDescription.userData.surname
                                      .toString(),
                                  false,
                                  surname,
                                  Icon(Icons.person,
                                      size: iconSize, color: iconColor),
                                  TextInputType.name,
                                )
                              : buildTextField(
                                  "Prezime",
                                  "Nema podataka",
                                  false,
                                  surname,
                                  Icon(Icons.person,
                                      size: iconSize, color: iconColor),
                                  TextInputType.name,
                                ),
                          ProfileDescription.dataExists == 1 ||
                                  DescriptionWeb.web == 1
                              ? buildTextField(
                                  "Broj telefona",
                                  ProfileDescription.userData.phoneNumber
                                      .toString(),
                                  false,
                                  phoneNumber,
                                  Icon(Icons.phone,
                                      size: iconSize, color: iconColor),
                                  TextInputType.number,
                                )
                              : buildTextField(
                                  "Broj telefona",
                                  "Nema podataka",
                                  false,
                                  phoneNumber,
                                  Icon(Icons.phone,
                                      size: iconSize, color: iconColor),
                                  TextInputType.number,
                                ),
                          ProfileDescription.dataExists == 1 ||
                                  DescriptionWeb.web == 1
                              ? buildTextField(
                                  "Adresa",
                                  ProfileDescription.userData.homeAddress
                                      .toString(),
                                  false,
                                  homeAddress,
                                  Icon(Icons.location_on,
                                      size: iconSize, color: iconColor),
                                  TextInputType.streetAddress,
                                )
                              : buildTextField(
                                  "Adresa",
                                  "Nema podataka",
                                  false,
                                  homeAddress,
                                  Icon(Icons.location_on,
                                      size: iconSize, color: iconColor),
                                  TextInputType.streetAddress,
                                ),
                          ProfileDescription.dataExists == 1 ||
                                  DescriptionWeb.web == 1
                              ? buildTextField(
                                  "E-mail",
                                  ProfileDescription.userData.email.toString(),
                                  false,
                                  email,
                                  Icon(Icons.mail_outline,
                                      size: iconSize, color: iconColor),
                                  TextInputType.emailAddress,
                                )
                              : buildTextField(
                                  "E-mail",
                                  "Nema podataka",
                                  false,
                                  email,
                                  Icon(Icons.mail_outline,
                                      size: iconSize, color: iconColor),
                                  TextInputType.emailAddress,
                                ),
                          SizedBox(height: 25),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              // ignore: deprecated_member_use
                              OutlineButton(
                                color: Theme.of(context).colorScheme.surface,
                                borderSide: BorderSide(
                                    color:
                                        Theme.of(context).colorScheme.surface),
                                padding: EdgeInsets.symmetric(horizontal: 5),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20)),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: Text("Otkaži",
                                    style: TextStyle(
                                      fontSize: 14,
                                      letterSpacing: 2.2,
                                      color:
                                          Theme.of(context).colorScheme.surface,
                                    )),
                              ),
                              // ignore: deprecated_member_use
                              RaisedButton(
                                  onPressed: () async {
                                    String hesh = "";
                                    // if (profileImageMob.image != null) {
                                    //   await uploadimage.upload(profileImageMob.image);
                                    //   hesh = uploadimage.hes;

                                    //   profileImageMob.image = null;
                                    // }

                                    if (profileImageWeb.image != null) {
                                      await uploadimage.uploadFileWeb(
                                          true, profileImageWeb.image);
                                      hesh = uploadimage.hes;

                                      profileImageWeb.image = null;
                                    }
                                    String getName = name.text;
                                    String getSurname = surname.text;
                                    String getPhoneNumber = phoneNumber.text;
                                    String getHomeAddress = homeAddress.text;
                                    String getEmail = email.text;
                                    print("RADIIIIIIIIIII $hesh");

                                    showProgress = true;

                                    listModel.addUserData(
                                      hesh,
                                      getName,
                                      getSurname,
                                      getPhoneNumber,
                                      getHomeAddress,
                                      getEmail,
                                      args.id,
                                    );
                                    // showProgress =false;
                                    listModel.getPrefilePitureHash(GetInfo.id);
                                    Navigator.pop(context);
                                    //Navigator.pushNamed(context, MyAcc.routeName);
                                    // Navigator.push(
                                    //     context,
                                    //     new MaterialPageRoute(
                                    //         builder: (context) => ));
                                  },
                                  color: Theme.of(context).primaryColor,
                                  padding: EdgeInsets.symmetric(horizontal: 5),
                                  elevation: 2,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20)),
                                  child: Text(
                                    "Sačuvaj",
                                    style: TextStyle(
                                        fontSize: 14,
                                        letterSpacing: 2.2,
                                        color: Colors.white),
                                  )),
                            ],
                          ),
                          SizedBox(height: 25),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ));
  }

  Widget buildTextField(
      String labelText,
      String placeholder,
      bool isPasswordTextField,
      TextEditingController controllerName,
      Icon icon,
      TextInputType keyboardType) {
    return Padding(
      padding: EdgeInsets.only(bottom: 20, left: 15, right: 15),
      child: TextField(
        keyboardType: keyboardType,
        controller: controllerName,
        obscureText: isPasswordTextField ? showPassword : false,
        decoration: InputDecoration(
          suffixIcon: isPasswordTextField
              ? IconButton(
                  onPressed: () {
                    setState(() {
                      showPassword = !showPassword;
                    });
                  },
                  icon: Icon(
                    Icons.remove_red_eye,
                    color: Colors.grey,
                  ),
                )
              : Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: icon,
                ),
          contentPadding: EdgeInsets.only(left: 15, bottom: 3),
          labelText: labelText,
          labelStyle: TextStyle(
            fontSize: 17,
            color: Theme.of(context).colorScheme.secondaryVariant,
            fontWeight: FontWeight.w600,
          ),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          hintText: placeholder,
          hintStyle: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              color: Theme.of(context).colorScheme.secondary),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(28),
            borderSide: BorderSide(color: Boje.enabledBorderColor, width: 1.5),
            gapPadding: 10,
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(28),
            borderSide: BorderSide(
                color: Theme.of(context).colorScheme.onSurface, width: 1.5),
            gapPadding: 10,
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(28),
            borderSide: BorderSide(
                color: Theme.of(context).colorScheme.primary, width: 2),
            gapPadding: 10,
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(28),
            borderSide: BorderSide(
                color: Theme.of(context).colorScheme.error, width: 1.5),
            gapPadding: 10,
          ),
        ),
      ),
    );
  }
}
