import 'package:flutter/material.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/widgets/productDetail/list_of_popular_product.dart';
import 'package:kotarica/widgets/products/productGridHomePage.dart';

class HomePage extends StatelessWidget {
  static String routeName = '/početna';

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: width < 800
          ? AppBar(
              centerTitle: true,
              title: Text("Početna",
                  style: TextStyle(
                      color: Boje.mainText, fontWeight: FontWeight.bold)),
            )
          : null,
      body: SafeArea(child: HomePage1()),
    );
  }
}

class HomePage1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            child: PopularProducts(),
          ),
          // Switch(
          //   value: false,
          //   onChanged: (value) {
          //     value = !value;
          //   },
          // ),
          SizedBox(height: 18),
          ProductListHomePage()
        ],
      ),
    );
  }
}
