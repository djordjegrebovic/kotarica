import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kotarica/routes.dart';
import 'package:kotarica/utils/themes.dart';
import 'package:kotarica/widgets/menu_bar.dart';

class WelcomeScreen extends StatefulWidget {
  static String routeName = '/welcome';

  WelcomeScreen({Key key, String title}) : super(key: key);

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  int selectedIndex;
  // final widgetOptions = [
  //   Text('Početna'),
  //   Text('Pretraži'),
  //   Text('Korpa'),
  //   Text('Moj nalog'),
  //   Text('Još'),
  // ];

  @override
  void initState() {
    selectedIndex = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Kotarica',
      theme: Themes.light,
      home: MenuBar(),
      routes: routes,
    );
  }

  void onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
      //print(selectedIndex);
    });
  }
}
