import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kotarica/widgets/reviews.dart';
import 'package:kotarica/widgets/userView.dart';

class Product extends StatefulWidget {
  Product({Key key}) : super(key: key);

  @override
  _ProductState createState() => _ProductState();
}

class _ProductState extends State<Product> {
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: GridView.count(
          crossAxisCount: 1,
          childAspectRatio: (3 / 1),
          children: <Widget>[
            Align(child: UserView(), alignment: Alignment.topLeft),
            Icon(
              Icons.photo,
              size: 100,
            ),
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Icon(Icons.photo),
                  Icon(Icons.photo),
                  Icon(Icons.photo),
                  Icon(Icons.photo),
                ]),
            Column(children: [
              Text("cena: 200"),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Text(
                      "Opis proizvoda _______________________________________________________________________________________________________________________________________________________________",
                      textAlign: TextAlign.start)),
              Text("Recenzije: ", textAlign: TextAlign.start),
            ]),
            Reviews()
          ],
        ),
      ),
    );
  }
}
