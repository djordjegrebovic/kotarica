import 'dart:async';
import 'package:flutter/material.dart';
import 'package:kotarica/utils/boje.dart';
// import 'package:kotarica/utils/boje.dart';
// import 'package:kotarica/utils/themes.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
// import 'dart:math' as math;

// ignore: must_be_immutable
class CustomProgressIndicator extends StatefulWidget {
  String text;
  CustomProgressIndicator(String text) {
    this.text = text;
  }
  @override
  _CustomProgressIndicatorState createState() =>
      _CustomProgressIndicatorState();
}

class _CustomProgressIndicatorState extends State<CustomProgressIndicator> {
  double _height;
  double _width;

  double percent = 0.0;

  @override
  void initState() {
    Timer timer;
    timer = Timer.periodic(Duration(milliseconds: 300), (_) {
      //print('Percent Update');
      setState(() {
        percent += 20;
        if (percent >= 100) {
          timer.cancel();
          percent = 100;
        }
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;

    return Container(
      child: Container(
        height: _height,
        width: _width,
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Center(
              child: Column(
                children: [
                  Text(
                    widget.text,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Boje.mainText,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w700,
                        fontSize: _width < 600 ? 15 : 18),
                  ),
                  SizedBox(height: 40),
                  Container(
                    height: 130,
                    width: 130,
                    child: LiquidCircularProgressIndicator(
                      value: percent / 100,
                      // Defaults to 0.5.
                      valueColor: AlwaysStoppedAnimation(
                          Theme.of(context).colorScheme.primary),
                      backgroundColor: Colors.white,
                      borderColor: Theme.of(context).colorScheme.onPrimary,
                      borderWidth: 4.0,
                      direction: Axis.vertical,
                      center: Text(
                        percent.toString() + "%",
                        style: TextStyle(
                            fontSize: 12.0,
                            fontWeight: FontWeight.w600,
                            color: Colors.black),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
