import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kotarica/widgets/menu_bar.dart';
import '../routes.dart';
import '../utils/themes.dart';

class WelcomeScreenNovi extends StatefulWidget {
  //WelcomeScreen({Key key, String title}) : super(key: key);
  static String routeName = '/welcome_novi';

  WelcomeScreenNovi(this.jwt, this.payload);

  factory WelcomeScreenNovi.fromBase64(String jwt) => WelcomeScreenNovi(
      jwt,
      json.decode(
          utf8.decode(base64.decode(base64.normalize(jwt.split(".")[1])))));

  final String jwt;
  final Map<String, dynamic> payload;
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState(jwt);
}

class _WelcomeScreenState extends State<WelcomeScreenNovi> {
  String jwt;
  _WelcomeScreenState(this.jwt);
  int selectedIndex;
  final widgetOptions = [
    Text('Početna'),
    Text('Pretraži'),
    Text('Korpa'),
    Text('Moj nalog'),
  ];

  ThemeData theme;

  @override
  void initState() {
    theme = Themes.light;
    selectedIndex = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // print(json.decode(
    //     utf8.decode(base64.decode(base64.normalize(jwt.split(".")[1])))));
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Kotarica',
      theme: theme,
      home: MenuBar(),
      routes: routes,
    );
  }

  void onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
      //print(selectedIndex);
    });
  }
}
