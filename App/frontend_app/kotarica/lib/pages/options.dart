// import 'package:flutter/material.dart';
// import 'package:kotarica/models/getInfo.dart';
// import 'package:kotarica/pages/welcome_screen.dart';
// import 'package:kotarica/utils/boje.dart';
// import 'package:kotarica/widgets/LoginSingin/default_button.dart';
// import 'package:kotarica/widgets/user/options/changePassword.dart';
// import 'package:kotarica/widgets/user/options/changePrivateKey.dart';

// // ignore: must_be_immutable
// class Opcije extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     // double width = MediaQuery.of(context).size.width;
//     // double height = MediaQuery.of(context).size.height;
//     Color color = Theme.of(context).colorScheme.surface;
//     double width = MediaQuery.of(context).size.width;

//     return Scaffold(
//       appBar: AppBar(
//         leading: IconButton(
//           icon: Icon(Icons.arrow_back_ios, color: color),
//           onPressed: () {
//             GetInfo.korpa = false;
//             // Navigator.pop(context);
//             Navigator.push(context,
//                 MaterialPageRoute(builder: (context) => WelcomeScreen()));
//           },
//         ),
//         title: Text("Opcije",
//             style:
//                 TextStyle(color: Boje.mainText, fontWeight: FontWeight.bold)),
//         centerTitle: true,
//       ),
//       body: Center(
//           child: Column(
//         children: [
//           DefaultButton(
//             text: "Promeni lozinku",
//             width: width < 500 ? width : 500,
//             press: () {
//               Navigator.push(context,
//                   MaterialPageRoute(builder: (context) => ChangePassword()));
//             },
//           ),
//           // GestureDetector(
//           //   onTap: () {
//           //     Navigator.push(context,
//           //         MaterialPageRoute(builder: (context) => ChangePassword()));
//           //   },
//           //   child: Text(
//           //     "Promeni lozinku",
//           //     style: TextStyle(
//           //       fontSize: 18,
//           //       color: Boje.loginRegisterButton,
//           //       // Colors.orange[600],
//           //       // Theme.of(context).colorScheme.onPrimary, //Colors.orange, //
//           //       fontWeight: FontWeight.w700,
//           //     ),
//           //   ),
//           // ),
//           SizedBox(
//             height: 20,
//           ),
//           DefaultButton(
//             text: "Promeni Etherium novcanik",
//             width: width < 500 ? width : 500,
//             press: () {
//               Navigator.push(context,
//                   MaterialPageRoute(builder: (context) => ChangePrivateKey()));
//             },
//           ),
//           // GestureDetector(
//           //   onTap: () {
//           //     Navigator.push(context,
//           //         MaterialPageRoute(builder: (context) => ChangePrivateKey()));
//           //   },
//           //   child: Text(
//           //     "Promeni Etherium novcanik",
//           //     style: TextStyle(
//           //       fontSize: 18,
//           //       color: Boje.loginRegisterButton,
//           //       // Colors.orange[600],
//           //       // Theme.of(context).colorScheme.onPrimary, //Colors.orange, //
//           //       fontWeight: FontWeight.w700,
//           //     ),
//           //   ),
//           // )
//         ],
//       )),
//     );
//   }
// }
