import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:kotarica/eth_models/user_loginModels.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/utils/boje.dart';
// import 'package:kotarica/utils/size_functions.dart';
import 'package:kotarica/widgets/cart/cart_item_card.dart';
import 'package:kotarica/widgets/cart/check_our_card.dart';
// import 'package:kotarica/widgets/cart/totalAndCheckoutButton.dart';
// import 'package:kotarica/widgets/cart/checkout_item_card.dart';
// import 'package:kotarica/widgets/cart/totalAndCheckoutButton.dart';
import 'package:kotarica/widgets/productDetail/add.dart';
import 'package:kotarica/widgets/user/profileDescription.dart';
import 'package:provider/provider.dart';

class Checkout extends StatefulWidget {
  static String routeName = '/račun';

  @override
  _CheckoutState createState() => _CheckoutState();
}

class _CheckoutState extends State<Checkout> {
  bool showPassword = false;
  int numOfItem;

  @override
  Widget build(BuildContext context) {
    var addListener = Provider.of<AddModel>(context);
    numOfItem = addListener.baskets.length;
    double width = MediaQuery.of(context).size.width;

    int br = addListener.baskets.length;

    // final TextEditingController name = TextEditingController();
    // final TextEditingController surname = TextEditingController();
    // final TextEditingController phoneNumber = TextEditingController();
    // final TextEditingController homeAddress = TextEditingController();
    // final TextEditingController email = TextEditingController();
    var provider = Provider.of<UserLoginModel>(context);

    return Scaffold(
      bottomNavigationBar: CheckOurCard(text: "Kupi", icon: Icons.money_sharp),
      appBar: buildAppBar(context),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Center(
                child: provider.isLoading1
                    ? LoadingDataforUser()
                    : FutureBuilder<UserData>(
                        future: provider.giveDataForUser1(GetInfo.id),
                        // ignore: missing_return
                        builder:
                            // ignore: missing_return
                            (BuildContext context,
                                AsyncSnapshot<UserData> snapshot) {
                          switch (snapshot.connectionState) {
                            case ConnectionState.waiting:
                              return SpinKitFadingCube(
                                  size: 35,
                                  color: Theme.of(context).colorScheme.primary);
                            case ConnectionState.done:
                              if (snapshot.hasError) {
                                return Text('Error: ${snapshot.error}');
                              } else if (!snapshot.hasData) {
                                // print("Usao u stranicu jer nema podata");
                                return NoDataToShow();
                              }
                              // print("Usao u stranicu");
                              ProfileDescription.dataExists = 1;
                              return DataToBeShownCheckout(snapshot.data);
                            case ConnectionState.none:
                              return Text('No connection');
                            case ConnectionState.active:
                              break;
                          }
                        },
                      ),
              ),
              Center(
                //Glavni kontejner
                child: Container(
                  alignment: Alignment.topCenter,
                  width: width < 1000
                      ? width > 850
                          ? width - 270
                          : width
                      : 1000,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        decoration: width > 850
                            ? BoxDecoration(
                                border: Border.all(
                                  color: Colors.white,
                                ),
                                borderRadius: BorderRadius.circular(10),
                                color: Theme.of(context)
                                    .colorScheme
                                    .onBackground
                                    .withOpacity(0.7),
                              )
                            : null,
                        margin: EdgeInsets.only(
                          top: width > 850 ? 20 : 0,
                          bottom: width > 850 ? 20 : 0,
                          left: width > 850
                              ? width < 1000
                                  ? 10
                                  : 20
                              : 0,
                        ),
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        // color: Colors.blue,
                        width: width > 850
                            ? width < 1000
                                ? width - 280
                                : 700
                            : width,
                        height: width <= 850
                            ? (addListener.baskets.length *
                                    (width < 600 ? width * 0.23 : 120) +
                                addListener.baskets.length * 12)
                            : br == 1
                                ? 144
                                : br == 2
                                    ? 144 * 2 - 12
                                    : br == 3
                                        ? 144 * 3 - 20
                                        : 144 * 4,
                        child: ListView.builder(
                          itemCount: addListener.baskets.length,
                          scrollDirection: Axis.vertical,
                          itemBuilder: (context, index) => Padding(
                            padding: EdgeInsets.only(top: 12.0),
                            child: Dismissible(
                                key: Key(
                                    addListener.baskets[index].id.toString()),
                                direction: DismissDirection.endToStart,
                                background: Container(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 20.0),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15.0),
                                    color: Colors.red[600],
                                  ),
                                  child: Row(
                                    children: [
                                      Spacer(),
                                      Icon(
                                        Icons.delete,
                                        size:
                                            MediaQuery.of(context).size.height *
                                                0.07,
                                        color: Colors.white,
                                      )
                                    ],
                                  ),
                                ),
                                onDismissed: (direction) {
                                  setState(() {
                                    addListener.removeItemFromBasket(
                                        addListener.baskets[index]);
                                  });
                                },
                                confirmDismiss: (direction) {
                                  return showDialog(
                                    context: context,
                                    builder: (context) => AlertDialog(
                                      title: Text('Da li ste sigurni?'),
                                      content: Text(
                                          'Da li zelite da uklonite ovaj proizvod iz korpe?'),
                                      actions: [
                                        // ignore: deprecated_member_use
                                        FlatButton(
                                          child: Text('Ne'),
                                          onPressed: () {
                                            Navigator.of(context).pop(false);
                                          },
                                        ),
                                        // ignore: deprecated_member_use
                                        FlatButton(
                                          child: Text('Da'),
                                          onPressed: () {
                                            Navigator.of(context).pop(true);
                                          },
                                        )
                                      ],
                                    ),
                                  );
                                },
                                child: CartItemCard(
                                  product: addListener.baskets[index],
                                  index: index,
                                  stepper: false,
                                )),
                          ),
                        ),
                      ),
                      // width > 850
                      //     ? TotalAndCheckoutButton(
                      //         text: "Kupi", icon: Icons.money_sharp)
                      //     : SizedBox(),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

AppBar buildAppBar(BuildContext context) {
  var addListener = Provider.of<AddModel>(context);
  return AppBar(
    centerTitle: true,
    leading: IconButton(
      icon: Icon(
        Icons.arrow_back_ios,
        color: Boje.mainText,
      ),
      onPressed: () {
        GetInfo.korpa = false;

        Navigator.pop(context);
        //CheckOurCard.korpa = false;
      },
    ),
    title: Column(
      children: <Widget>[
        Text(
          "Račun",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20.0,
            letterSpacing: 1.3,
            color: Boje.mainText,
          ),
        ),
        Text(
          "${addListener.baskets.length} ${addListener.baskets.length == 1 ? "proizvod" : "proizvoda"}", //
          style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
        ),
      ],
    ),
  );
}

// Widget buildTextField(String labelText, String placeholder,
//     bool isPasswordTextField, TextEditingController controllerName) {
//   return Padding(
//     padding: const EdgeInsets.only(bottom: 20, left: 16),
//     child: TextField(
//       controller: controllerName,
//       obscureText: isPasswordTextField ? showPassword : false,
//       decoration: InputDecoration(
//           suffixIcon: isPasswordTextField
//               ? IconButton(
//                   onPressed: () {
//                     setState(() {
//                       showPassword = !showPassword;
//                     });
//                   },
//                   icon: Icon(
//                     Icons.remove_red_eye,
//                     color: Colors.grey,
//                   ),
//                 )
//               : null,
//           contentPadding: EdgeInsets.only(bottom: 3),
//           labelText: labelText,
//           floatingLabelBehavior: FloatingLabelBehavior.always,
//           hintText: placeholder,
//           hintStyle: TextStyle(
//             fontSize: 16,
//             fontWeight: FontWeight.bold,
//             color: Colors.white,
//           )),
//     ),
//   );
// }
