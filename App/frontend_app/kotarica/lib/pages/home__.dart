import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:kotarica/utils/product.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool loggedIn;

  @override
  void initState() {
    loggedIn = false;
    super.initState();
  }

  getProducts() {
    if (!loggedIn) {
      return Column(children: [
        new ProductShowcaseGrid.withKey(
            UniqueKey(), Product.getRandomN(2), "Aktuelni:"),
        ProductShowcaseGrid.withKey(
            UniqueKey(), Product.getRandomN(2), "Najnoviji:"),
        ProductShowcaseGrid.withKey(UniqueKey(), Product.getRandomN(2),
            "Proizvodi korisnika koje pratite:"),
      ]);
    } else {
      return new Column(children: [
        ProductShowcaseGrid.withKey(
            UniqueKey(), Product.getRandomN(4), "Aktuelni:"),
        ProductShowcaseGrid(Product.getRandomN(2), "Najnoviji:"),
      ]);
    }
  }

  toggleLogin(bool value) {
    setState(() {
      loggedIn = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
              icon: Icon(
                Icons.search,
              ),
              iconSize: 30.0,
              padding: EdgeInsets.only(left: 15.0),
              alignment: Alignment.center,
              onPressed: () {}),
          title: Text('Search',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ))),
      body: Center(
          child: Column(
        children: [
          getProducts(),
          Align(
            alignment: Alignment.bottomCenter,
            child: Switch(
              value: loggedIn,
              onChanged: toggleLogin,
            ),
          ),
        ],
      )),
    );
  }
}

class ProductShowcaseGrid extends StatefulWidget {
  ProductShowcaseGrid.withKey(Key key, this.items, this.title)
      : super(key: key);
  ProductShowcaseGrid(this.items, this.title);
  final List<Product> items;
  final String title;

  @override
  _ProductShowcaseGridState createState() => _ProductShowcaseGridState();
}

class _ProductShowcaseGridState extends State<ProductShowcaseGrid> {
  List<Product> items;
  String title;

  @override
  void initState() {
    items = widget.items;
    title = widget.title;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (items != null && items.count() != 0) {
      return Container(
          margin: EdgeInsets.symmetric(vertical: 4, horizontal: 10),
          child: Column(children: [
            Align(
                alignment: Alignment.centerLeft,
                child: Text(title,
                    style:
                        TextStyle(fontSize: 20, fontWeight: FontWeight.w500))),
            GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 1 / 0.65,
                  crossAxisSpacing: 15,
                  mainAxisSpacing: 15),
              shrinkWrap: true,
              itemCount: items.count(),
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  margin: EdgeInsets.all(5),
                  alignment: Alignment.center,
                  child: Column(
                    children: [
                      Container(
                        //width: 180,
                        height: 90,
                        color: Colors.red,
                      ),
                      Text(items[index].name),
                      Text(items[index].price.toString()),
                    ],
                  ),
                  decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.background,
                      boxShadow: [
                        new BoxShadow(
                          blurRadius: 5.0,
                        ),
                      ],
                      borderRadius: BorderRadius.circular(15)),
                );
              },
            ),
            Align(
                alignment: Alignment.centerRight,
                child: Text(
                  "Pogledaj jos...",
                  style: TextStyle(
                    fontSize: 14,
                    decoration: TextDecoration.underline,
                  ),
                )),
          ]));
    } else {
      return Container(
        child: Text("Nema proizvoda."),
      );
    }
  }
}
