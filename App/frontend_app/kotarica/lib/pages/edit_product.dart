import 'dart:io';
import 'package:flutter/material.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/pages/customLoading.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/widgets/cart/purchaseSuccess.dart';
import 'package:provider/provider.dart';

class EditProduct extends StatefulWidget {
  static String routeName = '/izmena_podataka_o_proizvodu';

  // EditProduct(this.product);
  // Product product;
  @override
  // _EditInfoState createState() => _EditInfoState(product);
  _EditInfoState createState() => _EditInfoState();
}

class EditProductArguments {
  Product product;

  EditProductArguments({@required this.product});
}

class _EditInfoState extends State<EditProduct> {
  // ignore: unused_field
  File _image;
  bool showPassword = false;

  // _EditInfoState(@required this.product);
  // Product product;
  bool showProgress = false;
  int nesto = 0;
  @override
  Widget build(BuildContext context) {
    final TextEditingController cena = TextEditingController();
    final TextEditingController opis = TextEditingController();
    final EditProductArguments args = ModalRoute.of(context).settings.arguments;
    var listModel = Provider.of<ProductModels>(context);
    cena.text = args.product.cena.toString();
    opis.text = args.product.description.toString();

    uradiNesto();

    showAlert(BuildContext context, bool vrednost) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: vrednost == true
                ? Text('Pristupate brisanju oglasa.')
                : Text('Desila se greška.'),
            content: vrednost == true
                ? Text("Da li ste sigurni da želite da obrišete ovaj proizvod?")
                : Text("Nove izmene nisu unete"),
            actions: <Widget>[
              vrednost == true
                  // ignore: deprecated_member_use
                  ? FlatButton(
                      child: Text("DA"),
                      onPressed: () async {
                        setState(() {
                          showProgress = true;
                        });
                        nesto = 1;
                        Navigator.pop(context);
                      },
                    )
                  : Text(""),
              // ignore: deprecated_member_use
              FlatButton(
                child: vrednost == true ? Text("NE") : Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    double width = MediaQuery.of(context).size.width;
    double iconSize = 25;
    Color iconColor;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Boje.mainText),
          onPressed: () {
            GetInfo.korpa = false;
            Navigator.pop(context);
            // Navigator.push(context,
            //     MaterialPageRoute(builder: (context) => WelcomeScreen()));
          },
        ),
        title: Text(
          args.product.naziv,
          style: TextStyle(color: Boje.mainText, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      body: showProgress == false
          ? SingleChildScrollView(
              child: Center(
              child: Container(
                width: width < 600 ? width : 600,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      // width: MediaQuery.of(context).size.width * 0.75,
                      // height: MediaQuery.of(context).size.height * 0.80,
                      child: Column(
                        children: [
                          SizedBox(height: 35),
                          args.product.cena != null
                              ? buildTextField(
                                  "Cena",
                                  args.product.cena.toString(),
                                  false,
                                  cena,
                                  Icon(Icons.money,
                                      size: iconSize, color: iconColor),
                                  TextInputType.number,
                                )
                              : buildTextField(
                                  "Cena",
                                  "Nema podataka",
                                  false,
                                  cena,
                                  Icon(Icons.money,
                                      size: iconSize, color: iconColor),
                                  TextInputType.number,
                                ),
                          args.product.description != null
                              ? buildTextField(
                                  "Opis",
                                  args.product.description.toString(),
                                  false,
                                  opis,
                                  Icon(Icons.short_text_outlined,
                                      size: iconSize, color: iconColor),
                                  TextInputType.text,
                                )
                              : buildTextField(
                                  "Opis",
                                  "Nema podataka",
                                  false,
                                  opis,
                                  Icon(Icons.short_text_outlined,
                                      size: iconSize, color: iconColor),
                                  TextInputType.text,
                                ),
                          SizedBox(height: 25),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              // ignore: deprecated_member_use
                              OutlineButton(
                                color: Theme.of(context).colorScheme.surface,
                                borderSide: BorderSide(
                                    color:
                                        Theme.of(context).colorScheme.surface),
                                padding: EdgeInsets.symmetric(horizontal: 5),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20)),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: Text("Otkaži",
                                    style: TextStyle(
                                      fontSize: 14,
                                      letterSpacing: 2.2,
                                      color:
                                          Theme.of(context).colorScheme.surface,
                                    )),
                              ),
                              // ignore: deprecated_member_use
                              RaisedButton(
                                  onPressed: () {
                                    String getCena = cena.text;
                                    String getOpis = opis.text;
                                    //pozovi izmene

                                    if (getCena == "" && getOpis == "")
                                      showAlert(context, false);
                                    else {
                                      listModel.editProduct(
                                          args.product.id,
                                          args.product.idOglasavaca,
                                          getCena != ""
                                              ? BigInt.parse(getCena)
                                              : args.product.cena,
                                          getOpis != ""
                                              ? getOpis
                                              : args.product.description);

                                      // Navigator.pushNamed(
                                      //     context, PurchaseSuccessBody.routeName,
                                      //     arguments: PurchaseSuccessArguments(
                                      //         text:
                                      //             "Uspesno ste izmenili oglas."));
                                      // Navigator.pushNamed(
                                      //     context, UpdateSuccessBody.routeName);
                                      Navigator.push(
                                          context,
                                          new MaterialPageRoute(
                                              builder: (context) =>
                                                  PurchaseSuccessBody(
                                                      "Uspešno ste izmenili oglas")));
                                    }
                                  },
                                  color: Theme.of(context).primaryColor,
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  elevation: 2,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20)),
                                  child: Text(
                                    "Sačuvaj",
                                    style: TextStyle(
                                        fontSize: 14,
                                        letterSpacing: 2.2,
                                        color: Colors.white),
                                  ))
                            ],
                          ),
                          SizedBox(height: 50),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 15.0),
                            child: Text(
                              "Ukoliko želite da uklonite vaš oglas, to možete uraditi klikom na dugme ispod.",
                              textAlign: TextAlign.center,
                            ),
                          ),
                          SizedBox(height: 20),
                          Center(
                            // ignore: deprecated_member_use
                            child: RaisedButton(
                              onPressed: () {
                                showAlert(context, true);
                              },
                              color: Colors.red[700],
                              padding: EdgeInsets.symmetric(horizontal: 50),
                              elevation: 2,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              child: Text(
                                "Ukloni oglas",
                                style: TextStyle(
                                    fontSize: 14,
                                    letterSpacing: 2.2,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ))
          : CustomProgressIndicator('Molimo, sačekajte. Vaš oglas se briše.'),
    );
  }

  Future<void> uradiNesto() async {
    var listModel = Provider.of<ProductModels>(context);
    final EditProductArguments args = ModalRoute.of(context).settings.arguments;

    if (nesto == 1) {
      nesto = 0;
      await listModel.deleteProduct(args.product.id, args.product.idOglasavaca);
      // Navigator.pushNamed(context, PurchaseSuccessBody.routeName,
      //     arguments:
      //         PurchaseSuccessArguments(text: "Uspešno ste obrisali oglas."));
      Navigator.push(
          context,
          new MaterialPageRoute(
              builder: (context) =>
                  PurchaseSuccessBody("Uspešno ste obrisali oglas.")));
    }
  }

  Widget buildTextField(
      String labelText,
      String placeholder,
      bool isPasswordTextField,
      TextEditingController controllerName,
      Icon icon,
      TextInputType keyboardType) {
    return Padding(
      padding: EdgeInsets.only(bottom: 20, left: 15, right: 15),
      child: TextFormField(
        keyboardType: keyboardType,
        controller: controllerName,
        obscureText: isPasswordTextField ? showPassword : false,
        decoration: InputDecoration(
          suffixIcon: isPasswordTextField
              ? IconButton(
                  onPressed: () {
                    setState(() {
                      showPassword = !showPassword;
                    });
                  },
                  icon: Icon(Icons.remove_red_eye, color: Colors.grey),
                )
              : Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: icon,
                ),
          contentPadding: EdgeInsets.only(left: 15, bottom: 3),
          labelText: labelText,
          labelStyle: TextStyle(
            fontSize: 17,
            color: Theme.of(context).colorScheme.secondaryVariant,
            fontWeight: FontWeight.w600,
          ),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          hintText: placeholder,
          hintStyle: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              color: Theme.of(context).colorScheme.secondary),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(28),
            borderSide: BorderSide(color: Boje.enabledBorderColor, width: 1.5),
            gapPadding: 10,
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(28),
            borderSide: BorderSide(
                color: Theme.of(context).colorScheme.onSurface, width: 1.5),
            gapPadding: 10,
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(28),
            borderSide: BorderSide(
                color: Theme.of(context).colorScheme.primary, width: 2),
            gapPadding: 10,
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(28),
            borderSide: BorderSide(
                color: Theme.of(context).colorScheme.error, width: 1.5),
            gapPadding: 10,
          ),
        ),
      ),
    );
  }
}
