import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/pages/customLoading.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/widgets/add_product_widgets/dependet_dropdown.dart';
import 'package:kotarica/widgets/add_product_widgets/dropdown_measure.dart';
import 'package:kotarica/widgets/add_product_widgets/resizeable_input_text.dart';
import 'package:kotarica/widgets/cart/purchaseSuccess.dart';
//import 'package:kotarica/widgets/images/android/image_upload.dart';
//import 'package:kotarica/widgets/images/android/product_picture_upload.dart';
import 'package:kotarica/widgets/images/web/image_upload_web.dart';
import 'package:kotarica/widgets/images/web/product_picture_upload.dart';
import 'package:provider/provider.dart';

class AddProductPage extends StatefulWidget {
  static String routeName = '/dodavanje_oglasa';

  @override
  AddProductPageState createState() => AddProductPageState();
}

class AddProductPageState extends State<AddProductPage> {
  final TextEditingController naziv = TextEditingController();
  final TextEditingController cena = TextEditingController();
  CategoryDropDown _categoryDropdown = CategoryDropDown();
  MeasureDropDown _measureDropdown = MeasureDropDown();
  ResizeableTextInput _description = ResizeableTextInput("Opis proizvoda");
  UploadProductImage imageToUpload = UploadProductImage();
  UploadImageIpfs uploadImageIpfs = UploadImageIpfs();
  bool showProgress = false;
  Widget build(BuildContext context) {
    void displayDialog(context, title, text) => showDialog(
        context: context,
        builder: (context) =>
            AlertDialog(title: Text(title), content: Text(text)));
    double width = MediaQuery.of(context).size.width;

    return Consumer<ProductModels>(
        builder: (context, provider, child) => Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: Icon(Icons.arrow_back_ios, color: Boje.mainText),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              title: Text(
                "Napravi oglas",
                style: TextStyle(
                    color: Boje.mainText, fontWeight: FontWeight.bold),
              ),
              centerTitle: true,
            ),
            body: showProgress == false
                ? GestureDetector(
                    onTap: () {
                      FocusScope.of(context).requestFocus(new FocusNode());
                    },
                    child: SingleChildScrollView(
                        child: Center(
                      child: Container(
                        width: width < 600 ? width : 600,
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(height: 32),
                            imageToUpload,
                            SizedBox(height: 20),
                            TextField(
                              controller: naziv,
                              decoration:
                                  InputDecoration(labelText: "Naziv proizvoda"),
                            ),
                            TextField(
                              controller: cena,
                              decoration: InputDecoration(
                                  labelText: "Cena proizvoda (RSD)"),
                              keyboardType: TextInputType.number,
                              inputFormatters: <TextInputFormatter>[
                                FilteringTextInputFormatter.digitsOnly
                              ], // Only numbers can be entered
                            ),
                            _categoryDropdown,
                            _measureDropdown,
                            _description,
                            SizedBox(height: 30),
                            //RED SA DUGMICIMA (POSTAVI OGLAS I OTKAZI)
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                // ignore: deprecated_member_use
                                RaisedButton(
                                  onPressed: () async {
                                    if (checIfCorect()) {
                                      String hesh = "";
                                      if (imageToUpload.image != null) {
                                        await uploadImageIpfs.uploadFileWeb(
                                            true, imageToUpload.image);
                                        imageToUpload = null;
                                        hesh = uploadImageIpfs.hes;
                                      }
                                      setState(() {
                                        // set the progress indicator to true so it would not be visible
                                        showProgress = true;
                                      });
                                      await provider.addProduct(
                                          naziv.text,
                                          _categoryDropdown.category,
                                          GetInfo.id,
                                          BigInt.from(int.parse(cena.text)),
                                          _categoryDropdown.type,
                                          _measureDropdown.chosenValue
                                              .split(" ")[0],
                                          _description
                                              .textEditingController.text,
                                          hesh);
                                      setState(() {
                                        // set the progress indicator to true so it would not be visible
                                        //showProgress = false;
                                        // navigate to your desired page
                                        // Navigator.pushNamed(context,
                                        //     PurchaseSuccessBody.routeName,
                                        //     arguments:
                                        //         PurchaseSuccessArguments(
                                        //             text:
                                        //                 'Uspešno ste dodali oglas.'));
                                        Navigator.push(
                                            context,
                                            new MaterialPageRoute(
                                                builder: (context) =>
                                                    PurchaseSuccessBody(
                                                        'Uspešno ste dodali oglas.')));
                                      });
                                    } else {
                                      displayDialog(context, "Loš unos",
                                          "Niste popunili sva polja za unos");
                                    }
                                  },
                                  color: Theme.of(context).colorScheme.primary,
                                  child: Text(
                                    "Postavi oglas",
                                    style: TextStyle(
                                        fontSize: 15,
                                        fontFamily: 'Open-Sans',
                                        color: Theme.of(context)
                                            .colorScheme
                                            .surface),
                                  ),
                                ),
                                // ignore: deprecated_member_use
                                RaisedButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  color:
                                      Theme.of(context).colorScheme.secondary,
                                  child: Text(
                                    "Otkaži",
                                    style: TextStyle(
                                        fontSize: 15,
                                        fontFamily: 'Open-Sans',
                                        color: Theme.of(context)
                                            .colorScheme
                                            .surface),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    )),
                  )
                : CustomProgressIndicator(
                    'Molimo, sačekajte. Vaš oglas se dodaje.')));
  }

  bool checIfCorect() {
    if ((_categoryDropdown.category != "" && _categoryDropdown.type != "") ||
        _categoryDropdown.category == "Nekategorisano") {
      if (_measureDropdown.chosenValue != "" &&
          naziv.text != "" &&
          cena.text != "" &&
          _description.textEditingController.text != "") return true;
    }
    return false;
  }
}
