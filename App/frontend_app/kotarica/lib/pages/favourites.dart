import 'package:flutter/material.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/widgets/productDetail/add.dart';
import 'package:kotarica/widgets/productDetail/basketButton.dart';
import 'package:kotarica/widgets/products/favProducts.dart';
import 'package:provider/provider.dart';

class MyFavourites extends StatelessWidget {
  static String routeName = '/omiljeni';

  @override
  Widget build(BuildContext context) {
    var addListener = Provider.of<AddModel>(context);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Boje.mainText),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        centerTitle: true,
        title: Text("Omiljeni",
            style:
                TextStyle(color: Boje.mainText, fontWeight: FontWeight.bold)),
        actions: [
          BasketButton(
            addListener: addListener,
            color: Theme.of(context).colorScheme.onBackground,
            margin: 8,
          ),
          SizedBox(width: 10)
        ],
      ),
      body: Favourites(),
    );
  }
}

class Favourites extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(height: 10.0),
          FavProductListHomePage(),
        ],
      ),
    );
  }
}
