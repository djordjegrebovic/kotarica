import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/pages/welcome_screen.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/widgets/products/recieved_reviewsForUser.dart';
import 'package:kotarica/widgets/products/userProducts.dart';
import 'package:kotarica/widgets/user/profileDescription.dart';
import 'package:kotarica/widgets/user/profilePictureAndUsername.dart';

class MyAcc extends StatefulWidget {
  static String routeName = '/profil';

  MyAcc({
    this.id,
    this.oglasavac,
    /*BigInt id, bool oglasavac*/
  });
  // {
  // this.id = id;
  // this.oglasavac = oglasavac;
  // }
  final BigInt id;
  final bool oglasavac;
  @override
  _MyAccState createState() => _MyAccState();
}

class _MyAccState extends State<MyAcc> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: User(id: this.widget.id, oglasavac: this.widget.oglasavac));
  }
}

class User extends StatefulWidget {
  User({
    this.id,
    this.oglasavac,
    /*BigInt id, bool oglasavac*/
  });

  // {
  // this.id = id;
  // this.oglasavac = oglasavac;
  // }
  final BigInt id;
  final bool oglasavac;
  @override
  _UserState createState() => _UserState();
}

class _UserState extends State<User> with SingleTickerProviderStateMixin {
  TabController _tabController;
  _handleTabSelection() {
    if (_tabController.indexIsChanging) {
      setState(() {});
    }
  }

  @override
  void initState() {
    _tabController = new TabController(length: 2, vsync: this);
    _tabController.addListener(_handleTabSelection);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var boje = Theme.of(context).colorScheme;
   
  
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Boje.mainText),
          onPressed: () {
            GetInfo.korpa = false;
            // Navigator.pop(context);
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => WelcomeScreen()));
          },
        ),
        title: Text("Profil",
            style:
                TextStyle(color: Boje.mainText, fontWeight: FontWeight.bold)),
        centerTitle: true,
      ),
      body: Center(
        child: Container(
          width: width < 800 ? width : 800,
          // color: Colors.grey,
          child: ListView(
            children: [
              // SLIKA I USERNAME I AKO JE SIRINA VECA OD 800 KORISNICKI PODATCI
              this.widget.oglasavac == true
                  ? BoxWithUserNameAndImage(id: this.widget.id)
                  : BoxWithUserNameAndImage(id: GetInfo.id),
              SizedBox(height: 10),
              // PODATCI O KORISNIKU AKO JE SIRINA MANJA OD 800
              width < 800
                  ? (this.widget.oglasavac == true
                      ? ProfileDescription(id: this.widget.id)
                      : ProfileDescription(id: GetInfo.id))
                  : SizedBox(),
              SizedBox(height: width < 800 ? 10 : 0),
              // TABOVI
              Container(
                height: width > 450 ? 60 : 50,
                decoration: BoxDecoration(
                  borderRadius: width > 800 ? BorderRadius.circular(10) : null,
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 5,
                        color: Theme.of(context).colorScheme.secondary)
                  ],
                  color: Theme.of(context).primaryColor,
                ),
                child: TabBar(
                  indicatorWeight: 4.0,
                  controller: _tabController,
                  // labelColor: Colors.redAccent,
                  indicatorColor: Boje.mainText,
                  labelColor: Boje.mainText,
                  labelStyle:
                      TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                  unselectedLabelColor: boje.onBackground,
                  isScrollable: false,
                  tabs: [
                    Container(
                      // width: MediaQuery.of(context).size.width / 2,
                      alignment: Alignment.center,
                      child: Text("Moji oglasi"),
                    ),
                    Container(
                      // width: MediaQuery.of(context).size.width / 2,
                      alignment: Alignment.center,
                      child: Text("Dobijene recenzije",
                          maxLines: 2, textAlign: TextAlign.center),
                    ),
                  ],
                ),
              ),
              Center(
                child: [
                  this.widget.oglasavac == true
                      ? UserProducts(this.widget.id)
                      : UserProducts(GetInfo.id),
                  this.widget.oglasavac == true
                      ? RecievedReviewsForUser(this.widget.id)
                      : RecievedReviewsForUser(GetInfo.id),
                ][_tabController.index],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
