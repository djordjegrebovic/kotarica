import 'package:flutter/material.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/widgets/orders/sent_requests.dart';

class MySentBuyRequests extends StatelessWidget {
  static String routeName = '/poslati_zahtevi';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Boje.mainText),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        centerTitle: true,
        title: Text("Poslati zahtevi za kupovinu",
            style:
                TextStyle(color: Boje.mainText, fontWeight: FontWeight.bold)),
      ),
      body: SentBuyRequests(),
    );
  }
}

class SentBuyRequests extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Center(
      child: Container(
        padding: EdgeInsets.only(top: 10.0),
        child: Column(
          children: [
            SizedBox(height: 10),
            Container(
              alignment: Alignment.topCenter,
              child: Text(
                "Klikom na karticu možete videti više detalja porudžbine",
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: 10),
            SentRequestsProductListHomePage(),
          ],
        ),
      ),
    ));
  }
}
