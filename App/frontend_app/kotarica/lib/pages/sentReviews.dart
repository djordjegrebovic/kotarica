import 'package:flutter/material.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/utils/boje.dart';
import 'package:kotarica/widgets/products/reviewProductListHomePage.dart';

class MySentReviews extends StatelessWidget {
  static String routeName = '/napisane_recenzije';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Boje.mainText,
          ),
          onPressed: () {
            GetInfo.korpa = false;

            Navigator.pop(context);
            //CheckOurCard.korpa = false;
          },
        ),
        centerTitle: true,
        title: Text("Napisane recenzije",
            style:
                TextStyle(color: Boje.mainText, fontWeight: FontWeight.bold)),
      ),
      body: SentReviews(),
    );
  }
}

class SentReviews extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[ReviewRequestsProductListHomePage()],
      ),
    );
  }
}
