import 'package:flutter/material.dart';
import 'package:kotarica/widgets/search/searchBody.dart';
import 'package:kotarica/widgets/search/search_app_bar.dart';
// import '../widgets/search/drop_down_categories.dart';

class Search extends StatefulWidget {
  static String routeName = '/pretraga';

  @override
  State<StatefulWidget> createState() {
    return _SearchState();
  }
}

class _SearchState extends State<Search> {
  // int _value;
  // final List<String> categoriesList1 = [
  //   '1option',
  //   '2option',
  //   '3option',
  // ];
  // final List<String> categoriesList2 = [
  //   '1option',
  //   '2option',
  //   '3option',
  // ];
  // final List<String> categoriesList3 = [
  //   '1option',
  //   '2option',
  //   '3option',
  // ];
  // final List<bool> initialState1 = [
  //   false,
  //   false,
  //   false,
  // ];
  // final List<bool> initialState2 = [
  //   false,
  //   false,
  //   false,
  // ];
  // final List<bool> initialState3 = [
  //   false,
  //   false,
  //   false,
  // ];

  // String title1 = "Kategorije";
  // String title2 = "Filteri";
  // String title3 = "Mesto";
  // refresh() {
  //   // print("ovo treba da se trigeruje");
  //   setState(() {});
  // }

  @override
  Widget build(BuildContext context) {
    // double width = MediaQuery.of(context).size.width * 0.32;
    // double height = MediaQuery.of(context).size.height * 0.08;

    return Scaffold(
      appBar: SearchAppBar(),
      body: SafeArea(child: SearchBody()),
      //Tri dugmeta (Kategorije, Filteri, Mesto)
      // body: Padding(
      //     padding: EdgeInsets.symmetric(vertical: 5.0),
      //     child: Row(
      //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      //       children: [
      //         SizedBox(
      //             width: width,
      //             height: height,
      //             child: DropDownCategories(
      //                 categoriesList1, title1, initialState1)),
      //         SizedBox(
      //             width: width,
      //             height: height,
      //             child: DropDownCategories(
      //                 categoriesList2, title2, initialState2)),
      //         SizedBox(
      //             width: width,
      //             height: height,
      //             child: DropDownCategories(
      //                 categoriesList3, title3, initialState3)),
      //       ],
      //     )),
    );
  }
}
