import 'package:flutter/material.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/pages/welcome_screen.dart';
import 'package:kotarica/utils/boje.dart';
// import 'package:kotarica/pages/welcome_screen.dart';
import 'package:kotarica/widgets/productDetail/add.dart';
import 'package:provider/provider.dart';
import 'package:kotarica/widgets/cart/cart_body.dart';
import 'package:kotarica/widgets/cart/check_our_card.dart';

// ignore: must_be_immutable
class MyCart extends StatelessWidget {
  static String routeName = '/korpa';
  bool meni = false;
  MyCart({this.meni});
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    var addListener = Provider.of<AddModel>(context);

    return Scaffold(
      appBar: buildAppBar(context),
      body: SafeArea(child: CartBody()),
      bottomNavigationBar: width <= 850 && addListener.baskets.length > 0
          ? CheckOurCard(text: "Račun", icon: Icons.fact_check_outlined)
          : null,
    );
  }

  AppBar buildAppBar(BuildContext context) {
    var addListener = Provider.of<AddModel>(context);
  
    return AppBar(
      centerTitle: true,
      leading: IconButton(
        icon: Icon(Icons.arrow_back_ios, color: Boje.mainText),
        onPressed: () {
          GetInfo.korpa = false;
          if (meni == true) {
            meni = false;
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => WelcomeScreen()));
          } else
            Navigator.pop(context);
        },
      ),
      title: Column(
        children: <Widget>[
          Text(
            "Moja korpa",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20.0,
              letterSpacing: 1.3,
              color: Boje.mainText,
            ),
          ),
          Text(
            "${addListener.baskets.length} ${addListener.baskets.length == 1 ? "proizvod" : "proizvoda"}", //
            style: TextStyle(
              fontSize: 14.0,
              fontWeight: FontWeight.bold,
              color: Boje.mainText,
            ),
          ),
        ],
      ),
    );
  }
}
