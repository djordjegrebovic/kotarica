import 'package:flutter/widgets.dart';
import 'package:kotarica/main.dart';
import 'package:kotarica/pages/addProduct.dart';
import 'package:kotarica/pages/checkoutPage.dart';
import 'package:kotarica/pages/edit_product.dart';
import 'package:kotarica/pages/edit_user_info.dart';
import 'package:kotarica/pages/favourites.dart';
import 'package:kotarica/pages/homePage.dart';
import 'package:kotarica/pages/my_acc.dart';
import 'package:kotarica/pages/my_cart.dart';
import 'package:kotarica/pages/recievedBuyRequests.dart';
import 'package:kotarica/pages/search.dart';
import 'package:kotarica/pages/sentBuyRequests.dart';
import 'package:kotarica/pages/sentReviews.dart';
import 'package:kotarica/pages/welcome_screen.dart';
// import 'package:kotarica/pages/welcome_screen_novi.dart';
import 'package:kotarica/redirektNaLogin.dart';
import 'package:kotarica/widgets/LoginSingin/login_success_screen.dart';
import 'package:kotarica/widgets/LoginSingin/registration_success_screen.dart';
import 'package:kotarica/widgets/LoginSingin/sign_in_screen.dart';
import 'package:kotarica/widgets/SignUp/complete_profile_screen.dart';
import 'package:kotarica/widgets/SignUp/sign_up_screen.dart';
import 'package:kotarica/widgets/menu_bar.dart';
import 'package:kotarica/widgets/productDetail/product_details.dart';

// We use name route
// All our routes will be available here
final Map<String, WidgetBuilder> routes = {
  WelcomeScreen.routeName: (context) => WelcomeScreen(),
  // WelcomeScreenNovi.routeName: (context) => WelcomeScreenNovi(),
  MyApp.routeName: (context) => MyApp(),
  MenuBar.routeName: (context) => MenuBar(),
  HomePage.routeName: (context) => HomePage(),
  Search.routeName: (context) => Search(),
  MyCart.routeName: (context) => MyCart(),
  MyAcc.routeName: (context) => MyAcc(),
  SignInScreen.routeName: (context) => SignInScreen(),
  SignUpScreen.routeName: (context) => SignUpScreen(),
  CompleteProfileScreen.routeName: (context) => CompleteProfileScreen(),
  RedirektNaLogin.routeName: (context) => RedirektNaLogin(),
  ProductDetails.routeName: (context) => ProductDetails(),
  LoginSuccessScreen.routeName: (context) => LoginSuccessScreen(),
  Checkout.routeName: (context) => Checkout(),
  EditProduct.routeName: (context) => EditProduct(),
  EditInfo.routeName: (context) => EditInfo(),
  MyFavourites.routeName: (context) => MyFavourites(),
  MySentBuyRequests.routeName: (context) => MySentBuyRequests(),
  MyRecievedBuyRequests.routeName: (context) => MyRecievedBuyRequests(),
  MySentReviews.routeName: (context) => MySentReviews(),
  AddProductPage.routeName: (context) => AddProductPage(),
  RegistrationSuccessScreen.routeName: (context) => RegistrationSuccessScreen(),

  //  OtpScreen.routeName: (context) => OtpScreen(),
};
