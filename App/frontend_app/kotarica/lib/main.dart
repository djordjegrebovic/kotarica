import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:kotarica/eth_models/KupovinaModels.dart';
import 'package:kotarica/eth_models/ProductModels.dart';
import 'package:kotarica/models/getInfo.dart';
import 'package:kotarica/eth_models/RecenzijeModels.dart';
// import 'package:kotarica/pages/homePage.dart';
// import 'package:kotarica/pages/my_cart.dart';
import 'package:kotarica/pages/welcome_screen_novi.dart';
import 'package:kotarica/utils/themes.dart';
import 'package:kotarica/widgets/LoginSingin/sign_in_screen.dart';
// import 'package:kotarica/widgets/menu_bar.dart';
import 'package:kotarica/widgets/productDetail/add.dart';
import 'package:provider/provider.dart';
import 'eth_models/PorukeModels.dart';
import 'eth_models/user_loginModels.dart';
import 'pages/welcome_screen.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:universal_html/html.dart' show window;
import 'package:kotarica/routes.dart';

final storage = FlutterSecureStorage();
StreamController<List<Product>> controller1 =
    StreamController<List<Product>>.broadcast();

Stream stream = controller1.stream;
StreamSubscription<List<Product>> streamSubscription;
void main() {
  runApp(MyApp());
  // new getInfo();
}

class MyApp extends StatelessWidget {
  static String routeName = '/kotarica';

  static bool postojiJWT = false;
  Future<String> get jwtOrEmpty async {
    var jwt;
    if (kIsWeb) {
      jwt = window.localStorage['csrf'];
      postojiJWT = true;
    } else {
      jwt = await storage.read(key: "jwt");
      postojiJWT = true;
    }
    if (jwt == null) {
      postojiJWT = false;
      return "";
    } else {
      GetInfo();
    }
    return jwt;
  }

  @override
  Widget build(BuildContext context) {
    //new RecenzijeModel();
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (BuildContext context) {
          return PorukeModel();
        }),
        ChangeNotifierProvider(create: (BuildContext context) {
          return AddModel();
        }),
        ChangeNotifierProvider(create: (BuildContext context) {
          return UserLoginModel();
        }),
        ChangeNotifierProvider(create: (BuildContext context) {
          return ProductModels();
        }),
        ChangeNotifierProvider(create: (BuildContext context) {
          return KupovinaModels();
        }),
        ChangeNotifierProvider(create: (BuildContext context) {
          return RecenzijeModel();
        })
      ],
      child: MaterialApp(
        // initialRoute: WelcomeScreen.routeName,
        debugShowCheckedModeBanner: false,
        routes: routes,
        theme: Themes.light,
        home: FutureBuilder(
            future: jwtOrEmpty,
            builder: (context, snapshot) {
              if (!snapshot.hasData)
                return SpinKitFadingCube(
                    size: 35, color: Theme.of(context).colorScheme.primary);
              if (snapshot.data != null) {
                var str = snapshot.data;
                var jwt = str.split(".");
                if (jwt.length != 3) {
                  return MaterialApp(
                      // initialRoute: HomePage.routeName,
                      // routes: routes,
                      theme: Themes.light,
                      debugShowCheckedModeBanner: false,
                      home: WelcomeScreen());
                } else {
                  var payload = json.decode(
                      ascii.decode(base64.decode(base64.normalize(jwt[1]))));
                  //print("$payload ovo je payload");
                  if (DateTime.fromMillisecondsSinceEpoch(payload["exp"] * 1000)
                      .isAfter(DateTime.now())) {
                    return MaterialApp(
                        // initialRoute: HomePage.routeName,
                        // routes: routes,
                        debugShowCheckedModeBanner: false,
                        home: WelcomeScreenNovi(str, payload));
                  } else {
                    return MaterialApp(
                        // initialRoute: HomePage.routeName,
                        // routes: routes,
                        theme: Themes.light,
                        debugShowCheckedModeBanner: false,
                        home: SignInScreen());
                  }
                }
              } else {
                return MaterialApp(
                    // initialRoute: HomePage.routeName,
                    // routes: routes,
                    debugShowCheckedModeBanner: false,
                    home: SignInScreen());
              }
            }),
      ),
    );
  }
}
