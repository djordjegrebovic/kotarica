class TestModel {

  String username;
  String password;

  TestModel(String username, String password) {
    this.username = username;
    this.password = password;
  }

  TestModel.fromJson(Map<String, dynamic> json)
      : username = json['username'],
        password = json['password'];

   Map<String, dynamic> toJson() =>
  {
    'username': username,
    'password': password,
  };

}
