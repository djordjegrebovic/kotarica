import 'package:shared_preferences/shared_preferences.dart';

class GetInfo {
  static BigInt id = BigInt.zero;
  static bool korpa = false;
  static bool isLoadingKupovina = false;

  GetInfo() {
    id = BigInt.zero;
    _loadPreferences("id");
  }

  Future<int> _loadPreferences(String dataNeeded) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    // ignore: await_only_futures
    id = await BigInt.from(pref.getInt(dataNeeded));
    // print("Sacuvani id je : " + await pref.getInt(dataNeeded).toString());
    return pref.getInt(dataNeeded);
  }
}
