import 'dart:ui';

import 'package:flutter/material.dart';

class Themes {
  static ThemeData light = ThemeData.from(
    colorScheme: ColorScheme(
      primary: Color.fromARGB(255, 129, 192, 185),
      // Color.fromARGB(255, 133, 203, 195),
      // Color.fromARGB(255, 236, 177, 103),
      // Color.fromARGB(255, 101, 155, 149),
      onPrimary: Color.fromARGB(255, 51, 91, 89),
      primaryVariant: Color.fromARGB(255, 184, 147, 102),
      secondary: Color.fromARGB(255, 143, 166, 163),
      //Color.fromARGB(255, 168, 151, 130),
      onSecondary: Color.fromARGB(255, 169, 192, 182),
      //Color.fromARGB(255, 166, 149, 128),
      secondaryVariant: Color.fromARGB(255, 120, 105, 87),
      surface:
          // Color.fromARGB(255,92,13,13),
          // Color.fromARGB(255, 217, 136, 106),
          Color.fromARGB(255, 90, 81, 70),
      onSurface: Color.fromARGB(255, 165, 142, 113),
      background: Color.fromARGB(255, 220, 214, 206),
      onBackground: Color.fromARGB(255, 243, 239, 232),
      error: Colors.red[700],
      onError: Colors.redAccent,
      brightness: Brightness.light,
    ),
    textTheme: TextTheme(
      headline1: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.normal,
        fontFamily: 'Open-Sans',
      ),
      // bodyText1: TextStyle(),
      // bodyText2: TextStyle(
      //     fontFamily: 'Open-Sans',
      //     color: Colors.red, //Color.fromARGB(255, 90, 81, 70),
      //     fontSize: 16,
      //     fontWeight: FontWeight.bold),
    ),
  );
  static ThemeData dark = ThemeData.dark();

  // ignore: unused_element
  static TextStyle _textStyle(Color color, double size, FontWeight fontWeight) {
    return TextStyle(
        fontFamily: 'Open-Sans',
        color: color,
        fontSize: size,
        fontWeight: fontWeight);
  }

  static TextStyle profileData() {
    return TextStyle(
        fontFamily: 'Open-Sans',
        color: Color.fromARGB(255, 90, 81, 70),
        fontSize: 16,
        fontWeight: FontWeight.bold);
  }
}
