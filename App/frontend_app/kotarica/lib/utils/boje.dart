import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';

mixin Boje {
  static Color osnovnaBoja1 = Colors.teal[600];
  static Color textColor = Colors.grey[300];
  static Color osnovnaBoja2 = Vx.green100;
  static Color menuItemColor = Colors.deepPurple;
  static Color userPageContainerColor = Colors.blue[800];
  // static Color borderLabelColor = Colors.blue[800];
  // static Color focusedBorderColor = Colors.blue[800];
  static Color enabledBorderColor = Colors.teal;
  static Color selectedMenuItemColor = Colors.teal[600];
  // static Color hintTextInForm = Colors.blue[800];
  static Color price = Color.fromARGB(255, 193, 76, 47);
  static Color loginRegisterButton = Color.fromARGB(255, 200, 105, 47);
  static Color rsd = Color.fromARGB(255, 160, 52, 52);
  static Color mainText = Color.fromARGB(255, 0, 42, 42);
}
