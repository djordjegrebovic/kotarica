import 'package:flutter/material.dart';

double calculateHeight(BuildContext context, int _numOfItems, int padding) {
  return MediaQuery.of(context).size.width < 600
      ? MediaQuery.of(context).size.width * 0.23 * _numOfItems +
          _numOfItems * 12 +
          padding
      : 150 * _numOfItems + _numOfItems * 12 + padding;
}

double calculateHeightReview(
    BuildContext context, int _numOfItems, int padding) {
  // double width = MediaQuery.of(context).size.width;
  return MediaQuery.of(context).size.height * 0.28 * _numOfItems +
      _numOfItems * 12 +
      padding;
}

// Poziva se u widgets/products/: favProducts, recivedRequests, sentRequests, userProducts
double calculateHeightRequest(
    BuildContext context, int _numOfItems, int padding) {
  double width = MediaQuery.of(context).size.width;
  return (width < 400
              ? 148
              : width <= 445
                  ? width * 0.39
                  : width < 600
                      ? width * 0.368
                      : width > 850
                          ? 213
                          : 208) *
          _numOfItems +
      _numOfItems * 12 +
      padding;
}
