import 'dart:math';

class Product {
  int _id;
  String _name;
  String _desc;
  int _sellerId;
  int _price;

  Product(this._name, this._desc, this._sellerId, this._price);
  Product.withId(this._id, this._name, this._desc, this._sellerId, this._price);

  int get id => _id;
  // ignore: unnecessary_getters_setters
  String get name => _name;
  // ignore: unnecessary_getters_setters
  int get sellerId => _sellerId;
  // ignore: unnecessary_getters_setters
  int get price => _price;
  // ignore: unnecessary_getters_setters
  String get desc => _desc;

  // ignore: unnecessary_getters_setters
  set name(String newName) {
    _name = newName;
  }

  // ignore: unnecessary_getters_setters
  set sellerId(int newSellerId) {
    _sellerId = newSellerId;
  }

  // ignore: unnecessary_getters_setters
  set price(int newPrice) {
    _price = newPrice;
  }

  // ignore: unnecessary_getters_setters
  set desc(String newDesc) {
    _desc = newDesc;
  }

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map["name"] = _name;
    map["sellerId"] = _sellerId;
    map["price"] = _price;

    return map;
  }

  Product.fromObject(dynamic o) {
    this._id = o["id"];
    this._name = o["name"];
    this._sellerId = o["sellerId"];
    this._price = o["price"];
  }

  /*
   * For Debug
   */

  static List<String> testNames = [
    "Paradajz",
    "Krompir",
    "Kupus",
    "Sargarepa",
    "Piletina",
    "Svinjetina",
    "Lubenica"
  ];

  static List<int> testSellerIds = [0, 1, 2, 3];

  static List<int> testPrices = [150, 180, 200, 220, 350, 400, 470, 500];

  static String testDesc =
      "Lorem ipsum is simply dummy text of the printing and typesetting industry.";

  static Product getRandom() {
    Random r = new Random();
    String name = testNames[r.nextInt(testNames.length - 1)];
    String desc = testDesc;
    int sellerId = testSellerIds[r.nextInt(testSellerIds.length - 1)];
    int price = testPrices[r.nextInt(testPrices.length - 1)];

    return Product(name, desc, sellerId, price);
  }

  static List<Product> getRandomN(int n) {
    List<Product> products = [];
    for (int i = 0; i < n; i++) {
      products.add(getRandom());
    }
    return products;
  }
}
