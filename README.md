Projekat se pokrece na sledeci nacin:

1. Startujemo ganache,u odeljku za podesavanja pod kategorijom workspace dodamo nas projekat tako sto izaberemo ovaj fajl Codex\App\frontend_app\kotarica\truffle-config.js
   U delu za podesavanja servera izaberemo da nam hostname bude neka vasa adresa - enternet.

2. Izmeniti sledece fajlove sa vasim adresama iz ganache servera:
   - Codex\App\frontend_app\kotarica\lib\eth_models\ProductModels.dart na linijama 14 i 15 zameniti vasu adresu i privatni kljuc na liniji 23
   - Codex\App\frontend_app\kotarica\lib\eth_models\user_loginModels.dart na linijama 18 i 19 zameniti vasu adresu i privatni kljuc na liniji 27
   - Codex\App\frontend_app\kotarica\truffle-config.js upisati vasu adresu umesto postojece na liniji 4
    
3. Nakon svih zavrsenih izmena potrebno je iz cmd-a odraditi truffle migrate nad folderom Codex\App\frontend_app\kotarica\
    