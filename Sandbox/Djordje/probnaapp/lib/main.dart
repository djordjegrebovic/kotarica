import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'animation.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{
        '/animation': (BuildContext context) => new SignupPage(),
      },
      home: MyHomePage(title: 'Login'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

Color bckGreen = Color(0xFFC5E1A5);
Color txtGreen = Color(0xFF689F38);

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: bckGreen,
        body: Column(
          children: <Widget>[
            Container(
              child: Stack(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 82.0, left: 20.0),
                  ),
                ],
              ),
            ),
            Container(
              child: Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(10.0, 5.0, 0.0, 30.0),
                    child: Text(
                      'Prijavi se',
                      style: TextStyle(
                          fontSize: 40.0,
                          fontStyle: FontStyle.italic,
                          fontWeight: FontWeight.bold,
                          color: txtGreen),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 30.0, left: 20, right: 20.0),
              child: Column(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width / 1.2,
                    height: 45,
                    padding:
                        EdgeInsets.only(top: 4, left: 16, right: 16, bottom: 4),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50)),
                        color: Color.fromRGBO(197, 199, 201, 1),
                        boxShadow: [
                          BoxShadow(color: Colors.black12, blurRadius: 5)
                        ]),
                    child: TextField(
                      cursorColor: txtGreen,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        icon: Icon(
                          Icons.email,
                          color: txtGreen,
                        ),
                        hintText: 'Email',
                      ),
                    ),
                  ),
                  SizedBox(height: 15.0),
                  Container(
                    width: MediaQuery.of(context).size.width / 1.2,
                    height: 45,
                    padding:
                        EdgeInsets.only(top: 4, left: 16, right: 16, bottom: 4),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50)),
                        color: Color.fromRGBO(197, 199, 201, 1),
                        boxShadow: [
                          BoxShadow(color: Colors.black12, blurRadius: 5)
                        ]),
                    child: TextField(
                      cursorColor: txtGreen,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        icon: Icon(
                          Icons.lock,
                          color: txtGreen,
                        ),
                        hintText: 'Šifra',
                      ),
                      obscureText: true,
                    ),
                  ),
                  SizedBox(height: 50.0),
                  Container(
                    height: 50.0,
                    child: Material(
                      borderRadius: BorderRadius.circular(50.0),
                      color: Color.fromRGBO(197, 199, 201, 1),
                      elevation: 7.0,
                      child: GestureDetector(
                        child: Center(
                          child: InkWell(
                            onTap: () {
                              Navigator.of(context).pushNamed('/animation');
                            },
                            child: Center(
                              child: Text(
                                "Prijavi se",
                                style: TextStyle(
                                  fontStyle: FontStyle.italic,
                                  color: txtGreen,
                                  fontFamily: 'Oswald-Bold',
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ));
  }
}
