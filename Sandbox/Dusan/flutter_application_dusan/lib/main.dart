import 'dart:ui';

import 'package:flutter/material.dart';
import 'druga_strana.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dusan`s flutter app',
      theme: ThemeData(
        primarySwatch: Colors.red,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        scaffoldBackgroundColor: Color.fromRGBO(211,211,211,1),
         buttonTheme: ButtonThemeData(
         buttonColor: Colors.deepPurple,     //  <-- dark color
         textTheme: ButtonTextTheme.primary, //  <-- this auto selects the right color
    )
      ),
      home: MyHomePage(title: 'Dusan`s flutter app'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dusanova flutter aplikacija'),
        shadowColor: Colors.black
      ),
      body: Padding(
        padding: EdgeInsets.all(
          25.0
        ),
        child: Center(
          child: Form(
            child: Column(
             
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                 Padding(
                  padding: EdgeInsets.only(bottom:40.0),
                child : Image(image: AssetImage('assets/logo.png')),
                 ),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(

                    ),
                  labelText: 'Email'
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top:20.0),
                    child:  TextFormField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(

                        ),
                     labelText: 'Password'
                    ),
                  ),


                ),
                Padding(
                  padding: EdgeInsets.only(top:20.0),
                    child:  MaterialButton(
                      onPressed: (){
                        Navigator.push(
                          context,
                        MaterialPageRoute(builder: (context) => drugaStrana()),
                    );

                      },
                      child: Text('Login'),
                      color: Colors.red,
                      shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                      
                    ),


                ),
              ],
            ),
          )
        ),
      )
    );
  }
}


class drugaStrana extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("DOBRODOSLI"),
      ),
            body: Center(

        child: Column(

          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Klikom na dugme ispod vracas se na login',
            ),
             Padding(
                  padding: EdgeInsets.only(top:20.0),
                    child:  MaterialButton(
                      onPressed: (){
                        Navigator.pop(
                          context,
                       
                    );

                      },
                      child: Text('Idi na Login'),
                      color: Colors.red,
                      shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                      
                    ),


                )
          ],
        ),
      ),
    );
  }
}