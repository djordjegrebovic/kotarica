import 'dart:html';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:web3dart/web3dart.dart';
import 'package:http/http.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Client httpClient;
  Web3Client etcClient;
  bool data = false;
  double _value = 0;
  int myValue = 0;
  int myAmount = 0;
  final myAddress = "0xEe5FD6863b252a1C586026B3A2188dC14Dc10123";
  var myData;

  @override
  void initState() {
    super.initState();
    httpClient = Client();
    etcClient = Web3Client(
        "https://rinkeby.infura.io/v3/8cdc726093ab4c9580416fd2e250d2a1",
        httpClient);
    getBalance(myAddress);
  }

  Future<DeployedContract> loadContact() async {
    String abi = await rootBundle.loadString("assets/abi.json");
    String contractAddress = "0xE47D7a64C0d6B6eE244Abf424c5aBC349bC94e0D";
    final contract = DeployedContract(ContractAbi.fromJson(abi, "PKCoin"),
        EthereumAddress.fromHex(contractAddress));
    return contract;
  }

  Future<List<dynamic>> query(String functionName, List<dynamic> args) async {
    final contract = await loadContact();
    final ethFunction = contract.function(functionName);
    final result = await etcClient.call(
        contract: contract, function: ethFunction, params: args);

    return result;
  }

  Future<void> getBalance(String targetAddress) async {
    EthereumAddress address = EthereumAddress.fromHex(targetAddress);
    List<dynamic> result = await query("getBalance", []);

    myData = result[0];
    data = true;
    setState(() {});
  }

  Future<String> sendCoin() async {
    var bigAmount = BigInt.from(myValue);

    var response = await submit("depositBalance", [bigAmount]);

    print("Dodato $myValue");
    return response;
  }

  Future<String> withdrawCoin() async {
    var bigAmount = BigInt.from(myValue);

    var response = await submit("withdrawBalance", [bigAmount]);

    print("Skinuto $myValue");
    return response;
  }

  Future<String> submit(String functionName, List<dynamic> args) async {
    EthPrivateKey credentials = EthPrivateKey.fromHex(
        "a0e1f755f102377b82c1f9edbfbfa940e827dc42918ee09cfdfc60991c424b80");

    DeployedContract contract = await loadContact();
    final ethFunction = contract.function(functionName);
    final result = await etcClient.sendTransaction(
        credentials,
        Transaction.callContract(
            contract: contract, function: ethFunction, parameters: args),
        fetchChainIdFromNetworkId: true);

    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Vx.gray300,
      body: ZStack([
        VxBox()
            .blue600
            .size(context.screenWidth, context.percentHeight * 30)
            .make(),
        VStack([
          (context.percentHeight * 10).heightBox,
          "IMICoin".text.xl4.white.bold.center.makeCentered().py16(),
          (context.percentHeight * 5).heightBox,
          VxBox(
                  child: VStack([
            "Stanje racuna: ".text.gray700.xl2.semiBold.makeCentered(),
            10.heightBox,
            data
                ? "\$$myData".text.bold.xl6.makeCentered().shimmer()
                : CircularProgressIndicator().centered()
          ]))
              .white
              .size(context.screenWidth, context.percentHeight * 17)
              .rounded
              .shadowXl
              .make()
              .p16(),
          30.heightBox,
          VStack([
            "Slajder : $myValue".text.gray700.xl2.semiBold.makeCentered(),
          ]),
          Slider(
            min: 0,
            max: 100,
            value: _value,
            onChanged: (value) {
              setState(() {
                _value = value;
                myValue = _value.round();
                print(myValue);
              });
            },
          ),
          HStack(
            [
              FlatButton.icon(
                  onPressed: () => getBalance(myAddress),
                  color: Colors.green,
                  shape: Vx.roundedSm,
                  icon: Icon(
                    Icons.refresh,
                    color: Colors.white,
                  ).h(50),
                  label: "Refresuj".text.white.make()),
              FlatButton.icon(
                  onPressed: () => sendCoin(),
                  color: Colors.blue,
                  shape: Vx.roundedSm,
                  icon: Icon(
                    Icons.call_made_outlined,
                    color: Colors.white,
                  ).h(50),
                  label: "Dodaj".text.white.make()),
              FlatButton.icon(
                  onPressed: () => withdrawCoin(),
                  color: Colors.red,
                  shape: Vx.roundedSm,
                  icon: Icon(
                    Icons.call_received_outlined,
                    color: Colors.white,
                  ).h(50),
                  label: "Skini".text.white.make())
            ],
            alignment: MainAxisAlignment.spaceAround,
            axisSize: MainAxisSize.max,
          )
        ])
      ]),
    );
  }
}
