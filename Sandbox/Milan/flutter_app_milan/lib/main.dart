import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:web3dart/web3dart.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Milan app'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Client httpClient;
  Web3Client ethClient;
  var balance;
  final myAddress = "0x97DBA47DE4EAf46174EC25950BA4b3e3545203B4";
  bool data = false;
  void initState() {
    super.initState();
    httpClient = Client();
    ethClient = Web3Client(
        "https://rinkeby.infura.io/v3/f11649af4b0748999b8782418ea9e716",
        httpClient);
    getBalance(myAddress);
  }

  Future<DeployedContract> loadContract() async {
    String abi = await rootBundle.loadString("assets/abi.json");
    String contractAddress = "0xd9145CCE52D386f254917e481eB44e9943F39138";

    final contract = DeployedContract(ContractAbi.fromJson(abi, "MBCoin"),
        EthereumAddress.fromHex(contractAddress));
    return contract;
  }

  // ignore: missing_return
  Future<List<dynamic>> query(String functioName, List<dynamic> args) async {
    final contract = await loadContract();
    final ethFunction = contract.function(functioName);
    // ignore: unused_local_variable
    final result = await ethClient.call(
        contract: contract, function: ethFunction, params: args);
  }

  Future<void> getBalance(String targetAddress) async {
    // ignore: unused_local_variable
    //EthereumAddress address = EthereumAddress.fromHex(targetAddress);
    List<dynamic> result = await query("getBalance", []);

    balance = result[0];
    data = true;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    TextEditingController firstNameController = TextEditingController(text: '');
    TextEditingController lastNameConntroler = TextEditingController(text: '');

    TextEditingController balanceChangeControler =
        TextEditingController(text: '');
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
          child: ListView(
        padding: const EdgeInsets.all(10.0),
        children: [
          TextFormField(
            autofocus: false,
            controller: firstNameController,
            decoration: InputDecoration(
              hintText: 'Ime',
              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
            ),
          ),
          TextFormField(
            autofocus: false,
            controller: lastNameConntroler,
            decoration: InputDecoration(
              hintText: 'Prezime',
              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
            ),
          ),
          TextFormField(
            autofocus: false,
            controller: balanceChangeControler,
            decoration: InputDecoration(
              hintText: 'Suma',
              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
            ),
          ),
          HStack(
            [
              TextButton(
                child: Text('Dodaj'),
                onPressed: () {
                  return showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        content: Text("Korisnik " +
                            firstNameController.text +
                            " " +
                            lastNameConntroler.text +
                            " je dodao na racun " +
                            balanceChangeControler.text +
                            "."),
                        actions: <Widget>[
                          TextButton(
                            child: Text('Ok'),
                            onPressed: () {
                              firstNameController.text = '';
                              lastNameConntroler.text = '';
                              balanceChangeControler.text = '';

                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      );
                    },
                  );
                },
              ),
              TextButton(
                child: Text('Skini'),
                onPressed: () {
                  return showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        content: Text("Korisnik " +
                            firstNameController.text +
                            " " +
                            lastNameConntroler.text +
                            " je skinuo " +
                            balanceChangeControler.text +
                            " sa racuna."),
                        actions: <Widget>[
                          TextButton(
                            child: Text('Ok'),
                            onPressed: () {
                              firstNameController.text = '';
                              lastNameConntroler.text = '';
                              balanceChangeControler.text = '';

                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      );
                    },
                  );
                },
              ),
            ],
            alignment: MainAxisAlignment.spaceAround,
            axisSize: MainAxisSize.max,
          ),
          data
              ? Center(
                  child: Text("Trenutni balans na racunu je " +
                      balance.toString() +
                      "."),
                )
              : Center(
                  child: Text("Trenutni balans na racunu je 00."),
                )
        ],
      )),
    );
  }
}
