import 'package:flutter/material.dart';
//import 'package:flutter/src/semantics/semantics.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.deepPurple,
        secondaryHeaderColor: Colors.purpleAccent,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Danijelova Test Aplikacija'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  bool aSelected = false;
  bool bSelected = true;
  bool cSelected = false;
  String ddValue;

  void _restore() {
    setState(() {
      aSelected = false;
      bSelected = false;
      cSelected = false;
      ddValue = "One";
    });
  }

  String getMessage() {
    String str = "Izabrali ste: ";
    if (aSelected) {
      str += "A";
    }
    if (bSelected) {
      str += "B";
    }
    if (cSelected) {
      str += "C";
    }
    str += ddValue;
    return str;
  }

  void _alert() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Alert'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(getMessage()),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
          // Center is a layout widget. It takes a single child and positions it
          // in the middle of the parent.
          child: ListView(
        padding: const EdgeInsets.all(8),
        children: <Widget>[
          CheckboxListTile(
            title: const Text('A'),
            value: aSelected,
            onChanged: (bool value) {
              setState(() {
                aSelected = !aSelected;
              });
            },
          ),
          CheckboxListTile(
            title: const Text('B'),
            value: bSelected,
            onChanged: (bool value) {
              setState(() {
                bSelected = !bSelected;
              });
            },
          ),
          CheckboxListTile(
            title: const Text('C'),
            value: cSelected,
            onChanged: (bool value) {
              setState(() {
                cSelected = !cSelected;
              });
            },
          ),
          DropdownButton<String>(
            value: ddValue,
            icon: Icon(Icons.arrow_downward),
            iconSize: 24,
            elevation: 16,
            style: TextStyle(color: Colors.deepPurple),
            underline: Container(
              height: 2,
              color: Colors.deepPurpleAccent,
            ),
            onChanged: (String newValue) {
              setState(() {
                ddValue = newValue;
              });
            },
            items: <String>['One', 'Two', 'Three', 'Four']
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),
          OutlinedButton(
            onPressed: _alert,
            child: Text("Alert"),
          ),
        ],
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: _restore,
        tooltip: 'Restore',
        child: Icon(Icons.restore),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
